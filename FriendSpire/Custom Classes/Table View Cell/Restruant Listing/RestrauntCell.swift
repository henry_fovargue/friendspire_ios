//
//  RestrauntCell.swift
//  FriendSpire
//
//  Created by abhishek on 03/08/18.
//  Copyright © 2018 openkey. All rights reserved.
//

import UIKit

class RestrauntCell: UITableViewCell {

    //IBoutlet
    @IBOutlet weak var lblColorStrip: UILabel!
    @IBOutlet weak var imgViewPoster: UIImageView!
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblDescription: UILabel!
    @IBOutlet weak var lblAddress: UILabel!

    //    Rating Related IBoutlet
    @IBOutlet weak var imgViewRateFriends: UIImageView!
    @IBOutlet weak var lblRateFriends: UILabel!
    @IBOutlet weak var imgViewRateEveryone: UIImageView!
    @IBOutlet weak var lblRateEveryone: UILabel!
    @IBOutlet weak var lblRateFoursquare: UILabel!
    
    //   Action Related IBoutlet
    @IBOutlet weak var lblFriendsRatingCount: UILabel!
    @IBOutlet weak var lblEveryOneRatingCount: UILabel!
    
    @IBOutlet weak var btnShare: UIButton!
    @IBOutlet weak var btnBookmark: UIButton!
    @IBOutlet weak var btnAddRating: UIButton!
    @IBOutlet weak var constraintWidthBtnBookmark: NSLayoutConstraint!

    //some extra details as well
    @IBOutlet weak var lblComments: UILabel!
    @IBOutlet weak var lblOpenStatus: UILabel!

    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func configureCellLibrary(_ modal : FeedModal ,with index: IndexPath, with category :String){
        
        
        // intial setup
        
        btnBookmark.isHidden = false
        constraintWidthBtnBookmark.constant = 50
        btnShare.tag = index.row
        btnBookmark.tag = index.row
        btnAddRating.tag = index.row
        let color = Utility.getColor(using: category)
        lblColorStrip.backgroundColor = color
        lblEveryOneRatingCount.textColor = color
        lblFriendsRatingCount.textColor = color
        btnBookmark.isSelected = false
        btnAddRating.setImage(#imageLiteral(resourceName: "add"), for: .normal)
        btnAddRating.setTitle("", for: .normal)
        self.imgViewPoster.image = #imageLiteral(resourceName: "placeholderPoster")
        
        //Show Data on view
        lblTitle.text = modal.title == "" ? " " : modal.title
        lblComments.text = modal.description == "" ? "" : modal.description
        btnBookmark.isSelected = modal.isBookmarked ?? false
        
        if modal.image?.count ?? 0 > 0{
            var strURl =  modal.image?[0] ?? ""
            strURl = strURl.replacingOccurrences(of: "size", with: "190x130")
            if let imageUrl = URL.init(string:strURl) {
                self.imgViewPoster.af_setImage(withURL: imageUrl, placeholderImage: #imageLiteral(resourceName: "placeholderPoster") , filter: nil, progress: nil, imageTransition: .crossDissolve(0.7), runImageTransitionIfCached: false, completion: nil)
            }
        }

//        let strGenre = Utility.getModifiedJoinString(using: modal.genre)
        
        //category wise data show
        
        switch modal.type ?? 0 {
        case Constant.CategoryTypeBackend.restraunt.rawValue:

            var  strDistance = ""
            if let str = modal.distance?.getDistanceString(){
                strDistance = str
            }
            if modal.city != nil && modal.city != ""{
                let strAddress = "\(modal.city ?? "")"
                lblAddress.text =  strDistance + " | " +  "\(strAddress)"
            }else {
                lblAddress.text =  strDistance
            }
           lblDescription.text = Utility.getModifiedJoinString(using: modal.cuisine) + " | " + Utility.getPriceLevel(value: modal.priceTier ?? 0)
            
            
            break
        case Constant.CategoryTypeBackend.bars.rawValue:
            
            var  strDistance = ""
            if let str = modal.distance?.getDistanceString(){
                strDistance = str
            }
            if modal.city != nil && modal.city != ""{
                let strAddress = "\(modal.city ?? "")"
                lblAddress.text =  strDistance + " | " +  "\(strAddress)"
            }else {
                lblAddress.text =  strDistance
            }
            lblDescription.text = Utility.getModifiedJoinString(using: modal.cuisine) + " | " + Utility.getPriceLevel(value: modal.priceTier ?? 0)

            break
        default :
            DILog.print(items: "NA")
            break
        }
        
        //Show remaining data
        if modal.isOpen == OpenStatus.open.rawValue {
            lblOpenStatus.text = "Open".localized
        } else if modal.isOpen == OpenStatus.closed.rawValue {
            lblOpenStatus.text = "Closed".localized
        }else {
            lblOpenStatus.text = ""
        }
        lblRateFoursquare.text = String(format: "%.1f", arguments: [modal.rating ?? 0.0]).removeZero()
        lblRateEveryone.text = String(format: "%.1f", arguments: [modal.friendspireRating ?? 0.0]).removeZero()
        imgViewRateEveryone.image = Utility.getEmojiAccordindToRating(rating: Float(modal.friendspireRating ?? 0.0))
        lblRateFriends.text =  String(format: "%.1f", arguments:[modal.friendsRating ?? 0.0]).removeZero()
        imgViewRateFriends.image = Utility.getEmojiAccordindToRating(rating: Float(modal.friendsRating ?? 0.0))
        lblEveryOneRatingCount.text = "(\(modal.everyoneCount ?? 0))"
        lblFriendsRatingCount.text = "(\(modal.countFriends ?? 0))"

        if modal.userRating ?? 0 > 0 {
            let img = Utility.getEmojiAccordindToRating(rating: Float(modal.userRating ?? 0))
            btnAddRating.setImage( Utility.resizeImage(image: img, newWidth: 20.0), for: .normal)
            btnAddRating.setTitle("\(modal.userRating ?? 0)", for: .normal)
            
            /*************** Hide bookmark option not need on rated Inpiration  ****************/
            btnBookmark.isHidden = true
            constraintWidthBtnBookmark.constant = 0
        }
    }
}
