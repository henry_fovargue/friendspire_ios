//
//  SettingsCell.swift
//  FriendSpire
//
//  Created by Mac Test on 19/07/18.
//  Copyright © 2018 openkey. All rights reserved.
//

import UIKit

class SettingsCell: UITableViewCell {
    
    @IBOutlet weak var lbl_Title: UILabel!
    @IBOutlet weak var lbl_Version: UILabel!
    @IBOutlet weak var imgView_Arrow: UIImageView!
    
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
    func configureCell(_ str : String , showVersion : Bool){
        
        imgView_Arrow.isHidden = showVersion
        lbl_Version.isHidden = !showVersion
        lbl_Version.text = appVersion
        lbl_Title.text = str
    }
    
    func configureAccountCell(_ str : String , showArrow : Bool){
        
        imgView_Arrow.isHidden = !showArrow
        lbl_Title.text = str
    }
    
    func configureHelpCell(_ indexPath:IndexPath){
        if let currentItem = HelpSection(rawValue: indexPath.row){
            lbl_Title.text = currentItem.getName()
        }
    }
    
}

