//
//  GenralSettingCell.swift
//  FriendSpire
//
//  Created by abhishek on 25/07/18.
//  Copyright © 2018 openkey. All rights reserved.
//

import UIKit

class GenralSettingCell: UITableViewCell {
    
    
    //Miles Cell
    @IBOutlet weak var btn_Miles: UIButton?
    @IBOutlet weak var btn_Kms: UIButton?
    
    //PrivateCell
    @IBOutlet weak var lbl_Description: UILabel?
    
    
    
    //Genral Cell and Global For all
    @IBOutlet weak var lbl_Title: UILabel!
    @IBOutlet weak var switch_Status: UISwitch?
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
    func configureCell(title:String ,setting:SettingsModal?, index:Int){
        
        switch_Status?.tag = index
        
        self.lbl_Description?.isHidden = index == 1 ? false : true
        self.lbl_Title.text = title
        
        
        //        switch_Status?.isOn = true
        if setting?.isPrivate != nil && title.contains(Titles.SettingScreenTitles.GenralSettingTitles.privateAccount){
            switch_Status?.isOn = setting?.isPrivate == 0 ? false : true
        }
            
        else if setting?.pushNotification != nil && title.contains(Titles.SettingScreenTitles.GenralSettingTitles.push){
            switch_Status?.isOn = setting?.pushNotification == 0 ? false : true
        }
        else if setting?.isCategories != nil && title.contains(Titles.SettingScreenTitles.GenralSettingTitles.preferred){
            switch_Status?.isOn = setting?.isCategories ?? true
        }
        else if setting?.isLocation != nil && title.contains(Titles.SettingScreenTitles.GenralSettingTitles.locations){
            switch_Status?.isOn = setting?.isLocation ?? true
        }
        
        if setting?.distanceUnit != nil && title.contains(Titles.SettingScreenTitles.GenralSettingTitles.distance){
            
            if setting?.distanceUnit == DistanceUnit.Miles.rawValue{
                btn_Miles?.isSelected = true
                btn_Kms?.isSelected = false
                btn_Miles?.backgroundColor = .white
                btn_Kms?.backgroundColor = #colorLiteral(red: 0.9490196078, green: 0.968627451, blue: 0.9921568627, alpha: 1)
            }else {
                btn_Miles?.isSelected = false
                btn_Kms?.isSelected = true
                btn_Kms?.backgroundColor = .white
                btn_Miles?.backgroundColor = #colorLiteral(red: 0.9490196078, green: 0.968627451, blue: 0.9921568627, alpha: 1)
            }
        }
    }
    
    
    
}
