//
//  SearchMoreCell.swift
//  FriendSpire
//
//  Created by abhishek on 31/08/18.
//  Copyright © 2018 openkey. All rights reserved.
//

import UIKit

class SearchMoreCell: UITableViewCell {
    
    
    @IBOutlet weak var imgViewIcon: UIImageView!
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblDescription: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
    
    func configureCell(_ modal : SearchRecomendationModal){
        
        imgViewIcon.image = Utility.getIcon(using: Utility.getBackendTypeString(using: modal.type ?? 0)).withRenderingMode(.alwaysTemplate)
        imgViewIcon.tintColor = UIColor.lightGray
        lblTitle.text = modal.title
        
        var str = "NA"
        switch modal.type {
            
        case Constant.CategoryTypeBackend.movie.rawValue:
            if let year = modal.year{
                str = "\(year)"
            }
            
            break
        case Constant.CategoryTypeBackend.tv.rawValue:
            if let year = modal.start_year{
                str = "\(year) - "
                if let endYear = modal.end_year{
                    if endYear > 0 {
                    str.append("\(endYear)")
                    }
                }
            }
            break
        case Constant.CategoryTypeBackend.books.rawValue:
            if let author = modal.author{
                str = author
            }
            break
        case Constant.CategoryTypeBackend.restraunt.rawValue:
            if let arrAddress = modal.address{
                str = arrAddress.joined(separator: ",")
            }
            break
        case Constant.CategoryTypeBackend.bars.rawValue:
            if let arrAddress = modal.address{
                str = arrAddress.joined(separator: ",")
            }
            
            break
        default:
            DILog.print(items: "NA")
        }
        
        lblDescription.text = str
    }
}
