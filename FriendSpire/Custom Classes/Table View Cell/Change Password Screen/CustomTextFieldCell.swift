//
//  CustomTextFieldCell.swift
//  FriendSpire
//
//  Created by shubam on 28/08/18.
//  Copyright © 2018 openkey. All rights reserved.
//

import UIKit

class CustomTextFieldCell: UITableViewCell {

    @IBOutlet weak var leadingTxtFldConstraint: NSLayoutConstraint!
    @IBOutlet weak var imageWidthConstraint: NSLayoutConstraint!
    @IBOutlet weak var txtImgVw: UIImageView!
    @IBOutlet weak var textFld: UITextField!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    func configureChangePasswordCell(_ section:Int){
        switch section {
        case ChangePasswordFields.currentPassword.rawValue:
            textFld.placeholder = "Current Password"
            textFld.isSecureTextEntry = true
        case ChangePasswordFields.newPassword.rawValue:
            textFld.placeholder = "New Password"
            textFld.isSecureTextEntry = true
        case ChangePasswordFields.confirmPassword.rawValue:
            textFld.placeholder = "Confirm Password"
            textFld.isSecureTextEntry = true
        default:
            break
        }
        textFld.tag = section
    }
    
    func configureEditProfileCell(_ section:Int,editProfile:EditProfile){
        imageWidthConstraint.constant = 0.0
        leadingTxtFldConstraint.constant = 0.0
        txtImgVw.isHidden = true
        if let currentItem = EditProfileSection(rawValue: section){
            textFld.placeholder = currentItem.getPlaceholderTextAndImage().text
            switch currentItem {
            case .email:
                textFld.text = editProfile.email
                addRightImgViewInTxtField(textFld, image: #imageLiteral(resourceName: "edit"))
            case .dob:
                textFld.text = editProfile.dob.convertDate(.preDefined, toFormat: .editProfiledisplay) == "NA" ? "" : editProfile.dob.convertDate(.preDefined, toFormat: .editProfiledisplay)
                textFld.rightView = nil
            case .location:
                textFld.text = editProfile.location
                textFld.rightView = nil
            case .gender:
                textFld.text = editProfile.gender
                addRightImgViewInTxtField(textFld, image: #imageLiteral(resourceName: "down"))
            default:break
            }
        }
    }
    
    func addRightImgViewInTxtField(_ textField:UITextField,image:UIImage){
        textField.rightViewMode = .always
        let imageView = UIImageView(frame: CGRect(x: 0, y: 0, width: 13, height: 22))
        imageView.isUserInteractionEnabled = false
        imageView.contentMode = .scaleAspectFit
        imageView.image = image
        textField.rightView = imageView
    }
}


