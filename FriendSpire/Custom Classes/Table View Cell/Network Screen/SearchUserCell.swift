//
//  SearchUserCell.swift
//  FriendSpire
//
//  Created by abhishek on 26/07/18.
//  Copyright © 2018 openkey. All rights reserved.
//

import UIKit
import AlamofireImage

class SearchUserCell: UITableViewCell {
    
    //IBOutlets
    @IBOutlet weak var imgViewProfile: UIImageView!
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var lblFollowerCount: UILabel!
    @IBOutlet weak var lblRatingCount: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
    func configureCell(_ modal: SearchFriendModal){
        lblName.text =  "\(modal.firstName ?? "") \(modal.lastName ?? "")"
        lblFollowerCount.text = "\(modal.followers ?? 0)"
        
        if let url = URL.init(string:  modal.picture ?? ""){
            
            imgViewProfile.af_setImage(withURL: url, placeholderImage: #imageLiteral(resourceName: "user"), filter: nil, progress: nil, progressQueue: .global(qos: .background), imageTransition: .crossDissolve(0.2), runImageTransitionIfCached: false) { (response) in
            
            }
        }
        //        lblFollowerCount.text = "\(modal. ?? 0)"
        
    }
}
