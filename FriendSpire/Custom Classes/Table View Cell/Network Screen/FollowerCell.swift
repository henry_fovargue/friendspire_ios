//
//  FollowerCell.swift
//  FriendSpire
//
//  Created by Mac Test on 19/07/18.
//  Copyright © 2018 openkey. All rights reserved.
//

import UIKit

enum ArrayRefernce : Int{
    case follower = 900000
    case facebook = 500000
    case following = 100000
    
}

enum FollowStatus : Int{
    case NotConnected = 0
    case Following = 1
    case Requested = 2
    case FollowBack = 3
}

class FollowerCell: UITableViewCell {
    
    @IBOutlet weak var imgView_Profile: UIImageView!
    @IBOutlet weak var lbl_Name: UILabel!
    @IBOutlet weak var btn_Follow: UIButton!
    @IBOutlet weak var constraintTrailingName: NSLayoutConstraint!
    
    //Pending Regquest
    
    @IBOutlet weak var stackView_Pending: UIStackView!
    @IBOutlet weak var btn_Accept: UIButton!
    @IBOutlet weak var btn_Reject: UIButton!
    @IBOutlet weak var imgView_Approved: UIImageView!
    @IBOutlet weak var btnBlock: UIButton!
    @IBOutlet weak var constraintWidthBlock: NSLayoutConstraint!
    
    
    
    //Header View
    @IBOutlet weak var lbl_HeaderTitle: UILabel!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    /**
     this is used to set default state of elements
     */
    
    func setDefaultstate(){
        self.imgView_Profile.af_cancelImageRequest()
        self.imgView_Profile.image = #imageLiteral(resourceName: "user")
        stackView_Pending.isHidden = true
        imgView_Approved.isHidden = true
        btn_Follow.isHidden = true
        self.btnBlock.isHidden = true
        self.constraintWidthBlock.constant = 0
        self.constraintTrailingName.constant = 55
        //        btn_Follow.isUserInteractionEnabled = true
    }
    
    
    /**
     this is used to populate detail of user from facebook
     */
    func configureCellFB(user:FbModal, using index:IndexPath){
        self.imgView_Profile.image = #imageLiteral(resourceName: "user")
        self.lbl_Name.text = "\(user.userId?.firstName ?? "") \(user.userId?.lastName ?? "")"
        btn_Follow.tag =  ArrayRefernce.facebook.rawValue + index.row
        btn_Follow.isHidden = false
        if let imageUrl = URL.init(string: user.userId?.picture ?? "") {
            imgView_Profile.af_setImage(withURL: imageUrl, placeholderImage: #imageLiteral(resourceName: "user") , filter: nil, progress: nil, imageTransition: .crossDissolve(0.7), runImageTransitionIfCached: false, completion: nil)
        }
        
        _ = setUserStatus(status: user.status ?? 0)
        
    }
    
    /**
     this is used to populate detail of user follow you
     */
    
    func configureCellFollower(user:FollowerModal, index:IndexPath){
        
        
        self.imgView_Profile.image = #imageLiteral(resourceName: "user")
        lbl_Name.text =  "\(user.firstName ?? "") \(user.lastName ?? "")"
        if let imageUrl = URL.init(string: user.picture ?? "") {
            imgView_Profile.af_setImage(withURL: imageUrl, placeholderImage: #imageLiteral(resourceName: "user") , filter: nil, progress: nil, imageTransition: .crossDissolve(0.7), runImageTransitionIfCached: false, completion: nil)
        }
        if index.section == 0 {
            stackView_Pending.isHidden = false
            btn_Accept.tag = index.row
            btn_Reject.tag = index.row
            imgView_Approved.isHidden = true
        }else {
            btn_Follow.tag = ArrayRefernce.follower.rawValue + index.row
            btnBlock.tag = ArrayRefernce.follower.rawValue + index.row
            if User.sharedInstance.id == user.id{
                btn_Follow.isHidden = true
                return
            }
            
            btn_Follow.isHidden = false
            self.btnBlock.isHidden = false
            self.constraintWidthBlock.constant = 50
            self.constraintTrailingName.constant = 115
            if setUserStatus(status: user.status ?? 0) != nil{
                self.btn_Follow.isHidden = true
                self.constraintTrailingName.constant = 55
            }
        }
    }
    
    /**
     this is used to populate detail of user whom you follow
     */
    func configureCellFollowing(user:FollowerModal, index:IndexPath){
        self.imgView_Profile.image = #imageLiteral(resourceName: "user")
        btn_Follow.tag =  ArrayRefernce.following.rawValue + index.row
        btn_Follow.isHidden = false
        lbl_Name.text =  "\(user.firstName ?? "") \(user.lastName ?? "")"
        
        if let imageUrl = URL.init(string: user.picture ?? "") {
           imgView_Profile.af_setImage(withURL: imageUrl, placeholderImage: #imageLiteral(resourceName: "user") , filter: nil, progress: nil, imageTransition: .crossDissolve(0.7), runImageTransitionIfCached: false, completion: nil)
        }
        
        if User.sharedInstance.id == user.id{
            btn_Follow.isHidden = true
            return
        }
        
        _ = setUserStatus(status: user.status ?? 0)
        
    }
    
    /**
     this will show option to follow or unfollow etc
     parameter status : Int use to define the user state in your network
     
     */
    func setUserStatus(status : Int) -> Bool?{
        btn_Follow.isUserInteractionEnabled = true
        switch (status) {
            
        case FollowStatus.NotConnected.rawValue: //not in your network
            btn_Follow.setTitleColor(#colorLiteral(red: 0.3450980392, green: 0.6509803922, blue: 0.1333333333, alpha: 1), for: .normal)
            btn_Follow.isSelected = false // show follow Option
            btn_Follow.setImage( #imageLiteral(resourceName: "Follow"), for: .normal)
            btn_Follow.setTitle("Follow", for: .normal)
            //            return true  // this will indicate to show block button
            break
        case FollowStatus.Following.rawValue: //you follow this user
            btn_Follow.isSelected = true // show Unfollow Option
            btn_Follow.setImage( #imageLiteral(resourceName: "Unfollow"), for: .selected)
            btn_Follow.setTitle("Unfollow", for: .selected)
            btn_Follow.setTitleColor(#colorLiteral(red: 0.9921568627, green: 0.1921568627, blue: 0.2431372549, alpha: 1), for: .selected)
            return true  // this will indicate to hide follow button
            
        case FollowStatus.Requested.rawValue: // waiting for user response
            btn_Follow.isUserInteractionEnabled = false
            btn_Follow.setImage( #imageLiteral(resourceName: "pending"), for: .normal) // show pending Option
            btn_Follow.setImage( #imageLiteral(resourceName: "pending"), for: .selected) // show pending Option
            btn_Follow.setTitle("Requested", for: .normal)
            btn_Follow.setTitle("Requested", for: .selected)
            btn_Follow.setTitleColor(#colorLiteral(red: 0.9019607843, green: 0.4588235294, blue: 0.02745098039, alpha: 1), for: .normal)
            btn_Follow.setTitleColor(#colorLiteral(red: 0.8862745098, green: 0.4117647059, blue: 0.03921568627, alpha: 1), for: .selected)
            
            return true  // this will indicate to hide follow button
            
        case FollowStatus.FollowBack.rawValue: // user follows you but you didn't
            btn_Follow.isSelected = false // show follow  back Option
            btn_Follow.setImage( #imageLiteral(resourceName: "Follow"), for: .normal)
            btn_Follow.setTitle("Follow", for: .normal)
            btn_Follow.setTitleColor(#colorLiteral(red: 0.3450980392, green: 0.6509803922, blue: 0.1333333333, alpha: 1), for: .normal)
            //            return true  // this will indicate to show block button
            break
            
            
        default:
            DILog.print(items: "nothing to do")
            break
        }
        return nil
    }
}
