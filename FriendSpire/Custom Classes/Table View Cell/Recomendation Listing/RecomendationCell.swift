//
//  RecomendationCell.swift
//  FriendSpire
//
//  Created by abhishek on 06/08/18.
//  Copyright © 2018 openkey. All rights reserved.
//

import UIKit

class RecomendationCell: UITableViewCell {
    
    //IBoutlet
    
    @IBOutlet weak var commentButton: UIButton!
    @IBOutlet weak var lblColorStrip: UILabel!
    @IBOutlet weak var imgViewPoster: UIImageView!
    @IBOutlet weak var constraintHeightImgViewPoster: NSLayoutConstraint!
    
    @IBOutlet weak var viewAddBackground: UIView!
    @IBOutlet weak var commentStackViewLeadingConstraint: NSLayoutConstraint!
    
    @IBOutlet weak var constraintHeightImgviewRating: NSLayoutConstraint!
    @IBOutlet weak var imgViewRating: UIImageView!
    @IBOutlet weak var constraintTrailingLblTitle: NSLayoutConstraint!
    @IBOutlet weak var imgViewCategory: UIImageView!
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblDescription: UILabel!
    @IBOutlet weak var constraintHeightlblDescription: NSLayoutConstraint!
    
    @IBOutlet weak var stackView: UIStackView!
    @IBOutlet weak var constraintHeightStackView: NSLayoutConstraint!
    @IBOutlet weak var lblstatusHeightConstraint: NSLayoutConstraint!
    
    //    Rating Related IBoutlet
    @IBOutlet weak var imgViewRateFriends: UIImageView!
    @IBOutlet weak var lblRateFriends: UILabel!
    @IBOutlet weak var imgViewRateEveryone: UIImageView!
    @IBOutlet weak var lblRateEveryone: UILabel!
    @IBOutlet weak var lblRateThirdParty: UILabel!
    @IBOutlet weak var lblThirdPartyName: UILabel!
    
    @IBOutlet weak var lblFriendsRatingCount: UILabel!
    @IBOutlet weak var lblEveryOneRatingCount: UILabel!
    
    //   Action Related IBoutlet
    @IBOutlet weak var btnShare: UIButton!
    @IBOutlet weak var btnBookmark: UIButton!
    @IBOutlet weak var btnAddRating: UIButton!
    @IBOutlet weak var constraintWidthBtnBookmark: NSLayoutConstraint!
    
    //some extra details as well
    @IBOutlet weak var lblComments: UILabel!
    @IBOutlet weak var lblStatus: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
    
    func configureCellLibrary(_ modal : FeedModal ,with index: IndexPath, with category :String,isReview:Bool,isLoginUser:Bool){
        
        // intial setup
        btnBookmark.isHidden = false
        constraintWidthBtnBookmark.constant = 50
        btnShare.tag = index.row
        commentButton.tag = index.row
        btnBookmark.tag = index.row
        btnAddRating.tag = index.row
        lblColorStrip.backgroundColor = Utility.getColor(using: Utility.getBackendTypeString(using: modal.type ?? 0))
        viewAddBackground.backgroundColor = lblColorStrip.backgroundColor
        btnBookmark.isSelected = false
        btnAddRating.setImage(#imageLiteral(resourceName: "add -neq-white"), for: .normal)
        btnAddRating.setTitle("", for: .normal)
        stackView.isHidden = true
        constraintHeightStackView.constant = 0
        lblStatus.isHidden = true
        lblstatusHeightConstraint.constant = 0.0
        btnShare.setImage(#imageLiteral(resourceName: "share-symbolLarge"),for: .normal)
        btnBookmark.setImage(#imageLiteral(resourceName: "bookmark-blackLarge"), for: .normal)
        btnBookmark.setImage(#imageLiteral(resourceName: "bookmarkLargeActive"), for: .selected)
        self.imgViewPoster.af_cancelImageRequest()
        self.imgViewPoster.image = #imageLiteral(resourceName: "placeholderPoster")
        constraintHeightImgViewPoster.constant = 150
        constraintHeightImgviewRating.constant = 0
        commentStackViewLeadingConstraint.constant = 5
        lblComments.numberOfLines = 3
        imgViewRating.isHidden = true
        
        //Show Data on view
        lblTitle.text = modal.title
        if isReview == true{
            commentButton.isUserInteractionEnabled = true
            lblComments.text = modal.userReview
            self.configureCommentLabelViewLayout(forText: modal.userReview)
        }else{
            commentButton.isUserInteractionEnabled = false
            lblComments.text = modal.description
            self.configureCommentLabelViewLayout(forText: modal.description)
        }
        btnBookmark.isSelected = modal.myBookmarkStatus == 1 ? true : false
        if modal.image?.count ?? 0 > 0{
            var strURl =  modal.image?[0] ?? ""
            strURl = strURl.replacingOccurrences(of: "size", with: "190x130")
            if let imageUrl = URL.init(string:strURl) {
                self.imgViewPoster.af_setImage(withURL: imageUrl, placeholderImage: #imageLiteral(resourceName: "placeholderPoster") , filter: nil, progress: nil, imageTransition: .crossDissolve(0.7), runImageTransitionIfCached: false, completion: nil)
            }
        }
        var strGenre = ""
        if modal.type == Constant.CategoryTypeBackend.tv.rawValue || modal.type == Constant.CategoryTypeBackend.movie.rawValue {
            strGenre = Utility.getModifiedJoinString(using: modal.genre)
            strGenre = " \(strGenre) | "
        }
        
        //category wise data show
        switch modal.type ?? 0 {
            
        case Constant.CategoryTypeBackend.movie.rawValue:
            lblDescription.text = "\(modal.year ?? 0000)" + " |" + "\(strGenre)" + "\(Utility.minutesToHoursMinutes(minutes: modal.pagesDuration ?? 0))"
            break
        case Constant.CategoryTypeBackend.tv.rawValue:
            var strBegin = ""
            if let year = modal.startYear {
                strBegin = "\(year) -"
                if let year2 = modal.endYear {
                    if year2 > 0 {
                        strBegin.append(" \(year2)")
                    }
                }
                strBegin = strBegin + " |"
            }
            lblDescription.text = "\(strBegin)" + "\(strGenre)" + "\(Utility.minutesToHoursMinutes(minutes: modal.pagesDuration ?? 0))"
            break
        case Constant.CategoryTypeBackend.books.rawValue:
            if let authorName = modal.author{
                lblDescription.text = "\(authorName)" + " | " + "\(strGenre)" + "\( modal.year ?? 0)"
            }else {
                lblDescription.text = "\(strGenre)" + "\( modal.year ?? 0)"
            }
            break
        case Constant.CategoryTypeBackend.restraunt.rawValue:
            lblStatus.isHidden = false
            lblstatusHeightConstraint.constant = 20.0
            if modal.isOpen == OpenStatus.open.rawValue {
                lblStatus.text = "Open".localized
            } else if modal.isOpen == OpenStatus.closed.rawValue {
                lblStatus.text = "Closed".localized
            }else {
                lblStatus.text = ""
            }
            var  strDistance = ""
            if let str = modal.distance?.getDistanceString(){
                strDistance = str
            }
            if modal.city != nil && modal.city != ""{
                let strAddress = "\(modal.city ?? "")"
                lblDescription.text = "\(strAddress)" + " | " + strDistance + " | " + Utility.getModifiedJoinString(using: modal.cuisine) + " | " + Utility.getPriceLevel(value: modal.priceTier ?? 0)
            }else {
                lblDescription.text = strDistance + " | " + Utility.getModifiedJoinString(using: modal.cuisine) + " | " + Utility.getPriceLevel(value: modal.priceTier ?? 0)
            }
            break
        case Constant.CategoryTypeBackend.bars.rawValue:
            lblStatus.isHidden = false
            lblstatusHeightConstraint.constant = 20.0
            
            if modal.isOpen == OpenStatus.open.rawValue {
                lblStatus.text = "Open".localized
            } else if modal.isOpen == OpenStatus.closed.rawValue {
                lblStatus.text = "Closed".localized
            }else {
                lblStatus.text = ""
            }
            var  strDistance = ""
            if let str = modal.distance?.getDistanceString(){
                strDistance = str
            }
            if modal.city != nil && modal.city != ""{
                let strAddress = "\(modal.city ?? "")"
                lblDescription.text = "\(strAddress)" + " | " + strDistance  + " | " + Utility.getModifiedJoinString(using: modal.cuisine) + " | " + Utility.getPriceLevel(value: modal.priceTier ?? 0)
            }else {
                lblDescription.text =  strDistance  + " | " + Utility.getModifiedJoinString(using: modal.cuisine) + " | " + Utility.getPriceLevel(value: modal.priceTier ?? 0)
            }
            break
        default :
            DILog.print(items: "NA")
            break
        }
        
        if isLoginUser {
            if modal.userRating ?? 0 > 0 {
            ratingConfiguration(rating: modal.userRating ?? 0)
            }
            commentStackViewLeadingConstraint.constant = 5
            imgViewRating.isHidden = true
            constraintHeightImgviewRating.constant = 0
        }else{
            if modal.myRating ?? 0 > 0 {
                 ratingConfiguration(rating: modal.myRating ?? 0)
            }
            if modal.userRating ?? 0 > 0 {
                let img = Utility.getEmojiAccordindToRating(rating: Float(modal.userRating ?? 0))
                imgViewRating.image = img
                imgViewRating.isHidden = false
                commentStackViewLeadingConstraint.constant = 40
                lblComments.numberOfLines = 0
                constraintHeightImgviewRating.constant = 25
            }
        }
    }
    
    private func ratingConfiguration(rating : Int){
    let img = Utility.getEmojiAccordindToRating(rating: Float(rating))
    btnAddRating.setImage( Utility.resizeImage(image: img, newWidth: 25.0), for: .normal)
    btnAddRating.setTitle("\(rating)".removeZero(), for: .normal)
    /*************** Hide bookmark option not need on rated Inpiration  ****************/
    btnBookmark.isHidden = true
    constraintWidthBtnBookmark.constant = 0
        viewAddBackground.backgroundColor = .clear
    }
    
    func configureCellLatestRecom(_ modal : ProfileRecomendation ,with index: IndexPath, with category :String){
        
        //inital value
        /*************** Hide bookmark option not need on rated Inpiration  ****************/
        btnBookmark.isHidden = true
        commentButton.tag = index.row
        constraintWidthBtnBookmark.constant = 0
        btnShare.tag = index.row
        btnBookmark.tag = index.row
        btnAddRating.tag = index.row
        stackView.isHidden = true
        lblStatus.isHidden = true
        commentButton.isUserInteractionEnabled = true
        lblstatusHeightConstraint.constant = 0.0
        constraintHeightStackView.constant = 0
        lblColorStrip.backgroundColor = Utility.getColor(using: Utility.getBackendTypeString(using: modal.type ?? 0))
        self.imgViewPoster.af_cancelImageRequest()
        self.imgViewPoster.image = #imageLiteral(resourceName: "placeHolederCell")
        constraintHeightImgViewPoster.constant = 140

        //Show Data on view
        let modalToUse = modal.post
        lblTitle.text = modalToUse?.title
        lblComments.text = modal.review
        self.configureCommentLabelViewLayout(forText: modal.review)
        btnBookmark.isSelected = modal.is_bookmarked ?? false
        if modalToUse?.image?.count ?? 0 > 0{
            var strURl =  modalToUse?.image?[0] ?? ""
            strURl = strURl.replacingOccurrences(of: "size", with: "190x130")
            if let imageUrl = URL.init(string:strURl) {
                self.imgViewPoster.af_setImage(withURL: imageUrl, placeholderImage: #imageLiteral(resourceName: "placeHolederCell") , filter: nil, progress: nil, imageTransition: .crossDissolve(0.7), runImageTransitionIfCached: false, completion: nil)
            }
        }
        var strGenre = ""
        if modal.type == Constant.CategoryTypeBackend.tv.rawValue || modal.type == Constant.CategoryTypeBackend.movie.rawValue {
            strGenre = Utility.getModifiedJoinString(using: modalToUse?.genre)
            strGenre = " \(strGenre) | "
        }
 
        //category wise data show
        switch modal.type ?? 0 {
            
        case Constant.CategoryTypeBackend.movie.rawValue:
            lblDescription.text = "\(modalToUse?.year ?? 0000)" + " |" + "\(strGenre)" + "\(Utility.minutesToHoursMinutes(minutes: modalToUse?.pages_duration ?? 0))"
            break
        case Constant.CategoryTypeBackend.tv.rawValue:
            var strBegin = ""
            if let year = modalToUse?.startYear {
                strBegin = "\(year) -"
                if let year2 = modalToUse?.endYear {
                    if year2 > 0 {
                        strBegin.append(" \(year2)")
                    }
                }
                strBegin = strBegin + " |"
            }
            lblDescription.text = "\(strBegin)" + "\(strGenre)" + "\(Utility.minutesToHoursMinutes(minutes: modalToUse?.pages_duration ?? 0))"
            break
        case Constant.CategoryTypeBackend.books.rawValue:
            if let authorName = modalToUse?.author{
                lblDescription.text = "\(authorName)" + " | " + "\(strGenre)" + "\( modalToUse?.year ?? 0)"
            }else {
                lblDescription.text = "\(strGenre)" + "\( modalToUse?.year ?? 0)"
            }
            break
        case Constant.CategoryTypeBackend.restraunt.rawValue:
            var  strDistance = ""
            if let str = modalToUse?.distance?.getDistanceString(){
                strDistance = str
            }
            lblStatus.isHidden = false
            lblstatusHeightConstraint.constant = 20.0
            if modalToUse?.isOpen == OpenStatus.open.rawValue {
                lblStatus.text = "Open".localized
            } else if modalToUse?.isOpen == OpenStatus.closed.rawValue {
                lblStatus.text = "Closed".localized
            }else {
                lblStatus.text = ""
            }
            if modalToUse?.city != nil && modalToUse?.city != ""{
                let strAddress = "\(modalToUse?.city ?? "")"
                lblDescription.text = Utility.getModifiedJoinString(using: modalToUse?.cuisine) + " | " + Utility.getPriceLevel(value: modalToUse?.priceTier ?? 0) + "\n" + strDistance + " | " + "\(strAddress)"
            }else {
                lblDescription.text = Utility.getModifiedJoinString(using: modalToUse?.cuisine) + " | " + Utility.getPriceLevel(value: modalToUse?.priceTier ?? 0) + "\n" + strDistance
            }
            break
        case Constant.CategoryTypeBackend.bars.rawValue:
            var  strDistance = ""
            if let str = modalToUse?.distance?.getDistanceString(){
                strDistance = str
            }
            lblStatus.isHidden = false
            lblstatusHeightConstraint.constant = 20.0
            if modalToUse?.isOpen == OpenStatus.open.rawValue {
                lblStatus.text = "Open".localized
            } else if modalToUse?.isOpen == OpenStatus.closed.rawValue {
                lblStatus.text = "Closed".localized
            }else {
                lblStatus.text = ""
            }
            if modalToUse?.city != nil && modalToUse?.city != ""{
                let strAddress = "\(modalToUse?.city ?? "")"
                //                if modalToUse?.country?.count  ?? 0 > 0{
                //                    strAddress = strAddress + ", \(modalToUse?.country?[0] ?? "")"
                //                }
                //                lblDescription.text = "\(strAddress)" + " | " + strDistance + "\n" + Utility.getModifiedJoinString(using: modalToUse?.cuisine) + " | " + Utility.getPriceLevel(value: modalToUse?.priceTier ?? 0)
                //                if modalToUse?.country?.count  ?? 0 > 0{
                //                    strAddress = strAddress + ", \(modalToUse?.country?[0] ?? "")"
                //                }
                lblDescription.text = Utility.getModifiedJoinString(using: modalToUse?.cuisine) + " | " + Utility.getPriceLevel(value: modalToUse?.priceTier ?? 0) + "\n" + strDistance + " | " + strAddress
            }else {
                lblDescription.text = Utility.getModifiedJoinString(using: modalToUse?.cuisine) + " | " + Utility.getPriceLevel(value: modalToUse?.priceTier ?? 0) + "\n" + strDistance
            }
            break
        default :
            DILog.print(items: "NA")
            break
        }
        if modal.rating ?? 0 > 0 {
            let img = Utility.getEmojiAccordindToRating(rating: Float(modal.rating ?? 0))
            btnAddRating.setImage( Utility.resizeImage(image: img, newWidth: 20.0), for: .normal)
            btnAddRating.setTitle("\(modal.rating ?? 0)".removeZero(), for: .normal)
            viewAddBackground.backgroundColor = .clear
        }
    }
    
    /**
     Configure cell for add recomendation
     param : index(IndexPath) use to give tag to button , category (String) selected category
     */
    func configureCellAddRecomendationList(modal : FeedModal, with index: IndexPath , with category :String){
        
        // intial setup
        lblComments.numberOfLines = 3
        lblComments.lineBreakMode = NSLineBreakMode.byTruncatingTail
        btnBookmark.isHidden = false
        constraintWidthBtnBookmark.constant = 50
        btnShare.tag = index.row
        btnBookmark.tag = index.row
        btnAddRating.tag = index.row
        let color = Utility.getColor(using: category)
        lblColorStrip.backgroundColor = color
        lblEveryOneRatingCount.textColor = color
        lblFriendsRatingCount.textColor = color
        viewAddBackground.backgroundColor = color

        btnBookmark.isSelected = false
        btnAddRating.setImage(#imageLiteral(resourceName: "add -neq-white"), for: .normal)
        btnAddRating.setTitle("", for: .normal)
        imgViewCategory.isHidden = false
        imgViewCategory.image =  Utility.getIcon(using: category).withRenderingMode(.alwaysTemplate)
        constraintTrailingLblTitle.constant = 35.0
        constraintHeightImgViewPoster.constant = 200.0
        self.imgViewPoster.af_cancelImageRequest()
        self.imgViewPoster.image = #imageLiteral(resourceName: "placeHolederCell")
        lblstatusHeightConstraint.constant = 0.0
        lblStatus.isHidden = true

        
        //Show Data on view
        lblTitle.text = modal.title
        lblComments.text = modal.description
        self.configureCommentLabelViewLayout(forText: modal.description)
        btnBookmark.isSelected = modal.isBookmarked ?? false
        if modal.image?.count ?? 0 > 0{
            var strURl =  modal.image?[0] ?? ""
            strURl = strURl.replacingOccurrences(of: "size", with: "570x390")
            if let imageUrl = URL.init(string:strURl) {
                self.imgViewPoster.af_setImage(withURL: imageUrl, placeholderImage: #imageLiteral(resourceName: "placeHolederCell") , filter: nil, progress: nil, imageTransition: .crossDissolve(0.7), runImageTransitionIfCached: false, completion: nil)
            }
        }
        var strGenre = ""
        if modal.type == Constant.CategoryTypeBackend.tv.rawValue || modal.type == Constant.CategoryTypeBackend.movie.rawValue {
            strGenre = Utility.getModifiedJoinString(using: modal.genre)
            strGenre = " \(strGenre) | "
        }
        
        //category wise data show
        switch category {
            
        case AppMessages.Categories.movie:
            lblDescription.text = "\(modal.year ?? 0000)" + " | " + "\(strGenre)" + "\(Utility.minutesToHoursMinutes(minutes: modal.pagesDuration ?? 0))"
            lblThirdPartyName.text = Constant.RatingFromThirdParty.imdb
            break
        case AppMessages.Categories.tv:
            var strBegin = ""
            if let year = modal.startYear {
                strBegin = "\(year) -"
                if let year2 = modal.endYear {
                    if year2 > 0 {
                        strBegin.append(" \(year2)")
                    }
                }
                strBegin = strBegin + " |"
            }
            lblDescription.text = "\(strBegin)" + "\(strGenre)" + "\(Utility.minutesToHoursMinutes(minutes: modal.pagesDuration ?? 0))"
            lblThirdPartyName.text = Constant.RatingFromThirdParty.imdb
            break
        case AppMessages.Categories.books:
            let year =  modal.year ?? 0
            if let authorName = modal.author{
                lblDescription.text = "\(authorName)" + " | " + "\(strGenre)"  + "\(year == 0 ? "" : "\(year)")"
            }else {
                lblDescription.text =  "\(strGenre)"  + "\(year == 0 ? "" : "\(year)")"
            }
            lblThirdPartyName.text = Constant.RatingFromThirdParty.goodReads
            break
            
        case AppMessages.Categories.restruant:
            var  strDistance = ""
            if let str = modal.distance?.getDistanceString(){
                strDistance = str
            }
            lblStatus.isHidden = false
            lblstatusHeightConstraint.constant = 20.0
            constraintHeightImgViewPoster.constant = 220.0
            if modal.isOpen == OpenStatus.open.rawValue {
                lblStatus.text = "Open".localized
            } else if modal.isOpen == OpenStatus.closed.rawValue {
                lblStatus.text = "Closed".localized
            }else {
                lblStatus.text = ""
                lblstatusHeightConstraint.constant = 0.0
                constraintHeightImgViewPoster.constant = 200.0
            }
          strDistance =  strDistance + " | " + Utility.getModifiedJoinString(using: modal.cuisine) + " | " + Utility.getPriceLevel(value: modal.priceTier ?? 0)
            if modal.city != nil && modal.city != ""{
                let strAddress = "\(modal.city ?? "")"
                lblDescription.text = "\(strAddress)" + " | " + strDistance
            }else {
                lblDescription.text = strDistance
            }
            lblThirdPartyName.text = Constant.RatingFromThirdParty.fourSquare
            break
            
        case AppMessages.Categories.bars:
            //            constraintHeightImgViewPoster.constant = 200.0
            var  strDistance = " "
            if let str = modal.distance?.getDistanceString(){
                strDistance = str
            }
            lblStatus.isHidden = false
            lblstatusHeightConstraint.constant = 20.0
            constraintHeightImgViewPoster.constant = 220.0
            if modal.isOpen == OpenStatus.open.rawValue {
                lblStatus.text = "Open".localized
            } else if modal.isOpen == OpenStatus.closed.rawValue {
                lblStatus.text = "Closed".localized
            }else {
                lblStatus.text = ""
                lblstatusHeightConstraint.constant = 0.0
                constraintHeightImgViewPoster.constant = 200.0
            }
            strDistance =  strDistance + " | " + Utility.getModifiedJoinString(using: modal.cuisine) + " | " + Utility.getPriceLevel(value: modal.priceTier ?? 0)
            if modal.city != nil && modal.city != ""{
                let strAddress = "\(modal.city ?? "")"
                lblDescription.text = "\(strAddress)" + " | " + strDistance
            }else {
                lblDescription.text = strDistance
            }
            lblThirdPartyName.text = Constant.RatingFromThirdParty.fourSquare
            break
            
        default :
            DILog.print(items: "NA")
            break
        }
        
        //Show remaining data
        lblRateThirdParty.text =  String(format: "%.1f", arguments: [modal.rating ?? 0.0]).removeZero()
        lblRateEveryone.text = String(format: "%.1f", arguments: [modal.friendspireRating ?? 0.0]).removeZero()
        lblEveryOneRatingCount.text = modal.everyoneCount?.countFormationWithoutReview
        imgViewRateEveryone.image = Utility.getEmojiAccordindToRating(rating: Float(modal.friendspireRating ?? 0.0))
        lblRateFriends.text = String(format: "%.1f", arguments: [modal.friendsRating ?? 0.0]).removeZero()
        lblFriendsRatingCount.text = modal.countFriends?.countFormationWithoutReview
        imgViewRateFriends.image = Utility.getEmojiAccordindToRating(rating: Float(modal.friendsRating ?? 0.0))
        if modal.userRating ?? 0 > 0 {
            let img = Utility.getEmojiAccordindToRating(rating: Float(modal.userRating ?? 0))
            btnAddRating.setImage( Utility.resizeImage(image: img, newWidth: 20.0), for: .normal)
            btnAddRating.setTitle("\(modal.userRating ?? 0)".removeZero(), for: .normal)
            
            /*************** Hide bookmark option not need on rated Inpiration  ****************/
            btnBookmark.isHidden = true
            constraintWidthBtnBookmark.constant = 0
            viewAddBackground.backgroundColor = .clear
        }
    }
    
    //set the visibility of comment label for empty or non-empty text
    private func configureCommentLabelViewLayout(forText text: String?) {
        if let text = text {
            lblComments.isHidden = text.isEmpty
        } else { //fallback in case of nil setter
            lblComments.isHidden = true
        }
    }
}
