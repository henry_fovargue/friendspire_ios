//
//  RestrauntOnMapCell.swift
//  FriendSpire
//
//  Created by Harpreet_kaur on 25/10/18.
//  Copyright © 2018 openkey. All rights reserved.
//

import UIKit

class RestrauntOnMapCell: UICollectionViewCell {
    
    //IBoutlet
    
    @IBOutlet weak var commentButton: UIButton!
    @IBOutlet weak var lblColorStrip: UILabel!
    @IBOutlet weak var imgViewPoster: UIImageView!
    @IBOutlet weak var constraintTrailingLblTitle: NSLayoutConstraint!
    @IBOutlet weak var imgViewCategory: UIImageView!
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblDescription: UILabel!
    @IBOutlet weak var stackView: UIStackView!
    @IBOutlet weak var constraintHeightStackView: NSLayoutConstraint!
    @IBOutlet weak var lblstatusHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var constraintHeightImgViewRating: NSLayoutConstraint!
    @IBOutlet weak var commentStackViewLeadingConstraint: NSLayoutConstraint!
    @IBOutlet weak var imgViewRating: UIImageView!
    
    //    Rating Related IBoutlet
    @IBOutlet weak var imgViewRateFriends: UIImageView!
    @IBOutlet weak var lblRateFriends: UILabel!
    @IBOutlet weak var imgViewRateEveryone: UIImageView!
    @IBOutlet weak var lblRateEveryone: UILabel!
    @IBOutlet weak var lblRateThirdParty: UILabel!
    @IBOutlet weak var lblThirdPartyName: UILabel!
    @IBOutlet weak var lblFriendsRatingCount: UILabel!
    @IBOutlet weak var lblEveryOneRatingCount: UILabel!
    
    //   Action Related IBoutlet
    @IBOutlet weak var btnShare: UIButton!
    @IBOutlet weak var btnBookmark: UIButton!
    @IBOutlet weak var btnAddRating: UIButton!
    @IBOutlet weak var constraintWidthBtnBookmark: NSLayoutConstraint!
    
    //some extra details as well
    @IBOutlet weak var lblComments: UILabel!
    @IBOutlet weak var lblStatus: UILabel!
    
    func configureCell(modal : FeedModal, with index: IndexPath , with category :String,_ isReview:Bool? = false,_ ownProfile:Bool? = true){
        
        // intial setup
        lblComments.numberOfLines = 3
        lblComments.lineBreakMode = NSLineBreakMode.byTruncatingTail
        btnBookmark.isHidden = false
        constraintWidthBtnBookmark.constant = 50
        btnShare.tag = index.row
        btnBookmark.tag = index.row
        btnAddRating.tag = index.row
        let color = Utility.getColor(using: category)
        lblColorStrip.backgroundColor = color
        lblEveryOneRatingCount.textColor = color
        lblFriendsRatingCount.textColor = color
        btnBookmark.isSelected = false
        btnAddRating.setImage(#imageLiteral(resourceName: "add"), for: .normal)
        btnAddRating.setTitle("", for: .normal)
        imgViewCategory.isHidden = false
        imgViewCategory.image =  Utility.getIcon(using: category).withRenderingMode(.alwaysTemplate)
        constraintTrailingLblTitle.constant = 35.0
        lblstatusHeightConstraint.constant = 20.0
        self.imgViewPoster.image = #imageLiteral(resourceName: "placeHolederCell")
        constraintHeightImgViewRating.constant = 0
        imgViewRating.isHidden = true
        
        //*****************************Show Data on view*********************************/
        lblTitle.text = modal.title
        if isReview == true{
            commentButton.isUserInteractionEnabled = true
            lblComments.text = modal.userReview
            self.configureCommentLabelViewLayout(forText: modal.userReview)
        }else{
            commentButton.isUserInteractionEnabled = false
            lblComments.text = modal.description
            self.configureCommentLabelViewLayout(forText: modal.description)
        }
        
        //        lblComments.text = modal.description
        //
        //        self.configureCommentLabelViewLayout(forText: modal.description)
        if modal.image?.count ?? 0 > 0{
            var strURl =  modal.image?[0] ?? ""
            strURl = strURl.replacingOccurrences(of: "size", with: "570x390")
            if let imageUrl = URL.init(string:strURl) {
                self.imgViewPoster.af_setImage(withURL: imageUrl, placeholderImage: #imageLiteral(resourceName: "placeHolederCell") , filter: nil, progress: nil, imageTransition: .crossDissolve(0.7), runImageTransitionIfCached: false, completion: nil)
            }
        }
        var strGenre = ""
        if modal.type == Constant.CategoryTypeBackend.tv.rawValue || modal.type == Constant.CategoryTypeBackend.movie.rawValue {
            strGenre = Utility.getModifiedJoinString(using: modal.genre)
            strGenre = " \(strGenre) | "
        }
        var  strDistance = ""
        if let str = modal.distance?.getDistanceString(){
            strDistance = str
        }
        lblStatus.isHidden = false
        if modal.isOpen == OpenStatus.open.rawValue {
            lblStatus.text = "Open".localized
        } else if modal.isOpen == OpenStatus.closed.rawValue {
            lblStatus.text = "Closed".localized
        }else {
            lblStatus.text = ""
            lblstatusHeightConstraint.constant = 0.0
        }
        let strAttribute = Utility.getModifiedJoinString(using: modal.cuisine) + " | " + Utility.getPriceLevel(value: modal.priceTier ?? 0)
        if modal.city != nil && modal.city != ""{
            let strAddress = "\(modal.city ?? "")"
            lblDescription.text = "\(strAddress)" + " | " + strDistance + " | " + strAttribute
        }else {
            lblDescription.text = strDistance + " | " + strAttribute
        }
        lblThirdPartyName.text = Constant.RatingFromThirdParty.fourSquare
        
        //Show remaining data
        lblRateThirdParty.text =  String(format: "%.1f", arguments: [modal.rating ?? 0.0]).removeZero()
        lblRateEveryone.text = String(format: "%.1f", arguments: [modal.friendspireRating ?? 0.0]).removeZero()
        lblEveryOneRatingCount.text = modal.everyoneCount?.countFormationWithoutReview
        imgViewRateEveryone.image = Utility.getEmojiAccordindToRating(rating: Float(modal.friendspireRating ?? 0.0))
        lblRateFriends.text = String(format: "%.1f", arguments: [modal.friendsRating ?? 0.0]).removeZero()
        lblFriendsRatingCount.text = modal.countFriends?.countFormationWithoutReview
        imgViewRateFriends.image = Utility.getEmojiAccordindToRating(rating: Float(modal.friendsRating ?? 0.0))
        
        if ownProfile == true{
            btnBookmark.isSelected = modal.isBookmarked ?? false
            if modal.userRating ?? 0 > 0 {
                ratingConfiguration(rating: modal.userRating ?? 0)
            }
            commentStackViewLeadingConstraint.constant = 5
            imgViewRating.isHidden = true
            constraintHeightImgViewRating.constant = 0
        }else{
            btnBookmark.isSelected = modal.myBookmarkStatus == 0 ? false : true
            if modal.myRating ?? 0 > 0 {
                ratingConfiguration(rating: modal.myRating ?? 0)
            }
            if modal.userRating ?? 0 > 0 {
                let img = Utility.getEmojiAccordindToRating(rating: Float(modal.userRating ?? 0))
                imgViewRating.image = img
                imgViewRating.isHidden = false
                commentStackViewLeadingConstraint.constant = 40
                lblComments.numberOfLines = 0
                constraintHeightImgViewRating.constant = 25
            }
        }
        
        //        if modal.userRating ?? 0 > 0 {
        //            let img = Utility.getEmojiAccordindToRating(rating: Float(modal.userRating ?? 0))
        //            btnAddRating.setImage( Utility.resizeImage(image: img, newWidth: 20.0), for: .normal)
        //            btnAddRating.setTitle("\(modal.userRating ?? 0)".removeZero(), for: .normal)
        //
        //            /*************** Hide bookmark option not need on rated Inpiration  ****************/
        //            btnBookmark.isHidden = true
        //            constraintWidthBtnBookmark.constant = 0
        //        }
    }
    
    private func ratingConfiguration(rating : Int){
        let img = Utility.getEmojiAccordindToRating(rating: Float(rating))
        btnAddRating.setImage( Utility.resizeImage(image: img, newWidth: 25.0), for: .normal)
        btnAddRating.setTitle("\(rating)".removeZero(), for: .normal)
        /*************** Hide bookmark option not need on rated Inpiration  ****************/
        btnBookmark.isHidden = true
        constraintWidthBtnBookmark.constant = 0
    }
    
    //set the visibility of comment label for empty or non-empty text
    private func configureCommentLabelViewLayout(forText text: String?) {
        if let text = text {
            lblComments.isHidden = text.isEmpty
        } else { //fallback in case of nil setter
            lblComments.isHidden = true
        }
    }
}
