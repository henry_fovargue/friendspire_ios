//
//  SlidingTutorialCell.swift
//  FriendSpire
//
//  Created by abhishek on 13/11/18.
//  Copyright © 2018 openkey. All rights reserved.
//

import UIKit

enum KeyTutorial : String {
    case image = "image"
    case title = "title"
}

class SlidingTutorialCell: UICollectionViewCell {
    
    @IBOutlet weak var viewShadow: UIView!
    @IBOutlet weak var imgViewScreen: UIImageView!
    @IBOutlet weak var lblTitle: UILabel!
 
    func configureCell(modal:[KeyTutorial:Any]){
        
        imgViewScreen.image = modal[KeyTutorial.image] as? UIImage
        let str = modal[KeyTutorial.title] as? String
        if str?.contains("Bookmark") == true{
            lblTitle.attributedText = attributedString()
        }else{
            lblTitle.text = str
        }
    }
    
    func attributedString() -> NSAttributedString {
        let fullString = NSMutableAttributedString(string: "Finally,press the ' ")
        let addAttachment = NSTextAttachment()
        addAttachment.image = #imageLiteral(resourceName: "add-icon-purple")
        let addString = NSAttributedString(attachment: addAttachment)
         fullString.append(addString)
        fullString.append(NSAttributedString(string: " ' to rate or press \n the '"))
        let bookmarkAttachment = NSTextAttachment()
        bookmarkAttachment.image = #imageLiteral(resourceName: "bookmark-white-shape")
        let bookmarkString = NSAttributedString(attachment: bookmarkAttachment)
        fullString.append(bookmarkString)
        fullString.append(NSAttributedString(string: "' to save for later \n That was easy right?"))
        return fullString
    }
}
