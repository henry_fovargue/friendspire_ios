//
//  CustomFlowLayout.swift
//  FriendSpire
//
//  Created by Mac Test on 17/07/18.
//  Copyright © 2018 openkey. All rights reserved.
//

import UIKit

@objc protocol CustomFlowLayoutDelegate : class {
    func collectionView(_ collectionView:UICollectionView,heightForPhotoAtIndexPath indexPath : IndexPath) -> CGFloat
    
    @objc optional
    func collectionView(_ collectionView:UICollectionView,widthForMessageAtIndexPath indexPath : IndexPath) -> CGFloat
    
}


class CustomFlowLayout: UICollectionViewFlowLayout {
    
    weak var delegate : CustomFlowLayoutDelegate!
    
    var numberOfColumns = 2
    fileprivate var cellPadding : CGFloat = 6
    var cache = [UICollectionViewLayoutAttributes]()
    var contentHeight : CGFloat = 0
    fileprivate var contentWidth : CGFloat{
        guard let collectionView = collectionView else{return 0}
        let insets = collectionView.contentInset
        return collectionView.bounds.width - (insets.left +  insets.right)
    }
    
    override var collectionViewContentSize: CGSize{
        return CGSize(width: contentWidth, height: contentHeight)
    }
    
    
    
    override func prepare() {
        guard cache.isEmpty == true , let collectionView = collectionView  else {
            return
        }
        let columnWidth = contentWidth / CGFloat(numberOfColumns)
        var xOffset = [CGFloat]()
        for column in 0 ..< numberOfColumns{
            xOffset.append(CGFloat(column) * columnWidth)
        }
        var  column = 0
        var yOffset = [CGFloat](repeating: 0, count: numberOfColumns)
        
//        DILog.print(items: "column count is \(numberOfColumns)  ======= \(yOffset)")
        
        for  item in 0 ..< collectionView.numberOfItems(inSection :0){
            let indexPath = IndexPath(item: item, section: 0)
            let photoHeight = delegate.collectionView(collectionView, heightForPhotoAtIndexPath: indexPath)
            let height = cellPadding * 2 +  photoHeight
            let frame = CGRect(x: xOffset[column], y: yOffset[column], width: columnWidth, height: height )
            let insetFrame = frame.insetBy(dx: cellPadding, dy: cellPadding)
            let attributes = UICollectionViewLayoutAttributes(forCellWith: indexPath)
            
            attributes.frame = insetFrame
            cache.append(attributes)
            
            contentHeight = max(contentHeight, frame.maxY)
            yOffset[column] = yOffset[column] +  height
            
//            DILog.print(items: "We are in section 0 =======  \(yOffset[column])")
            column = column < (numberOfColumns - 1) ? (column + 1 ): 0
        }
        
        numberOfColumns = 1
//        for column in 0 ..< numberOfColumns{
//            xOffset.append(CGFloat(column) * contentWidth)
//        }
        for  item in 0 ..< collectionView.numberOfItems(inSection :1){
            let indexPath = IndexPath(item: item, section: 1)
            let height = cellPadding * 2 +  50.0
            let frame = CGRect(x: 0.0, y: contentHeight, width: contentWidth, height: height )
            let insetFrame = frame.insetBy(dx: cellPadding, dy: cellPadding)
            let attributes = UICollectionViewLayoutAttributes(forCellWith: indexPath)
            
            attributes.frame = insetFrame
            cache.append(attributes)
            contentHeight = max(contentHeight, frame.maxY)
            
            
            // set all the yOffset to the same point
            column = 0
            while column < yOffset.count{
                yOffset[column] =  contentHeight
                column =  column + 1
            }
            
            column = column < (numberOfColumns - 1) ? (column + 1 ): 0
        }
        
        numberOfColumns = 2
        for column in 0 ..< numberOfColumns{
            xOffset.append(CGFloat(column) * columnWidth)
            
        }
        for  item in 0 ..< collectionView.numberOfItems(inSection :2){
            let indexPath = IndexPath(item: item, section: 2)
            let photoHeight = delegate.collectionView(collectionView, heightForPhotoAtIndexPath: indexPath)
            let height = cellPadding * 2 +  photoHeight
            
            
//            DILog.print(items: "comparision height------------------------------- \(contentHeight)")
//            DILog.print(items: "comparision offset------------------------------- \(yOffset[column])")

            let frame = CGRect(x: xOffset[column], y: yOffset[column], width: columnWidth, height: height )
            
            let insetFrame = frame.insetBy(dx: cellPadding, dy: cellPadding)
            let attributes = UICollectionViewLayoutAttributes(forCellWith: indexPath)
            
            attributes.frame = insetFrame
            cache.append(attributes)
            
            contentHeight = max(contentHeight, frame.maxY)
            yOffset[column] = yOffset[column] +  height
//            DILog.print(items: "We are in section 2 =======  \(yOffset[column])")
            
            column = column < (numberOfColumns - 1) ? (column + 1 ): 0
        }
        
    }
    
    override func layoutAttributesForElements(in rect: CGRect) -> [UICollectionViewLayoutAttributes]? {
        
        var visibleLayoutAttributes = [UICollectionViewLayoutAttributes]()
        
        // Loop through the cache and look for items in the rect
        for attributes in cache {
            if attributes.frame.intersects(rect) {
                visibleLayoutAttributes.append(attributes)
            }
        }
        return visibleLayoutAttributes
    }
    
    override func layoutAttributesForItem(at indexPath: IndexPath) -> UICollectionViewLayoutAttributes? {
        return cache[indexPath.item]
    }
    
}
