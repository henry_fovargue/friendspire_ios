
//
//  InspirationCell.swift
//  FriendSpire
//
//  Created by Mac Test on 17/07/18.
//  Copyright © 2018 openkey. All rights reserved.
//

import UIKit

enum AlgorimthmStatus : Int{
    case Applied = 1
    case NotApplied = 0
}

class InspirationCell: UICollectionViewCell {
    
    @IBOutlet weak var imgViewCategory: UIImageView!
    @IBOutlet weak var imgViewFeed: UIImageView!
    @IBOutlet weak var lblRating: UILabel!
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblGenre: UILabel!
    @IBOutlet weak var constraintHeightImageFeed: NSLayoutConstraint!
    @IBOutlet weak var viewColorStrip: UIView!
    @IBOutlet weak var imgViewRateIcon: UIImageView!
    @IBOutlet weak var imgViewReason: UIImageView!
    @IBOutlet weak var lblReason: UILabel!
    
    
    func configureInpiration(using modal:InspirationFeedModal){
        
        imgViewReason.af_cancelImageRequest()
        imgViewFeed.af_cancelImageRequest()
        lblTitle.text = modal.title
        imgViewRateIcon.image =  nil
        imgViewFeed.image = #imageLiteral(resourceName: "1_launch")
        imgViewReason.image = #imageLiteral(resourceName: "user")
        lblRating.text = ""
        if modal.image?.count ?? 0 > 0{
            
            var strURl =  modal.image?[0] ?? ""
            strURl = strURl.replacingOccurrences(of: "size", with: "190x130")
            if let imageUrl = URL.init(string:strURl) {
                
                self.imgViewFeed.af_setImage(withURL: imageUrl, placeholderImage: #imageLiteral(resourceName: "1_launch") , filter: nil, progress: nil, imageTransition: .crossDissolve(0.7), runImageTransitionIfCached: false, completion: nil)
            }
        }
        lblReason.text = modal.reason
        var category = Utility.getBackendTypeString(using: modal.type ?? 3)
        imgViewCategory.image =  Utility.getIcon(using: category)
        viewColorStrip.backgroundColor = Utility.getColor(using: category)
        if modal.algorithm == AlgorimthmStatus.Applied.rawValue{
            
            imgViewReason.image = #imageLiteral(resourceName: "popluar")
            imgViewRateIcon.image = Utility.getEmojiAccordindToRating(rating: Float(modal.friendspire_rating ?? 0.0))
            lblRating.text = String(format: "%.1f", arguments: [modal.friendspire_rating ?? 0.0]).removeZero()
        }else if modal.friends?.count ?? 0 > 0 {
            
            imgViewRateIcon.image = Utility.getEmojiAccordindToRating(rating: Float(modal.friends![0].rating ?? 0))
            lblRating.text = "\(modal.friends![0].rating ?? 0)".removeZero()
            let strURl =  modal.friends![0].user_detail?.profile_pic ?? ""
            
            if let imageUrl = URL.init(string:strURl) {
                self.imgViewReason.af_setImage(withURL: imageUrl, placeholderImage: #imageLiteral(resourceName: "user-signup") , filter: nil, progress: nil, imageTransition: .crossDissolve(0.7), runImageTransitionIfCached: false, completion: nil)
            }
            if category.last == "s" && category != AppMessages.Categories.tv{
                category = String(category.dropLast())
            }
            category = category.lowercased()
            if modal.friends?.count ?? 0 > 1 {
                
                let personCount = (modal.friends?.count)! - 1
                lblReason.text = "Your friend \(modal.friends![0].user_detail?.firstName?.capitalized ?? "") and \(personCount) \( personCount == 1 ? "other just reviewed" : "others just reviewed") this \(category)"
            }else{
                
                lblReason.text = showComment(with: modal.friends![0], category: category)
            }
        }
        let count = modal.recomendationCount ?? 0
        lblRating.text = (lblRating.text ?? "") + count.countFormation
        
        //category wise data show
        switch modal.type ?? 0 {
            
        case Constant.CategoryTypeBackend.movie.rawValue  :
            lblGenre.text = Utility.getModifiedJoinString(using: modal.genre)
            break
        case Constant.CategoryTypeBackend.tv.rawValue:
            lblGenre.text = Utility.getModifiedJoinString(using: modal.genre)
            break
        case Constant.CategoryTypeBackend.books.rawValue:
            lblGenre.text = modal.author
            break
        case Constant.CategoryTypeBackend.restraunt.rawValue:
            if modal.city != nil && modal.city != ""{
                
                let strAddress = "\(modal.city ?? " ")"
                lblGenre.text = "\(strAddress)" + " | " + Utility.getModifiedJoinString(using: modal.cuisine)
            }else {
                
                lblGenre.text = " " + Utility.getModifiedJoinString(using: modal.cuisine)
            }
            break
        case Constant.CategoryTypeBackend.bars.rawValue:
            if modal.city != nil && modal.city != ""{
                
                let strAddress = "\(modal.city ?? " ")"
                lblGenre.text = "\(strAddress)" + " | " + Utility.getModifiedJoinString(using: modal.cuisine)
            }else {
                
                lblGenre.text = " " + Utility.getModifiedJoinString(using: modal.cuisine)
            }
            break
        default :
            DILog.print(items: "NA")
            break
        }
    }
    
    func showComment(with modalLocal:FriendsInspiration ,category:String)-> String{
        
        switch modalLocal.rating ?? 0 {
            
        case 1:
            return "Your friend \(modalLocal.user_detail?.firstName?.capitalized ?? "") does not recommend this \(category)"
        case 2:
            return "Your friend \(modalLocal.user_detail?.firstName?.capitalized ?? "") is indifferent about this \(category)"
        case 3:
            return "Your friend \(modalLocal.user_detail?.firstName?.capitalized ?? "") thinks this \(category) is good"
        case 4:
            return "Your friend \(modalLocal.user_detail?.firstName?.capitalized ?? "") thinks this \(category) is excellent"
        case 5:
            return "Your friend \(modalLocal.user_detail?.firstName?.capitalized ?? "") loves this \(category)"
        default:
            return ""
        }
    }
}
extension Int64{
    
    var countFormation:String{
        if self == 0{return " (No review)"}
        var stringToAppend = "k"
        stringToAppend = self%1000 > 0 ? "k+" : "k" //decide how much reviews left to show
        let str = self > 999 ? " (\(self/1000)\(stringToAppend) ": " (\(self) " // get intial value to place before 'k
        return ( str + "\(self > 1 ? "reviews":"review"))")
    }
    
    var countFormationWithoutReview:String{
//        if self == 0{return ""}
        var stringToAppend = "k"
        stringToAppend = self%1000 > 0 ? "k+" : "k" //decide how much reviews left to show
        let str = self > 999 ? " (\(self/1000)\(stringToAppend))": " (\(self))" // get intial value to place before 'k
        return ( str )
    }
    
}
