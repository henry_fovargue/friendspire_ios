//
//  CategoryCell.swift
//  FriendSpire
//
//  Created by Mac Test on 17/07/18.
//  Copyright © 2018 openkey. All rights reserved.
//

import UIKit

enum CategoryEnum  :  String{
    case Title = "title"
    case Icon = "icon"
    case Color = "color"
}

class CategoryCell: UICollectionViewCell {
    
    @IBOutlet weak var imgView_Category: UIImageView!
    @IBOutlet weak var lbl_Category: UILabel!
    @IBOutlet weak var view_Background: UIView?
    
    var isSearch : Bool?  // this will keep check cell rendered are on search or somewhere else
    
    //Search Recomendation
    
    //    @IBOutlet weak var btnCategoryName: UIButton!
    
    
    override var isSelected: Bool {
        didSet {
            if isSearch == true{ // if its from search apply these effects
                lbl_Category.textColor = isSelected ? #colorLiteral(red: 0.9764705882, green: 0.2117647059, blue: 0.4039215686, alpha: 1) : #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
                self.backgroundColor = isSelected ?  #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1) : .clear
            }
//            else {
//                lbl_Category.textColor = #colorLiteral(red: 0.2392156863, green: 0.2509803922, blue: 0.3254901961, alpha: 1)
//            }
            
            // else if self.reuseIdentifier ==  CellIdentifiers.SearchRecomendationScreen.categoryIcon{
//               imgView_Category.tintColor = isSelected ? #colorLiteral(red: 0.9764705882, green: 0.2117647059, blue: 0.4039215686, alpha: 1) : #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
            //   lbl_Category.textColor = isSelected ? #colorLiteral(red: 0.2392156863, green: 0.2509803922, blue: 0.3254901961, alpha: 1) : #colorLiteral(red: 0.9764705882, green: 0.2117647059, blue: 0.4039215686, alpha: 1)
            //}
        }
    }
    
    /**
     This method use to render cell on dashboard
     param:  modal consist name (string), icon( Uiimage ), color(UIColor)
     */
    func configureCell(modal:[CategoryEnum:Any]){
        
        //populate data on view
        lbl_Category.text =  modal[.Title] as? String
        let icon  = modal[.Icon] as? UIImage
        if self.reuseIdentifier ==  CellIdentifiers.SearchRecomendationScreen.categoryIcon{
            imgView_Category.image = icon?.withRenderingMode(.alwaysTemplate)
        }else {
            imgView_Category.image = icon
        }
        imgView_Category.backgroundColor = modal[.Color] as? UIColor
    }
    
    /**
     This method use to render cell on search screen
     param:  title (string) state(Bool)
     */
    func configureCellName(title:String)  {
        //populate data on view
        lbl_Category.text =  title
        isSearch = true // this will indicate to change background color of selection
    }
    
    func configureCategory(modal:[CategoryEnum:Any]){
        
        //populate data on view
        lbl_Category.text =  modal[.Title] as? String
        let icon  = modal[.Icon] as? UIImage
            imgView_Category.image = icon
        self.view_Background?.backgroundColor = modal[.Color] as? UIColor
    }
    
}
