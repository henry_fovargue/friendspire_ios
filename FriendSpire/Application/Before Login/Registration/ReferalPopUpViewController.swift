//
//  ReferalPopUpViewController.swift
//  FriendSpire
//
//  Created by abhishek on 31/10/18.
//  Copyright © 2018 openkey. All rights reserved.
//

import UIKit

//FIXME: LatestCR

class ReferalPopUpViewController: DIBaseController ,UITextFieldDelegate{
    
    @IBOutlet weak var txtReferalCode: UITextField!    
    @IBOutlet weak var btnSubmit: UIButton!
    @IBOutlet weak var imgViewValid: UIImageView!
    
    fileprivate var referalValidated = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        txtReferalCode.delegate = self
        btnSubmit.isUserInteractionEnabled = false
        btnSubmit.alpha = 0.5
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func submitButtonTapped(_ sender: UIButton) {
        linkReferralToProfile()
        
    }
    
    @IBAction func skipButtonTapped(_ sender: UIButton) {
//        AppDelegate.shared().navigateToFriendsTab()
        showTutorialPopup()
    }
    
    //MARK:- Custom Methods
    
    func showTutorialPopup(){
        let tutorial : SlidingTutorialViewController =  UIStoryboard.init(storyboard: .tutorial).initVC()
        tutorial.modalTransitionStyle = .crossDissolve
        tutorial.modalPresentationStyle = .overCurrentContext
        tutorial.delegate = self
        tutorial.arrImages = [[KeyTutorial.image: #imageLiteral(resourceName: "slide1"),KeyTutorial.title:AppMessages.TutorialMessage.one]]
        self.present(tutorial, animated: true, completion: nil)
    }
    
    //MARK:- Textfield delegate
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        let maxLength = Constant.Validations.limitReferal
        let currentString: NSString = textField.text! as NSString
        let newString: NSString =
            currentString.replacingCharacters(in: range, with: string) as NSString
        
        if newString.length < maxLength{
            imgViewValid.alpha = 0.5
            btnSubmit.isUserInteractionEnabled = false
            btnSubmit.alpha = 0.5
             imgViewValid.image = nil
//            imgViewValid.image = newString.length != 0 ? #imageLiteral(resourceName: "cancel") : nil
        }else{
//            imgViewValid.image = #imageLiteral(resourceName: "checked")
            imgViewValid.alpha = 1.0
            btnSubmit.isUserInteractionEnabled = true
            btnSubmit.alpha = 1.0
            applyReferal(str: newString)
        }
        
        return newString.length <= maxLength
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        
        linkReferralToProfile()
        return true
    }
    
    fileprivate func isFormValid() -> Bool {
        var message : String?
         if (txtReferalCode.text?.isBlank == false && referalValidated == false){
            message = AppMessages.Password.invalidReferal
        }
        if message != nil {
            self.showAlert( message: message)
            return false
        }
        return true
    }
    
    
    //MARK: API Integration
    
    fileprivate func applyReferal(str:NSString){
        if !Utility.isConnectedToInternet {
            self.showAlert( message: AppMessages.ErrorAlerts.noInternet)
            return
        }
        self.showLoader()
        DIWebLayerUserAPI().applyReferal(parameters: ["code":str], success: { [weak self](response) in
            self?.hideLoader()
            self?.imgViewValid.image = #imageLiteral(resourceName: "checked")
            self?.referalValidated = true
            DILog.print(items: response)
            })
        {[weak self] in
            self?.hideLoader()
            self?.referalValidated = false
            self?.imgViewValid.image = #imageLiteral(resourceName: "cancel")
            self?.showAlert( message: $0.message, okayTitle:AppMessages.AlertTitles.Ok)
        }
    }
    
    fileprivate func linkReferralToProfile(){
        if isFormValid(){
        if !Utility.isConnectedToInternet {
            self.showAlert( message: AppMessages.ErrorAlerts.noInternet)
            return
        }
        self.showLoader()
        DIWebLayerUserAPI().linkReferralToProfile(parameters: ["code":txtReferalCode.text ?? ""], success: { [weak self](response) in
            self?.hideLoader()
//            AppDelegate.shared().navigateToFriendsTab()
            self?.showTutorialPopup()
            })
        {[weak self] in
            self?.hideLoader()
            self?.showAlert( message: $0.message, okayTitle:AppMessages.AlertTitles.Ok)
        }
        }
    }
}

extension ReferalPopUpViewController: SlidingTutorialDelegate{
    
    func finishWithTutorial(sender: Bool) {
        if sender == false{
            AppDelegate.shared().navigateToFriendsTab()
        }else{
            AppDelegate.shared().navigateToDashboard()
        }
    }
}
