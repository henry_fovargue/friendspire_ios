//
//  RegistrationViewController.swift
//  BaseProject
//
//  Created by Navpreet Gogana on 16/03/17.
//  Copyright © 2017 openkey. All rights reserved.
//

import UIKit
import IQKeyboardManagerSwift

//FIXME: LatestCR
class RegistrationController: DIBaseController {
    
    @IBOutlet weak var txt_FirstName: UITextField!
    @IBOutlet weak var txt_LastName: UITextField!
    @IBOutlet weak var txt_EmailAddress: UITextField!
    @IBOutlet weak var txt_Password: UITextField!
    @IBOutlet weak var txt_Confirm_Password: UITextField!
    @IBOutlet weak var btn_Submit: UIButton!
    @IBOutlet weak var txt_Referral: UITextField!
    @IBOutlet weak var imgViewValid: UIImageView!
    
    var user: User!
    var photoManager:PhotoManager!
    var isImageChanged = false
    fileprivate var referalValidated = false
    //MARK:- ViewController Life Cycle
    override func viewDidLoad() {
        super.viewDidLoadWithKeyboardManager(viewController: self)
        
        txt_Password.delegate = self
        txt_EmailAddress.delegate = self
        txt_Password.delegate = self
        txt_Confirm_Password.delegate = self
        txt_LastName.delegate = self
        txt_FirstName.delegate = self
        txt_Referral.delegate = self
        
        // Do any additional setup after loading the view.
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //MARK:- IBActions
    
    @IBAction func backButtonTapped(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
    
    @IBAction func submitButtonTapped(_ sender: UIButton) {
        self.submitAction()
    }
    
    func submitAction() {
        if isFormValid() {
            self.view.endEditing(true)
            DILog.print(items: "Now you can proceed with signup api")
            self.proceedWithRegistration()
        }
    }
 
    //MARK:- Private Methods
    fileprivate func isFormValid() -> Bool {
        var message : String?
        if (txt_FirstName.text?.isBlank)! {
            message = AppMessages.UserName.emptyNameSignup
        }
        else if !(txt_FirstName.text?.isValidInRange(minLength: 0,maxLength: Constant.Validations.limitName))! {
            message = AppMessages.UserName.maxLengthFirstname
        }
        else if (txt_LastName.text?.isBlank)! {
            message = AppMessages.UserName.emptyLNameSignup
        }
        else if !(txt_LastName.text?.isValidInRange(minLength: 0, maxLength: Constant.Validations.limitName))! {
            message = AppMessages.UserName.maxLengthLastname
        }
        else if (txt_EmailAddress.text?.isBlank)! {
            message = AppMessages.Email.emptyEmailSignup
        }
        else if !(txt_EmailAddress.text?.trim.isValidEmail)! {
            message = AppMessages.Email.invalidEmailSignup
        }
        else if (txt_Password.text?.isBlank)! && !(btn_Submit.titleLabel!.text!.contains(AppMessages.ButtonTitle.update)) {
            message = AppMessages.Password.emptyPasswordSignup
        }
        else if !(txt_Password.text?.isValidPassword)! && !(txt_Password.text?.isBlank)! {
            message = AppMessages.Password.invalidPassword
        }
        else if (txt_Confirm_Password.text?.isBlank)!{
            message = AppMessages.Password.emptyConfirmPassword
        }else if (txt_Confirm_Password.text != txt_Password.text){
            message = AppMessages.Password.passwordNotMatch
        }
//        else if (txt_Referral.text?.isBlank == false && referalValidated == false){
//            message = AppMessages.Password.invalidReferal
//        }
        
        if message != nil {
            self.showAlert( message: message)
            return false
        }
        return true
    }
    
    func getRegistrationDict () -> [String: Any]? {
        let registrationDict = User()
        registrationDict.firstName = txt_FirstName.text ?? ""
        registrationDict.lastName = txt_LastName.text ?? ""
        registrationDict.email = txt_EmailAddress.text?.trim ?? ""
        registrationDict.password = txt_Password.text ?? ""
        registrationDict.confirmPassword = txt_Confirm_Password.text ?? ""
//        registrationDict.referralCode = txt_Referral.text
        registrationDict.snsType = 1
        
        return registrationDict.getDictionary()
    }
    
    fileprivate func proceedWithRegistration(){
        if !Utility.isConnectedToInternet {
            self.showAlert( message: AppMessages.ErrorAlerts.noInternet)
            return
        }
        self.showLoader()
        DIWebLayerUserAPI().registation(parameters: self.getRegistrationDict(), success: { (response) in
            self.hideLoader()
            DILog.print(items: response)
            
            self.showAlert(withTitle: appName.capitalized, message:  response["message"] as? String ?? "", okayTitle: "Ok", cancelTitle: nil, okCall: {
                if  2 ==  response["status"] as? Int ?? 0{
                    self.performSegue(withIdentifier: SegueIdentifiers.RegistrationScreen.otp, sender: nil)
                }
            }, cancelCall: {
                
            })
        })
        {
            self.hideLoader()
            self.showAlert( message: $0.message, okayTitle:AppMessages.AlertTitles.Ok)
            
        }
    }
    
    
//    fileprivate func applyReferal(str:NSString){
//        if !Utility.isConnectedToInternet {
//            self.showAlert( message: AppMessages.ErrorAlerts.noInternet)
//            return
//        }
//        self.showLoader()
//        DIWebLayerUserAPI().applyReferal(parameters: ["code":str], success: { [weak self](response) in
//            self?.hideLoader()
////            if let status = response["status"] as? Int{
//
////                if status == 1{
//                    self?.imgViewValid.image = #imageLiteral(resourceName: "checked")
//                    self?.referalValidated = true
////                }else{
////                    self?.referalValidated = false
////                    self?.imgViewValid.image = #imageLiteral(resourceName: "cancel")
////                    self?.showAlert( message: response["message"] as? String ?? "Invalid referral", okayTitle:AppMessages.AlertTitles.Ok)
////                }
////            }
//            DILog.print(items: response)
//        })
//        {[weak self] in
//            self?.hideLoader()
//            self?.referalValidated = false
//            self?.imgViewValid.image = #imageLiteral(resourceName: "cancel")
//            self?.showAlert( message: $0.message, okayTitle:AppMessages.AlertTitles.Ok)
//        }
//    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == SegueIdentifiers.RegistrationScreen.otp{
            let controller = segue.destination as! OtpVerifyViewController
            controller.strEmail = txt_EmailAddress.text ?? ""
            controller.isSignupProcess = true
        }
    }
}

// MARK: - Textfield Delegation
extension RegistrationController: UITextFieldDelegate {
    
    
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if textField == txt_Password {
            if string == " " {
                return false
            }
        }
        
        if textField == txt_EmailAddress{
            let maxLength = Constant.Validations.limitEmail-1
            let currentString: NSString = textField.text! as NSString
            let newString: NSString =
                currentString.replacingCharacters(in: range, with: string) as NSString
            return newString.length <= maxLength
        }
        else if textField == txt_FirstName || textField == txt_LastName{
            
            let cs = NSCharacterSet(charactersIn: ACCEPTABLE_CHARACTERS).inverted
            let filtered = string.components(separatedBy: cs).joined(separator: "")
            
            let maxLength = Constant.Validations.limitName-1
            let currentString: NSString = textField.text! as NSString
            let newString: NSString =
                currentString.replacingCharacters(in: range, with: string) as NSString
            return  (newString.length <= maxLength && (string == filtered))
        }
        else if textField == txt_Password || textField == txt_Confirm_Password {
            let maxLength = Constant.Validations.limitPass-1
            let currentString: NSString = textField.text! as NSString
            let newString: NSString =
                currentString.replacingCharacters(in: range, with: string) as NSString
            return newString.length <= maxLength
        }
//        else if textField == txt_Referral{
//            let maxLength = Constant.Validations.limitReferal
//            let currentString: NSString = textField.text! as NSString
//            let newString: NSString =
//                currentString.replacingCharacters(in: range, with: string) as NSString
//
//            if newString.length < maxLength{
//                imgViewValid.alpha = 0.5
//                imgViewValid.image = nil
////                imgViewValid.image = newString.length != 0 ? #imageLiteral(resourceName: "cancel") : nil
//            }else{
////                imgViewValid.image = #imageLiteral(resourceName: "checked")
//                imgViewValid.alpha = 1.0
////                applyReferal(str: newString)
//            }
//            return newString.length <= maxLength
//        }
        
        return true
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        switch textField {
        case txt_FirstName:
            txt_LastName.becomeFirstResponder()
        case txt_LastName:
            txt_EmailAddress.becomeFirstResponder()
        case txt_EmailAddress:
            txt_Password.becomeFirstResponder()
        case txt_Password:
            txt_Confirm_Password.becomeFirstResponder()
        case txt_Confirm_Password:
//            txt_Referral.resignFirstResponder()
//        case txt_Referral:
//            txt_Referral.resignFirstResponder()
            self.submitAction()
        default:
            break
        }
        return true
    }
}

