//
//  OtpVerifyViewController.swift
//  FriendSpire
//
//  Created by Mac Test on 13/07/18.
//  Copyright © 2018 openkey. All rights reserved.
//

import UIKit


class OtpVerifyViewController: DIBaseController {
    
    var strEmail : String?
    var isSignupProcess :Bool?
    
    //IBOutlets
    @IBOutlet weak var txt_Otp: UITextField!
    @IBOutlet weak var btn_Resend: UIButton!
    @IBOutlet weak var lbl_Time: UILabel!
    
    private var timer: Timer?
    var count = 30
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        startTimer()
        txt_Otp.delegate = self
        txt_Otp.keyboardType = .numberPad
        txt_Otp.canPerformAction(#selector(UIResponderStandardEditActions.paste(_:)), withSender: Any)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //MARK:- IBAction
    
    @IBAction func backButtonTapped(_ sender: UIButton) {
        stopTimer()
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func resendButton_Tapped(_ sender: UIButton) {
        
        self.resendOTP()
    }
    
    @IBAction func submitButton_Tapped(_ sender: UIButton) {
        
        
        var message : String?
        if (txt_Otp.text?.isBlank)! {
            message = AppMessages.userAttibute.emptyOtp
        }
        
        
        if message != nil {
            self.showAlert( message: message)
            return
        }
        
        if self.isSignupProcess == true{
            otpVerificationSignUp()
        }else{
            otpVerificationForgot()
        }
    }
    
    //MARK:- Custom Methods
    
    func showTutorialPopup(){
        let tutorial : SlidingTutorialViewController =  UIStoryboard.init(storyboard: .tutorial).initVC()
        tutorial.modalTransitionStyle = .crossDissolve
        tutorial.modalPresentationStyle = .overCurrentContext
        tutorial.delegate = self
        tutorial.arrImages = [[KeyTutorial.image: #imageLiteral(resourceName: "slide1"),KeyTutorial.title:AppMessages.TutorialMessage.one]]
        AppDelegate.shared().window?.rootViewController?.present(tutorial, animated: true, completion: nil)
    }
    
    
    //MARK:- Timer Methods
    func startTimer() {
        guard timer == nil else { return }
        
        btn_Resend.isEnabled = false
        timer = Timer.scheduledTimer(timeInterval: 1, target: self, selector: #selector(timerOngoing), userInfo: nil, repeats: true)
    }
    
    func stopTimer() {
        guard timer != nil else { return }
        timer?.invalidate()
        timer = nil
        btn_Resend.isEnabled = true
    }
    
    
    @objc func timerOngoing() {
        
        if(count == 0) {
            stopTimer()
            return
        }
        count = count-1
        lbl_Time.text = "Resend OTP after: 0." + (count < 10 ? "0\(count)s" : "\(count)s")
    }
    
    
    fileprivate func getDictonary() -> [String:Any]{
        
        var modal =  Otp()
        modal.otpCode = txt_Otp.text ?? ""
        modal.email = strEmail ?? ""
        
        
        return modal.getDictonary()
    }
    
    // MARK: - Navigation
    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
        
        if segue.identifier == SegueIdentifiers.OtpScreen.resetPassword{
            let controller = segue.destination as! ResetPasswordViewController
            controller.strEmail = self.strEmail
            controller.strToken = sender as? String ?? ""
        }
    }
}
//MARK:-
extension OtpVerifyViewController{
    
    //MARK:- API Calling
    
    fileprivate func otpVerificationSignUp(){
        if !Utility.isConnectedToInternet {
            self.showAlert( message: AppMessages.ErrorAlerts.noInternet)
            return
        }
        self.showLoader()
        DIWebLayerUserAPI().regiseterOtp(parameters: self.getDictonary(), success: { (user) in
            User.sharedInstance.setHomeCity()
            UserManager.saveCurrentUser(user: user)
            DILog.print(items: User.sharedInstance.firstName!
            )
//            DILog.print(items: "Logged in user: \(User.sharedInstance.firstName!) \(User.sharedInstance.lastName!)")
            self.hideLoader()
            
            //            self.performSegue(withIdentifier: SegueIdentifiers.OtpScreen.signupCompleted, sender: nil)
            if LoginViewController.isComingFromDeepLink {
                LoginViewController.isComingFromDeepLink = false
                if let link = LoginViewController.deepLinkUrl {
                    appDelegate.redirectUserToScreenOnLinkClick(url: link)
                    return
                }
            }
            if LoginViewController.isComingNotification {
                LoginViewController.isComingNotification = false
                appDelegate.redirectUserToScreenOnNotificationClick(responseData: LoginViewController.notificationData)
                return
            }
            //            AppDelegate.shared().navigateToDashboard()
//            AppDelegate.shared().navigateToFriendsTab()
            self.showTutorialPopup()
            
            
        }) { (error) in
            self.hideLoader()
            self.showAlert( message: error.message, okayTitle:AppMessages.AlertTitles.Ok)
            DILog.print(items:error)
        }
    }
    
    fileprivate func otpVerificationForgot(){
        if !Utility.isConnectedToInternet {
            self.showAlert( message: AppMessages.ErrorAlerts.noInternet)
            return
        }
        self.showLoader()
        DIWebLayerUserAPI().forgotOtp(parameters: self.getDictonary(), success: { (response) in
            self.hideLoader()
            DILog.print(items:response)
            if let token = response["reset_token"] as? String{
                self.performSegue(withIdentifier: SegueIdentifiers.OtpScreen.resetPassword, sender: token)
            }
        }) { (error) in
            self.hideLoader()
            self.showAlert( message: error.message, okayTitle:AppMessages.AlertTitles.Ok)
            DILog.print(items:error)
        }
    }
    
    func resendOTP(){
        if !Utility.isConnectedToInternet {
            self.showAlert( message: AppMessages.ErrorAlerts.noInternet)
            return
        }
        count = 30
        startTimer()
        
        if self.isSignupProcess == true{  // for signup
            var modal =  ResendOtp()
            modal.type = 1
            modal.email = strEmail ?? ""
            
            showLoader()
            DIWebLayerUserAPI().resendOtp(parameters: modal.getDictonary(), success: { (responseMsg) in
                self.hideLoader()
                self.showAlert( message: responseMsg, okayTitle:AppMessages.AlertTitles.Ok)
                
            }) { (error) in
                self.hideLoader()
                self.showAlert( message: error.message, okayTitle:AppMessages.AlertTitles.Ok)
                DILog.print(items:error)
            }
        }else{   // for forgot
            let forgotKey = User()
            forgotKey.email = strEmail ?? ""
            
            if !Utility.isConnectedToInternet {
                self.showAlert( message: AppMessages.ErrorAlerts.noInternet)
                return
            }
            showLoader()
            DIWebLayerUserAPI().forgotPassword(parameters: forgotKey.getDictionary(), success: { (responseMsg) in
                DILog.print(items: responseMsg)
                self.hideLoader()
                
                self.showAlert(message: responseMsg,okCall: {
                    
                })
                
            }, failure: { (error) in
                DILog.print(items: error)
                self.showAlert(message: error.message,okCall: {
                    
                })
                self.hideLoader()
            })
            
        }
    }
}

// MARK: - Textfield Delegation
extension OtpVerifyViewController: UITextFieldDelegate {
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        if textField == txt_Otp{
            let maxLength = Constant.Validations.limitOtp-1
            let currentString: NSString = textField.text! as NSString
            let newString: NSString =
                currentString.replacingCharacters(in: range, with: string) as NSString
            return newString.length <= maxLength
        }
        
        return true
    }
    
}

extension OtpVerifyViewController: SlidingTutorialDelegate{
   
    func finishWithTutorial(sender: Bool) {
        if sender == false{
            AppDelegate.shared().navigateToFriendsTab()
        }else{
            AppDelegate.shared().navigateToDashboard()
        }
    }
}
