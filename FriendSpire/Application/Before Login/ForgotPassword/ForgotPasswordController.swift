//
//  ForgotPassword.swift
//  BaseProject
//
//  Created by TpSingh on 17/03/17.
//  Copyright © 2017 openkey. All rights reserved.
//

import UIKit

class ForgotPasswordController: DIBaseController {
    
    //MARK:IBOutlets
    @IBOutlet weak var txt_Email: UITextField!
    @IBOutlet weak var submitButton: UIButton!
    
    
    //MARK:- ViewController Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        txt_Email.delegate = self
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
         self.navigationController?.interactivePopGestureRecognizer?.isEnabled = true
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //MARK:- IBActions
    
    @IBAction func backButtonTapped(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func submitButtonTapped(_ sender: UIButton) {
        if isEmailValid(){
            let forgotKey = User()
            forgotKey.email = txt_Email.text ?? ""
            DILog.print(items: forgotKey)
            forgotPasswordApi(modal: forgotKey)
        }
    }

    
    //MARK:- Private Methods
    fileprivate func isEmailValid() -> Bool {
        var message : String?
        
        if (txt_Email.text?.isBlank)! {
            message = AppMessages.Email.emptyEmailForgot
        }else if !(txt_Email.text?.trim.isValidEmail)! {
            message = AppMessages.Email.invalidEmailForgot
        }
        
        if message != nil {
            self.showAlert( message: message)
            return false
        }
        
        
        return true
    }
    
    // MARK: - Navigation
    
    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        let controller = segue.destination as! OtpVerifyViewController
        controller.strEmail = self.txt_Email.text?.trim
    }
}

extension ForgotPasswordController :UITextFieldDelegate {

    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        if textField == txt_Email{
            let maxLength = Constant.Validations.limitEmail-1
            let currentString: NSString = textField.text! as NSString
            let newString: NSString =
                currentString.replacingCharacters(in: range, with: string) as NSString
            return newString.length <= maxLength
        }
        
        return true
    }
}

  // MARK: -
extension ForgotPasswordController {
      // MARK: - Api Methods
    func forgotPasswordApi(modal : User){
        if !Utility.isConnectedToInternet {
            self.hideLoader()
            self.showAlert( message: AppMessages.ErrorAlerts.noInternet)
            return
        }
        showLoader()
        DIWebLayerUserAPI().forgotPassword(parameters: modal.getDictionary(), success: { (responseMsg) in
            DILog.print(items: responseMsg)
            self.hideLoader()
            
            self.showAlert(message: responseMsg,okCall: {
                self.performSegue(withIdentifier: SegueIdentifiers.ForgotPasswordScreen.verification, sender: nil)
            })
            
        }, failure: { (error) in
            DILog.print(items: error)
            self.showAlert(message: error.message,okCall: {
                _ = self.navigationController?.popToRootViewController(animated: true)
            })
            self.hideLoader()
        })
        
    }
}
