//
//  ResetPasswordViewController.swift
//  FriendSpire
//
//  Created by Mac Test on 18/07/18.
//  Copyright © 2018 openkey. All rights reserved.
//

import UIKit

class ResetPasswordViewController: DIBaseController {
    
    var strEmail : String?
    var strToken : String?
    
    
    @IBOutlet weak var txt_Pass: UITextField!
    @IBOutlet weak var txt_ConfirmPass: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        txt_Pass.delegate = self
        txt_ConfirmPass.delegate = self
        
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        self.navigationController?.interactivePopGestureRecognizer?.isEnabled = false
    }

  
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //MARK:- IBAction
    
    @IBAction func backButtonTapped(_ sender: UIButton) {
        
        for controller in  self.navigationController?.viewControllers ?? []{
            if controller.isKind(of: ForgotPasswordController.self){
                self.navigationController?.popToViewController(controller, animated: true)
            }
        }
        

    }
    
    
    @IBAction func submitButton_Tapped(_ sender: UIButton) {
        
        if isFormValid() == true {
            
            var reset = ResetPassword()
            reset.email = strEmail ?? ""
            reset.resetToken = strToken ?? ""
            reset.password = txt_Pass.text ?? ""
            
            resetPasswordApi(modal: reset)
        }
    }
    
    //MARK:- Private Methods
    fileprivate func isFormValid() -> Bool {
        
        
        var message : String?
        
         if (txt_Pass.text?.isBlank)! {
            message = AppMessages.Password.emptyPasswordReset
        }
        else if !(txt_Pass.text?.isValidPassword)! {
            message = AppMessages.Password.invalidPassword
        }
        else if (txt_ConfirmPass.text?.isBlank)!{
            message = AppMessages.Password.emptyConfirmPasswordReset
        }else if (txt_ConfirmPass.text != txt_Pass.text){
            message = AppMessages.Password.passwordNotMatch
        }
        if message != nil {
            self.showAlert( message: message)
            return false
        }
        return true
    }
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
}
// MARK: -
extension ResetPasswordViewController {
    // MARK: - Api Methods
    
    func resetPasswordApi(modal : ResetPassword) {
        if !Utility.isConnectedToInternet {
            self.showAlert( message: AppMessages.ErrorAlerts.noInternet)
            return
        }
        showLoader()
        DIWebLayerUserAPI().resetPassword(parameters: modal.getDictonary(), success: { (responseMsg) in
            DILog.print(items: responseMsg)
            self.hideLoader()
            
            self.showAlert(message: responseMsg,okCall: {
                _ = self.navigationController?.popToRootViewController(animated: true)
            })
            
        }, failure: { (error) in
            DILog.print(items: error)
            self.showAlert(message: error.message,okCall: {
                //                _ = self.navigationController?.popToRootViewController(animated: true)
            })
            self.hideLoader()
        })
        
    }
}


// MARK: - Textfield Delegation
extension ResetPasswordViewController: UITextFieldDelegate {
    
    
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if textField == txt_Pass {
            if string == " " {
                return false
            }
        }
        
        if textField == txt_Pass || textField == txt_ConfirmPass {
            let maxLength = Constant.Validations.limitPass-1
            let currentString: NSString = textField.text! as NSString
            let newString: NSString =
                currentString.replacingCharacters(in: range, with: string) as NSString
            return newString.length <= maxLength
        }
        
        return true
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        switch textField {
        case txt_Pass:
            txt_ConfirmPass.becomeFirstResponder()
        case txt_ConfirmPass:
            txt_ConfirmPass.resignFirstResponder()
        default:
            break
        }
        return true
    }
}
