//
//  loginViewController.swift
//  BaseProject
//
//  Created by TpSingh on 08/03/17.
//  Copyright © 2017 Debut Infotech. All rights reserved.
//

import UIKit
import IQKeyboardManagerSwift

import FBSDKCoreKit
import FBSDKLoginKit
import FBSDKShareKit


class LoginViewController: DIBaseController  {
    
    //MARK:- IBOutlets
    @IBOutlet var txt_Email:UITextField!
    @IBOutlet var txt_Pass:UITextField!
    @IBOutlet weak var btn_SignIn: UIButton!
    @IBOutlet weak var btn_RememberMe: UIButton!
    
    ///MARK:-Variable.
    static var isComingFromDeepLink:Bool = false
    static var deepLinkUrl:URL?
    static var isComingNotification:Bool = false
    static var notificationData = [AnyHashable:Any]()
    var emailToSend : String?
    var snsIdToSend : String?
    //MARK:- Life Cycle
    override func viewDidLoad() {
        
        super.viewDidLoadWithKeyboardManager(viewController: self)
        //custom intialization for google
        
        txt_Email.delegate = self
        txt_Pass.delegate = self
        
        

    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        
          self.navigationController?.interactivePopGestureRecognizer?.isEnabled = true
        
        // check for remember me check
        guard let email = Defaults.shared.get(forKey: .rememberMe) else {
            return
        }
        txt_Email.text = email as? String
        btn_RememberMe.isSelected = true
        
    }

    /**
     Validate Form Proceed with API Call
     */
    
    func loginButtonAction() {
        if isLoginFormValid() {
            self.view.endEditing(true)
            
            let loginDic = User()
            loginDic.email = self.txt_Email.text?.trim ?? ""
            loginDic.password = self.txt_Pass.text ?? ""
            loginDic.snsType = 1
            DILog.print(items: "Now you can proceed with login api", loginDic.getDictionary())
            
            if !Utility.isConnectedToInternet {
                self.showAlert( message: AppMessages.ErrorAlerts.noInternet)
                return
            }
            callLoginAPI(loginDic: loginDic.getDictionary(),success: { (finish) in
                if finish {
                    if LoginViewController.isComingFromDeepLink {
                         LoginViewController.isComingFromDeepLink = false
                        if let link = LoginViewController.deepLinkUrl {
                        appDelegate.redirectUserToScreenOnLinkClick(url: link)
                        return
                        }
                    }
                    if LoginViewController.isComingNotification {
                        LoginViewController.isComingNotification = false
                            appDelegate.redirectUserToScreenOnNotificationClick(responseData: LoginViewController.notificationData)
                            return
                    }
                    AppDelegate.shared().navigateToDashboard()
//                    AppDelegate.shared().navigateToFriendsTab()
                }
                else {
                    DILog.print(items: "failure")
                }
            })
        }
    }
    
    //MARK:IBActions
    @IBAction func loginButtonTapped(_ sender: UIButton) {
        self.loginButtonAction()
    }
    
    
    @IBAction func rememberMeTapped(_ sender: UIButton) {
        btn_RememberMe.isSelected = !btn_RememberMe.isSelected
    }
    
    
    @IBAction func facebookLoginTapped(_ sender: UIButton) {
        if !Utility.isConnectedToInternet {
            self.showAlert( message: AppMessages.ErrorAlerts.noInternet)
            return
        }
          btn_RememberMe.isSelected = false
         Defaults.shared.remove(.rememberMe)
        DispatchQueue.main.async {
           
        FacebookManager.shared.login([.email, .publicProfile, .birthday, .friends], success: { (user) in
            
            let login = User() // this will use for is Social Profile exist
            let userNew = User() //keep records if new user required
            
            // modal for user exitence
            login.snsType = user.snsType
            login.snsID = user.snsID
            
            // modal for new user
            userNew.email = user.email
            userNew.snsType = user.snsType
            userNew.snsID = user.snsID
            userNew.firstName = user.firstName
            userNew.lastName = user.lastName
            userNew.profilePicUrl = user.profilePicUrl
            //            if userNew.email != ""{  // yes we have email from FB
            self.callSocialLoginAPI(loginDic: login.getDictionary(), success: { [weak self](finish) in
                self?.hideLoader()
                if finish { // yes previous user

                    AppDelegate.shared().navigateToDashboard()
//                    AppDelegate.shared().navigateToFriendsTab()

                }
                else {  // update new user detail in backend
                    DILog.print(items: "New User Login needed")
                    if userNew.email != ""{  // yes we have email from FB
                        userNew.isEmailVerified = 1
                        self?.callLoginAPI(loginDic: userNew.getDictionary(), success: { (finish) in
                            if finish {
//                                AppDelegate.shared().navigateToDashboard()
//                                AppDelegate.shared().navigateToFriendsTab()
                                //change to show option for referal popup
//                                self?.showReferalPopUp()
                                self?.showTutorialPopup()

                            }
                            else {
                                DILog.print(items: "failure")
                            }
                        })
                    }else{   // No email found proceed for signup
                        self?.hideLoader()
                        let message = AppMessages.userAttibute.emailNotFound
                        self?.showAlert(withTitle: AppMessages.AlertTitles.Alert, message: message, okayTitle: AppMessages.AlertTitles.Ok,  okCall: {
                            userNew.isEmailVerified = 0
                            self?.showALertEmail(user: userNew)
                        })
                    }
                }
            })
            
        }, failure: { (error) in
            self.hideLoader()
            self.showAlert(message:error.message)
        }, onController: self)
    }
    }
    
    //MARK:- .......... Private method
    /// This Method is used to call login api success: @escaping (_ user: User) -> (),
    fileprivate func callLoginAPI(loginDic :Parameters,success: @escaping(_ finished: Bool)-> ()) {
        if !Utility.isConnectedToInternet {
            self.showAlert( message: AppMessages.ErrorAlerts.noInternet)
            return
        }
        showLoader()
        let webLayerLogin = DIWebLayerUserAPI()
        webLayerLogin.login(parameters: loginDic, success: { (response) in
            
            if self.btn_RememberMe.isSelected {
                Defaults.shared.set(value: "\(self.txt_Email.text ?? "")", forKey: .rememberMe)
            }else{
                Defaults.shared.remove(.rememberMe)
            }
            
            
            self.hideLoader()
            if  2 ==  response["status"] as? Int ?? 0{
                self.performSegue(withIdentifier: SegueIdentifiers.LoginScreen.segueOtpTime, sender: nil)
                
                success(false)
                return
            }else if 1 ==  response["status"] as? Int ?? 0{
                if let responseDict = response["data"] as? NSDictionary {
                    let user = User(responseDict as! Parameters)
                    user.oauthToken = responseDict["token"] as? String ?? ""
                     User.sharedInstance.setHomeCity()
                    UserManager.saveCurrentUser(user: user)                    
                    success(true)
                    DILog.print(items: User.sharedInstance.firstName ?? "")
                    DILog.print(items: "Logged in user: \(User.sharedInstance.firstName ?? "") \(User.sharedInstance.lastName ?? "")")
                    return
                }
            }

        }) {
            self.hideLoader()
            self.showAlert(withError: $0)
            success(false)
        }
    }
    
    /**
     Check if a user already signup using Social media
     Parameters: loginDic  : [String:Any] use to send over network , success to return the status of processing done on data
     */
    
    fileprivate func callSocialLoginAPI(loginDic :Parameters,success: @escaping(_ finished: Bool)-> ()) {
        if !Utility.isConnectedToInternet {
            self.showAlert( message: AppMessages.ErrorAlerts.noInternet)
            return
        }
        showLoader()
        let webLayerLogin = DIWebLayerUserAPI()
        webLayerLogin.socialLogin(parameters: loginDic, success: { (user) in
            self.hideLoader()
            
            /* user modal recieved (Old user)*/
            if user.0 != nil {
                UserManager.saveCurrentUser(user: user.0!)
//                DILog.print(items: User.sharedInstance.firstName ?? "")
                DILog.print(items: "Logged in user: \(User.sharedInstance.firstName ?? "") \(User.sharedInstance.lastName ?? "")")
                self.hideLoader()
                
                success(true)
            }
            /* Message for new registration require*/
            if user.1 != nil{
                success(false)
            }
            
        }) {
            self.hideLoader()
            self.showAlert(withError: $0)
            success(false)
        }
    }
    
    
    /**
     return true for valid data  otherwise false represent user enter something wrong
     */
    private func isLoginFormValid() -> Bool {
        var message:String?
        if (txt_Email.text?.isBlank)! {
            message = AppMessages.Email.emptyEmail
        }else if !((txt_Email.text?.trim.isValidEmail)!) {
            message = AppMessages.Email.invalidEmail
        }else if (txt_Pass.text?.isEmpty)! {
            message = AppMessages.Password.emptyPassword
        }
        if message != nil {
            self.showAlert( message: message)
            return false
        }
        return true
    }
    /**/
    func showALertEmail(user : User){
        //1. Create the alert controller.
        let alert = UIAlertController(title: AppMessages.AlertTitles.Alert, message: "Enter your email", preferredStyle: .alert)
        
        //2. Add the text field. You can configure it however you need.
        alert.addTextField { (textField) in
            textField.placeholder = "email@domain.com"
        }
        
        // 3. Grab the value from the text field, and handle it when the user clicks OK.
        alert.addAction(UIAlertAction(title: AppMessages.AlertTitles.Ok, style: .default, handler: { [weak alert] (_) in
            let textField = alert?.textFields![0] // Force unwrapping because we know it exists.
            user.isEmailVerified = 0
            user.email = textField?.text
            
            var message:String?
            if (textField?.text?.isBlank)! {
                message = AppMessages.Email.emptyEmail
            }else if !((textField?.text?.isValidEmail)!) {
                message = AppMessages.Email.invalidEmail
            }
            
            if message != nil {
                self.showAlert(withTitle: AppMessages.AlertTitles.Alert, message: message, okayTitle: AppMessages.AlertTitles.Ok,  okCall: {
                    
                    self.showALertEmail(user: user)
                })
                return
            }
            self.emailToSend = textField?.text
            self.snsIdToSend = user.snsID
            self.callLoginAPI(loginDic: user.getDictionary(), success: { [weak self](finish) in
                if finish {
//                    self.performSegue(withIdentifier: SegueIdentifiers.LoginScreen.segueDashboard, sender: nil)
                    
//                    AppDelegate.shared().navigateToDashboard()
//                    AppDelegate.shared().navigateToFriendsTab()
                    //change to show option for referal popup
//                    self?.showReferalPopUp()
                    self?.showTutorialPopup()
                }
                else {
                    DILog.print(items: "failure")
                }
            })
        }))
        
        alert.addAction(UIAlertAction.init(title: AppMessages.AlertTitles.Cancel, style: .default, handler: { (action) in
            
            
        }))
        // 4. Present the alert.
        self.present(alert, animated: true, completion: nil)
    }
    
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == SegueIdentifiers.LoginScreen.segueOtpTime{
            let controller = segue.destination as! OtpVerifyViewController
            controller.strEmail = emailToSend ?? ""
            controller.isSignupProcess = true
            
        }
    }
    
//    fileprivate func showReferalPopUp(){
//        let controller : ReferalPopUpViewController = UIStoryboard.init(storyboard: .registration).initVC()
//        controller.modalTransitionStyle = .crossDissolve
//        controller.modalPresentationStyle = .overCurrentContext
//        self.present(controller, animated: true, completion: nil)
//    }
    
    func showTutorialPopup(){
        let tutorial : SlidingTutorialViewController =  UIStoryboard.init(storyboard: .tutorial).initVC()
        tutorial.modalTransitionStyle = .crossDissolve
        tutorial.modalPresentationStyle = .overCurrentContext
        tutorial.delegate = self
        tutorial.arrImages = [[KeyTutorial.image: #imageLiteral(resourceName: "slide1"),KeyTutorial.title:AppMessages.TutorialMessage.one]]
        self.present(tutorial, animated: true, completion: nil)
    }
    
}

extension LoginViewController: SlidingTutorialDelegate{
    
    func finishWithTutorial(sender: Bool) {
        if sender == false{
            AppDelegate.shared().navigateToFriendsTab()
        }else{
            AppDelegate.shared().navigateToDashboard()
        }
    }
}


//MARK:LoginTextFieldExtension
extension LoginViewController: UITextFieldDelegate {
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if textField == txt_Pass {
            self.loginButtonAction()
        }
        if textField == txt_Email {
            txt_Pass.becomeFirstResponder()
        }
        return true
    }
    
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        if textField == txt_Email{
            let maxLength = Constant.Validations.limitEmail-1
            let currentString: NSString = textField.text! as NSString
            let newString: NSString =
                currentString.replacingCharacters(in: range, with: string) as NSString
            return newString.length <= maxLength
        }
        if textField == txt_Pass{
            let maxLength = Constant.Validations.limitPass-1
            let currentString: NSString = textField.text! as NSString
            let newString: NSString =
                currentString.replacingCharacters(in: range, with: string) as NSString
            return newString.length <= maxLength
        }
        
        return true
    }
}
