//
//  MovieDetailViewController.swift
//  FriendSpire
//
//  Created by Harpreet_kaur on 07/08/18.
//  Copyright © 2018 openkey. All rights reserved.
//

import UIKit
import AlamofireImage

class MovieDetailViewController: DIBaseController {
    
    
    //MARK:- Variables.
    var objMovieModelData = MovieModelData()
    var headings = ["Rated:","Writer:","Director:","Actors:","Plot:","Language:","Country:","Address:","Release Date:"]
    
    //MARK:-IBOutlets.
    @IBOutlet weak var genreLabel: UILabel!
    @IBOutlet weak var yearLabel: UILabel!
    @IBOutlet weak var timeLabel: UILabel!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var movieDetailTabelView: UITableView!
    @IBOutlet weak var friendsRatingImageView: UIImageView!
    @IBOutlet weak var profileImageview: UIImageView!
    @IBOutlet weak var friendsRatingLabel: UILabel!
    @IBOutlet weak var everyoneRatingLabel: UILabel!
    @IBOutlet weak var fourSquareRatingLabel: UILabel!
    @IBOutlet weak var loggedInUserProfileImageView: UIImageView!
    @IBOutlet weak var userRatingLabel: UILabel!
    @IBOutlet weak var everyOneRatingImageView: UIImageView!
    @IBOutlet weak var fourSquareRatingImageView: UIImageView!
    
    
    //MARK:-ViewLifeCycle.
    override func viewDidLoad() {
        super.viewDidLoad()
        getMovieDeatilsFromServer()
        movieDetailTabelView.register(UINib(nibName: Constant.Xib.reviewsTabelCellXib, bundle: nil), forCellReuseIdentifier: Constant.TableviewCellIdentifier.reviewsTabelCellXib)
        
    }
    
    //MARK:-IBActions.
    @IBAction func shareButtonAction(_ sender: UIButton) {
    }
    @IBAction func saveToLibraryAction(_ sender: UIButton) {
    }
    @IBAction func addRatingAction(_ sender: UIButton) {
    }
    @IBAction func viewAllButtonAction(_ sender: UIButton) {
        
    }
    
    //MARK:-Back Button Action.
    @IBAction func backButtonAction(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
    //MARK:-PrivateFunction.
    //MARK:-Get Data From Api.
    func getMovieDeatilsFromServer() {
        DIWebLayerCategoriesDetailApi().getCategoryDeatils(category: 3, postID: "5b6c12633f66357c7d9b82d8", success: { (data) in
            self.hideLoader()
            self.objMovieModelData = data
            self.setData()
        }) { (error) in
            self.hideLoader()
            self.showAlert(message:error.message)
        }
    }
    
    //MARK:-SetData to UIComponent.
    func setData() {
        userRatingLabel.text = objMovieModelData.userReview
        if let imageUrl = URL.init(string: objMovieModelData.profilePic ?? "") {
            loggedInUserProfileImageView.af_setImage(withURL: imageUrl, placeholderImage: #imageLiteral(resourceName: "user") , filter: nil, progress: nil, imageTransition: .crossDissolve(0.7), runImageTransitionIfCached: false, completion: nil)
        }
        if objMovieModelData.image.count >= 1 {
        if let imageUrl = URL.init(string: objMovieModelData.image[0] ) {
            profileImageview.af_setImage(withURL: imageUrl, placeholderImage: #imageLiteral(resourceName: "user") , filter: nil, progress: nil, imageTransition: .crossDissolve(0.7), runImageTransitionIfCached: false, completion: nil)
        }
        }
        titleLabel.text = objMovieModelData.title
        yearLabel.text = "\(objMovieModelData.year ?? 1998)"
        genreLabel.text = objMovieModelData.genre.joined(separator: ", ")
        timeLabel.text = Utility.minutesToHoursMinutes(minutes: objMovieModelData.pagesDuration)
        friendsRatingLabel.text = "\(objMovieModelData.friendsRating ?? 0.0)"
        everyoneRatingLabel.text = "\(objMovieModelData.friendspireRating ?? 0.0)"
        fourSquareRatingLabel.text = "\(objMovieModelData.rating ?? 0.0)"
        friendsRatingImageView.image = Utility.getEmojiAccordindToRating(rating: objMovieModelData.friendsRating)
        everyOneRatingImageView.image = Utility.getEmojiAccordindToRating(rating: objMovieModelData.friendspireRating)
        fourSquareRatingImageView.image = Utility.getEmojiAccordindToRating(rating: objMovieModelData.rating)
        movieDetailTabelView.reloadData()
    }
    
}




//MARK:-UITabelViewDelegate&UITabelViewDataSource.
extension MovieDetailViewController : UITableViewDelegate , UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return 2
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section == 0 {
            return objMovieModelData.reviews.count
        }else{
            return headings.count
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.section == 0 {
            guard let cell = movieDetailTabelView.dequeueReusableCell(withIdentifier: Constant.TableviewCellIdentifier.reviewsTabelCellXib, for: indexPath) as? ReviewsTabelCellXib else {
                return UITableViewCell()
            }
            cell.contentView.backgroundColor = .white
            cell.setData(reviewsData: objMovieModelData.reviews[indexPath.row])
            return cell
        }else{
            guard let cell = movieDetailTabelView.dequeueReusableCell(withIdentifier: Constant.TableviewCellIdentifier.movieTableCell, for: indexPath) as? MovieTableCell else {
                return UITableViewCell()
            }
            cell.itemLabel.text = headings[indexPath.row]
            cell.setData(index: indexPath.row, objMovieModelData: self.objMovieModelData)
            return cell
        }
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    
    
}

