//
//  MovieTableCell.swift
//  FriendSpire
//
//  Created by Harpreet_kaur on 14/08/18.
//  Copyright © 2018 openkey. All rights reserved.
//

import UIKit

class MovieTableCell: UITableViewCell {
    
    
    //MARK:-IBOutlets.
    @IBOutlet weak var itemLabel: UILabel!
    @IBOutlet weak var valueLabel: UILabel!
    
    
    //MARK:-UITabelViewCell Functions.
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    func setData(index:Int,objMovieModelData:MovieModelData) {
        switch index {
        case 0:
            valueLabel.text = objMovieModelData.rated
        case 1:
            valueLabel.text = objMovieModelData.author
        case 2:
            valueLabel.text =  objMovieModelData.director
        case 3:
            valueLabel.text = objMovieModelData.actors
        case 4:
            valueLabel.text = objMovieModelData.descriptionField
        case 5:
            valueLabel.text =  objMovieModelData.language.joined(separator: ", ")
        case 6:
            valueLabel.text = objMovieModelData.country.joined(separator: ", ")
        case 7:
            valueLabel.text = objMovieModelData.releaseDate
        default:
            break
        }
        
    }
    
    
}
