//
//  NotificationCell.swift
//  Noah
//
//  Created by Harpreet_kaur on 21/03/18.
//  Copyright © 2018 Harpreet. All rights reserved.
//

import UIKit

class NotificationCell: UITableViewCell {
    
    
    //MARK:-IBOutlets.
    @IBOutlet weak var userImageView: UIImageView!
    @IBOutlet weak var messageLabel: UILabel!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var constraintHeightName: NSLayoutConstraint!
    
    
    
    //MARK:-UITableView Functions.
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    
    //MARK:-Function to SetData.
    func setData(data:[Notifications],index:Int) {
        let modifiedFont = String(format:"<span style=\"font-family: '-apple-system', 'SFUIText-Regular'; font-size: \(messageLabel.font!.pointSize)\">%@</span>", data[index].message ?? "")
        messageLabel.attributedText = modifiedFont.html2String
        messageLabel.sizeToFit()
        if let imageUrl = URL.init(string: data[index].userDetail?.profilePic ?? "") {
            userImageView.af_setImage(withURL: imageUrl, placeholderImage: #imageLiteral(resourceName: "user") , filter: nil, progress: nil, imageTransition: .crossDissolve(0.7), runImageTransitionIfCached: false, completion: nil)
        }else{
           userImageView.image = #imageLiteral(resourceName: "user")
        }
//        nameLabel.isHidden = false
//        constraintHeightName.constant = 23.0
//        if data[index].type != 8 {
//        nameLabel.text = "\(data[index].userDetail?.firstName ?? "") \(data[index].userDetail?.lastName ?? "")"
//    //    messageLabel.text = data[index].message
//        }else{
        
//            constraintHeightName.constant = 0.0
//            nameLabel.isHidden = true
//        }
    }
}


