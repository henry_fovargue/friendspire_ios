//
//  NotificationsController.swift
//  Noah
//
//  Created by Harpreet_kaur on 21/03/18.
//  Copyright © 2018 Harpreet. All rights reserved.
//

import UIKit

class NotificationsController: DIBaseController {
    
    
    //MARK:-Variables.
    var defaultPage:Int = 1
    var pageNumber:Int = 1
    var currentAddress:Address?
    var notificationRefreshControl: UIRefreshControl!
    var notifications = [Notifications]()
    
    
    //MARK:-IBOutlets.
    @IBOutlet weak var notificationTableView: UITableView!
    
    
    //MARK:-ViewLifeCycle.
    //MARK:-ViewDidLoad.
    override func viewDidLoad() {
        super.viewDidLoad()
        addRefreshController()
        getInitialData()
        notificationTableView.tableFooterView = UIView()
    }
    
    //MARK:-ViewWillAppear.
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        self.navigationController?.isNavigationBarHidden = true
    }
    
    //MARK:-IBActions
    @IBAction func backButtonAction(_ sender: UIButton) {
         self.navigationController?.popViewController(animated: true)
    }
    
    //MARK:-PrivateFunction.
    //MARK:-Function to get Intial Notification Data.
    private func getInitialData(){
        showLoader()
        getNotifications()
    }

    //MARK:- AddRefreshController To TableView.
    private func addRefreshController() {
        notificationRefreshControl = UIRefreshControl()
        notificationRefreshControl.tintColor = .darkGray
        notificationRefreshControl.addTarget(self, action: #selector(refreshNews(sender:)) , for: .valueChanged)
        notificationTableView.addSubview(notificationRefreshControl)
    }
    
    //MARK:- Function To Refresh News Tableview Data.
    @objc func refreshNews(sender:AnyObject) {
        pageNumber = 1
        defaultPage = 1
        if !Utility.isConnectedToInternet {
            self.hideLoader()
            self.showAlert(message:AppMessages.ErrorAlerts.noInternet)
            self.notificationRefreshControl.endRefreshing()
            return
        }
        getNotifications()
    }
    
    //MARK:- Function to getnotifications.
    func getNotifications() {
        if !Utility.isConnectedToInternet {
            self.hideLoader()
            self.showAlert(message:AppMessages.ErrorAlerts.noInternet)
            self.notificationRefreshControl.endRefreshing()
            return
        }
        DIWebLayerCategoriesDetailApi().getNotifications (page: pageNumber,success: { (data) in
            self.hideLoader()
            if self.pageNumber == 1{
                self.notifications.removeAll()
            }
            self.notifications += data
            if data.count == 10 {
                self.pageNumber += 1
            }
            self.notificationRefreshControl.endRefreshing()
            self.notificationTableView.reloadData()
            self.notificationTableView.tableFooterView = UIView()
        }, failure: { (error) in
            self.hideLoader()
            self.showAlert(message:error.message)
            self.notificationRefreshControl.endRefreshing()
            self.notificationTableView.tableFooterView = UIView()
        })
    }
    
    //MARK:-Mark notification as read.
    func readNotification(id:String,index:Int) {
        if !Utility.isConnectedToInternet {
            self.hideLoader()
            self.showAlert(message:AppMessages.ErrorAlerts.noInternet)
            self.notificationRefreshControl.endRefreshing()
            return
        }
        DIWebLayerCategoriesDetailApi().updateNotifications(parameters: ["notification_id":id], success: { (message) in
            self.hideLoader()
            self.notifications[index].is_read = 1
            self.notificationTableView.reloadData()
            let count = Defaults.shared.get(forKey: DefaultKey.notificationCount) as? Int
            if count != 0 {
                Defaults.shared.set(value: (count ?? 0 - 1), forKey: DefaultKey.notificationCount)
            }
            self.navigateToNextController(index: index)
            
        }, failure: { (error) in
            self.hideLoader()
            self.showAlert(message:error.message)
        })
    }
    
    //MARK:-ScrollViewDelegate.
    func scrollViewDidEndDragging(_ scrollView: UIScrollView, willDecelerate decelerate: Bool) {
        if scrollView == notificationTableView {
            if ((scrollView.contentOffset.y + scrollView.frame.size.height) >= scrollView.contentSize.height) {
                if defaultPage < pageNumber {
                    defaultPage = pageNumber
                    self.notificationTableView.tableFooterView = Utility.getPagingSpinner()
                    getNotifications()
                }
            }
        }
    }
    
    //MARK:-Redirection Of User On Notification Click.
    func navigateToNextController(index:Int) {
        if let type = notifications[index].type {
            let stringType = "\(type)"
            if stringType == Constant.pushNotificationType.commentOnPost.rawValue {
                if let category =  notifications[index].postInfo?.type {
                    if category == 1 || category == 2 {
                        let  controller: DetailPageViewController = UIStoryboard.init(storyboard: .inspiration).initVC()
                        controller.category =  category
                        controller.postId = notifications[index].postInfo?.id ?? ""
                       self.navigationController?.pushViewController(controller, animated: true)
                    }else if category == 3 || category == 4 || category == 5 {
                        let  controller: MovieDetailViewController = UIStoryboard.init(storyboard: .inspiration).initVC()
                        controller.category =  category
                        controller.postId = notifications[index].postInfo?.id ?? ""
                        self.navigationController?.pushViewController(controller, animated: true)
                    }
                }
            }else if stringType == Constant.pushNotificationType.accept.rawValue || stringType == Constant.pushNotificationType.reject.rawValue || stringType == Constant.pushNotificationType.requestSent.rawValue{
                if let id = notifications[index].userDetail?.id {
                    let  controller: UserProfileViewController = UIStoryboard.init(storyboard: .networks).initVC()
                    controller.userId =  id
                   self.navigationController?.pushViewController(controller, animated: true)
                }
            }
//            else if stringType == Constant.pushNotificationType.requestSent.rawValue {
//                
////                let  controller: NetworkViewController = UIStoryboard.init(storyboard: .networks).instantiateViewController(withIdentifier: "userNetwork") as! NetworkViewController
////                controller.fromNotification = true
////                self.navigationController?.pushViewController(controller, animated: true)
////                notifications[index].userDetail?.id
//                let  controller: UserProfileViewController = UIStoryboard.init(storyboard: .networks).initVC()
//                controller.userId = notifications[index].userDetail?.id
//                self.navigationController?.pushViewController(controller, animated: true)
//                
////                let dashboardVC = UIStoryboard(storyboard: .inspiration).instantiateViewController(withIdentifier: "InspirationViewController")  as! UITabBarController
////                dashboardVC.selectedIndex = 3
////                appDelegate.setNavigationToRoot(viewContoller: dashboardVC)
//            }
            else if stringType == Constant.pushNotificationType.replyOnComment.rawValue {
                
                
                if let category =  notifications[index].postInfo?.type {
                    if category == 1 || category == 2 {
                        let  controller: DetailPageViewController = UIStoryboard.init(storyboard: .inspiration).initVC()
                        controller.category =  category
                        controller.postId = notifications[index].postInfo?.recomendationId ?? ""
                        controller.openReplyFor = notifications[index].postInfo?.id 
                        controller.openCommentTo = notifications[index].commentId
                        self.navigationController?.pushViewController(controller, animated: true)
                    }else if category == 3 || category == 4 || category == 5 {
                        let  controller: MovieDetailViewController = UIStoryboard.init(storyboard: .inspiration).initVC()
                        controller.category =  category
                        controller.postId = notifications[index].postInfo?.recomendationId ?? ""
                        controller.openReplyFor = notifications[index].postInfo?.id
                        controller.openCommentTo = notifications[index].commentId
                        self.navigationController?.pushViewController(controller, animated: true)
                    }
                }
            }
        }
    }
}

//MARK:-UITableViewDelegate&UITableViewDataSource.
extension NotificationsController:UITableViewDelegate,UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        tableView.setEmptyMessage("")
        if notifications.count <= 0 {
            tableView.setEmptyMessage("No notifications found!")
        }
        return notifications.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell:NotificationCell = notificationTableView.dequeueReusableCell(withIdentifier: "NotificationCell", for: indexPath) as? NotificationCell else {
            return UITableViewCell()
        }
        cell.selectionStyle = .none
        cell.setData(data:notifications,index:indexPath.row)
        if notifications[indexPath.row].is_read == 1 {
            cell.backgroundColor = .clear
        }else{
            cell.backgroundColor = UIColor.init(red: 215/255, green: 213/255, blue:  216/255, alpha: 1.0)
        }
        return cell
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if notifications[indexPath.row].is_read == 1{
          navigateToNextController(index: indexPath.row)
        }else{
            if let notificationID = notifications[indexPath.row].id {
                self.showLoader()
                readNotification(id:notificationID,index:indexPath.row)
            }
        }
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
}
