//
//  AddRatingViewController.swift
//  FriendSpire
//
//  Created by abhishek on 02/08/18.
//  Copyright © 2018 openkey. All rights reserved.
//

import UIKit


enum RatingSelected : Int{
    case one = 1
    case two
    case three
    case four
    case five
}


enum ratingAlpha : CGFloat{
    case unselected = 0.3
    case selected = 1.0
}

enum ratingBorder : CGFloat {
    case unselected = 0
//    case selected = 4
}

protocol AddRatingDelegate {
    func refreshViewDetail(rated: Bool)
}
class AddRatingViewController: DIBaseController {
    
    
    //variable From another view controller
    
    var categoryType : Constant.CategoryTypeBackend.RawValue?
    var referenceID : String?
    var delegate : AddRatingDelegate?
    var modalRef : FeedModal?
    var fromAnotherUserLibrary : Bool?
    
    @IBOutlet weak var viewRate1: UIView!
    @IBOutlet weak var viewRate2: UIView!
    @IBOutlet weak var viewRate3: UIView!
    @IBOutlet weak var viewRate4: UIView!
    @IBOutlet weak var viewRate5: UIView!
    @IBOutlet weak var lblRateText: UILabel!
    @IBOutlet weak var btnRateMovie: UIButton!
    @IBOutlet weak var btnDeleteRating: UIButton!
    @IBOutlet weak var txtViewReview: UITextView!
    @IBOutlet weak var viewHeader: UIView!
    @IBOutlet weak var viewTapbleArea: UIView!
    @IBOutlet weak var scrollView: UIScrollView!
    
    fileprivate var currentRating = 0
    fileprivate var alreadyRated = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        
        let swipeDown = UISwipeGestureRecognizer.init(target: self, action: #selector(hideCurrentView))
        swipeDown.direction = .down
        self.view.addGestureRecognizer(swipeDown)
        setIntialValue()
        view.isOpaque = false
        
        let tap = UITapGestureRecognizer(target: self, action: #selector(hideCurrentView))
        viewTapbleArea.addGestureRecognizer(tap)
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        if referenceID != nil {
            showLoader()
            DispatchQueue.global(qos: .background).async {
                self.getRatingDetail(for: self.referenceID ?? "")
            }
        }
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        txtViewReview.becomeFirstResponder()
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    //MARK:- IBAction
    
    
    @IBAction func ratingTapped(_ sender: UIButton) {
        
        setIntialValue()
        currentRating = sender.tag
        setRating()
    }
    
    
    @IBAction func deleteRatingTapped(_ sender: UIButton) {
        
        if referenceID != nil && categoryType != nil {
            
            var modal = RatingRequest()
            modal.type = categoryType ?? 0
            modal.referenceid = referenceID ?? ""
            showLoader()
            deleteRating(modal: modal) { (finish) in
                if finish {
                    if self.fromAnotherUserLibrary == true{
                        self.modalRef?.myRating = 0
                        self.modalRef?.myBookmarkStatus = 0
                    }else{
                    self.modalRef?.userReview = ""
                    self.modalRef?.userRating = 0
                    self.modalRef?.isBookmarked = false
                    }
                    self.delegate?.refreshViewDetail(rated: true)
                    self.dismiss(animated: true, completion: nil)
                }
            }
            
        }
    }
    
    @IBAction func rateMovieTapped(_ sender: UIButton) {
        
        if referenceID != nil && categoryType != nil {
            
            var ratingModal = RatingRequest()
            
            ratingModal.referenceid = referenceID ?? ""
            ratingModal.rating = currentRating
            ratingModal.type = categoryType ?? 0
            if txtViewReview.text != "Be a recommendation hero! Add some extra flair to your rating with a comment (optional)" {
                ratingModal.review = txtViewReview.text
            }
            showLoader()
            
            if alreadyRated == true{ // update rating
                updateRating(modal: ratingModal) { (finish) in
                    self.delegate?.refreshViewDetail(rated: finish)
                    if finish == true{
                        if self.fromAnotherUserLibrary == true {
                            self.modalRef?.myRating = ratingModal.rating
                            self.modalRef?.myBookmarkStatus = 0
                        }else{
                        self.modalRef?.userRating = ratingModal.rating
                        self.modalRef?.userReview = ratingModal.review
                        self.modalRef?.isBookmarked = false
                        }
                    }
                    self.dismiss(animated: true, completion: nil)
                }
            }else{ //add rating
                addRating(modal: ratingModal) { (finish) in
                    if finish == true{
                        if self.fromAnotherUserLibrary == true{
                            self.modalRef?.myRating = ratingModal.rating
                            self.modalRef?.myBookmarkStatus = 0
                        }else{
                        self.modalRef?.userRating = ratingModal.rating
                        self.modalRef?.userReview = ratingModal.review
                        self.modalRef?.isBookmarked = false
                        }
                    }
                    self.delegate?.refreshViewDetail(rated: finish)
                    self.dismiss(animated: true, completion: nil)
                }
            }
        }        
    }
    
    @IBAction func cancelTapped(_ sender: UIButton) {
        hideCurrentView()
    }
    
    
    @objc fileprivate func hideCurrentView(){
        self.dismiss(animated: true, completion: nil)
    }
    
    //MARK:- Cutom method
    fileprivate  func setIntialValue(){
        
//        viewRate1.borderWidth = ratingBorder.unselected.rawValue
//        viewRate2.borderWidth = ratingBorder.unselected.rawValue
//        viewRate3.borderWidth = ratingBorder.unselected.rawValue
//        viewRate4.borderWidth = ratingBorder.unselected.rawValue
//        viewRate5.borderWidth = ratingBorder.unselected.rawValue
        
        viewRate1.alpha = ratingAlpha.unselected.rawValue
        viewRate2.alpha = ratingAlpha.unselected.rawValue
        viewRate3.alpha = ratingAlpha.unselected.rawValue
        viewRate4.alpha = ratingAlpha.unselected.rawValue
        viewRate5.alpha = ratingAlpha.unselected.rawValue
        
        currentRating = 0
        btnRateMovie.isEnabled = false
        btnRateMovie.alpha = ratingAlpha.unselected.rawValue
        btnDeleteRating.isEnabled = false
        btnDeleteRating.alpha = ratingAlpha.unselected.rawValue
        btnRateMovie.setTitle(Utility.rateForString(using: categoryType ?? 0) , for: .normal)
    }
    
    fileprivate func setSelectedState(viewToSet : UIView){
//        viewToSet.borderWidth = ratingBorder.selected.rawValue
        viewToSet.alpha = ratingAlpha.selected.rawValue
        btnRateMovie.isEnabled = true
        btnRateMovie.alpha = ratingAlpha.selected.rawValue
    }
    
    fileprivate func setRating(){
        
        switch currentRating{
            
        case RatingSelected.one.rawValue:
            setSelectedState(viewToSet: viewRate1)
            lblRateText.text = AppMessages.RatingSelected.one.uppercased()
            break
            
        case RatingSelected.two.rawValue:
            setSelectedState(viewToSet: viewRate2)
            lblRateText.text = AppMessages.RatingSelected.two.uppercased()
            break
            
        case RatingSelected.three.rawValue:
            setSelectedState(viewToSet: viewRate3)
            
            lblRateText.text = AppMessages.RatingSelected.three.uppercased()
            break
            
        case RatingSelected.four.rawValue:
            setSelectedState(viewToSet: viewRate4)
            lblRateText.text = AppMessages.RatingSelected.four.uppercased()
            break
            
        case RatingSelected.five.rawValue:
            setSelectedState(viewToSet: viewRate5)
            lblRateText.text = AppMessages.RatingSelected.five.uppercased()
            break
            
        default:
            setIntialValue()
            break
        }
    }
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
}
// MARK: -
extension AddRatingViewController{
    // MARK: - API Integration
    
    fileprivate func addRating(modal : RatingRequest , success: @escaping (_ finish: Bool)-> ()){
        if !Utility.isConnectedToInternet {
            self.hideLoader()
            self.showAlert( message: AppMessages.ErrorAlerts.noInternet)
            return
        }
        
        DIWebLayerUserAPI().addRating(parameters: modal.getDictonary(), success: { [weak self](status) in
            DispatchQueue.main.async {
                self?.hideLoader()
            }
            if status.0  == 1 {
                self?.showToast(message: "Recommendation has been added to your library")
                self?.modalRef?.everyoneCount = status.2 // count of everyone
                self?.modalRef?.friendspireRating = status.1 // rating of everyone
                success(true)
            }else {
                success(false)
            }
            
            
        }) { [weak self] (error) in
            DispatchQueue.main.async {
                self?.hideLoader()
                self?.showToast(message: error.message ?? "")
                
            }
            DILog.print(items: error.message ?? "")
            success(false)
        }
    }
    
    fileprivate func updateRating(modal : RatingRequest , success: @escaping (_ finish: Bool)-> ()){
        if !Utility.isConnectedToInternet {
            self.hideLoader()
            self.showAlert( message: AppMessages.ErrorAlerts.noInternet)
            return
        }
        
        DIWebLayerUserAPI().updateRating(parameters: modal.getDictonary(), success: { [weak self](status) in
            DispatchQueue.main.async {
                self?.hideLoader()
            }
            if status.0  == 1 {
                self?.showToast(message: "Recommendation has been updated successfully")
                self?.modalRef?.everyoneCount = status.2 // count of everyone
                self?.modalRef?.friendspireRating = status.1 // rating of everyone
                success(true)
            }else {
                success(false)
            }
        }) { [weak self] (error) in
            DispatchQueue.main.async {
                self?.hideLoader()
                self?.showToast(message: error.message ?? "")
                
            }
//            DILog.print(items: error.message ?? "")
            
            success(false)
        }
    }
    
    fileprivate func deleteRating(modal : RatingRequest , success: @escaping (_ finish: Bool)-> ()){
        if !Utility.isConnectedToInternet {
            self.hideLoader()
            self.showAlert( message: AppMessages.ErrorAlerts.noInternet)
            return
        }
        
        DIWebLayerUserAPI().removeRating(parameters: modal.getDictonary(), success: { [weak self](status) in
            DispatchQueue.main.async {
                self?.hideLoader()
            }
            if status.0 == 1 {
                self?.showToast(message: "Recommendation has been removed successfully")
                self?.modalRef?.everyoneCount = status.2 // count of everyone
                self?.modalRef?.friendspireRating = status.1 // rating of everyone
                success(true)
            }else {
                success(false)
            }
        }) { [weak self] (error) in
            DispatchQueue.main.async {
                self?.hideLoader()
                self?.showToast(message: error.message ?? "")
                
            }
            DILog.print(items: error.message ?? "")
            
            success(false)
        }
    }
    
    fileprivate func getRatingDetail(for referenceId: String){
        if !Utility.isConnectedToInternet {
            self.hideLoader()
            self.showAlert( message: AppMessages.ErrorAlerts.noInternet)
            return
        }
        
        DIWebLayerUserAPI().getRatingDetail(for: referenceId, success: { [weak self](response) in
            DispatchQueue.main.async {
                self?.hideLoader()
                
                self?.alreadyRated = true // User rated this before
                //Populate data on screen
                self?.currentRating = Int(response.rating?.rounded(.towardZero) ?? 0.0)
                self?.setRating()
                if (response.review?.trim.count ?? 0) > 0{
                    self?.txtViewReview.text = response.review
                }
                self?.btnDeleteRating.isEnabled = true
                self?.btnDeleteRating.alpha = ratingAlpha.selected.rawValue
            }
            
        }) { [weak self] (error) in
            DispatchQueue.main.async {
                self?.hideLoader()
                DILog.print(items: error.message ?? "")
            }
        }
    }
}
