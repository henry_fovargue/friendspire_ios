//
//  SearchRecomendationViewController.swift
//  FriendSpire
//
//  Created by abhishek on 03/08/18.
//  Copyright © 2018 openkey. All rights reserved.
//

import UIKit
import CoreLocation
import GooglePlaces

enum CategoryHeight:CGFloat{
    case iPhone10 = 68.0
    case others = 48.0
    case iPhone10Inital = 35.0
    case othersInital = 20.0
}

class AddRecomendationViewController: DIBaseController {
    
    final let CANCEL_BUTTON_WIDTH = CGFloat(50.0)
    
    //variable from another controller
    var fromAnotherController = false
    var isSearchingOn = true // this vaiable is used locally as well
    
    //IBOutlet
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var txtSearch: UITextField!
    @IBOutlet weak var collectionViewCategories: UICollectionView!
    @IBOutlet weak var tableViewSearch: UITableView!
    @IBOutlet weak var btnCancel: UIButton!
    @IBOutlet weak var constraintWidthbtnCancel: NSLayoutConstraint!
    @IBOutlet weak var btnLocation: UIButton!
    @IBOutlet weak var constraintLeadingCollectionView: NSLayoutConstraint!
    @IBOutlet weak var lblTableViewCurve: UILabel!
    @IBOutlet weak var constraintHeightCollectionViewCategories: NSLayoutConstraint!
    @IBOutlet weak var viewTopBar: GradientView!
    @IBOutlet weak var constaintHeightViewCategorySelected: NSLayoutConstraint!
    @IBOutlet weak var viewCategorySelected: UIView!
    @IBOutlet weak var lblCurrentCategory: UILabel!
    @IBOutlet weak var txtLocation: UITextField!
    @IBOutlet weak var constraintHeightTxtLocation: NSLayoutConstraint!
    @IBOutlet weak var btnCurrentLocation: UIButton!
    @IBOutlet weak var constraintTopTxtLocation: NSLayoutConstraint!
    @IBOutlet weak var imgViewCategory: UIImageView!
    
    fileprivate var arrSearchResult = [SearchRecomendationModal]()
    fileprivate var objAttributes = SupportiveAttribute()
    fileprivate var customLocation : CLLocationCoordinate2D?
    
    var currentCategory :String?
    let refreshControl = UIRefreshControl()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        initialSetup()
        if currentCategory != nil && fromAnotherController == true{
            categorySelected(category: currentCategory!)
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        //Show or hide cancel button as per searching status
        if   User.sharedInstance.isLocationEnabled  == false && (currentCategory == AppMessages.Categories.restruant || currentCategory == AppMessages.Categories.bars){
            self.cancelTapped(btnCancel)
            collectionViewCategories.reloadData()
            showAlert(message: AppMessages.AlertTitles.locationOff, okCall: {[weak self] in
                self?.selectCategory(change: true)
            })
        }
        showCancelButton(isSearchingOn)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        selectCategory(change: false)
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //MARK:- IBAction
    
    @IBAction func currentLocationTapped(_ sender: UIButton) {
        txtLocation.text = AppMessages.Location.current
        showLoader(message: AppMessages.Location.fetch)
        getUserCurrentLcoation()
    }
    
    @IBAction func changeCategory(_ sender: UIButton) {
        selectCategory(change: true)
    }
    
    @IBAction func cancelTapped(_ sender: UIButton) {
        if fromAnotherController == true{
            //    self.dismiss(animated: true,completion: nil)
            self.navigationController?.popViewController(animated: true)
            return
        }
        
        if txtSearch.text?.isEmpty == true{
            selectCategory(change: true)
        }
        
        txtSearch.text = ""
        arrSearchResult.removeAll()
        //        isSearchingOn = false
        showCancelButton(isSearchingOn)
        
    }
    
    @IBAction func changeLocation(_ sender: UIButton) {
        //        self.view.endEditing(true)
        self.view.endEditing(true)
        let autocompleteController = GMSAutocompleteViewController()
        let filter = GMSAutocompleteFilter()
        filter.type = .city
        autocompleteController.autocompleteFilter = filter
        autocompleteController.delegate = self
        present(autocompleteController, animated: true, completion: nil)
    }
    
    //MARK:- Custom Methods
    func initialSetup(){
        
        if UIScreen.main.nativeBounds.height > iPHONE10{
            constaintHeightViewCategorySelected.constant = CategoryHeight.iPhone10Inital.rawValue
        }else{
            constaintHeightViewCategorySelected.constant = CategoryHeight.othersInital.rawValue
        }
        constraintHeightTxtLocation.constant = 0
        constraintTopTxtLocation.constant = 0
        txtLocation.isHidden = true
        btnCurrentLocation.isHidden = true
        viewCategorySelected.isHidden = true
        lblCurrentCategory.isHidden = true
        getUserCurrentLcoation()
        let viewFooter = UIView()
        viewFooter.backgroundColor = .clear
        tableViewSearch.tableFooterView = viewFooter
        tableViewSearch.keyboardDismissMode = .onDrag
        self.view.backgroundColor = .white  // this will reflect over tab bar tint color
        /*handling if User come from dashboard etc.*/
        if fromAnotherController == true{
            lblTableViewCurve.isHidden = true
        }else{
            showCancelButton(isSearchingOn)
        }
    }
    
    
    /**
     This method do some specfic settings (e.g.  tableview hidden etc.) as per search is on or off
     */
    fileprivate func showCancelButton(_ status:Bool) {
        
        //        self.arrSearchResult.removeAll()
        collectionViewCategories.reloadData()
        txtSearch.resignFirstResponder()
        
        
        //Show or hide cancel button
        btnCancel.isHidden = !status
        constraintWidthbtnCancel.constant =  status == true ? CANCEL_BUTTON_WIDTH : 0
        
        //search table hidden manage
        tableViewSearch.isHidden = false  //!status
        
        if status == true {
            self.view.bringSubview(toFront: tableViewSearch)
            self.view.bringSubview(toFront: btnLocation)
            tableViewSearch.reloadData()
            //            txtSearch.becomeFirstResponder()
        }else{
            //            txtSearch.text = ""
            self.view.bringSubview(toFront: tableView)
            tableView.reloadData()
        }
    }
    
    @objc fileprivate func searchReomendation(){
        
        if txtSearch.text?.trim.count == 0{
            self.arrSearchResult.removeAll()
            return// not allowed to search
        }
        
        var modal = SearchRecomendationRequest()        
        modal.title = txtSearch.text?.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed) ?? ""
        if currentCategory == nil {return}
        modal.type = Utility.getBackendType(using: currentCategory!)
        if (modal.type == Constant.CategoryTypeBackend.bars.rawValue ||  modal.type == Constant.CategoryTypeBackend.restraunt.rawValue)  {
            
            if customLocation != nil {
                modal.latLng = "[\(customLocation?.latitude ?? 0.0),\(customLocation?.longitude ?? 0.0)]"
            }else {
                modal.latLng = "[\(User.sharedInstance.userCurrentLocation?.latitude ?? 0.0),\(User.sharedInstance.userCurrentLocation?.longitude ?? 0.0)]"
            }
        }
        searchAPiHit(modal: modal)
    }
    
    //    fileprivate func updateTutorialReadStatus(){
    //
    //        if let readStatus = TutorialStatus.getCurrentStatus(){
    //            readStatus.addRecommendationScreen = true
    //             TutorialStatus.saveCurrentStatus(status: readStatus)
    //        }else{
    //            let status = TutorialStatus.sharedInstance
    //            status.addRecommendationScreen = true
    //            TutorialStatus.saveCurrentStatus(status: status)
    //        }
    //
    //
    //    }
    
    
    //MARK:-Get UserCurrentLocation.
    @objc fileprivate func getUserCurrentLcoation() -> Void {
        
        if LocationManager.shared.permissionStatus == false{
            self.hideLoader()
            self.openSetting()
        }
        LocationManager.shared.start()
        LocationManager.shared.success = { address,timestamp in
            User.sharedInstance.userCurrentLocation = CLLocationCoordinate2D.init(latitude: address.lat ?? 0.0, longitude: address.long ?? 0.0)
            self.customLocation = nil
            self.hideLoader()
        }
        LocationManager.shared.failure = {
            self.hideLoader()
            if $0.code == .locationPermissionDenied {
                User.sharedInstance.userCurrentLocation = nil
                self.openSetting()
            } else {
                self.showAlert(withError: $0)
            }
        }
    }
    
    fileprivate  func openSetting() {
        self.showAlert(withTitle: AppMessages.Location.title, message: AppMessages.Location.off, okayTitle: "Settings".localized, cancelTitle: "Cancel", okCall: {
            UIApplication.shared.open(URL(string: UIApplicationOpenSettingsURLString)! as URL)
            
        }, cancelCall: {
        })
    }
    
    // MARK: - Navigation
    
    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
        
        if segue.identifier == SegueIdentifiers.AddRecommendationScreen.segueSearchRecommendation{
            let controller = segue.destination as! SearchMoreRecommendationViewController
            controller.searchModal = sender as! SearchRecomendationRequest
            controller.strSearch = txtSearch.text ?? ""
        }
    }
    
    /// This will check if user didn't select category
    fileprivate func selectCategory(change:Bool){
        if change || (currentCategory == nil){
            let controller : SelectCategoryViewController = UIStoryboard.init(storyboard: .inspiration).initVC()
            controller.modalPresentationStyle = .overCurrentContext
            controller.delegate = self
            self.present(controller, animated: true, completion: nil)
        }else{
            txtSearch.becomeFirstResponder()
        }
    }
}
//MARK:-
extension AddRecomendationViewController : UITextFieldDelegate{
    //MARK:- TextField Delegate
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        if txtSearch == textField{
            isSearchingOn = true
            showCancelButton(isSearchingOn)
        }
        return true
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        selectCategory(change: false) // choose category if required
        
        //**********  Check if textfield is empty retrict user to enter space
        if string.trim.length == 0 && textField.text?.length == 0{
            print(string)
            return false
        }
        //        let cs = NSCharacterSet(charactersIn: ACCEPTABLE_CHARACTERS).inverted
        //        let filtered = string.components(separatedBy: cs).joined(separator: "")
        if string == "" && textField.text?.length == 1 { // back space check
            self.perform(#selector(self.reloadWithDelay), with: nil, afterDelay: 0.2)
        }
        else {
            //  hit  api now after canceling all previous schedule task
            NSObject.cancelPreviousPerformRequests(withTarget: self)
            self.perform(#selector(self.searchReomendation), with: nil, afterDelay: 0.5)
        }
        return  true
    }
    
    func textFieldShouldClear(_ textField: UITextField) -> Bool {
        self.perform(#selector(self.reloadWithDelay), with: nil, afterDelay: 0.3)
        return true
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        
        if txtSearch.text?.trimmed.count ?? 0 > 0{
            hitThirPartyRequest()
        }
        return true
    }
    
    /**
     reload table view and clear search result as well
     */
    func reloadWithDelay(){
        self.arrSearchResult.removeAll()
        self.tableViewSearch.reloadData()
    }
    
    /**
     This will create request and push the search more controller
     */
    fileprivate func hitThirPartyRequest(){
        // search from third party api
        var modal = SearchRecomendationRequest()
        modal.title = txtSearch.text?.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed) ?? ""
        if currentCategory == nil {return}
        modal.type = Utility.getBackendType(using: currentCategory!)
        if modal.type == Constant.CategoryTypeBackend.restraunt.rawValue || modal.type == Constant.CategoryTypeBackend.bars.rawValue {
            
            if customLocation != nil {
                modal.latLng = "[\(customLocation?.latitude ?? 0.0),\(customLocation?.longitude ?? 0.0)]"
            }else {
                modal.latLng = "[\(User.sharedInstance.userCurrentLocation?.latitude ?? 0.0),\(User.sharedInstance.userCurrentLocation?.longitude ?? 0.0)]"
            }
            
        }
        self.performSegue(withIdentifier: SegueIdentifiers.AddRecommendationScreen.segueSearchRecommendation, sender: modal)
    }
}

// MARK: -
extension AddRecomendationViewController : UITableViewDataSource,UITableViewDelegate{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        tableView.backgroundView = nil // reset to default state
        /*  handling for search table  */
        return txtSearch.text?.trim.count ?? 0 > 0 ? arrSearchResult.count+1 : arrSearchResult.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        //Search is On
        let cell = tableView.dequeueReusableCell(withIdentifier: CellIdentifiers.SearchRecomendationScreen.search)
        cell?.textLabel?.font = UIFont.init(name: "SFUIText-Regular", size: 13.0)
        /* show extra option to search for same if he want*/
        if indexPath.row == arrSearchResult.count {
            
            cell?.textLabel?.text =  "search for \(txtSearch.text ?? "")"
            cell?.textLabel?.textColor = #colorLiteral(red: 0.1764705926, green: 0.4980392158, blue: 0.7568627596, alpha: 1)
            cell?.imageView?.image = #imageLiteral(resourceName: "search").withRenderingMode(.alwaysTemplate)
            cell?.imageView?.tintColor = #colorLiteral(red: 0.1764705926, green: 0.4980392158, blue: 0.7568627596, alpha: 1)
            return cell!
        }else {
            
            //Populate data on listing
            let modal = arrSearchResult[indexPath.row]
            cell?.textLabel?.text =  modal.title
            cell?.textLabel?.textColor = #colorLiteral(red: 0.2392156863, green: 0.2509803922, blue: 0.3294117647, alpha: 1)
            cell?.imageView?.image = Utility.getIcon(using: currentCategory ?? AppMessages.Categories.movie).withRenderingMode(.alwaysTemplate)
            cell?.imageView?.tintColor = UIColor.lightGray
            
            return cell!
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        /*******Cancel another task if scheduled********/
        NSObject.cancelPreviousPerformRequests(withTarget: self)
        let controller : UIViewController?
        if currentCategory == AppMessages.Categories.movie ||  currentCategory == AppMessages.Categories.tv || currentCategory == AppMessages.Categories.books{
            let controllerLocal : MovieDetailViewController = UIStoryboard.init(storyboard: .inspiration).initVC()
            if currentCategory == nil {return}
            controllerLocal.category = Utility.getBackendType(using: currentCategory!)
            if indexPath.row ==  arrSearchResult.count && isSearchingOn == true{
                controllerLocal.isThirdPartySearchOn = true
            } else if isSearchingOn == true{
                controllerLocal.postId = arrSearchResult[indexPath.row].id ?? ""
            }
            controller = controllerLocal
        }else {
            let  controllerLocal : DetailPageViewController = UIStoryboard.init(storyboard: .inspiration).initVC()
            if currentCategory == nil {return}
            controllerLocal.category = Utility.getBackendType(using: currentCategory!)
            if indexPath.row ==  arrSearchResult.count &&  isSearchingOn == true{
                controllerLocal.isThirdPartySearchOn = true
            }else if isSearchingOn == true{
                controllerLocal.postId = arrSearchResult[indexPath.row].id ?? ""
            }
            controller = controllerLocal
        }
        
        if tableView == tableViewSearch {
            if indexPath.row ==  arrSearchResult.count { // search from third party api
                hitThirPartyRequest()
                return
            }
        }
        
        if controller != nil{
            self.navigationController?.pushViewController(controller!, animated: true)
        }
    }
}

//MARK:-
extension AddRecomendationViewController {
    //MARK:- Api Integration
    fileprivate func searchAPiHit(modal:SearchRecomendationRequest){
        
        if !Utility.isConnectedToInternet {
            self.hideLoader()
            self.showAlert( message: AppMessages.ErrorAlerts.noInternet)
            return
        }
        DIWebLayerUserAPI().getSearchResultRecomendation(for: modal, success: { [weak self](response) in
            
            self?.arrSearchResult = response
            DispatchQueue.main.async {
                self?.hideLoader()
                self?.tableViewSearch.reloadData()
            }
            
            
        }) { [weak self] (error) in
            DispatchQueue.main.async {
                self?.hideLoader()
                self?.showAlert( message: error.message ?? AppMessages.ErrorAlerts.unknownError)
                self?.tableViewSearch.reloadData()
            }
            DILog.print(items: error.message ?? "")
        }
    }
}

extension AddRecomendationViewController : GMSAutocompleteViewControllerDelegate{
    func viewController(_ viewController: GMSAutocompleteViewController, didAutocompleteWith place: GMSPlace) {
        DILog.print(items: "success place picking")
        customLocation =  place.coordinate
        txtLocation.text = place.name
        viewController.dismiss(animated: true, completion: nil)
        
    }
    
    func viewController(_ viewController: GMSAutocompleteViewController, didFailAutocompleteWithError error: Error) {
        DILog.print(items: "failure place picking")
        viewController.dismiss(animated: true, completion: nil)
        
    }
    
    func wasCancelled(_ viewController: GMSAutocompleteViewController) {
        DILog.print(items: "cancel place picking")
        viewController.dismiss(animated: true, completion: nil)
        
    }
}

extension AddRecomendationViewController : CategorySelectionDelegate{
    
    func categorySelected(category: String) {
        
        if UIScreen.main.nativeBounds.height > iPHONE10{
            constaintHeightViewCategorySelected.constant = CategoryHeight.iPhone10.rawValue
        }else{
            constaintHeightViewCategorySelected.constant = CategoryHeight.others.rawValue
        }
        
        viewCategorySelected.isHidden = false
        lblCurrentCategory.isHidden = false
        if category == AppMessages.Categories.restruant || category == AppMessages.Categories.bars{
            constraintHeightTxtLocation.constant = 45
            constraintTopTxtLocation.constant = 10
            txtLocation.isHidden = false
            btnCurrentLocation.isHidden = false
            
        }else{
            constraintHeightTxtLocation.constant = 0
            constraintTopTxtLocation.constant = 0
            txtLocation.isHidden = true
            btnCurrentLocation.isHidden = true
        }
        currentCategory = category
        viewCategorySelected.backgroundColor = Utility.getColor(using: category)
        viewTopBar.backgroundColor = Utility.getColor(using: category)
        imgViewCategory.image = Utility.getIcon(using: category)
        lblCurrentCategory.text = category
        /*******Cancel another task if scheduled********/
        NSObject.cancelPreviousPerformRequests(withTarget: self)
        searchReomendation()
        tableView.reloadData()
        
        //        //show Tutorial screen to user
        //        if TutorialStatus.getCurrentStatus() == nil || TutorialStatus.getCurrentStatus()?.addRecommendationScreen == nil{
        //            self.perform(#selector(showTutorialPopup(arr:)), with: [#imageLiteral(resourceName: "layer9")], afterDelay: 0.3)
        //            updateTutorialReadStatus()
        //        }
        
        txtSearch.becomeFirstResponder()
    }
}
