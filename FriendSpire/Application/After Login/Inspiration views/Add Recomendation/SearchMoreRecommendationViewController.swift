//
//  SearchMoreRecommendationViewController.swift
//  FriendSpire
//
//  Created by abhishek on 31/08/18.
//  Copyright © 2018 openkey. All rights reserved.
//

import UIKit


fileprivate var MAXIUM_PAGE_LIMIT = 1

class SearchMoreRecommendationViewController: DIBaseController {
    
    //    variable from another controller
    var searchModal = SearchRecomendationRequest()
    var strSearch : String?
    
    //IBOutlets
    
    @IBOutlet weak var txtSearch: CustomTextField!
    @IBOutlet weak var tableView: UITableView!
    
    //Local Variable
    fileprivate var arrSearchedResult = [SearchRecomendationModal]()
    
    var isReachedOfEnd = false
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        tableView.register(UINib(nibName: CellIdentifiers.SearchRecomendationScreen.searchMoreCell, bundle: nil), forCellReuseIdentifier: CellIdentifiers.SearchRecomendationScreen.searchMoreCell)
        tableView.estimatedRowHeight = 60.0
        tableView.rowHeight = UITableViewAutomaticDimension
        txtSearch.text = strSearch ?? ""
        addPagination()
        showLoader()
        self.searchFromThirdParty(modal: searchModal)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        RECORDS_PER_PAGE = Constant.PageLimit.searchThirdParty
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        RECORDS_PER_PAGE = Constant.PageLimit.localApi
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //MARK:- Custom Methods
    
    /**
     each time table view reaches to its limit callback will allow you to manage api Hits
     */
    func addPagination(){
        
        self.tableView.infiniteScrolling(self) {
            
            if MAXIUM_PAGE_LIMIT > self.searchModal.page{
                // if user not loaded the result upto allowed limit
            self.searchModal.page = self.searchModal.page + 1
            self.searchFromThirdParty(modal: self.searchModal)
            }else {
                self.tableView.endPull2RefreshAndInfiniteScrolling(count: 0)
            }
        }
    }
    
    
    //MARK:- IBACTION
    
    @IBAction func backTapped(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
}


extension SearchMoreRecommendationViewController : UITableViewDataSource,UITableViewDelegate{
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return arrSearchedResult.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: CellIdentifiers.SearchRecomendationScreen.searchMoreCell) as! SearchMoreCell
        cell.configureCell(arrSearchedResult[indexPath.row])
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let controller : UIViewController?
        if searchModal.type == Constant.CategoryTypeBackend.movie.rawValue ||  searchModal.type == Constant.CategoryTypeBackend.tv.rawValue || searchModal.type == Constant.CategoryTypeBackend.books.rawValue{
            let controllerLocal : MovieDetailViewController = UIStoryboard.init(storyboard: .inspiration).initVC()
            controllerLocal.category = searchModal.type
            controllerLocal.postId = arrSearchedResult[indexPath.row].id ?? ""
            controller = controllerLocal
        }else {
            let  controllerLocal : DetailPageViewController = UIStoryboard.init(storyboard: .inspiration).initVC()
            controllerLocal.category = searchModal.type
            controllerLocal.postId = arrSearchedResult[indexPath.row].id ?? ""
            controller = controllerLocal
        }
        if controller != nil{
        self.navigationController?.pushViewController(controller!, animated: true)
        }
    }
}



//MARK:-
extension SearchMoreRecommendationViewController {
    //MARK:- Api Integration
    
    fileprivate func searchFromThirdParty(modal : SearchRecomendationRequest){
        
        if !Utility.isConnectedToInternet {
            self.hideLoader()
            self.showAlert( message: AppMessages.ErrorAlerts.noInternet)
            self.tableView.endPull2RefreshAndInfiniteScrolling(count: arrSearchedResult.count)
            return
        }
        
        DIWebLayerUserAPI().searchRecomendationResultThirdParty(for: modal, success: { [weak self](response) in
            
            if response.0.count > 0 {
                
                self?.isReachedOfEnd = false
                
                if modal.page == 1{
                    self?.arrSearchedResult.removeAll()
                }
                self?.arrSearchedResult.append(contentsOf: response.0)
            }else {
                if self?.arrSearchedResult.count ?? 0 > 0{
                    self?.isReachedOfEnd = true
                    
                }else {
                self?.showAlert(withTitle: appName.capitalized, message: "Bummer! We cound not find anything in our database. Could you try again?", okayTitle: AppMessages.AlertTitles.Ok , okCall: {
                    self?.navigationController?.popViewController(animated: true)
                })
                }
            }
           MAXIUM_PAGE_LIMIT = response.1 // store max limit
            DispatchQueue.main.async {
                self?.tableView.reloadData()
                self?.tableView.endPull2RefreshAndInfiniteScrolling(count: self?.arrSearchedResult.count ?? 0)
                self?.hideLoader()
            }
            
        }) { [weak self] (error) in
            DILog.print(items: error.message ?? "")
            DispatchQueue.main.async {
                self?.hideLoader()
                self?.tableView.endPull2RefreshAndInfiniteScrolling(count: self?.arrSearchedResult.count ?? 0)
                self?.showAlert(withTitle: appName.capitalized, message: error.message, okayTitle: AppMessages.AlertTitles.Ok , okCall: {
                    self?.navigationController?.popViewController(animated: true)
                })
                
            }
        }
    }
}
