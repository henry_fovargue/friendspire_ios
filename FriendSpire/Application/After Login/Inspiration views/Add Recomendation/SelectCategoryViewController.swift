//
//  SelectCategoryViewController.swift
//  FriendSpire
//
//  Created by abhishek on 24/10/18.
//  Copyright © 2018 openkey. All rights reserved.
//

import UIKit


protocol CategorySelectionDelegate {
    func categorySelected(category : String)
}
class SelectCategoryViewController: DIBaseController {
    
    var delegate : CategorySelectionDelegate?
    
    fileprivate let arrCategories = [[CategoryEnum.Title:AppMessages.Categories.movie ,CategoryEnum.Icon: #imageLiteral(resourceName: "movie"),CategoryEnum.Color:#colorLiteral(red: 0.5461447239, green: 0.3740770221, blue: 0.7494846582, alpha: 1)],
                                     [CategoryEnum.Title:AppMessages.Categories.tv ,CategoryEnum.Icon: #imageLiteral(resourceName: "television"),CategoryEnum.Color:#colorLiteral(red: 0.2260053158, green: 0.6834762096, blue: 0.9318693876, alpha: 1)],
                                     [CategoryEnum.Title:AppMessages.Categories.books ,CategoryEnum.Icon: #imageLiteral(resourceName: "book"),CategoryEnum.Color:#colorLiteral(red: 0.1082813069, green: 0.59888798, blue: 0.5456139445, alpha: 1)],
                                     [CategoryEnum.Title:AppMessages.Categories.restruant ,CategoryEnum.Icon: #imageLiteral(resourceName: "Restraunt"),CategoryEnum.Color:#colorLiteral(red: 0.9959798455, green: 0.6115841269, blue: 0.3324229121, alpha: 1)],
                                     [CategoryEnum.Title:AppMessages.Categories.bars ,CategoryEnum.Icon: #imageLiteral(resourceName: "bar"),CategoryEnum.Color:#colorLiteral(red: 0.9861864448, green: 0.3759915829, blue: 0.6583402753, alpha: 1)]]
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        
        //show Tutorial screen to user
        //        if TutorialStatus.getCurrentStatus() == nil || TutorialStatus.getCurrentStatus()?.categorySelectionScreen == nil{
        //            self.perform(#selector(showTutorial), with: nil, afterDelay: 0.3)
        //            updateTutorialReadStatus()
        //        }
        
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.dismiss(animated: true, completion: nil)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func cancel(_ sender: UIButton) {
        self.dismiss(animated: true, completion: nil)
    }
    
    
    //    @objc fileprivate func showTutorial(){
    //        let tutorial : TutroialViewController =  UIStoryboard.init(storyboard: .tutorial).initVC()
    //        tutorial.modalTransitionStyle = .crossDissolve
    //        tutorial.modalPresentationStyle = .overCurrentContext
    //        tutorial.arrImages = [#imageLiteral(resourceName: "layer8")]
    //        self.present(tutorial, animated: true, completion: nil)
    //        updateTutorialReadStatus()
    //    }
    
    //    private func updateTutorialReadStatus(){
    //
    //        if let readStatus = TutorialStatus.getCurrentStatus(){
    //            readStatus.categorySelectionScreen = true
    //            TutorialStatus.saveCurrentStatus(status: readStatus)
    //        }else{
    //            let status = TutorialStatus.sharedInstance
    //            status.categorySelectionScreen = true
    //            TutorialStatus.saveCurrentStatus(status: status)
    //        }
    //
    //    }
    
}
// MARK: -
extension SelectCategoryViewController : UICollectionViewDataSource,UICollectionViewDelegate,UICollectionViewDelegateFlowLayout{
    // MARK: -  Collection View Delegate
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        return   arrCategories.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cellCategory  = collectionView.dequeueReusableCell(withReuseIdentifier: CellIdentifiers.SearchRecomendationScreen.categoryIcon, for: indexPath) as! CategoryCell
        
        cellCategory.configureCategory(modal: arrCategories[indexPath.row])
        return cellCategory
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let tempCheck = arrCategories[indexPath.item][CategoryEnum.Title] as! String
        if   User.sharedInstance.isLocationEnabled  == false && (tempCheck == AppMessages.Categories.restruant || tempCheck == AppMessages.Categories.bars){
            collectionView.reloadData()
            self.showAlert(message:AppMessages.AlertTitles.locationOff)
            return
        }
        
        delegate?.categorySelected(category: arrCategories[indexPath.item][CategoryEnum.Title] as! String)
        self.dismiss(animated: true, completion: nil)
    }
    
    
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        return CGSize(width: collectionView.frame.size.width , height: 80.0)
        
    }
    
    
}
