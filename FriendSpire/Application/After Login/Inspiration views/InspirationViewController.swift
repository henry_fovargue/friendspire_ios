//
//  InspirationViewController.swift
//  FriendSpire
//
//  Created by Mac Test on 13/07/18.
//  Copyright © 2018 openkey. All rights reserved.
//

import UIKit
import CoreLocation

class InspirationViewController: DIBaseController {
    
    //variable from another view controller
    var respondTo3DTouch : Bool?
    
    @IBOutlet weak var collectionViewFeed: UICollectionView!
    @IBOutlet weak var collectionViewCategory: UICollectionView!
    @IBOutlet weak var lblNotificationBadge: UILabel!
    @IBOutlet weak var customFlowLayout: CustomFlowLayout!
    
    fileprivate var paginationHitInProcess = false
    fileprivate var lastUsedLayout = 10
    fileprivate var isLastPageReached = false
    fileprivate var bottomRefresh : UIActivityIndicatorView?
    fileprivate var refreshControl = UIRefreshControl()
    fileprivate var arrStaticHeight = [360.0,400.0,320.0,370.0,300.0]
    fileprivate var arrInspirationNew = [InspirationFeedModal]()
    fileprivate var arrInspiration = [InspirationFeedModal]()
    fileprivate var currentPage = 1
    fileprivate let arrCategory : [[CategoryEnum:Any]] = [[CategoryEnum.Title:AppMessages.Categories.movie ,CategoryEnum.Icon: #imageLiteral(resourceName: "movie"),CategoryEnum.Color:#colorLiteral(red: 0.5461447239, green: 0.3740770221, blue: 0.7494846582, alpha: 1)],
                                                          [CategoryEnum.Title:AppMessages.Categories.tv ,CategoryEnum.Icon: #imageLiteral(resourceName: "television"),CategoryEnum.Color:#colorLiteral(red: 0.2260053158, green: 0.6834762096, blue: 0.9318693876, alpha: 1)],
                                                          [CategoryEnum.Title:AppMessages.Categories.books ,CategoryEnum.Icon: #imageLiteral(resourceName: "book"),CategoryEnum.Color:#colorLiteral(red: 0.1082813069, green: 0.59888798, blue: 0.5456139445, alpha: 1)],
                                                          [CategoryEnum.Title:AppMessages.Categories.restruant ,CategoryEnum.Icon: #imageLiteral(resourceName: "Restraunt"),CategoryEnum.Color:#colorLiteral(red: 0.9959798455, green: 0.6115841269, blue: 0.3324229121, alpha: 1)],
                                                          [CategoryEnum.Title:AppMessages.Categories.bars ,CategoryEnum.Icon: #imageLiteral(resourceName: "bar"),CategoryEnum.Color:#colorLiteral(red: 0.9861864448, green: 0.3759915829, blue: 0.6583402753, alpha: 1)]]
    fileprivate var currentCity = ""
    fileprivate var currentMessage = "Loading..."
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        
        getUserCurrentLocation()
        self.navigationController?.interactivePopGestureRecognizer?.isEnabled = false
        
        if let layout = collectionViewFeed?.collectionViewLayout as? CustomFlowLayout {
            layout.delegate = self
        }
        
        bottomRefresh = UIActivityIndicatorView(frame: CGRect(x: self.view.frame.midX - 15, y: self.view.frame.height - 140, width: 30, height: 30))
        bottomRefresh?.activityIndicatorViewStyle = .white
        bottomRefresh?.color = UIColor.darkGray
        bottomRefresh?.hidesWhenStopped = true
        bottomRefresh?.backgroundColor = .clear
        bottomRefresh?.layer.cornerRadius = 15
        self.view.addSubview(bottomRefresh!)
        self.view.bringSubview(toFront: bottomRefresh!)
        refreshControl.addTarget(self, action: #selector(pullToRefresh), for: UIControlEvents.valueChanged)
        self.collectionViewFeed.refreshControl = refreshControl
        
        //3D Touch option
        if respondTo3DTouch == true{
            self.shareData(id: nil, type: nil, notificationType: 3, title: "")
        }
        
        //show Tutorial screen to user
        //        if TutorialStatus.getCurrentStatus() == nil ||  TutorialStatus.getCurrentStatus()?.inpirationScreen == nil{
        //            self.perform(#selector(showTutorialPopup(arr:)), with: [#imageLiteral(resourceName: "layer13"),#imageLiteral(resourceName: "layer14")], afterDelay: 0.3)
        //            updateTutorialReadStatus()
        //        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        DispatchQueue.global(qos: .background).async {
            self.updateDeviceToken()
            
            //this one indicate pagination size changed
            //            RECORDS_PER_PAGE = Constant.PageLimit.searchThirdParty
            
            if User.sharedInstance.userCurrentLocation == nil || self.currentCity.isEmpty{
                //            showLoader(message: AppMessages.Location.fetch)
                self.showLoader()
                self.performSelector(onMainThread: #selector(self.getUserCurrentLocation), with: nil, waitUntilDone: false)
                return
            }
            var modalRequest = InpirationListRequest()
            modalRequest.page = 1
            self.getFeeds(modal: modalRequest, nextPage: true)
        }
        
        //    pushNotificationCountHandler()
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: "increaseNotificationCount"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(InspirationViewController.pushNotificationCountHandler), name: NSNotification.Name(rawValue: "increaseNotificationCount"), object: nil)
        
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        //revert pagination size changes
        //        RECORDS_PER_PAGE = Constant.PageLimit.localApi
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func updateDeviceToken(){
        guard let token = Defaults.shared.get(forKey: DefaultKey.deviceToken) as? String else {
            return
        }
        DIWebLayerCategoriesDetailApi().updateDeviceToken(parameter: ["device_token":token], success: { (message) in
            
        }) { (error) in
        }
    }
    
    //MARK:- This function will append id to array and store them in local.
    @objc func pushNotificationCountHandler() {
        let count = Defaults.shared.get(forKey: DefaultKey.notificationCount) as? Int
        if count == 0 {
            lblNotificationBadge.isHidden = true
        }else{
            lblNotificationBadge.isHidden = false
            lblNotificationBadge.text = "\(count ?? 0)"
            
        }
        UIApplication.shared.applicationIconBadgeNumber = count ?? 0
    }
    
    //MARK:-Get UserCurrentLocation.
    @objc fileprivate func getUserCurrentLocation() -> Void {
        if LocationManager.shared.permissionStatus == false{
            self.hideAllLoader()
            self.openSetting()
            return
        }
        
        LocationManager.shared.start()
        LocationManager.shared.success = {[weak self] address,timestamp in
            
            User.sharedInstance.userCurrentLocation = CLLocationCoordinate2D.init(latitude: address.lat ?? NEWYORK_LAT, longitude: address.long ?? NEWYORK_LNG)
            self?.currentCity = address.city ?? ""
            self?.hideAllLoader()
            self?.showLoader()
            var modalRequest = InpirationListRequest()
            modalRequest.page = 1
            self?.getFeeds(modal: modalRequest, nextPage: true)
        }
        LocationManager.shared.failure = {
            self.hideAllLoader()
            if $0.code == .locationPermissionDenied {
                self.openSetting()
            } else {
                self.showAlert(withError: $0)
            }
        }
    }
    
    @objc fileprivate  func openSetting() {
        self.showAlert(withTitle: AppMessages.Location.title, message: AppMessages.Location.off, okayTitle: "Settings".localized, cancelTitle: "Cancel", okCall: {
            UIApplication.shared.open(URL(string: UIApplicationOpenSettingsURLString)! as URL)
        }, cancelCall: {
            //Little hack it will pop any unwanted view controller, other wise keep the view on steady state
             UIApplication.topViewController()?.navigationController?.popViewController(animated: true)
        })
    }
    
    @objc fileprivate func pullToRefresh(){
        //check if location available
        if User.sharedInstance.userCurrentLocation == nil || self.currentCity.isEmpty{
            self.hideAllLoader()
           if LocationManager.shared.permissionStatus == true{
               self.perform(#selector(self.getUserCurrentLocation), with: nil, afterDelay: 0.2)
           }else{
            self.perform(#selector(self.openSetting), with: nil, afterDelay: 0.2)
            }
            return
        }
        NSObject.cancelPreviousPerformRequests(withTarget: self)
        self.perform(#selector(self.refreshOnAnotherThread), with: nil, afterDelay: 0.1)
    }
    
    @objc fileprivate  func refreshOnAnotherThread(){
        //        resetAllCheck()
        collectionViewFeed.collectionViewLayout.invalidateLayout()
        var modalRequest = InpirationListRequest()
        modalRequest.page = 1
        self.getFeeds(modal: modalRequest, nextPage: true)
    }
    
    //    private func updateTutorialReadStatus(){
    //
    //        if let readStatus = TutorialStatus.getCurrentStatus(){
    //            readStatus.inpirationScreen = true
    //            TutorialStatus.saveCurrentStatus(status: readStatus)
    //        }else{
    //            let status = TutorialStatus.sharedInstance
    //            status.inpirationScreen = true
    //            TutorialStatus.saveCurrentStatus(status: status)
    //        }
    //    }
    
    //MARK:- IBAction
    
    @IBAction func inviteToAppTapped(_ sender: UIButton) {
        self.shareData(id: nil, type: nil, notificationType: 3, title: "")
    }
    
    
    @IBAction func searchTapped(_ sender: UIButton) {
        self.redirectToAddRecommendation(category: nil)
    }
    
    @IBAction func notificationTapped(_ sender: UIButton) {
        Defaults.shared.set(value: 0, forKey: DefaultKey.notificationCount)
        pushNotificationCountHandler()
        
        DispatchQueue.global(qos: .background).async {
            DIWebLayerCategoriesDetailApi().clearNotificationsCount(success: { (message) in
            }) { (error) in
            }
        }
        let controller: NotificationsController = UIStoryboard(storyboard: .setting).initVC()
        self.navigationController?.pushViewController(controller, animated: true)
    }
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
}
// MARK: -
extension InspirationViewController : UICollectionViewDataSource,UICollectionViewDelegate{
    // MARK: -  collection View Delegate
    
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        if collectionView == collectionViewCategory{
            return 1
        }
        return 3
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        //top bar category
        if collectionView == collectionViewCategory{
            return arrCategory.count
        }
        let totalCount = arrInspiration.count + arrInspirationNew.count
        collectionViewFeed.setEmptyMessage("")
        if totalCount == 0{
            collectionViewFeed.setEmptyMessage(currentMessage)
        }
        
        if section  == 0{ // latest Feed (Before 5 days)
            return arrInspirationNew.count
        }else if section == 1{ // Message indication
            return totalCount > 0 ? 1 : 0
        }
        
        return arrInspiration.count // older Feed (after 5 days)
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        if collectionViewCategory == collectionView{
            let cellCategory  = collectionView.dequeueReusableCell(withReuseIdentifier: "CategoryCell", for: indexPath) as! CategoryCell
            cellCategory.configureCell(modal: arrCategory[indexPath.row])
            return cellCategory
        }
        
        
        //****************************reached at end cell
        if indexPath.section == 1{
            let cellEndSuggestion = collectionView.dequeueReusableCell(withReuseIdentifier: "noMoreSuggestion", for: indexPath) as! NoSuggestionCell
            
            return cellEndSuggestion
        }
        
        //*************************inpiration cell feed list
        guard let  cellInspiration = collectionView.dequeueReusableCell(withReuseIdentifier: "InspirationCell", for: indexPath) as? InspirationCell else {return UICollectionViewCell()}
        cellInspiration.configureInpiration(using: getModalFromArray(index: indexPath))
        
        return cellInspiration
        
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        /*******Cancel another task if scheduled********/
        NSObject.cancelPreviousPerformRequests(withTarget: self)
        
        if collectionViewCategory == collectionView{
            
            let category = arrCategory[indexPath.row][CategoryEnum.Title] as? String
            var controller : DIBaseController?
            switch category{
                
            case AppMessages.Categories.movie:
                controller  = UIStoryboard.init(storyboard: .library).instantiateViewController(withIdentifier: "MoviesLibraryViewController") as! MoviesLibraryViewController
                (controller as! MoviesLibraryViewController).type = Constant.CategoryTypeBackend.movie
                
                break
            case AppMessages.Categories.tv:
                controller = UIStoryboard.init(storyboard: .library).instantiateViewController(withIdentifier: "MoviesLibraryViewController") as! MoviesLibraryViewController
                (controller as! MoviesLibraryViewController).type = Constant.CategoryTypeBackend.tv
                
                break
                
            case AppMessages.Categories.books:
                controller = UIStoryboard.init(storyboard: .library).instantiateViewController(withIdentifier: "MoviesLibraryViewController") as! MoviesLibraryViewController
                (controller as! MoviesLibraryViewController).type = Constant.CategoryTypeBackend.books
                
                break
                
            case AppMessages.Categories.restruant:
                controller = UIStoryboard.init(storyboard: .library).instantiateViewController(withIdentifier: "RestrauntsLibraryonListViewController") as! RestrauntsLibraryonListViewController
                (controller as! RestrauntsLibraryonListViewController).type = Constant.CategoryTypeBackend.restraunt
                
                break
                
            case AppMessages.Categories.bars:
                controller = UIStoryboard.init(storyboard: .library).instantiateViewController(withIdentifier: "RestrauntsLibraryonListViewController") as! RestrauntsLibraryonListViewController
                (controller as! RestrauntsLibraryonListViewController).type = Constant.CategoryTypeBackend.bars
                
                break
                
            default:
                DILog.print(items: "NA")
                return
            }
            
            controller?.title = category
            controller?.colorToUse = arrCategory[indexPath.row][CategoryEnum.Color] as? UIColor
            self.navigationController?.pushViewController(controller!, animated: true)
            
            return
        }else if collectionViewFeed == collectionView{
            
            //Not allowed to click on end of message cell
            if indexPath.section == 1{
                return
            }
            
            let modal = getModalFromArray(index: indexPath)
            let controller : UIViewController?
            if modal.type ?? 0 == Constant.CategoryTypeBackend.movie.rawValue ||  modal.type ?? 0 == Constant.CategoryTypeBackend.tv.rawValue || modal.type ?? 0 == Constant.CategoryTypeBackend.books.rawValue{
                let controllerLocal : MovieDetailViewController = UIStoryboard.init(storyboard: .inspiration).initVC()
                controllerLocal.category =  modal.type ?? 0
                controllerLocal.postId = modal.id ?? ""
                controller = controllerLocal
            }else {
                let  controllerLocal : DetailPageViewController = UIStoryboard.init(storyboard: .inspiration).initVC()
                controllerLocal.category =  modal.type ?? 0
                controllerLocal.postId = modal.id ?? ""
                controller = controllerLocal
            }
            if controller != nil{
                self.navigationController?.pushViewController(controller!, animated: true)
            }
        }
        
    }
    
    
    func getModalFromArray(index : IndexPath) -> InspirationFeedModal{
        
        if index.section == 0{
            return arrInspirationNew[index.row]
        }else if index.section == 1{
            return InspirationFeedModal()
        }
        
        return arrInspiration[index.row]
    }
    
}

// MARK: -
extension InspirationViewController: CustomFlowLayoutDelegate {
    // MARK: - Custom FlowDelegate
    func collectionView(_ collectionView: UICollectionView,
                        heightForPhotoAtIndexPath indexPath:IndexPath) -> CGFloat {
        
        
        if indexPath.section == 1{
            return CGFloat(50.0)
        }
        var randomNumber = getRandomNumber()
        while lastUsedLayout == randomNumber{
            randomNumber =  getRandomNumber()
            continue
        }
        //        DILog.print(items: "yes we sending ----------- ======== \(randomNumber)")
        return CGFloat(arrStaticHeight[getRandomNumber()])
    }
    
    func getRandomNumber() -> Int{
        
        let lower : UInt32 = 0
        let upper : UInt32 = 4
        let randomNumber = Int(arc4random_uniform(upper - lower) + lower)
        return randomNumber
    }
}

extension InspirationViewController : UIScrollViewDelegate{
    
    func scrollViewDidEndDragging(_ scrollView: UIScrollView, willDecelerate decelerate: Bool) {
        if ((scrollView.contentOffset.y + scrollView.frame.size.height) >= scrollView.contentSize.height) {
            
            if isLastPageReached == false && scrollView == collectionViewFeed{
                //check  location available or not and check for empty feed already a hit made for server in process
                if User.sharedInstance.userCurrentLocation == nil || self.currentCity.isEmpty || self.arrInspirationNew.count == 0 || self.paginationHitInProcess == true{
                    return
                }
                self.paginationHitInProcess = true
                bottomRefresh?.startAnimating()
                self.view.bringSubview(toFront: bottomRefresh!)
                var modalRequest = InpirationListRequest()
                currentPage = currentPage + 1
                modalRequest.page = currentPage
                self.getFeeds(modal: modalRequest, nextPage: true)
            }
        }
    }
}

extension InspirationViewController{
    //MARK:- API Integration
    
    fileprivate func getFeeds( modal : InpirationListRequest, nextPage : Bool){
        
        if !Utility.isConnectedToInternet {
            paginationHitInProcess = false
            currentMessage = AppMessages.ErrorAlerts.noInternet
            DispatchQueue.main.async {
                self.hideAllLoader()
                self.showAlert( message: AppMessages.ErrorAlerts.noInternet)
                return
            }
        }
        
        DispatchQueue.main.async {
            self.collectionViewFeed.delegate = self
            self.collectionViewFeed.dataSource = self
        }
        
        var modalToSend = modal
        modalToSend.lat = "\(User.sharedInstance.userCurrentLocation?.latitude ?? NEWYORK_LAT)"
        modalToSend.lng = "\(User.sharedInstance.userCurrentLocation?.longitude ?? NEWYORK_LNG)"
        modalToSend.currentCity = currentCity
        modalToSend.homeCity = User.sharedInstance.homeCity ?? ""
        
        DIWebLayerUserAPI().getListOfInpiration(modal: modalToSend, success: { [weak self](response) in
             self?.paginationHitInProcess = false
            self?.currentMessage = ""
            self?.hideAllLoader()
            if response.count > 0 {
                self?.fillDataInArray(response: response, page: modal.page)
            }else {
                self?.isLastPageReached = true // end of  feeds
            }
            self?.pushNotificationCountHandler()
        }) { [weak self] (error) in
             self?.paginationHitInProcess = false
             self?.currentMessage = "Something not went well! please pull to refresh"
            DILog.print(items: error.message ?? "")
            DispatchQueue.main.async {
                self?.showAlert( message: error.message ?? AppMessages.ErrorAlerts.unknownError)
                self?.hideAllLoader()
            }
        }
    }
    func fillDataInArray(response: [InspirationFeedModal],page:Int){
        
        self.isLastPageReached = false  // more pages left to fetch
        if page == 1{
            //remove all for inital hit
            self.currentPage = 1
            self.arrInspiration.removeAll()
            self.arrInspirationNew.removeAll()
        }else{
            //*********** Make sure to scroll to last feed user was, when user request for new feeds
            if self.collectionViewFeed.numberOfItems(inSection: 2) > 0{
                let count = self.arrInspiration.count-1
                DispatchQueue.main.asyncAfter(deadline: .now() + .milliseconds(100)) {
                    self.collectionViewFeed.scrollToItem(at: IndexPath(row: count, section: 2), at: .bottom, animated: true)
                }
            }else if self.collectionViewFeed.numberOfItems(inSection: 0) > 0{
                let count = self.arrInspirationNew.count-1
                DispatchQueue.main.asyncAfter(deadline: .now() + .milliseconds(100)) {
                    self.collectionViewFeed.scrollToItem(at: IndexPath(row: count, section: 0), at: .bottom, animated: true)
                }
            }
        }
        
        //Check date to show on first and section of feed
        for item in response.enumerated(){
            
            //if post by algorithm no need to check rated date
            if item.element.algorithm == AlgorimthmStatus.Applied.rawValue{
                self.arrInspirationNew.append(item.element) // new feeds
            }else if let dateUpdated = item.element.rated_at?.getDate(){//check for date rated
                
                let result =  Date().interval(ofComponent: .day, fromDate: dateUpdated)
                
                //Check if post is no older than 5
                if result > DAY5{
                    self.arrInspiration.append(item.element) // older feeds
                }else {
                    self.arrInspirationNew.append(item.element) // new feeds
                }
            }
        }
        
        self.realodFeedCollection()
    }
    /**
     This will hide all loaders used in this class
     
     */
    fileprivate func hideAllLoader(){
        DispatchQueue.main.async {
            self.hideLoader()
            self.refreshControl.endRefreshing()
            self.bottomRefresh?.stopAnimating()
        }
    }
    
    /**
     This func especially handle the feed list collection and invalidate collectionview attribute as well
     
     */
    fileprivate func realodFeedCollection(){
        self.collectionViewFeed.collectionViewLayout.invalidateLayout()
        if let layout = self.collectionViewFeed?.collectionViewLayout as? CustomFlowLayout {
            
            // clear cached flowlayout calculations
            layout.cache.removeAll()
            layout.contentHeight = 0
        }
        //call again collectionview to do flowlayout calculations
        self.collectionViewFeed.reloadData()
    }
}
