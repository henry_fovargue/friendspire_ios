//
//  RestaurantsFiltersViewController.swift
//  FriendSpire
//
//  Created by Harpreet_kaur on 06/08/18.
//  Copyright © 2018 openkey. All rights reserved.
//

import UIKit
import GooglePlaces


class RestaurantsFiltersViewController: DIBaseController {
    
    //MARK:-Variables
    var category:Int?
    var reloadSections = [0,1,2,3,4]
    var sectionHeading = ["Remove rated","Location:","Distance:","Price Level:","Cuisine","Open now","Rating:"]
    let priceLevelArray = [Constant.priceLevel.one.rawValue,Constant.priceLevel.two.rawValue,Constant.priceLevel.three.rawValue,Constant.priceLevel.four.rawValue]
    var selectedSection:Int = 10
    var panGestureRecognizer: UIPanGestureRecognizer?
    var initialTouchPoint: CGPoint = CGPoint(x: 0,y: 0)
    var currentAddress:Address?
    var delegate:BookFilterDelegate?
    var comingFromLibrary:Bool = false
    var objRestaurantsFilters:RestuarantFilterModel = RestuarantFilterModel()
    var objFeedListRequest:FeedListRequest = FeedListRequest()
    
    //MARK:-IBOutlets.
    @IBOutlet weak var topView: UIView!
    @IBOutlet weak var restaurantTableView: UITableView!
    @IBOutlet weak var btnCurrentLocation: UIButton!
    
    @IBOutlet weak var viewTappbleArea: UIView!
    @IBOutlet weak var filterCountLabel: UILabel!
    
    var ratingLabel : UILabel?
    var fourSquareRatingLabel : UILabel?
    var ratingMin : UILabel?
    var ratingMax : UILabel?
    var forSquareMin : UILabel?
    var forSquareMax : UILabel?
    var headerRef : BookFilterHeaderView?
    
    //MARK:-ViewLifeCycle.
    //MARK:-ViewDidLoad.
    override func viewDidLoad() {
        super.viewDidLoad()
        objRestaurantsFilters = RestuarantFilterModel.sharedInstance
        objFeedListRequest = RestuarantFilterModel.objFeedListRequest
        RestuarantFilterModel.filterCount = 0
        RestuarantFilterModel.filterCountKey = 0
        let viewFooter = UIView()
        viewFooter.backgroundColor = #colorLiteral(red: 0.9019607843, green: 0.9254901961, blue: 0.9607843137, alpha: 1)
        restaurantTableView.tableFooterView = viewFooter
        self.restaurantTableView.register(UITableViewCell.self, forCellReuseIdentifier: "cell")
        restaurantTableView.register(UINib(nibName: Constant.Xib.doubleSliderTableCell, bundle: nil), forCellReuseIdentifier: Constant.TableviewCellIdentifier.doubleSliderTableCell)
        restaurantTableView.estimatedSectionHeaderHeight = 50
        addGestureToViewToDismissPresentController()
        
        let tap = UITapGestureRecognizer(target: self, action: #selector(showPopupOnDismiss))
        viewTappbleArea.addGestureRecognizer(tap)
    }
    
    //MARK:-ViewWillAppear.
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        setFilerCount()
        CuisinesViewController.delegate = self
        BookFilterHeaderView.delegate = self
        
    }
    
    //MARK:-Private Functions.
    //MARK:-AddGesture to View.
    func addGestureToViewToDismissPresentController() {
        let swipeDown = UISwipeGestureRecognizer.init(target: self, action: #selector(showPopupOnDismiss))
        swipeDown.direction = .down
        self.topView.addGestureRecognizer(swipeDown)
    }
    
    @objc fileprivate func hideCurrentView(){
        self.dismiss(animated: true, completion: nil)
    }
    
    //MARK:-Get UserCurrentLocation.
    fileprivate func getUserCurrentLcoation() -> Void {
        LocationManager.shared.start()
        LocationManager.shared.success = { address,timestamp in
            RestuarantFilterModel.sharedInstance.address = address
            RestuarantFilterModel.sharedInstance.location = "Current Location"
        }
        LocationManager.shared.failure = {
            if $0.code == .locationPermissionDenied {
                self.openSetting()
            } else {
                self.showAlert(withError: $0)
            }
        }
    }
    
    fileprivate func openSetting() {
        self.showAlert(withTitle: AppMessages.Location.title, message: AppMessages.Location.off, okayTitle: "Settings".localized, cancelTitle: "Cancel", okCall: {
            
            UIApplication.shared.open(URL(string: UIApplicationOpenSettingsURLString)! as URL)
            //            UIApplication.shared.openURL(NSURL(string: UIApplicationOpenSettingsURLString)! as URL)
            
        }, cancelCall: {
        })
    }
    
    
    @objc func levelSelected(_ sender:UIButton ){
        sender.isSelected = !sender.isSelected
        if RestuarantFilterModel.sharedInstance.priceLevel.contains(priceLevelArray[sender.tag-1]) {
            for (index,data) in  RestuarantFilterModel.sharedInstance.priceLevel.enumerated() {
                if data == priceLevelArray[sender.tag-1] {
                    RestuarantFilterModel.sharedInstance.priceLevel.remove(at: index)
                    sender.isSelected = false
                }
            }
        }else{
            sender.isSelected = true
            RestuarantFilterModel.sharedInstance.priceLevel.append(priceLevelArray[sender.tag-1])
        }
        
        var arrPrice = [Int]()
        for obj in RestuarantFilterModel.sharedInstance.priceLevel{
            
            arrPrice.append(Utility.priceLevelInt(value:obj))
            
        }
        
        RestuarantFilterModel.objFeedListRequest.price = "[\(arrPrice.map{String($0)}.joined(separator: ","))]"
        //        labelRef?.text = "\(RestuarantFilterModel.sharedInstance.priceLevel.joined(separator: ", ") )"
        
        restaurantTableView.beginUpdates()
        restaurantTableView.setContentOffset(CGPoint.init(x: 0.0, y: 0.0), animated: false)
        restaurantTableView.endUpdates()
        setFilerCount()
        restaurantTableView.reloadSections([3], with: .none)
        
        //        restaurantTableView.reloadRows(at: [IndexPath.init(row: 0, section: 0)], with: .none)
    }
    
    
    func showPopupOnDismiss() {
        if  RestuarantFilterModel.sharedInstance.distanceUnit != User.sharedInstance.distanceUnit {
            self.doneButtonAction()
            return
        }
        let objState = Utility.compareRestaurantFilterObject(obj1: RestuarantFilterModel.sharedInstance, obj2:  self.objRestaurantsFilters)
        if objState == true {
            self.hideCurrentView()
            return
        }
        self.showAlert(withTitle: "Filter", message: "Would you like to save your changes?", okayTitle: "Save", cancelTitle: "No", okCall: {
            self.doneButtonAction()
        }) {
            RestuarantFilterModel.sharedInstance = self.objRestaurantsFilters
            RestuarantFilterModel.objFeedListRequest = self.objFeedListRequest
            self.hideCurrentView()
        }
    }
    
    func doneButtonAction() {
        if  RestuarantFilterModel.sharedInstance.distanceUnit != User.sharedInstance.distanceUnit {
            var distance = Double(RestuarantFilterModel.sharedInstance.distance)
            if DistanceUnit.Miles.rawValue == User.sharedInstance.distanceUnit{
                distance =  distance * MILES_VALUE
                
            }else {
                distance =  distance * KM_VALUE
            }
            RestuarantFilterModel.objFeedListRequest.distance = "\(distance)"
            RestuarantFilterModel.sharedInstance.distanceUnit = User.sharedInstance.distanceUnit
        }else{
            let objState = Utility.compareRestaurantFilterObject(obj1: RestuarantFilterModel.sharedInstance, obj2:  self.objRestaurantsFilters)
            if objState == true {
                self.hideCurrentView()
                return
            }
        }
        _ = RestuarantFilterModel.objFeedListRequest.getDictonary()
        RestuarantFilterModel.filterCount = RestuarantFilterModel.filterCountKey
        delegate?.filterApplied()
        hideCurrentView()
    }
    
    //MARK:-IBActions.
    @IBAction func setCurrentLocationTapped(_ sender: UIButton) {
        LocationManager.shared.start()
        LocationManager.shared.success = { address,timestamp in
            RestuarantFilterModel.sharedInstance.location = "Current Location"
            RestuarantFilterModel.sharedInstance.address.lat = address.lat
            RestuarantFilterModel.sharedInstance.address.long = address.long
            RestuarantFilterModel.objFeedListRequest.latLong = "[\(String(describing: RestuarantFilterModel.sharedInstance.address.lat ?? 0.0)),\(String(describing: RestuarantFilterModel.sharedInstance.address.long ?? 0.0))]"
            RestuarantFilterModel.objFeedListRequest.filterLatLong = 1
            self.setFilerCount()
            self.restaurantTableView.reloadSections([1], with: .none)
        }
        LocationManager.shared.failure = {
            if $0.code == .locationPermissionDenied {
                self.openSetting()
            } else {
                self.showAlert(withError: $0)
            }
        }
    }
    
    
    @IBAction func doneButtonAction(_ sender: UIButton) {
        doneButtonAction()
    }
    
    @IBAction func resetButtonAction(_ sender: UIButton) {
        
        //FIXME: Reset Location manipulation
      let locSave = RestuarantFilterModel.objFeedListRequest.latLong
       let locStatusSave = RestuarantFilterModel.objFeedListRequest.filterLatLong
        
        Utility.resetFilter()
        setFilerCount()
         RestuarantFilterModel.objFeedListRequest.latLong = locSave
         RestuarantFilterModel.objFeedListRequest.filterLatLong = locStatusSave
        
        restaurantTableView.reloadData()
    }
    
    @IBAction func searchBarButtonAction(_ sender: UIButton) {
        self.view.endEditing(true)
        let autocompleteController = GMSAutocompleteViewController()
        autocompleteController.delegate = self
        let filter = GMSAutocompleteFilter()
        filter.type = .city
        autocompleteController.autocompleteFilter = filter
        present(autocompleteController, animated: true, completion: nil)
    }
    
    @IBAction func distanceSliderAction(_ sender: UISlider) {
        RestuarantFilterModel.sharedInstance.distanceUnit = User.sharedInstance.distanceUnit
        RestuarantFilterModel.sharedInstance.distance = sender.value
        var distance = Double(RestuarantFilterModel.sharedInstance.distance)
        if DistanceUnit.Miles.rawValue == User.sharedInstance.distanceUnit{
            distance =  distance * MILES_VALUE
            
        }else {
            distance =  distance * KM_VALUE
        }
        RestuarantFilterModel.objFeedListRequest.distance = "\(distance)"
        setFilerCount()
        self.restaurantTableView.reloadSections([2], with: .none)
    }
    
    func setFilerCount() {
        _ = RestuarantFilterModel.objFeedListRequest.getDictonary()
        RestuarantFilterModel.filterCount = RestuarantFilterModel.filterCountKey
        if RestuarantFilterModel.filterCount == 0 {
            filterCountLabel.text = ""
            filterCountLabel.isHidden = true
        }else {
            filterCountLabel.isHidden = false
            filterCountLabel.text = "\(RestuarantFilterModel.filterCount)"
        }
        RestuarantFilterModel.filterCountKey = 0
    }
    
}

//MARK:-Prortocol to activate Selected Header.
extension RestaurantsFiltersViewController: BookFilterHeaderViewProtocol {
    func setActiveHaederCell(section: Int) {
        if selectedSection == section {
            selectedSection = 10
        }else if section == 4{
            let controller: CuisinesViewController = UIStoryboard(storyboard: .filter).initVC()
            controller.type = category ?? 1
            controller.modalTransitionStyle = .crossDissolve
            controller.modalPresentationStyle = .overCurrentContext
            self.present(controller, animated: true, completion: nil)
        }else{
            selectedSection = section
        }
        if selectedSection != 10{
            restaurantTableView.reloadSections([selectedSection], with: .none)
        }
    }
}

//MARK:-Delegate Set Selected Cusisines.
extension RestaurantsFiltersViewController : SetCuisines {
    func reloadTableView(sectionToLoad: Int) {
        
        if sectionToLoad == 6 {
            UIView.setAnimationsEnabled(false)
            restaurantTableView.beginUpdates()
            ratingLabel?.text = "Friendspire Rating: \(RestuarantFilterModel.sharedInstance.rating[0]) - \(RestuarantFilterModel.sharedInstance.rating[1])"
            fourSquareRatingLabel?.text = "Foursquare Rating: \(RestuarantFilterModel.sharedInstance.fourSquareRating[0]) - \(RestuarantFilterModel.sharedInstance.fourSquareRating[1])"
            ratingMax?.text = "\(RestuarantFilterModel.sharedInstance.rating[1])"
            ratingMin?.text = "\(RestuarantFilterModel.sharedInstance.rating[0])"
            forSquareMin?.text = "\(RestuarantFilterModel.sharedInstance.fourSquareRating[0])"
            forSquareMax?.text = "\(RestuarantFilterModel.sharedInstance.fourSquareRating[1])"
            
            restaurantTableView.endUpdates()
            UIView.setAnimationsEnabled(true)
            setFilerCount()
            return
        }else{
            restaurantTableView.reloadSections([sectionToLoad], with: .none)
            setFilerCount()
        }
        restaurantTableView.endUpdates()
        UIView.setAnimationsEnabled(true)
        //        if sectionToLoad == 3{
        //
        //            //            UIView.setAnimationsEnabled(false)
        //            //            restaurantTableView.beginUpdates()
        //
        //            let string = "Friendspire:  \(RestuarantFilterModel.sharedInstance.rating[0]) - \(RestuarantFilterModel.sharedInstance.rating[1])\nForsquare:    \(RestuarantFilterModel.sharedInstance.fourSquareRating[0]) - \(RestuarantFilterModel.sharedInstance.fourSquareRating[1])"
        //            var myMutableString = NSMutableAttributedString()
        //            myMutableString = NSMutableAttributedString(string: string as String, attributes: [NSFontAttributeName: labelRef?.font ?? UIFont()])
        //            myMutableString.addAttribute(NSForegroundColorAttributeName, value: UIColor.gray, range: NSRange(location:0,length:12))
        //            myMutableString.addAttribute(NSForegroundColorAttributeName, value: UIColor.gray, range: NSRange(location:23,length:10))
        //            labelRef?.attributedText = myMutableString
        //            ratingMax?.text = "\(RestuarantFilterModel.sharedInstance.rating[1])"
        //            ratingMin?.text = "\(RestuarantFilterModel.sharedInstance.rating[0])"
        //            forSquareMin?.text = "\(RestuarantFilterModel.sharedInstance.fourSquareRating[0])"
        //            forSquareMax?.text = "\(RestuarantFilterModel.sharedInstance.fourSquareRating[1])"
        //
        //            //            restaurantTableView.endUpdates()
        //            //            UIView.setAnimationsEnabled(true)
        //
        //            return
        //        }
        //        if sectionToLoad == 5{
        //            headerRef?.removeRatingSwitch.isOn = RestuarantFilterModel.sharedInstance.openNow ?? true
        //            return
        //        }
        //        restaurantTableView.scrollToNearestSelectedRow(at: .top, animated: false)
        //        self.sectionValueSetup(using: sectionToLoad)
        
        //  restaurantTableView.reloadSections([sectionToLoad], with: .none)
    }
    
    //    func setCuisines(cuisinesArray: [String]) {
    //        RestuarantFilterModel.sharedInstance.cuisines = cuisinesArray
    //        restaurantTableView.reloadData()
    //    }
    
    
}

//MARK:-UITabelViewDelegate&UITabelViewDataSource.
extension RestaurantsFiltersViewController : UITableViewDelegate , UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return sectionHeading.count
        
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        switch section {
        case 1,2,3,6:
            return 1
        default:
            return 0
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        switch indexPath.section {
        case 1:
            guard let cell = restaurantTableView.dequeueReusableCell(withIdentifier: Constant.TableviewCellIdentifier.searchBarTableCell, for: indexPath) as? SearchBarTableCell else {
                return UITableViewCell()
            }
            return cell
        case 2:
            guard let cell = restaurantTableView.dequeueReusableCell(withIdentifier: Constant.TableviewCellIdentifier.sliderTableCell, for: indexPath) as? SliderTableCell else {
                return UITableViewCell()
            }
            cell.distanceSlider.value = RestuarantFilterModel.sharedInstance.distance ?? 0.0
            return cell
            
        case 3:
            let cell:PriceLevelCell = restaurantTableView.dequeueReusableCell(withIdentifier: "priceLevelCell", for: indexPath) as! PriceLevelCell
            cell.selectionStyle = .none
            //            cell.textLabel?.font = UIFont(name: "sf-ui-text-regular-58646b56a688c", size: 13)
            //            cell.textLabel?.textColor = #colorLiteral(red: 0.2392156863, green: 0.2509803922, blue: 0.3254901961, alpha: 1)
            //            cell.textLabel?.text = priceLevelArray[indexPath.row]
            
            cell.backgroundColor = .clear
            cell.contentView.backgroundColor = .clear
            cell.btnLevel1.addTarget(self, action:#selector(levelSelected(_ :)) , for: .touchUpInside)
            cell.btnLevel2.addTarget(self, action:#selector(levelSelected(_ :)) , for: .touchUpInside)
            cell.btnLevel3.addTarget(self, action:#selector(levelSelected(_ :)) , for: .touchUpInside)
            cell.btnLevel4.addTarget(self, action:#selector(levelSelected(_ :)) , for: .touchUpInside)
            cell.selectedState()
            
            return cell
        case 6:
            guard let cell = restaurantTableView.dequeueReusableCell(withIdentifier: Constant.TableviewCellIdentifier.doubleSliderTableCell, for: indexPath) as? DoubleSliderTableCell else {
                return UITableViewCell()
            }
            cell.rangeSlider.tag = 10
            cell.fourSquareRangeSlider.tag = 10
            ratingLabel = cell.headingLabel
            fourSquareRatingLabel = cell.fourSquareHeadingLabel
            ratingMin = cell.minimumRangeLabel
            ratingMax = cell.maximumRangeLabel
            forSquareMax = cell.fourSquareMaximumRangeLabel
            forSquareMin = cell.fourSquareMinimumRangeLabel
            cell.minimumRangeLabel.text = "\(RestuarantFilterModel.sharedInstance.rating[0])"
            cell.maximumRangeLabel.text = "\(RestuarantFilterModel.sharedInstance.rating[1])"
            cell.rangeSlider.lowerValue = Double(RestuarantFilterModel.sharedInstance.rating[0]) ?? cell.rangeSlider.minimumValue
            cell.rangeSlider.upperValue = Double(RestuarantFilterModel.sharedInstance.rating[1]) ?? cell.rangeSlider.maximumValue
            cell.headingLabel.text = "Friendspire Rating: \(RestuarantFilterModel.sharedInstance.rating[0]) - \(RestuarantFilterModel.sharedInstance.rating[1])"
            cell.fourSquareMinimumRangeLabel.text = "\(RestuarantFilterModel.sharedInstance.fourSquareRating[0])"
            cell.fourSquareMaximumRangeLabel.text = "\(RestuarantFilterModel.sharedInstance.fourSquareRating[1])"
            cell.fourSquareRangeSlider.lowerValue = Double(RestuarantFilterModel.sharedInstance.fourSquareRating[0]) ?? cell.fourSquareRangeSlider.minimumValue
            cell.fourSquareRangeSlider.upperValue = Double(RestuarantFilterModel.sharedInstance.fourSquareRating[1]) ?? cell.fourSquareRangeSlider.maximumValue
            cell.fourSquareHeadingLabel.text = "Foursquare Rating: \(RestuarantFilterModel.sharedInstance.fourSquareRating[0]) - \(RestuarantFilterModel.sharedInstance.fourSquareRating[1])"
            return cell
        default:
            return UITableViewCell()
        }
        
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        switch indexPath.section {
        case 2:
            break
        default:
            break
        }
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        if section == 1 || section == 4 {
            return 0
        }
        if comingFromLibrary {
            if section == 0 {
                return 0
            }
        }
        return UITableViewAutomaticDimension
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.section == 1 {
            return 0 
        }
        return UITableViewAutomaticDimension
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        switch section {
        case 5,4,0:
            let nibView = Bundle.main.loadNibNamed(Constant.Xib.bookFilterHeaderView , owner: self, options: nil)?.first as?  BookFilterHeaderView
            nibView?.headingLabel.text = sectionHeading[section]
            nibView?.headerActionButton.tag = section
            nibView?.removeRatingSwitch.tag = section
            nibView?.anyLabel.isHidden = true
            nibView?.headerButton.isHidden = true
            nibView?.headerActionButton.isUserInteractionEnabled = false
            nibView?.removeRatingSwitch.isHidden = true
            if section == 4 {
                nibView?.headingLabelWidthConstarint.constant = 70
                nibView?.headerButton.isHidden = false
                nibView?.headerActionButton.isUserInteractionEnabled = true
                nibView?.valueLabel.text = "\(RestuarantFilterModel.sharedInstance.cuisines.joined(separator: ", "))"
                if RestuarantFilterModel.sharedInstance.cuisines.count == 0 {
                    nibView?.anyLabel.isHidden = false
                }
                nibView?.headerButton.setImage(#imageLiteral(resourceName: "Disclosure Indicator"), for: .normal)
            }else{
                nibView?.headingLabelWidthConstarint.constant = 200
                nibView?.valueLabel.text = ""
                nibView?.headerButton.setImage(nil, for: .normal)
                nibView?.removeRatingSwitch.isHidden = false
                if section == 5 {
                    nibView?.removeRatingSwitch.setOn(RestuarantFilterModel.sharedInstance.openNow ?? false , animated: false)
                }else{
                    nibView?.removeRatingSwitch.setOn(RestuarantFilterModel.sharedInstance.showMyRated ?? false , animated: false)
                }
            }
            headerRef = nibView
            return nibView
        case 1,2,3,6:
            let nibView = Bundle.main.loadNibNamed(Constant.Xib.restaurantFilterHeader , owner: self, options: nil)?.first as?  RestaurantFilterHeader
            nibView?.headerActionButton.tag = section
            nibView?.headingLabel.sizeToFit()
            nibView?.headerActionButton.isUserInteractionEnabled = false
            nibView?.headingLabel.text = sectionHeading[section]
            switch section {
            case 1 :
                nibView?.headingLabelWidthConstraint.constant = 75
                nibView?.valueLabel.text = RestuarantFilterModel.sharedInstance.location
            case 2 :
                nibView?.headingLabelWidthConstraint.constant = 75
                let distance = Double(RestuarantFilterModel.sharedInstance.distance)
                var strUnit = " Miles"
                if DistanceUnit.Miles.rawValue == User.sharedInstance.objAttributes?.distanceUnit.rawValue ?? User.sharedInstance.distanceUnit{
                    //                  distance =  distance * MILES_VALUE
                    strUnit = " Miles"
                }else {
                    //                    distance =  distance * KM_VALUE
                    strUnit = " Kms"
                }
                if distance.clean != "0" {
                nibView?.valueLabel.text = distance.clean + strUnit
                }else{
                  nibView?.valueLabel.text = ""
                }
//                    String(format: "%.1f", arguments: [distance]) + strUnit
            case 3 :
                nibView?.headingLabelWidthConstraint.constant = 91
                nibView?.valueLabel.text = "\(RestuarantFilterModel.sharedInstance.priceLevel.joined(separator: ", ") )"
            case 6 :
                nibView?.headingLabelWidthConstraint.constant = 90
                //                let string = "Friendspire:  \(RestuarantFilterModel.sharedInstance.rating[0]) - \(RestuarantFilterModel.sharedInstance.rating[1])\nForsquare:    \(RestuarantFilterModel.sharedInstance.fourSquareRating[0]) - \(RestuarantFilterModel.sharedInstance.fourSquareRating[1])"
                //                var myMutableString = NSMutableAttributedString()
                //                myMutableString = NSMutableAttributedString(string: string as String, attributes: [NSFontAttributeName: nibView?.valueLabel.font ?? UIFont()])
                //                myMutableString.addAttribute(NSForegroundColorAttributeName, value: UIColor.gray, range: NSRange(location:0,length:12))
                //                myMutableString.addAttribute(NSForegroundColorAttributeName, value: UIColor.gray, range: NSRange(location:23,length:10))
                nibView?.valueLabel.text = ""
            // labelRef = nibView?.valueLabel
            default:
                break
            }
            return nibView
        default:
            break
        }
        return UIView()
        
    }
    
    
}

