//
//  RestaurantFilterHeader.swift
//  FriendSpire
//
//  Created by Harpreet_kaur on 06/08/18.
//  Copyright © 2018 openkey. All rights reserved.
//

import UIKit

class RestaurantFilterHeader: UIView {


    //MARK:-IBOutlets.
    @IBOutlet weak var headingLabel: UILabel!
    @IBOutlet weak var valueLabel: UILabel!
    @IBOutlet weak var minHeadingLabel: UILabel!
    @IBOutlet weak var headerActionButton: UIButton!
    @IBOutlet weak var headingLabelWidthConstraint: NSLayoutConstraint!
    //MARK:-IBActions.
    @IBAction func headerClickAction(_ sender: UIButton) {
        BookFilterHeaderView.delegate?.setActiveHaederCell(section: sender.tag)
    }
}
