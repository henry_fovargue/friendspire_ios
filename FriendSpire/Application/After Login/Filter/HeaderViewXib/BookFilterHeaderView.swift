//
//  BookFilterHeaderView.swift
//  FriendSpire
//
//  Created by Harpreet_kaur on 06/08/18.
//  Copyright © 2018 openkey. All rights reserved.
//

import UIKit
protocol BookFilterHeaderViewProtocol {
    func setActiveHaederCell(section:Int)
}

class BookFilterHeaderView: UIView {
    
    
    //MARK:-Variables.
    static var delegate:BookFilterHeaderViewProtocol?
    
    //MARK:-IBOutlets.
    @IBOutlet weak var headingLabel: UILabel!
    @IBOutlet weak var headerButton: UIButton!
    @IBOutlet weak var removeRatingSwitch: UISwitch!
    @IBOutlet weak var headerActionButton: UIButton!
    @IBOutlet weak var valueLabel: UILabel!
    @IBOutlet weak var headingLabelWidthConstarint: NSLayoutConstraint!
    @IBOutlet weak var anyLabel: UILabel!
    
    
    //MARK:-IBActions.
    @IBAction func headerClickAction(_ sender: UIButton) {
        BookFilterHeaderView.delegate?.setActiveHaederCell(section: sender.tag)
    }
    @IBAction func headerSwitchAction(_ sender: UISwitch) {
        if sender.tag == 5 {
            if sender.isOn {
                RestuarantFilterModel.sharedInstance.openNow = true
                RestuarantFilterModel.objFeedListRequest.openNow = "1"
                sender.isOn = true
            }else{
                RestuarantFilterModel.sharedInstance.openNow = false
                RestuarantFilterModel.objFeedListRequest.openNow = "0"
                sender.isOn = false
            }
            CuisinesViewController.delegate?.reloadTableView(sectionToLoad: sender.tag)
        }else{
            if sender.isOn {
                RestuarantFilterModel.bookSharedInstance.showMyRated = true
                RestuarantFilterModel.sharedInstance.showMyRated = true
                RestuarantFilterModel.objFeedListRequest.removeRated = "1"
                sender.isOn = true

            }else{
                RestuarantFilterModel.sharedInstance.showMyRated = false
                RestuarantFilterModel.bookSharedInstance.showMyRated = false
                RestuarantFilterModel.objFeedListRequest.removeRated = "0"
                sender.isOn = false

            }
             CuisinesViewController.delegate?.reloadTableView(sectionToLoad: sender.tag)
        }
        
    }
}
