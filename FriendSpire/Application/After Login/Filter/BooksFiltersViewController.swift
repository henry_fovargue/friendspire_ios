//
//  BooksFiltersViewController.swift
//  FriendSpire
//
//  Created by Harpreet_kaur on 06/08/18.
//  Copyright © 2018 openkey. All rights reserved.
//

import UIKit
protocol BookFilterDelegate{
    func filterApplied()
}

class BooksFiltersViewController: DIBaseController {

    //MARK:-Variables
    var category:Int?
    var sectionHeading = ["Remove rated","Pages:","Genre:","Publish Year:","Rating:"]
    let booksPagesCount = ["Between 1-250","Between 250-500","Between 500-750","Between 750-1000","More than 1000 Pages"]
    let moviesDuration = ["Between 1-30 min","Between 30-60 min","Between 60-90 min","Between 90-120 min","More than 120 min"]
    var genreOptions = ["Actions","Adventure","Animation Biography","Documentry","Fantasy"]
    let movieSectionsBookHeading =  ["Remove rated","Duration:","Genre:","Release Year:","Rating:"]
    var selectedSection:Int = 10
    var panGestureRecognizer: UIPanGestureRecognizer?
    var initialTouchPoint: CGPoint = CGPoint(x: 0,y: 0)
    var delegate:BookFilterDelegate?
    var strThirdParty = "IMDB Rating"
    var comingFromLibrary:Bool = false
    var publishYearHeadingLabel : UILabel?
    var publishYearMin : UILabel?
    var publishYearMax : UILabel?
    var ratingLabel : UILabel?
    var fourSquareRatingLabel : UILabel?
    var ratingMin : UILabel?
    var ratingMax : UILabel?
    var forSquareMin : UILabel?
    var forSquareMax : UILabel?
    var objBooksFilters:RestuarantFilterModel = RestuarantFilterModel()
    var objFeedListRequest:FeedListRequest = FeedListRequest()
    
    
    //MARK:-IBOutlets.
    @IBOutlet weak var topView: UIView!
    @IBOutlet weak var booksRatingTableView: UITableView!
    @IBOutlet weak var viewTappbleArea: UIView!
    @IBOutlet weak var filterCountLabel: UILabel!
   
    //MARK:-ViewLifeCycle.
    //MARK:-ViewDidLoad.
    override func viewDidLoad() {
        super.viewDidLoad()
        RestuarantFilterModel.filterCount = 0
        RestuarantFilterModel.filterCountKey = 0
        let tap = UITapGestureRecognizer(target: self, action: #selector(showPopupOnDismiss))
        viewTappbleArea.addGestureRecognizer(tap)
        
        if RestuarantFilterModel.objFeedListRequest.rating == "" {
            if category == Constant.CategoryTypeBackend.books.rawValue {
                RestuarantFilterModel.bookSharedInstance.rating = ["1.0","5.0"]
            }else{
                RestuarantFilterModel.bookSharedInstance.rating = ["1.0","10.0"]
            }
        }
        if RestuarantFilterModel.objFeedListRequest.friendspireRating == ""{
            RestuarantFilterModel.bookSharedInstance.fourSquareRating = ["1.0","5.0"]
        }
        objBooksFilters = RestuarantFilterModel.bookSharedInstance
        objFeedListRequest = RestuarantFilterModel.objFeedListRequest
        booksRatingTableView.tableFooterView = UIView()
        self.booksRatingTableView.register(UITableViewCell.self, forCellReuseIdentifier: "cell")
        booksRatingTableView.register(UINib(nibName: Constant.Xib.doubleSliderTableCell, bundle: nil), forCellReuseIdentifier: Constant.TableviewCellIdentifier.doubleSliderTableCell)
        booksRatingTableView.register(UINib(nibName: Constant.Xib.singleSliderTableCell, bundle: nil), forCellReuseIdentifier: Constant.TableviewCellIdentifier.singleSliderTableCell)
        booksRatingTableView.estimatedSectionHeaderHeight = 50
        addGestureToViewToDismissPresentController()
        if category == Constant.CategoryTypeBackend.books.rawValue{
            strThirdParty = "Goodreads"
        }else {
            sectionHeading.remove(at: 0)
            sectionHeading.insert("Duration", at: 0)
        }
        
    }
    
    //MARK:-ViewWillAppear.
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        setFilerCount()
        CuisinesViewController.delegate = self
        BookFilterHeaderView.delegate = self
    }
    
    //MARK:-Private Functions.
    //MARK:-AddGesture to View.
    func addGestureToViewToDismissPresentController() {
        let swipeDown = UISwipeGestureRecognizer.init(target: self, action: #selector(showPopupOnDismiss))
        swipeDown.direction = .down
        self.topView.addGestureRecognizer(swipeDown)
    }
    
    @objc fileprivate func hideCurrentView(){
        self.dismiss(animated: true, completion: nil)
    }
    
    //MARK:-IBActions.
    @IBAction func resetButtonAction(_ sender: UIButton) {
        Utility.resetFilter()
        if category == Constant.CategoryTypeBackend.books.rawValue {
            RestuarantFilterModel.bookSharedInstance.rating = ["1.0","5.0"]
        }else{
            RestuarantFilterModel.bookSharedInstance.rating = ["1.0","10.0"]
        }
        RestuarantFilterModel.bookSharedInstance.fourSquareRating = ["1.0","5.0"]
        setFilerCount()
        booksRatingTableView.reloadData()
    }
    
    @IBAction func doneButtonAction(_ sender: UIButton) {
        doneButtonAction()
    }
    
    func setPagesFilter() {
        RestuarantFilterModel.bookSharedInstance.pages.removeAll()
        if category == Constant.CategoryTypeBackend.tv.rawValue || category == Constant.CategoryTypeBackend.movie.rawValue {
            for (_,data) in  RestuarantFilterModel.bookSharedInstance.selectedPages.enumerated() {
                switch data {
                case 0:
                    RestuarantFilterModel.bookSharedInstance.pages.append("[1,30]")
                case 1:
                    RestuarantFilterModel.bookSharedInstance.pages.append("[30,60]")
                case 2:
                    RestuarantFilterModel.bookSharedInstance.pages.append("[60,90]")
                case 3:
                    RestuarantFilterModel.bookSharedInstance.pages.append("[90,120]")
                case 4:
                    RestuarantFilterModel.bookSharedInstance.pages.append("[120]")
                default:
                    break
                }
            }
            RestuarantFilterModel.objFeedListRequest.duration = "[\(RestuarantFilterModel.bookSharedInstance.pages.joined(separator: ","))]"
        }else{
            for (_,data) in  RestuarantFilterModel.bookSharedInstance.selectedPages.enumerated() {
                switch data {
                case 0:
                    RestuarantFilterModel.bookSharedInstance.pages.append("[1,250]")
                case 1:
                    RestuarantFilterModel.bookSharedInstance.pages.append("[250,500]")
                case 2:
                    RestuarantFilterModel.bookSharedInstance.pages.append("[500,750]")
                case 3:
                    RestuarantFilterModel.bookSharedInstance.pages.append("[750,1000]")
                case 4:
                    RestuarantFilterModel.bookSharedInstance.pages.append("[1000]")
                default:
                    break
                }
                RestuarantFilterModel.objFeedListRequest.pages = "[\(RestuarantFilterModel.bookSharedInstance.pages.joined(separator: ","))]"
            }
            
        }
    }
    func doneButtonAction() {
        let objState = Utility.compareBooksFilterObject(obj1: RestuarantFilterModel.bookSharedInstance, obj2:  self.objBooksFilters)
        if objState == true {
            self.hideCurrentView()
            return
        }
        _ = RestuarantFilterModel.objFeedListRequest.getDictonary()
        RestuarantFilterModel.filterCount = RestuarantFilterModel.filterCountKey
        delegate?.filterApplied( )
        hideCurrentView()
    }
    
    func showPopupOnDismiss() {
        let objState = Utility.compareBooksFilterObject(obj1: RestuarantFilterModel.bookSharedInstance, obj2:  self.objBooksFilters)
        if objState == true {
            self.hideCurrentView()
            return
        }
        self.showAlert(withTitle: "Filter", message: "Would you like to save your changes?", okayTitle: "Save", cancelTitle: "No", okCall: {
            self.doneButtonAction()
        }) {
            RestuarantFilterModel.bookSharedInstance = self.objBooksFilters
            RestuarantFilterModel.objFeedListRequest = self.objFeedListRequest
            self.hideCurrentView()
        }
    }
    func setFilerCount() {
        _ = RestuarantFilterModel.objFeedListRequest.getDictonary()
        RestuarantFilterModel.filterCount = RestuarantFilterModel.filterCountKey
        if RestuarantFilterModel.filterCount == 0 {
            filterCountLabel.text = ""
            filterCountLabel.isHidden = true
        }else {
            filterCountLabel.isHidden = false
            filterCountLabel.text = "\(RestuarantFilterModel.filterCount)"
        }
        RestuarantFilterModel.filterCountKey = 0
    }
}
//MARK:-Prortocol to activate Selected Header.
extension BooksFiltersViewController: BookFilterHeaderViewProtocol {
    func setActiveHaederCell(section: Int) {
        if selectedSection == section {
            selectedSection = 10
        }else if section == 2 {
            let controller: CuisinesViewController = UIStoryboard(storyboard: .filter).initVC()
            controller.type = category ?? 1
            controller.isGener = true
            controller.modalTransitionStyle = .crossDissolve
            controller.modalPresentationStyle = .overCurrentContext
            self.present(controller, animated: true, completion: nil)
        }else{
            selectedSection = section
        }
        if selectedSection != 10{
            booksRatingTableView.reloadSections([selectedSection], with: .none)
        }
    }
}

//MARK:-Delegate Set Selected Cusisines.
extension BooksFiltersViewController : SetCuisines {
    func reloadTableView(sectionToLoad: Int) {
        if sectionToLoad == 3 {
            if RestuarantFilterModel.bookSharedInstance.publishYear[0] == "1950" {
                publishYearMin?.text = "1950 And Before"
            }else{
                publishYearMin?.text = "\(RestuarantFilterModel.bookSharedInstance.publishYear[0])"
            }
            publishYearMax?.text = "\(RestuarantFilterModel.bookSharedInstance.publishYear[1])"
            publishYearHeadingLabel?.text = "Start Year:\(RestuarantFilterModel.bookSharedInstance.publishYear[0]), End Year:\(RestuarantFilterModel.bookSharedInstance.publishYear[1])"
            setFilerCount()
            return
        }else if sectionToLoad == 4 {
            UIView.setAnimationsEnabled(false)
            booksRatingTableView.beginUpdates()
            ratingLabel?.text = "\(strThirdParty): \(RestuarantFilterModel.bookSharedInstance.rating[0]) - \(RestuarantFilterModel.bookSharedInstance.rating[1])"
            fourSquareRatingLabel?.text = "Friendspire Rating: \(RestuarantFilterModel.bookSharedInstance.fourSquareRating[0]) - \(RestuarantFilterModel.bookSharedInstance.fourSquareRating[1])"
            ratingMax?.text = "\(RestuarantFilterModel.bookSharedInstance.rating[1])"
            ratingMin?.text = "\(RestuarantFilterModel.bookSharedInstance.rating[0])"
            forSquareMin?.text = "\(RestuarantFilterModel.bookSharedInstance.fourSquareRating[0])"
            forSquareMax?.text = "\(RestuarantFilterModel.bookSharedInstance.fourSquareRating[1])"
            booksRatingTableView.endUpdates()
            UIView.setAnimationsEnabled(true)
            setFilerCount()
            return
        }
        booksRatingTableView.reloadSections([sectionToLoad], with: .none)
        setFilerCount()
    }
    
}

//MARK:-UITabelViewDelegate&UITabelViewDataSource.
extension BooksFiltersViewController : UITableViewDelegate , UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return sectionHeading.count
        
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        switch section {
        case 1:
            if category == Constant.CategoryTypeBackend.tv.rawValue || category == Constant.CategoryTypeBackend.movie.rawValue {
                return moviesDuration.count
            }else{
                return booksPagesCount.count
            }
        case 3,4:
            return 1
        default:
            return 0
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        switch indexPath.section {
        case 1:
            let cell:UITableViewCell = booksRatingTableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as UITableViewCell
            cell.selectionStyle = .none
            cell.textLabel?.font = UIFont(name: "sf-ui-text-regular", size: 13)
            cell.textLabel?.textColor = .darkGray
            cell.backgroundColor = #colorLiteral(red: 0.9019607843, green: 0.9254901961, blue: 0.9607843137, alpha: 1)
            cell.contentView.backgroundColor = .clear
            if indexPath.section == 1 {
                if category == Constant.CategoryTypeBackend.tv.rawValue || category == Constant.CategoryTypeBackend.movie.rawValue {
                    cell.textLabel?.text = moviesDuration[indexPath.row]
                }else{
                    cell.textLabel?.text = booksPagesCount[indexPath.row]
                }
                if RestuarantFilterModel.bookSharedInstance.selectedPages.contains(indexPath.row) {
                    cell.accessoryType = .checkmark
                }else{
                    cell.accessoryType = .none
                }
            }
            return cell
        case 3:
            guard let cell = booksRatingTableView.dequeueReusableCell(withIdentifier: Constant.TableviewCellIdentifier.singleSliderTableCell, for: indexPath) as? SingleSliderTableCell else {
                return UITableViewCell()
            }
            cell.backgroundColor = #colorLiteral(red: 0.9019607843, green: 0.9254901961, blue: 0.9607843137, alpha: 1)
            cell.contentView.backgroundColor = .clear
            cell.rangeSlider.minimumValue = 1950
            cell.rangeSlider.maximumValue = Double(Date().string().convertDate(.preDefined, toFormat: .year)) ?? 2000
            cell.rangeSlider.tag = 11
            if RestuarantFilterModel.bookSharedInstance.publishYear[0] == "1950" {
                cell.minimumRangeLabel.text = "1950 And Before"
            }else{
                cell.minimumRangeLabel.text = "\(RestuarantFilterModel.bookSharedInstance.publishYear[0])"
            }
            publishYearMax = cell.maximumRangeLabel
            publishYearMin = cell.minimumRangeLabel
            cell.maximumRangeLabel.text = "\(RestuarantFilterModel.bookSharedInstance.publishYear[1])"
            cell.rangeSlider.lowerValue = Double(RestuarantFilterModel.bookSharedInstance.publishYear[0]) ?? cell.rangeSlider.minimumValue
            cell.rangeSlider.upperValue = Double(RestuarantFilterModel.bookSharedInstance.publishYear[1]) ?? cell.rangeSlider.maximumValue
            return cell
        case 4:
            guard let cell = booksRatingTableView.dequeueReusableCell(withIdentifier: Constant.TableviewCellIdentifier.doubleSliderTableCell, for: indexPath) as? DoubleSliderTableCell else {
                return UITableViewCell()
            }
            cell.backgroundColor = #colorLiteral(red: 0.9019607843, green: 0.9254901961, blue: 0.9607843137, alpha: 1)
            cell.contentView.backgroundColor = .clear
            cell.rangeSlider.tag = 11
            cell.fourSquareRangeSlider.tag = 11
            if category == Constant.CategoryTypeBackend.books.rawValue {
                cell.rangeSlider.minimumValue = 1.0
                cell.rangeSlider.maximumValue = 5.0
            }else{
                cell.rangeSlider.minimumValue = 1.0
                cell.rangeSlider.maximumValue = 10.0
            }
            cell.fourSquareRangeSlider.minimumValue = 1.0
            cell.fourSquareRangeSlider.maximumValue = 5.0
            cell.minimumRangeLabel.text = "\(RestuarantFilterModel.bookSharedInstance.rating[0])"
            cell.maximumRangeLabel.text = "\(RestuarantFilterModel.bookSharedInstance.rating[1])"
            cell.rangeSlider.lowerValue = Double(RestuarantFilterModel.bookSharedInstance.rating[0]) ?? cell.rangeSlider.minimumValue
            cell.rangeSlider.upperValue = Double(RestuarantFilterModel.bookSharedInstance.rating[1]) ?? cell.rangeSlider.maximumValue
            cell.headingLabel.text = "\(strThirdParty): \(RestuarantFilterModel.bookSharedInstance.rating[0]) - \(RestuarantFilterModel.bookSharedInstance.rating[1])"
            cell.fourSquareMinimumRangeLabel.text = "\(RestuarantFilterModel.bookSharedInstance.fourSquareRating[0])"
            cell.fourSquareMaximumRangeLabel.text = "\(RestuarantFilterModel.bookSharedInstance.fourSquareRating[1])"
            cell.fourSquareRangeSlider.lowerValue = Double(RestuarantFilterModel.bookSharedInstance.fourSquareRating[0]) ?? cell.fourSquareRangeSlider.minimumValue
            cell.fourSquareRangeSlider.upperValue = Double(RestuarantFilterModel.bookSharedInstance.fourSquareRating[1]) ?? cell.fourSquareRangeSlider.maximumValue
            cell.fourSquareHeadingLabel.text =  "Friendspire Rating: \(RestuarantFilterModel.bookSharedInstance.fourSquareRating[0]) - \(RestuarantFilterModel.bookSharedInstance.fourSquareRating[1])"
            fourSquareRatingLabel = cell.fourSquareHeadingLabel
            ratingLabel = cell.headingLabel
            ratingMax = cell.maximumRangeLabel
            ratingMin = cell.minimumRangeLabel
            forSquareMax =  cell.fourSquareMaximumRangeLabel
            forSquareMin = cell.fourSquareMinimumRangeLabel
            return cell
        default:
            return UITableViewCell()
        }
        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        switch indexPath.section {
        case 1:
            if RestuarantFilterModel.bookSharedInstance.selectedPages.contains(indexPath.row) {
                for (index,data) in  RestuarantFilterModel.bookSharedInstance.selectedPages.enumerated() {
                    if data == indexPath.row {
                        RestuarantFilterModel.bookSharedInstance.selectedPages.remove(at: index)
                    }
                }
            }else{
                RestuarantFilterModel.bookSharedInstance.selectedPages.append(indexPath.row)
            }
            RestuarantFilterModel.bookSharedInstance.selectedPages.sort()
            setPagesFilter()
            booksRatingTableView.reloadSections([indexPath.section], with: .none)
        default:
            break
        }
        setFilerCount()
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        if section == 2 {
        //    if category == Constant.CategoryTypeBackend.books.rawValue{
                return 0
         //   }
        }
        if comingFromLibrary {
            if category == Constant.CategoryTypeBackend.tv.rawValue || category == Constant.CategoryTypeBackend.movie.rawValue || category == Constant.CategoryTypeBackend.books.rawValue {
                if section == 0 {
                    return 0
                }
            }
        }
        return UITableViewAutomaticDimension
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let nibView = Bundle.main.loadNibNamed(Constant.Xib.bookFilterHeaderView , owner: self, options: nil)?.first as?  BookFilterHeaderView
        nibView?.anyLabel.isHidden = true
        nibView?.headerActionButton.isUserInteractionEnabled = false
        nibView?.headerActionButton.tag = section
        nibView?.removeRatingSwitch.tag = section
        nibView?.removeRatingSwitch.isHidden = true
        nibView?.headerButton.isHidden = true
        if category == Constant.CategoryTypeBackend.tv.rawValue || category == Constant.CategoryTypeBackend.movie.rawValue {
            nibView?.headingLabel.text = movieSectionsBookHeading[section]
        }else{
            nibView?.headingLabel.text = sectionHeading[section]
        }
        nibView?.headerActionButton.tag = section
        nibView?.valueLabel.text = ""
        if selectedSection == section {
            nibView?.headerButton.setImage(#imageLiteral(resourceName: "upward"), for: .normal)
        }else{
            nibView?.headerButton.setImage(#imageLiteral(resourceName: "down"), for: .normal)
        }
        switch section {
        case 1:
            if category == Constant.CategoryTypeBackend.tv.rawValue || category == Constant.CategoryTypeBackend.movie.rawValue {
                nibView?.headingLabelWidthConstarint.constant = 73.50
            }else{
                nibView?.headingLabelWidthConstarint.constant = 53.50
            }
            var slectedPages = [String]()
            for(_,data) in RestuarantFilterModel.bookSharedInstance.selectedPages.enumerated() {
                if category == Constant.CategoryTypeBackend.tv.rawValue || category == Constant.CategoryTypeBackend.movie.rawValue {
                    slectedPages.append(moviesDuration[data])
                }else{
                    slectedPages.append(booksPagesCount[data])
                }
                
            }
            nibView?.valueLabel.text = slectedPages.joined(separator: ", ")
        case 3:
            if category == Constant.CategoryTypeBackend.tv.rawValue || category == Constant.CategoryTypeBackend.movie.rawValue {
                nibView?.headingLabelWidthConstarint.constant = 106.0
            }else{
                nibView?.headingLabelWidthConstarint.constant = 103.0
            }
            nibView?.valueLabel.text = "Start Year:\(RestuarantFilterModel.bookSharedInstance.publishYear[0]), End Year:\(RestuarantFilterModel.bookSharedInstance.publishYear[1])"
            publishYearHeadingLabel = nibView?.valueLabel
        case 4:
            nibView?.headingLabelWidthConstarint.constant = 56.5
            nibView?.valueLabel.text = ""
        case 2:
            nibView?.headerActionButton.isUserInteractionEnabled = true
            nibView?.headerButton.isHidden = false
            if RestuarantFilterModel.bookSharedInstance.genre.count == 0 {
                nibView?.anyLabel.isHidden = false
            }else{
                nibView?.anyLabel.isHidden = true
            }
            nibView?.headerButton.setImage(#imageLiteral(resourceName: "Disclosure Indicator"), for: .normal)
            nibView?.headingLabelWidthConstarint.constant = 53.0
            nibView?.valueLabel.text = RestuarantFilterModel.bookSharedInstance.genre.joined(separator: ", ")
        case 0:
            nibView?.headingLabelWidthConstarint.constant = 170.0
            nibView?.removeRatingSwitch.isHidden = false
            nibView?.removeRatingSwitch.setOn(RestuarantFilterModel.bookSharedInstance.showMyRated ?? false, animated: true)
        default:
            break
        }
        return nibView
    }
}

