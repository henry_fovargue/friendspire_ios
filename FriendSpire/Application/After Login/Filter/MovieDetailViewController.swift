//
//  MovieDetailViewController.swift
//  FriendSpire
//
//  Created by Harpreet_kaur on 07/08/18.
//  Copyright © 2018 openkey. All rights reserved.
//

import UIKit
import AlamofireImage

class MovieDetailViewController: DIBaseController {
    
    
    //MARK:- Variables.
    var isThirdPartySearchOn : Bool?
    var objMovieModelData = MovieModelData()
    var category:Int = 1
    var postId:String = ""
    var modalRef : FeedModal?
    var isComingFromLink:Bool = false
    var headings = ["Plot:","Actors:","Director:","Rated:","Writer:","Language:","Country:","Address:","Release:"]
    var booksHeading = ["Plot:","Year:","Language:","Publisher:","Pages:","ISBN:"]
    var openReplyFor: String?
    var openCommentTo: String?
    var fromAntherUserLibrary : Bool?
    
    //MARK:-IBOutlets.
    @IBOutlet weak var backview: UIView!
    @IBOutlet weak var navigationBarView: UIView!
    @IBOutlet weak var genreLabel: UILabel!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var movieDetailTabelView: UITableView!
    @IBOutlet weak var friendsRatingImageView: UIImageView!
    @IBOutlet weak var profileImageview: UIImageView!
    @IBOutlet weak var friendsRatingLabel: UILabel!
    @IBOutlet weak var everyoneRatingLabel: UILabel!
    @IBOutlet weak var fourSquareRatingLabel: UILabel!
    @IBOutlet weak var everyOneRatingImageView: UIImageView!
    @IBOutlet weak var lblThirdPartyName: UILabel!
    @IBOutlet weak var lblFriendReviewCount: UILabel!
    @IBOutlet weak var lblEveryoneReviewCount: UILabel!
    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var reviewLabel: UILabel!
    @IBOutlet weak var userImageView: UIImageView!
    @IBOutlet weak var bookmarkButton: UIButton!
    @IBOutlet weak var addRatingButton: UIButton!
    @IBOutlet weak var constraintWidthBookmark: NSLayoutConstraint!
    @IBOutlet weak var userRatingView: UIView!
    @IBOutlet weak var constraintWidthStackView: NSLayoutConstraint!
    @IBOutlet weak var btnReply: UIButton!
    
    @IBOutlet weak var imgViewCategory: UIImageView!
    
    //MARK:-ViewLifeCycle.
    override func viewDidLoad() {
        super.viewDidLoad()
        movieDetailTabelView.estimatedSectionHeaderHeight = 100
        changeCategoryColor()
        
        if isThirdPartySearchOn == true{
            showLoader()
        }else {
            showLoader()
            getMovieDeatilsFromServer()
        }
        movieDetailTabelView.register(UINib(nibName: Constant.Xib.reviewsTabelCellXib, bundle: nil), forCellReuseIdentifier: Constant.TableviewCellIdentifier.reviewsTabelCellXib)
        movieDetailTabelView.register(UINib(nibName: Constant.Xib.emptyTableCell, bundle: nil), forCellReuseIdentifier: Constant.TableviewCellIdentifier.emptyTableCell)
        
        lblThirdPartyName.text = "IMDB"
        if category == Constant.CategoryTypeBackend.books.rawValue{
            lblThirdPartyName.text = "GoodReads"
        }
        
        if openReplyFor != nil{
            self.perform(#selector(presentReviewController(recommendationId:)), with: openReplyFor, afterDelay: 0.3)
        }//show Tutorial screen to user
        //        else if TutorialStatus.getCurrentStatus() == nil ||  TutorialStatus.getCurrentStatus()?.detailScreen == nil{
        //            self.perform(#selector(showTutorialPopup(arr:)), with: [#imageLiteral(resourceName: "layer10")], afterDelay: 0.3)
        //            updateTutorialReadStatus()
        //        }
        
        
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(true)
        self.hideLoader()
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        //        UserReviewHeader.delegate = self
        //        ReviewsTabelCellXib.delegate = self
    }
    
    override func viewWillLayoutSubviews() {
        super.viewWillLayoutSubviews()
    }
    
    //MARK:-IBActions.
    @IBAction func shareButtonAction(_ sender: UIButton) {
        self.shareData(id: objMovieModelData.id, type: category, notificationType: 1, title: objMovieModelData.title ?? "")
    }
    @IBAction func bookmarkButtonAction(_ sender: UIButton) {
        addToLibrary(slected: objMovieModelData.isBookMarked ?? false)
    }
    @IBAction func addRatingButtonAction(_ sender: UIButton) {
        openRatingPopup()
    }
    
    //MARK:-View All Ratings Button Action.
    @IBAction func viewAllButtonAction(_ sender: UIButton) {
        let controller: ReviewsViewController = UIStoryboard(storyboard: .inspiration).initVC()
//        controller.modalPresentationStyle = .overCurrentContext
//        controller.modalTransitionStyle = .crossDissolve
controller.colorToUse = self.colorToUse
        controller.category = self.category
        controller.postId = self.postId
        controller.delegate = self
        self.navigationController?.pushViewController(controller, animated: true)

//        AppDelegate.shared().window?.rootViewController?.present(controller, animated: true, completion: nil)
    }
    
    //MARK:-Back Button Action.
    @IBAction func backButtonAction(_ sender: UIButton) {
        if isComingFromLink {
            appDelegate.navigateToDashboard()
        }else{
            self.navigationController?.popViewController(animated: true)
        }
    }
    
    @IBAction func replyButtonTapped(_ sender: UIButton) {
        if let recommendationId = objMovieModelData.userRecommendationId{
            presentReviewController(recommendationId: recommendationId)
        }
    }
    
    //MARK:-PrivateFunction.    
    //    private func updateTutorialReadStatus(){
    //
    //        if let readStatus = TutorialStatus.getCurrentStatus(){
    //            readStatus.detailScreen = true
    //            TutorialStatus.saveCurrentStatus(status: readStatus)
    //        }else{
    //            let status = TutorialStatus.sharedInstance
    //            status.detailScreen = true
    //            TutorialStatus.saveCurrentStatus(status: status)
    //        }
    //    }
    
    //MARK:-Get Data From Api.
    func getMovieDeatilsFromServer() {
        if !Utility.isConnectedToInternet {
            self.hideLoader()
            self.showAlert(withTitle: appName.capitalized, message:  AppMessages.ErrorAlerts.noInternet, okayTitle: AppMessages.AlertTitles.Ok, okCall: {
                self.navigationController?.popViewController(animated: true)
            })
            //            self.showAlert( message: AppMessages.ErrorAlerts.noInternet)
            return
        }
        
        DIWebLayerCategoriesDetailApi().getCategoryDeatils(category: self.category, postID: self.postId, success: {[weak self] (data) in
            self?.hideLoader()
            self?.objMovieModelData = data
            self?.setData()
        }) { [weak self](error) in
            
            self?.hideLoader()
            self?.showAlert(withTitle: appName.capitalized, message: error.message, okayTitle: AppMessages.AlertTitles.Ok, okCall: {
                self?.navigationController?.popViewController(animated: true)
            })
        }
    }
    
    func addToLibrary(slected:Bool) {
        var ratingModal = RatingRequest()
        ratingModal.referenceid = postId
        ratingModal.type = category
        objMovieModelData.isBookMarked = !(objMovieModelData.isBookMarked ?? false)
        
        if slected == false { // addBookmark
            showLoader()
            self.addBookmark(modal: ratingModal) { (finish) in
                self.hideLoader()
                if finish == false{
                    self.objMovieModelData.isBookMarked = false  // failure, change back to previous state
                    // store for local change
                    self.modalRef?.isBookmarked = false
                    self.modalRef?.myBookmarkStatus = 0
                }else{
                    self.objMovieModelData.isBookMarked = true
                    self.modalRef?.isBookmarked = true
                    self.modalRef?.myBookmarkStatus = 1
                }
                self.bookmarkButton.isSelected = self.objMovieModelData.isBookMarked ?? false
                self.movieDetailTabelView.reloadData()
            }
        }else{//remove bookmark
            self.showAlert(withTitle: appName.capitalized, message: AppMessages.AlertTitles.removeBookMark, okayTitle: AppMessages.AlertTitles.Yes, cancelTitle: AppMessages.AlertTitles.No, okCall: {
                self.showLoader()
                self.removeBookmark(modal: ratingModal) { (finish) in
                    self.hideLoader()
                    if finish == false{
                        self.objMovieModelData.isBookMarked = true //failure, change back to previous state
                        // store for local change
                        self.modalRef?.isBookmarked = true
                        self.modalRef?.myBookmarkStatus = 1
                    }else{
                        self.objMovieModelData.isBookMarked = false
                        self.modalRef?.isBookmarked = false
                        self.modalRef?.myBookmarkStatus = 0
                    }
                    self.bookmarkButton.isSelected = self.objMovieModelData.isBookMarked ?? false
                    self.movieDetailTabelView.reloadData()
                }
            }) {
                //Nothing to do
                //  sender.isSelected = !sender.isSelected
                // store for local change
                self.modalRef?.isBookmarked = slected
                self.modalRef?.myBookmarkStatus = slected == true ? 1 : 0
            }
        }
    }
    
    func openRatingPopup() {
        self.addRating(using:category, referenceID: postId,modalRef,fromAntherUserLibrary)
    }
    
    //MARK:-SetData to UIComponent.
    func setData() {
        setUserRatingViewData()
        //banner image
        if objMovieModelData.image.count >= 1 {
            if let imageUrl = URL.init(string: objMovieModelData.image[0] ) {
                profileImageview.af_setImage(withURL: imageUrl, placeholderImage: #imageLiteral(resourceName: "user") , filter: nil, progress: nil, imageTransition: .crossDissolve(0.7), runImageTransitionIfCached: false, completion: nil)
            }
        }
        
        // subtitle on banner set data
        titleLabel.text =   (objMovieModelData.title ?? "")
        var strBegin = "NA | "
        if objMovieModelData.year != nil{
            strBegin = "\(objMovieModelData.year ?? 1998) | "
        }
        if category == Constant.CategoryTypeBackend.tv.rawValue{
            if let year = objMovieModelData.startYear {
                strBegin = "\(year) -"
                if let year2 = objMovieModelData.endYear {
                    if year2 > 0{
                        strBegin.append(" \(year2) | ")
                    }else{
                        strBegin.append(" | ")
                    }
                }else{
                    strBegin.append(" | ")
                }
            }
        }
        
        profileImageview.contentMode = UIViewContentMode.scaleAspectFill // for movie and tv
        if category == Constant.CategoryTypeBackend.books.rawValue{
            strBegin = "\(objMovieModelData.author ?? "") | "
            let year =  objMovieModelData.year ?? 0
            let strBegin = strBegin + (year == 0 ? "" : "\(year)")
            genreLabel.text = strBegin
        }
        if category == Constant.CategoryTypeBackend.tv.rawValue || category == Constant.CategoryTypeBackend.movie.rawValue {
            strBegin = strBegin + Utility.getModifiedJoinString(using: objMovieModelData.genre) + " | "
            genreLabel.text =  strBegin + Utility.minutesToHoursMinutes(minutes: objMovieModelData.pagesDuration ?? 0)
        }
        //ramaining rating of post
        friendsRatingLabel.text = String(format: "%.1f", arguments: [objMovieModelData.friendsRating ?? 0.0]).removeZero()
        everyoneRatingLabel.text = String(format: "%.1f", arguments: [objMovieModelData.friendspireRating ?? 0.0]).removeZero()
        fourSquareRatingLabel.text = String(format: "%.1f", arguments: [objMovieModelData.rating ?? 0.0]).removeZero()
        friendsRatingImageView.image = Utility.getEmojiAccordindToRating(rating: objMovieModelData.friendsRating ?? 0.0)
        everyOneRatingImageView.image = Utility.getEmojiAccordindToRating(rating: objMovieModelData.friendspireRating ?? 0.0)
        lblFriendReviewCount.text = objMovieModelData.countFriends?.countFormationWithoutReview
        lblEveryoneReviewCount.text = objMovieModelData.everyoneCount?.countFormationWithoutReview
        movieDetailTabelView.reloadData()
        self.perform(#selector(sizeHeaderToFit), with: self, afterDelay: 0.2)
        
    }
    
    func setUserRatingViewData() {
        let tap = UITapGestureRecognizer(target: self, action: #selector(redirectToUserProfile))
        userImageView?.addGestureRecognizer(tap)
        let tapReview = UITapGestureRecognizer(target: self, action: #selector(openRatingPopup))
        reviewLabel.addGestureRecognizer(tapReview)
        if let imageUrl = URL.init(string: User.sharedInstance.profilePicUrl ?? "") {
            userImageView.af_setImage(withURL: imageUrl, placeholderImage: #imageLiteral(resourceName: "user") , filter: nil, progress: nil, imageTransition: .crossDissolve(0.7), runImageTransitionIfCached: false, completion: nil)
        }
        dateLabel.text = objMovieModelData.userUpdatedAt?.convertDateLocal(.serverFormat, toFormat: .reviewsDate)
        bookmarkButton.isSelected = objMovieModelData.isBookMarked ?? false
        if objMovieModelData.isBookMarked == true {
            bookmarkButton.setImage(#imageLiteral(resourceName: "bookmark-black-shape-active"), for: .normal)
        }else{
            bookmarkButton.setImage(#imageLiteral(resourceName: "bookmark-black-shape"), for: .normal)
        }
        let color = Utility.getColor(using: Utility.getBackendTypeString(using: category))
        addRatingButton.backgroundColor = color
        if objMovieModelData.userRating == 0.0 {
            addRatingButton.setImage(#imageLiteral(resourceName: "add -neq-white"), for: .normal)
            bookmarkButton.isHidden = false
            constraintWidthBookmark.constant = 40
            constraintWidthStackView.constant = 110
            reviewLabel.text = "Click + to leave a review"
            reviewLabel.isUserInteractionEnabled = false
            btnReply.isHidden = true
        }else{
            let img = Utility.getEmojiAccordindToRating(rating: objMovieModelData.userRating ?? 0.0)
            addRatingButton.setImage(Utility.resizeImage(image: img, newWidth: 20.0), for: .normal)
            bookmarkButton.isHidden = true
            constraintWidthBookmark.constant = 0
            constraintWidthStackView.constant = 70
            if objMovieModelData.userReview != nil{
                reviewLabel.isUserInteractionEnabled = true
                reviewLabel.text = objMovieModelData.userReview  == "" ? "You have not left a comment. " : objMovieModelData.userReview
            }else{
                reviewLabel.isUserInteractionEnabled = false
            }
            btnReply.isHidden = false
            let count = objMovieModelData.userCommentCount ?? 0 > 0 ? "(\(objMovieModelData.userCommentCount ?? 0))" : ""
            
            if objMovieModelData.userCommentCount ?? 0 > 1 {
                btnReply.setTitle("Comments \(count)", for: .normal)
            }else{
                btnReply.setTitle("Comment \(count)", for: .normal)
            }
            
        }
    }
    
    
    /// This will compress the size of header according to content
    func sizeHeaderToFit() {
        let headerView = movieDetailTabelView.tableHeaderView!
        headerView.setNeedsLayout()
        headerView.layoutIfNeeded()
        var frame = headerView.frame
        frame.size.height = headerView.systemLayoutSizeFitting(UILayoutFittingCompressedSize).height
        headerView.frame = frame
        movieDetailTabelView.tableHeaderView = headerView
    }
    
    
    private func changeCategoryColor(){
        
        let strCategory = Utility.getBackendTypeString(using: category)
        let color = Utility.getColor(using: strCategory)
        backview.backgroundColor = color
        navigationBarView.backgroundColor = color
        lblEveryoneReviewCount.textColor = color
        lblFriendReviewCount.textColor = color
        imgViewCategory.image = Utility.getIcon(using: strCategory)
        imgViewCategory.backgroundColor = color
        self.colorToUse = color
    }
    
    func redirectToUserProfile() {
        let  controller: UserProfileViewController = UIStoryboard.init(storyboard: .networks).initVC()
        controller.userId =  User.sharedInstance.id
        self.navigationController?.pushViewController(controller, animated: true)
    }
    
    func reportReview(indexPath:IndexPath){
        if (objMovieModelData.reviews[indexPath.row].userId?.id ?? "") == User.sharedInstance.id{
            self.showAlert(message:"You can't report your own review.".localized)
            return
        }
        self.showLoader()
        let params = ["recommendationid":objMovieModelData.reviews[indexPath.row].id]
        DIWebLayerCategoriesDetailApi().reportReview(parameter: params , success: { (value) in
            self.hideLoader()
            let message = value["message"] as? String ?? ""
            self.showAlert(message:message)
        }) { (error) in
            self.hideLoader()
            self.showAlert(message:error.message)
        }
    }
}




//MARK:-UITabelViewDelegate&UITabelViewDataSource.
extension MovieDetailViewController : UITableViewDelegate , UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return 2
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section == 0 {
            if objMovieModelData.reviews.count == 0 {
                return 0
            }
            
            return objMovieModelData.reviews.count == 6 ? 5 : objMovieModelData.reviews.count
        }else{
            if category == 5 {
                return booksHeading.count
            }
            return headings.count
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.section == 0 {
//            tableView.separatorStyle = .singleLineEtched
            if objMovieModelData.reviews.count == 0 {
                guard let cell = movieDetailTabelView.dequeueReusableCell(withIdentifier: Constant.TableviewCellIdentifier.emptyTableCell, for: indexPath) as? EmptyTableCell else {
                    return UITableViewCell()
                }
                return cell
            }
            guard let cell = movieDetailTabelView.dequeueReusableCell(withIdentifier: Constant.TableviewCellIdentifier.reviewsTabelCellXib, for: indexPath) as? ReviewsTabelCellXib else {
                return UITableViewCell()
            }
            cell.userImageView.tag = indexPath.row
            cell.userNameLabel.tag = indexPath.row
            cell.contentView.backgroundColor = .white
            cell.setData(reviewsData: objMovieModelData.reviews[indexPath.row])
            cell.btnReply.tag = indexPath.row
            cell.btnReply.addTarget(self, action: #selector(replyOnReview), for: .touchUpInside)
            cell.delegate = self
            return cell
        }else{
//            tableView.separatorStyle = .none
            guard let cell = movieDetailTabelView.dequeueReusableCell(withIdentifier: Constant.TableviewCellIdentifier.movieTableCell, for: indexPath) as? MovieTableCell else {
                return UITableViewCell()
            }
            if category == 5 {
                cell.itemLabel.text = booksHeading[indexPath.row]
                cell.setBookData(index: indexPath.row, objMovieModelData: self.objMovieModelData)
            }else{
                cell.itemLabel.text = headings[indexPath.row]
                cell.setData(index: indexPath.row, objMovieModelData: self.objMovieModelData)
                if category == Constant.CategoryTypeBackend.tv.rawValue  || category == Constant.CategoryTypeBackend.movie.rawValue    {
                    if indexPath.section == 1 {
                        if indexPath.row == 7 {
                            cell.itemLabel.text = "Awards:"
                            cell.valueLabel.text =  objMovieModelData.awards != "" ? objMovieModelData.awards : "NA"
                        }
                    }
                }
            }
            return cell
        }
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        if section == 1 {
            return UIView()
        }
        let nibView = Bundle.main.loadNibNamed("UserReviewHeader" , owner: self, options: nil)?.first as? UserReviewHeader
        nibView?.delegate = self
        let tap = UITapGestureRecognizer(target: self, action: #selector(redirectToUserProfile))
        nibView?.imgViewUser.addGestureRecognizer(tap)
        let tapReview = UITapGestureRecognizer(target: self, action: #selector(openRatingPopup))
        nibView?.lblReviews.addGestureRecognizer(tapReview)
        if let imageUrl = URL.init(string: User.sharedInstance.profilePicUrl ?? "") {
            nibView?.imgViewUser.af_setImage(withURL: imageUrl, placeholderImage: #imageLiteral(resourceName: "user") , filter: nil, progress: nil, imageTransition: .crossDissolve(0.7), runImageTransitionIfCached: false, completion: nil)
        }
        nibView?.lblName.text = objMovieModelData.userUpdatedAt?.convertDateLocal(.serverFormat, toFormat: .reviewsDate)
        nibView?.bookmarkButton.isSelected = objMovieModelData.isBookMarked ?? false
        if objMovieModelData.isBookMarked == true {
            nibView?.bookmarkButton.setImage(#imageLiteral(resourceName: "bookmark-black-shape-active"), for: .normal)
        }else{
            nibView?.bookmarkButton.setImage(#imageLiteral(resourceName: "bookmark-black-shape"), for: .normal)
        }
        let color = Utility.getColor(using: Utility.getBackendTypeString(using: category))
        nibView?.addRatingButton.backgroundColor = color
        if objMovieModelData.userRating == 0.0 {
            nibView?.addRatingButton.setImage(#imageLiteral(resourceName: "add -neq-white"), for: .normal)
            nibView?.bookmarkButton.isHidden = false
            nibView?.constraintWidthBookmark.constant = 40
            nibView?.constraintWidthStackView.constant = 110
            nibView?.lblReviews.text = "Click + to leave a review"
            nibView?.lblReviews.isUserInteractionEnabled = false
            nibView?.btnReply.isHidden = true
        }else{
            let img = Utility.getEmojiAccordindToRating(rating: objMovieModelData.userRating ?? 0.0)
            nibView?.addRatingButton.setImage(Utility.resizeImage(image: img, newWidth: 20.0), for: .normal)
            nibView?.bookmarkButton.isHidden = true
            nibView?.constraintWidthBookmark.constant = 0
            nibView?.constraintWidthStackView.constant = 70
            if objMovieModelData.userReview != nil{
                nibView?.lblReviews.isUserInteractionEnabled = true
                nibView?.lblReviews.text = objMovieModelData.userReview  == "" ? "You have not left a comment. " : objMovieModelData.userReview
            }else{
                nibView?.lblReviews.isUserInteractionEnabled = false
            }
            nibView?.btnReply.isHidden = false
            let count = objMovieModelData.userCommentCount ?? 0 > 0 ? "(\(objMovieModelData.userCommentCount ?? 0))" : ""
            
            if objMovieModelData.userCommentCount ?? 0 > 1 {
                nibView?.btnReply.setTitle("Comments \(count)", for: .normal)
            }else{
                nibView?.btnReply.setTitle("Comment \(count)", for: .normal)
            }
        }
        nibView?.btnReply.addTarget(self, action: #selector(replyButtonTapped(_:)), for: .touchUpInside)
        
        if objMovieModelData.reviews.count <= 5 {
            nibView?.viewAllButton.isHidden = true
            nibView?.viewAllButton.isUserInteractionEnabled = false
        }else {
            nibView?.viewAllButton.isHidden = false
            nibView?.viewAllButton.isUserInteractionEnabled = true
        }
        if objMovieModelData.reviews.count == 0 {
            nibView?.lblNoReview.isHidden = false
        }else{
            nibView?.lblNoReview.isHidden = true
        }
        
        return nibView
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        if section == 1 {
            return 0
        }
        return UITableViewAutomaticDimension
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.section == 1 {
            if category != 5 {
                switch indexPath.row {
                case 0:
                    if objMovieModelData.descriptionField == "" || objMovieModelData.descriptionField == "NA" || objMovieModelData.descriptionField == nil {
                        return 0
                    }
                case 1:
                    if objMovieModelData.actors == "" || objMovieModelData.actors == "NA" || objMovieModelData.actors == nil {
                        return 0
                    }
                case 2:
                    if objMovieModelData.director == "" || objMovieModelData.director == "NA" || objMovieModelData.director == nil {
                        return 0
                    }
                case 3:
                    if objMovieModelData.rated == "" || objMovieModelData.rated == "NA" || objMovieModelData.rated == nil {
                        return 0
                    }
                case 4:
                    if objMovieModelData.author == "" || objMovieModelData.author == "NA" || objMovieModelData.author == nil {
                        return 0
                    }
                case 5:
                    if objMovieModelData.language.count == 0 {
                        return 0
                    }
                case 6:
                    if objMovieModelData.country.count == 0 {
                        return 0
                    }
                case 7:
                    return 0
                case 8:
                    if objMovieModelData.releaseDate == "" || objMovieModelData.releaseDate == "NA" || objMovieModelData.releaseDate == nil {
                        return 0
                    }
                    
                default:
                    break
                }
            }else{
                switch indexPath.row {
                case 0:
                    if objMovieModelData.descriptionField == "" || objMovieModelData.descriptionField == "NA" || objMovieModelData.descriptionField == nil {
                        return 0
                    }
                case 1:
                    if objMovieModelData.year == 0 {
                        return 0
                    }
                case 2:
                    if objMovieModelData.language.count == 0 {
                        return 0
                    }
                case 5:
                    if objMovieModelData.isbn == "" || objMovieModelData.isbn == "NA" || objMovieModelData.isbn == nil {
                        return 0
                    }
                case 3:
                    if objMovieModelData.publisher == "" || objMovieModelData.publisher == "NA" || objMovieModelData.publisher == nil {
                        return 0
                    }
                case 4:
                    if objMovieModelData.pagesDuration == 0 {
                        return 0
                    }
                default:
                    break
                }
            }
        }
        return UITableViewAutomaticDimension
    }
    
    
    
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        if indexPath.section == 0 && objMovieModelData.reviews.count != 0 {
            return true
        }else{
            return false
        }
    }
    
    func tableView(_ tableView: UITableView, editActionsForRowAt indexPath: IndexPath) -> [UITableViewRowAction]? {
        
        let report = UITableViewRowAction(style: UITableViewRowActionStyle.destructive, title: AppMessages.AlertTitles.Report){(UITableViewRowAction,NSIndexPath) -> Void in
            self.reportReview(indexPath:indexPath)
        }
        return [report]
    }
    
    @objc func replyOnReview(sender:UIButton){
        guard let recommendationId = objMovieModelData.reviews[sender.tag].id else {
            return
        }
        presentReviewController(recommendationId: recommendationId)
    }
    
    @objc fileprivate func presentReviewController(recommendationId : String){
        let controller: CommentsViewController = UIStoryboard(storyboard: .inspiration).initVC()
//        controller.modalPresentationStyle = .overCurrentContext
//        controller.modalTransitionStyle = .crossDissolve
//        controller.category = self.category
//        controller.postId = self.postId
        controller.colorToUse = self.colorToUse
        controller.commentID = self.openCommentTo
        controller.delegate = self
        controller.reviewId = recommendationId
        self.navigationController?.pushViewController(controller, animated: true)
//        AppDelegate.shared().window?.rootViewController?.present(controller, animated: true, completion: nil)
    }
}

extension MovieDetailViewController : AddRatingDelegate{
    func refreshViewDetail(rated: Bool) {
        if rated == true{
            showLoader()
            getMovieDeatilsFromServer()
        }
    }
}

extension MovieDetailViewController:ReviewsTabelCellXibDelegate {
    func redirectToProfile(tag: Int,section: Int) {
        let  controller: UserProfileViewController = UIStoryboard.init(storyboard: .networks).initVC()
        controller.userId =  objMovieModelData.reviews[tag].userId?.id ?? ""
        self.navigationController?.pushViewController(controller, animated: true)
    }
}

extension MovieDetailViewController : ReviewsDelegate{
    func moveToProfileController(using userId: String?) {
        
        let  controller: UserProfileViewController = UIStoryboard.init(storyboard: .networks).initVC()
        controller.userId = userId
        self.navigationController?.pushViewController(controller, animated: true)
    }
    
    func refresh() {
        getMovieDeatilsFromServer()
    }
}


extension MovieDetailViewController:UserActions {
    func userAction(tag: Int, selected: Bool) {
        switch tag {
        case 0:
            self.shareData(id: objMovieModelData.id, type: category, notificationType: 1, title: objMovieModelData.title ?? "")
        case 1:
            addToLibrary(slected: selected)
        case 2:
            openRatingPopup()
        case 3:
            let controller: ReviewsViewController = UIStoryboard(storyboard: .inspiration).initVC()
            controller.colorToUse = self.colorToUse
            controller.category = self.category
            controller.postId = self.postId
            controller.delegate = self
            self.navigationController?.pushViewController(controller, animated: true)
        default:
            break
        }
    }
}


extension MovieDetailViewController : UIScrollViewDelegate{
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        if scrollView == movieDetailTabelView {
            if scrollView.contentOffset.y >= 290.0 {
                userRatingView.isHidden = false
            }else{
                userRatingView.isHidden = true
            }
        }
    }
}
