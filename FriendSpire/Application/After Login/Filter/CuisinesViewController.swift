//
//  CuisinesViewController.swift
//  FriendSpire
//
//  Created by Harpreet_kaur on 02/08/18.
//  Copyright © 2018 openkey. All rights reserved.
//

import UIKit
protocol SetCuisines {
    func reloadTableView(sectionToLoad : Int)
}

class CuisinesViewController: DIBaseController {
    
    
    //MARK:-variables
    var type = 1
    var tableArray = [String]()
    static var delegate:SetCuisines?
    var panGestureRecognizer: UIPanGestureRecognizer?
    var initialTouchPoint: CGPoint = CGPoint(x: 0,y: 0)
    var isGener:Bool = false
    
    
    //MARK:-IBOutlets.
    @IBOutlet weak var cuisinesTableView: UITableView!
    @IBOutlet weak var topView: UIView!
    @IBOutlet weak var selectedCuisinesCountLabel: UILabel!
    @IBOutlet weak var titleLabel: UILabel!
    
    
    //MARK:-ViewLifeCycle.
    //MARK:-ViewDidLoad.
    override func viewDidLoad() {
        super.viewDidLoad()
        if isGener {
            getGenre()
            titleLabel.text = "SELECT GENRES"
        }else{
            getCuisines()
        }
        self.cuisinesTableView.tableFooterView = UIView()
        addGestureToViewToDismissPresentController()
    }
    //MARK:-ViewWillAppear.
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        if isGener {
            if RestuarantFilterModel.bookSharedInstance.genre.count != 0 {
                selectedCuisinesCountLabel.text = "\(RestuarantFilterModel.bookSharedInstance.genre.count) SELECTED"
            }else{
                selectedCuisinesCountLabel.text = ""
            }
        }else{
            if RestuarantFilterModel.sharedInstance.cuisines.count != 0 {
                selectedCuisinesCountLabel.text = "\(RestuarantFilterModel.sharedInstance.cuisines.count) SELECTED"
            }else{
                selectedCuisinesCountLabel.text = ""
            }
        }
        
    }
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(true)
        
    }
    
    func addGestureToViewToDismissPresentController() {
        let swipeDown = UISwipeGestureRecognizer.init(target: self, action: #selector(hideCurrentView))
        swipeDown.direction = .down
        self.view.addGestureRecognizer(swipeDown)
    }
    
    //MARK:-Function To fecth Cuisines.
    func getCuisines() {
        self.showLoader()
        DIWebLayerCategoriesDetailApi().getCuisines(type:type, success: { (data) in
            self.tableArray = data
            self.cuisinesTableView.reloadData()
            self.hideLoader()
        }) { (error) in
            self.showAlert(message:error.message)
            self.hideLoader()
        }
    }
    
    //MARK:-Function To fecth Gener.
    func getGenre() {
        self.showLoader()
        DIWebLayerCategoriesDetailApi().getGenre(type:type, success: { (data) in
            self.tableArray = data
            self.cuisinesTableView.reloadData()
            self.hideLoader()
        }) { (error) in
            self.showAlert(message:error.message)
            self.hideLoader()
        }
    }
    
    @objc fileprivate func hideCurrentView(){
        self.dismiss(animated: true, completion: nil)
        if isGener {
            CuisinesViewController.delegate?.reloadTableView(sectionToLoad: 2)
        }else{
            CuisinesViewController.delegate?.reloadTableView(sectionToLoad: 4)
        }
    }
    
    
    //MARK:-IBActions
    //MARK:-Function to reset Selected Cuisines.
    @IBAction func resetSelectedCuisines(_ sender: UIButton) {
        RestuarantFilterModel.objFeedListRequest.genre.removeAll()
        RestuarantFilterModel.objFeedListRequest.cuisine.removeAll()
        RestuarantFilterModel.sharedInstance.cuisines.removeAll()
        RestuarantFilterModel.bookSharedInstance.genre.removeAll()
        if RestuarantFilterModel.sharedInstance.cuisines.count != 0 {
            selectedCuisinesCountLabel.text = "\(RestuarantFilterModel.sharedInstance.cuisines.count) SELECTED"
        }else{
            selectedCuisinesCountLabel.text = ""
        }
        cuisinesTableView.reloadData()
    }
    @IBAction func DoneButtonAction(_ sender: UIButton) {
        self.dismiss(animated: true, completion: nil)
        if isGener {
            CuisinesViewController.delegate?.reloadTableView(sectionToLoad: 2)
        }else{
            CuisinesViewController.delegate?.reloadTableView(sectionToLoad: 4)
        }
    }
}


//MARK:-UITabelViewDelegate&UITabelViewDataSource.
extension CuisinesViewController : UITableViewDelegate , UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        cuisinesTableView.showEmptyScreen(withMessage:"")
        if tableArray.count == 0 {
            cuisinesTableView.showEmptyScreen(withMessage:AppMessages.ErrorAlerts.noRecords)
        }
        return tableArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell:CuisinesTableCell = cuisinesTableView.dequeueReusableCell(withIdentifier: "CuisinesTableCell", for: indexPath) as? CuisinesTableCell else {
            return UITableViewCell()
        }
        cell.optionsLabel.text = tableArray[indexPath.row]
        if !isGener {
            if RestuarantFilterModel.sharedInstance.cuisines.contains(tableArray[indexPath.row]) {
                cell.accessoryType = .checkmark
            }else{
                cell.accessoryType = .none
            }
        }else{
            if RestuarantFilterModel.bookSharedInstance.genre.contains(tableArray[indexPath.row]) {
                cell.accessoryType = .checkmark
            }else{
                cell.accessoryType = .none
            }
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if isGener {
            if RestuarantFilterModel.bookSharedInstance.genre.contains(tableArray[indexPath.row]) {
                for (index,data) in  RestuarantFilterModel.bookSharedInstance.genre.enumerated() {
                    if data == tableArray[indexPath.row] {
                        RestuarantFilterModel.bookSharedInstance.genre.remove(at: index)
                    }
                }
            }else{
                RestuarantFilterModel.bookSharedInstance.genre.append(tableArray[indexPath.row])
            }
            if RestuarantFilterModel.bookSharedInstance.genre.count > 0{
                let str = "\""
                RestuarantFilterModel.objFeedListRequest.genre = "[\(str)\(RestuarantFilterModel.bookSharedInstance.genre.joined(separator: "\(str),\(str)"))\(str)]"
            }else {
                RestuarantFilterModel.objFeedListRequest.genre.removeAll()
            }
            if RestuarantFilterModel.bookSharedInstance.genre.count != 0 {
                selectedCuisinesCountLabel.text = "\(RestuarantFilterModel.bookSharedInstance.genre.count) SELECTED"
            }else{
                selectedCuisinesCountLabel.text = ""
            }
            cuisinesTableView.reloadData()
        }else{
            if RestuarantFilterModel.sharedInstance.cuisines.contains(tableArray[indexPath.row]) {
                for (index,data) in  RestuarantFilterModel.sharedInstance.cuisines.enumerated() {
                    if data == tableArray[indexPath.row] {
                        RestuarantFilterModel.sharedInstance.cuisines.remove(at: index)
                    }
                }
            }else{
                RestuarantFilterModel.sharedInstance.cuisines.append(tableArray[indexPath.row])
            }
            if RestuarantFilterModel.sharedInstance.cuisines.count != 0 {
                selectedCuisinesCountLabel.text = "\(RestuarantFilterModel.sharedInstance.cuisines.count) SELECTED"
            }else{
                selectedCuisinesCountLabel.text = ""
            }
            if RestuarantFilterModel.sharedInstance.cuisines.count > 0{
                RestuarantFilterModel.objFeedListRequest.cuisine = "[\"\(RestuarantFilterModel.sharedInstance.cuisines.joined(separator: "\",\""))\"]"
            }else {
                RestuarantFilterModel.objFeedListRequest.cuisine.removeAll()
            }
            
            cuisinesTableView.reloadData()
        }
    }
    
    
}
