//
//  MovieTableCell.swift
//  FriendSpire
//
//  Created by Harpreet_kaur on 14/08/18.
//  Copyright © 2018 openkey. All rights reserved.
//

import UIKit

class MovieTableCell: UITableViewCell {
    
    
    //MARK:-IBOutlets.
    @IBOutlet weak var itemLabel: UILabel!
    @IBOutlet weak var valueLabel: UILabel!
    
    
    //MARK:-UITabelViewCell Functions.
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    func setData(index:Int,objMovieModelData:MovieModelData) {
        switch index {
        case 0:
            valueLabel.text = objMovieModelData.descriptionField != "" ? objMovieModelData.descriptionField : "NA"
        case 1:
            valueLabel.text = objMovieModelData.actors != "" ? objMovieModelData.actors : "NA"
        case 2:
            valueLabel.text =  objMovieModelData.director != "" ? objMovieModelData.director : "NA"
        case 3:
            valueLabel.text = objMovieModelData.rated != "" ? objMovieModelData.rated == "0" ? "NA" : objMovieModelData.rated : "NA" // 0 and blank will replace with NA
        case 4:
             valueLabel.text = objMovieModelData.author != "" ? objMovieModelData.author : "NA"
        case 5:
            valueLabel.text =  objMovieModelData.language.count != 0 ? objMovieModelData.language.joined(separator: ", ") : "NA"
        case 6:
            valueLabel.text = objMovieModelData.country.count != 0 ? objMovieModelData.country.joined(separator: ", ") : "NA"
        case 7:
            valueLabel.text = "NA"
        case 8:
            valueLabel.text = (objMovieModelData.releaseDate != nil) ? objMovieModelData.releaseDate?.convertDate() : "NA"
        default:
            break
        }
        
    }
    
   func setBookData(index:Int,objMovieModelData:MovieModelData) {
    switch index {
    case 0:
          valueLabel.text =  objMovieModelData.descriptionField != "" ? objMovieModelData.descriptionField : "NA"
    case 1:
         valueLabel.text = objMovieModelData.year != 0 ? "\(objMovieModelData.year ?? 0)" : "NA"
    case 2:
         valueLabel.text = objMovieModelData.language.count != 0 ? objMovieModelData.language.joined(separator: ", ") : "NA"
    case 5:
        valueLabel.text =  objMovieModelData.isbn != "" ? objMovieModelData.isbn : "NA"
    case 3:
       valueLabel.text =  objMovieModelData.publisher != "" ? objMovieModelData.publisher : "NA"
    case 4:
        valueLabel.text =  objMovieModelData.pagesDuration != 0 ? "\(objMovieModelData.pagesDuration ?? 0)" : "NA"
    default:
        break
    }
    }
}
