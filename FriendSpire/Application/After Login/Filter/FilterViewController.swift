//
//  FilterViewController.swift
//  FriendSpire
//
//  Created by Harpreet_kaur on 03/08/18.
//  Copyright © 2018 openkey. All rights reserved.
//

import UIKit

class FilterViewController: UIViewController {
    
    //MARK:-Variables
    var selectedCuisines = [String]()
    var panGestureRecognizer: UIPanGestureRecognizer?
    var initialTouchPoint: CGPoint = CGPoint(x: 0,y: 0)
    
    //MARK:-IBOutlets.
    @IBOutlet weak var topView: UIView!
    @IBOutlet weak var cuisinesLabel: UILabel!
    
    
    //MARK:-ViewLifeCycle.
    //MARK:-ViewDidLoad.
    override func viewDidLoad() {
        super.viewDidLoad()
        addGestureToViewToDismissPresentController()
    }
    
    //MARK:-ViewWillAppear.
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        CuisinesViewController.delegate = self
    }
    
    //MARK:-Private Functions.
    //MARK:-AddGesture to View.
    func addGestureToViewToDismissPresentController() {
        panGestureRecognizer = UIPanGestureRecognizer(target: self, action: #selector(panGestureAction(_:)))
        self.topView.addGestureRecognizer(panGestureRecognizer!)
    }
    //MARK:-Function to Dismiss Present Controller.
    func panGestureAction(_ panGesture: UIPanGestureRecognizer) {
        let touchPoint = panGesture.location(in: self.view?.window)
        
        if panGesture.state == UIGestureRecognizerState.began {
            initialTouchPoint = touchPoint
        } else if panGesture.state == UIGestureRecognizerState.changed {
            if touchPoint.y - initialTouchPoint.y > 0 {
                self.view.frame = CGRect(x: 0, y: touchPoint.y - initialTouchPoint.y, width: self.view.frame.size.width, height: self.view.frame.size.height)
            }
        } else if panGesture.state == UIGestureRecognizerState.ended || panGesture.state == UIGestureRecognizerState.cancelled {
            if touchPoint.y - initialTouchPoint.y > 500 {
                self.dismiss(animated: true, completion: nil)
            } else {
                UIView.animate(withDuration: 0.3, animations: {
                    self.view.frame = CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: self.view.frame.size.height)
                })
            }
        }
    }
    
    //MARK:-IBActions.
    //MARK:-Function to Select Cuisines.
    @IBAction func selectCuisinesButtonAction(_ sender: UIButton) {
        let controller: CuisinesViewController = UIStoryboard(storyboard: .filter).initVC()
       // controller.selectedCuisines = selectedCuisines
        controller.modalPresentationStyle = .overCurrentContext
        self.present(controller, animated: true, completion: nil)
    }
}


//MARK:-Delegate Set Selected Cusisines.
extension FilterViewController : SetCuisines {
    func reloadTableView(sectionToLoad: Int) {
        
    }
    
   
    
    
    //    func setCuisines(cuisinesArray: [String]) {
    //        selectedCuisines = cuisinesArray
    //        cuisinesLabel.text = selectedCuisines.joined(separator: ", ")
    //    }
    
    
}
