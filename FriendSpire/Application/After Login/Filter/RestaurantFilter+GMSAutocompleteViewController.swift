//
//  RestaurantFilter+GMSAutocompleteViewController.swift
//  FriendSpire
//
//  Created by Harpreet_kaur on 06/08/18.
//  Copyright © 2018 openkey. All rights reserved.
//

import Foundation
import GooglePlaces
extension RestaurantsFiltersViewController:GMSAutocompleteViewControllerDelegate {
    
    func viewController(_ viewController: GMSAutocompleteViewController, didAutocompleteWith place: GMSPlace) {
        RestuarantFilterModel.sharedInstance.address = Address(gmsPlace: place)
        RestuarantFilterModel.sharedInstance.location =   RestuarantFilterModel.sharedInstance.address.formatted ?? "Current Location"
        RestuarantFilterModel.objFeedListRequest.latLong = "[\(String(describing: RestuarantFilterModel.sharedInstance.address.lat ?? 0.0)),\(String(describing: RestuarantFilterModel.sharedInstance.address.long ?? 0.0))]"
        RestuarantFilterModel.objFeedListRequest.filterLatLong = 1
        selectedSection = 10
        restaurantTableView.reloadSections([1], with: .none)
        setFilerCount()
        viewController.dismiss(animated: true, completion: nil)
    }
    
    func viewController(_ viewController: GMSAutocompleteViewController, didFailAutocompleteWithError error: Error) {
//        print(error.localizedDescription)
    }
    
    func wasCancelled(_ viewController: GMSAutocompleteViewController) {
        viewController.dismiss(animated: true, completion: nil)
    }
    
    func didRequestAutocompletePredictions(_ viewController: GMSAutocompleteViewController) {
        UIApplication.shared.isNetworkActivityIndicatorVisible = true
    }
    
    func didUpdateAutocompletePredictions(_ viewController: GMSAutocompleteViewController) {
        UIApplication.shared.isNetworkActivityIndicatorVisible = false
    }
}
