//
//  SliderTableCell.swift
//  FriendSpire
//
//  Created by Harpreet_kaur on 06/08/18.
//  Copyright © 2018 openkey. All rights reserved.
//

import UIKit

class SliderTableCell: UITableViewCell {

    
    //MARK:-IBOutlets.
    @IBOutlet weak var distanceSlider: UISlider!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    //MARK:-IBActions.
    @IBAction func distanceSliderAction(_ sender: UISlider) {
      
    }
    
}
