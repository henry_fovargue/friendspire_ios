//
//  DoubleSliderTableCell.swift
//  FriendSpire
//
//  Created by Harpreet_kaur on 06/08/18.
//  Copyright © 2018 openkey. All rights reserved.
//

import UIKit

class DoubleSliderTableCell: UITableViewCell {
    
    
    //MARK:-IBOutlets.
    @IBOutlet weak var headingLabel: UILabel!
    @IBOutlet weak var minimumRangeLabel: UILabel!
    @IBOutlet weak var maximumRangeLabel: UILabel!
    @IBOutlet weak var rangeSlider: RangeSlider!
    @IBOutlet weak var fourSquareRangeSlider: RangeSlider!
    @IBOutlet weak var fourSquareMinimumRangeLabel: UILabel!
    @IBOutlet weak var fourSquareMaximumRangeLabel: UILabel!
    @IBOutlet weak var fourSquareHeadingLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    @IBAction func ratingSliderAction(_ sender: RangeSlider) {
        

        var sectionToLoad = 0
        if sender.tag == 11 {
            sectionToLoad = 4
            RestuarantFilterModel.bookSharedInstance.rating[0] = String(format: "%.1f", arguments: [sender.lowerValue])
            RestuarantFilterModel.bookSharedInstance.rating[1] = String(format: "%.1f", arguments: [sender.upperValue])
            RestuarantFilterModel.objFeedListRequest.rating = "[\(RestuarantFilterModel.bookSharedInstance.rating.joined(separator: ","))]"
        }else if sender.tag == 10 {
            sectionToLoad = 6
            RestuarantFilterModel.sharedInstance.rating[0] = String(format: "%.1f", arguments: [sender.lowerValue])
            RestuarantFilterModel.sharedInstance.rating[1] = String(format: "%.1f", arguments: [sender.upperValue])
            RestuarantFilterModel.objFeedListRequest.friendspireRating = "[\(RestuarantFilterModel.sharedInstance.rating.joined(separator: ","))]"
            
        }
        CuisinesViewController.delegate?.reloadTableView(sectionToLoad: sectionToLoad)
    }
    
    @IBAction func fourSquareRangeSliderAction(_ sender: RangeSlider) {
        var sectionToLoad = 0
        if sender.tag == 11 {
            sectionToLoad = 4
            RestuarantFilterModel.bookSharedInstance.fourSquareRating[0] = String(format: "%.1f", arguments: [sender.lowerValue])
            RestuarantFilterModel.bookSharedInstance.fourSquareRating[1] = String(format: "%.1f", arguments: [sender.upperValue])
             RestuarantFilterModel.objFeedListRequest.friendspireRating = "[\(RestuarantFilterModel.bookSharedInstance.fourSquareRating.joined(separator: ","))]"
        }else if sender.tag == 10 {
            sectionToLoad = 6
            RestuarantFilterModel.sharedInstance.fourSquareRating[0] = String(format: "%.1f", arguments: [sender.lowerValue])
            RestuarantFilterModel.sharedInstance.fourSquareRating[1] = String(format: "%.1f", arguments: [sender.upperValue])
            RestuarantFilterModel.objFeedListRequest.rating = "[\(RestuarantFilterModel.sharedInstance.fourSquareRating.joined(separator: ","))]"
        }
        CuisinesViewController.delegate?.reloadTableView(sectionToLoad: sectionToLoad)
    }
    
}
