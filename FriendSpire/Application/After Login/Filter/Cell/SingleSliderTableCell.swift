//
//  SingleSliderTableCell.swift
//  FriendSpire
//
//  Created by Harpreet_kaur on 24/08/18.
//  Copyright © 2018 openkey. All rights reserved.
//

import UIKit

class SingleSliderTableCell: UITableViewCell {
    
    
    //MARK:-IBOutlets.
    @IBOutlet weak var minimumRangeLabel: UILabel!
    @IBOutlet weak var maximumRangeLabel: UILabel!
    @IBOutlet weak var rangeSlider: RangeSlider!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
    @IBAction func rangeSliderAction(_ sender: RangeSlider) {
        RestuarantFilterModel.bookSharedInstance.publishYear[0] = String(format: "%.0f", arguments: [sender.lowerValue])
        RestuarantFilterModel.bookSharedInstance.publishYear[1] = String(format: "%.0f", arguments: [sender.upperValue])
        if String(format: "%.0f", arguments: [sender.lowerValue]) == "1950" {
            RestuarantFilterModel.objFeedListRequest.publishYear = "[1,\(RestuarantFilterModel.bookSharedInstance.publishYear[1])]"
        }else{
        RestuarantFilterModel.objFeedListRequest.publishYear = "[\(RestuarantFilterModel.bookSharedInstance.publishYear.joined(separator: ","))]"
        RestuarantFilterModel.objFeedListRequest.releaseYear = "[\(RestuarantFilterModel.bookSharedInstance.publishYear.joined(separator: ","))]"
        }
        
        CuisinesViewController.delegate?.reloadTableView(sectionToLoad: 3)
    }
}
