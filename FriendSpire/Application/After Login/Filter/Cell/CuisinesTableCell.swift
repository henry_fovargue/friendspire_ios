//
//  CuisinesTableCell.swift
//  FriendSpire
//
//  Created by Harpreet_kaur on 03/09/18.
//  Copyright © 2018 openkey. All rights reserved.
//

import UIKit

class CuisinesTableCell: UITableViewCell {

    @IBOutlet weak var optionsLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
