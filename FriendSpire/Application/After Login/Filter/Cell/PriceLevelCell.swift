//
//  PriceLevelCell.swift
//  FriendSpire
//
//  Created by abhishek on 21/08/18.
//  Copyright © 2018 openkey. All rights reserved.
//

import UIKit

enum PriceLevelTag : Int{
    case level1 = 1
     case level2
     case level3
     case level4 
}

class PriceLevelCell: UITableViewCell {

    @IBOutlet weak var btnLevel1: UIButton!
    @IBOutlet weak var btnLevel2: UIButton!
    @IBOutlet weak var btnLevel3: UIButton!
    @IBOutlet weak var btnLevel4: UIButton!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

    func selectedState(){
        
       defaultState()
        
       for obj in  RestuarantFilterModel.sharedInstance.priceLevel.enumerated(){
        
        if obj.element == "$"{
            btnLevel1.isSelected = true
            btnLevel1.backgroundColor = .white
        }else if obj.element == "$$"{
            btnLevel2.isSelected = true
            btnLevel2.backgroundColor = .white
        }
        else if obj.element == "$$$"{
            btnLevel3.isSelected = true
            btnLevel3.backgroundColor = .white
        }
        else if obj.element == "$$$$"{
            btnLevel4.isSelected = true
            btnLevel4.backgroundColor = .white
        }
    }
    }
    
    func defaultState(){
            btnLevel1.isSelected = false
            btnLevel1.backgroundColor = #colorLiteral(red: 0.8823529412, green: 0.9098039216, blue: 0.9411764706, alpha: 1)
            btnLevel2.isSelected = false
            btnLevel2.backgroundColor = #colorLiteral(red: 0.8823529412, green: 0.9098039216, blue: 0.9411764706, alpha: 1)
            btnLevel3.isSelected = false
            btnLevel3.backgroundColor = #colorLiteral(red: 0.8823529412, green: 0.9098039216, blue: 0.9411764706, alpha: 1)
            btnLevel4.isSelected = false
            btnLevel4.backgroundColor = #colorLiteral(red: 0.8823529412, green: 0.9098039216, blue: 0.9411764706, alpha: 1)
      
    }
}
