//
//  SettingsViewController.swift
//  FriendSpire
//
//  Created by Mac Test on 18/07/18.
//  Copyright © 2018 openkey. All rights reserved.
//

import UIKit
enum settingsKey : String{
    case Title = "title"
    case Segue = "segue"
}

class SettingsViewController: DIBaseController {
    
    @IBOutlet weak var tableView: UITableView!
    
    let arrTitle = [[settingsKey.Title : "General Settings",settingsKey.Segue : SegueIdentifiers.SettingScreen.genralSetting],
                    [settingsKey.Title :"Edit Profile",settingsKey.Segue : SegueIdentifiers.SettingScreen.editProfile],
                    [settingsKey.Title :"Account Settings",settingsKey.Segue : SegueIdentifiers.SettingScreen.accountSetting],
                    [settingsKey.Title :"Help",settingsKey.Segue : SegueIdentifiers.SettingScreen.help],
                     [settingsKey.Title :"Blocked User",settingsKey.Segue : SegueIdentifiers.SettingScreen.blockedUser],
                    [settingsKey.Title :"Privacy Policy",settingsKey.Segue : SegueIdentifiers.SettingScreen.privacyPolicy],
                    [settingsKey.Title :"About Us",settingsKey.Segue : SegueIdentifiers.SettingScreen.privacyPolicy],
                    [settingsKey.Title :"Terms and Conditions",settingsKey.Segue : SegueIdentifiers.SettingScreen.privacyPolicy],
                    [settingsKey.Title :"App Version",settingsKey.Segue : ""]]
    
    override func viewDidLoad() {
        super.viewDidLoad()
//        getUserData()
        tableView.tableFooterView = UIView()
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        getUserData()
    }
    //MARk: prepare for segue
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        if segue.identifier == SegueIdentifiers.SettingScreen.privacyPolicy{
            let controller = segue.destination as! PrivacyPolicyViewController
            
            if let title = sender as? String {
                
                if title.contains("About"){
                    controller.loadViewFor = webViewType.About
                }
                else if title.contains("Privacy"){
                    controller.loadViewFor = webViewType.Privacy
                }
                else if title.contains("Terms"){
                    controller.loadViewFor = webViewType.Terms
                    
                }
            }
        }
    }
    
    func getUserData() {
        DIWebLayerCategoriesDetailApi().getUserData(success: { (message) in
        }) { (error) in
        }
    }
}
// MARK: -
extension SettingsViewController:UITableViewDataSource,UITableViewDelegate{
    // MARK: - Table View Delegate
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrTitle.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: CellIdentifiers.SettingScreen.settingsCell) as! SettingsCell
        cell.configureCell(arrTitle[indexPath.row][settingsKey.Title]!,showVersion: arrTitle.count-1 == indexPath.row ? true : false)
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if  indexPath.row == arrTitle.count - 1/* || indexPath.row == 1 ||  indexPath.row == 3 || indexPath.row == 4 || indexPath.row == 5 || indexPath.row == 6 */{
            return
        }
        let segue = arrTitle[indexPath.row][settingsKey.Segue]
        
        self.performSegue(withIdentifier: segue!, sender: arrTitle[indexPath.row][settingsKey.Title])
        
    }
    
}


