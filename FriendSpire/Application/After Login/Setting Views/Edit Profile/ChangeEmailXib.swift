//
//  ChangeEmailXib.swift
//  FriendSpire
//
//  Created by Harpreet_kaur on 06/09/18.
//  Copyright © 2018 openkey. All rights reserved.
//

import UIKit

//MARK:-Change Email Delegate
protocol ChangeEmailDelegate {
    func getEmailOtp()
    func showAlertMessage(message:String)
    func showLoaderView()
    func hideLoaderView()
}

class ChangeEmailXib: UIView {
    
    
    //MARK:-Variables.
    static var delegate:ChangeEmailDelegate?
    static var email:String = ""
    
    
    //MARK:-IBOutlets.
    @IBOutlet weak var dimView: UIView!
    @IBOutlet weak var emailTextField: CustomTextField!
    @IBOutlet weak var errorLabel: UILabel!
    
    
    //MARK:-IBActions.
    //MARK:-Change Email Button Action.
    @IBAction func changePasswordEmailAction(_ sender: UIButton) {
        self.endEditing(true)
        if !Utility.isConnectedToInternet {
            self.errorLabel.text = AppMessages.ErrorAlerts.noInternet
            return
        }
        if (emailTextField.text?.isBlank)! {
            self.errorLabel.text = "emailEmpty".localized
            return
        }else if !(emailTextField.text?.trim.isValidEmail)! {
            self.errorLabel.text = AppMessages.Email.invalidEmailSignup
             return
        }
        ChangeEmailXib.delegate?.showLoaderView()
        DIWebLayerCategoriesDetailApi().getOtpToChangeEmail(parameter: ["email":emailTextField.text?.trim ?? ""], success: { (message) in
            ChangeEmailXib.delegate?.hideLoaderView()
            ChangeEmailXib.email = self.emailTextField.text?.trim ?? ""
            self.errorLabel.text = ""
            self.removeXib()
            ChangeEmailXib.delegate?.getEmailOtp()
        }) { (error) in
            ChangeEmailXib.delegate?.hideLoaderView()
            self.errorLabel.text = error.message
        }
    }
    //MARK:-Cancel Button Action.
    @IBAction func cancelButtonAction(_ sender: UIButton) {
        self.endEditing(true)
        self.removeXib()
    }
    
    func removeXib() {
        UIView.animate(withDuration: 0.2, animations: {
            self.alpha = 0.0
        }) { (true) in
            UIView.animate(withDuration: 0.2, animations: {
                self.removeFromSuperview()
            }) { (true) in
            }
        }
    }
    
    func setData() {
        emailTextField.delegate = self
    }
}

extension ChangeEmailXib:UITextFieldDelegate {
    func textFieldDidBeginEditing(_ textField: UITextField) {
        self.errorLabel.text = "".localized
    }
}
