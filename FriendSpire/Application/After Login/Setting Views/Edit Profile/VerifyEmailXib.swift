//
//  VerifyEmailXib.swift
//  FriendSpire
//
//  Created by Harpreet_kaur on 06/09/18.
//  Copyright © 2018 openkey. All rights reserved.
//

import UIKit
protocol VerifyEmailDelegate {
    func verifyEmail()
}

class VerifyEmailXib: UIView {
    
    
    //MARK:-Variables.
    static var delegate:VerifyEmailDelegate?
    private var timer: Timer?
    var count = 30
    
    
    //MARK:-IBOutlets.
    @IBOutlet weak var otpTextField: CustomTextField!
    @IBOutlet weak var timeCountLabel: UILabel!
    @IBOutlet weak var dimView: UIView!
    @IBOutlet weak var errorLabel: UILabel!
    @IBOutlet weak var resendButton: UIButton!
    
    
    //MARK:-IBActions.
    //MARK:-Resend OtpCode Button Action.
    @IBAction func resendButtonAction(_ sender: UIButton) {
        self.endEditing(true)
        if !Utility.isConnectedToInternet {
             self.errorLabel.text = AppMessages.ErrorAlerts.noInternet
            return
        }
        count = 30
        startTimer()
        ChangeEmailXib.delegate?.showLoaderView()
        DIWebLayerCategoriesDetailApi().getOtpToChangeEmail(parameter: ["email":ChangeEmailXib.email], success: { (message) in
            ChangeEmailXib.delegate?.hideLoaderView()
            self.errorLabel.textColor = UIColor.green
            self.errorLabel.text = message
        }) { (error) in
            ChangeEmailXib.delegate?.hideLoaderView()
            self.errorLabel.text = error.message
        }
    }
    //MARK:-Verify OTPCode Button Action.
    @IBAction func submitButtonAction(_ sender: UIButton) {
        self.endEditing(true)
        if !Utility.isConnectedToInternet {
            self.errorLabel.text = AppMessages.ErrorAlerts.noInternet
            return
        }
        if (otpTextField.text?.isBlank)! {
            self.errorLabel.text = "emptyOtp".localized
            return
        }
        ChangeEmailXib.delegate?.showLoaderView()
        DIWebLayerCategoriesDetailApi().verifyEmail(parameter: ["otp":otpTextField.text?.trim ?? ""], success: { (message) in
            ChangeEmailXib.delegate?.hideLoaderView()
            User.sharedInstance.email = ChangeEmailXib.email
            UserManager.saveCurrentUser(user: User.sharedInstance)
            self.removeXib()
            VerifyEmailXib.delegate?.verifyEmail()
        }) { (error) in
            self.errorLabel.text = error.message
            ChangeEmailXib.delegate?.hideLoaderView()
        }
        
    }
    
    //MARK:-Cancel Button Action.
    @IBAction func cancelButtonAction(_ sender: UIButton) {
        self.endEditing(true)
        self.removeXib()
    }
    
    func removeXib() {
        timer?.invalidate()
        UIView.animate(withDuration: 0.2, animations: {
            self.alpha = 0.0
        }) { (true) in
            UIView.animate(withDuration: 0.2, animations: {
                self.removeFromSuperview()
            }) { (true) in
            }
        }
    }
    func setData() {
        startTimer()
        otpTextField.delegate = self
        
    }
    
    //MARK:- Custom Methods
    //MARK:- Timer Methods
    func startTimer() {
        guard timer == nil else { return }
        
        resendButton.isEnabled = false
        timer = Timer.scheduledTimer(timeInterval: 1, target: self, selector: #selector(timerOngoing), userInfo: nil, repeats: true)
    }
    
    func stopTimer() {
        guard timer != nil else { return }
        timer?.invalidate()
        timer = nil
        resendButton.isEnabled = true
    }
    
    
    @objc func timerOngoing() {
        if(count == 0) {
            stopTimer()
            return
        }
        count = count-1
        timeCountLabel.text = "Resend OTP after: 0." + (count < 10 ? "0\(count)s" : "\(count)s")
    }
    
}

extension VerifyEmailXib:UITextFieldDelegate {
    func textFieldDidBeginEditing(_ textField: UITextField) {
        self.errorLabel.textColor = UIColor.red
        self.errorLabel.text = "".localized
    }
}
