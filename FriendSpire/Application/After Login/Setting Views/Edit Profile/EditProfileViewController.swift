//
//  EditProfileViewController.swift
//  FriendSpire
//
//  Created by Mac Test on 19/07/18.
//  Copyright © 2018 openkey. All rights reserved.
//

import UIKit
import GooglePlaces
struct EditProfile{
    var profilePicUrl = ""
    var firstName = ""
    var lastName = ""
    var email = ""
    var gender = ""
    var dob = ""
    var location = ""
    var latLong = [Double]()
}
enum Gender:Int{
    case male = 1,female,others
    
    func getTextAndInt() -> (str : String, index : Int){
        
        switch self {
        case .male:
            return(str :"Male",index :1)
        case .female:
            return(str :"Female",index :2)
        default:
            return(str :"Others",index :3)
        }
    }
}


enum EditProfileSection:Int,CaseCountable{
    case firstLastName,email,gender,dob,location,updateBtn
    func getPlaceholderTextAndImage()->(text:String,image:UIImage){
        switch self{
        case .firstLastName:
            return("",UIImage())
        case .email:
            return ("Email".localized,UIImage())
        case .gender:
            return ("Gender".localized,UIImage())
        case .dob:
            return ("DOB".localized,UIImage())
        case .location:
            return ("Location".localized,UIImage())
        case .updateBtn:
            return ("",UIImage())
        }
    }
}

class EditProfileViewController: DIBaseController {
    
    var fromTutorial:Bool?
    
    @IBOutlet weak var constraintHeightViewNavBar: NSLayoutConstraint!
    
    @IBOutlet weak var btnContinue: GradientButton!
    @IBOutlet weak var profileImgVw: UIImageView!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet var genderView: UIView!
    @IBOutlet weak var innerGenderView: UIView!
    @IBOutlet weak var innerGenderTopConstraint: NSLayoutConstraint!
    var photoManager:PhotoManager!
    
    var gmsPlace : GMSPlace?
    var currentAddress:Address?
    internal var datePicker: UIDatePicker?
    internal var editProfile = EditProfile()
    var height:CGFloat = 0
    var imageUrl:String = ""
    var indicator = UIActivityIndicatorView()
    
    
    
    
    //MARK:-LifeCycle.
    //MARK:-ViewDidLoad.
    override func viewDidLoad() {
        super.viewDidLoad()
        activityIndicator()
        createuserExperience()
        //show Tutorial screen to user
        //        if TutorialStatus.getCurrentStatus() == nil ||  TutorialStatus.getCurrentStatus()?.editprofileScreen == nil{
        //            self.perform(#selector(showTutorialPopup(arr:)), with: [#imageLiteral(resourceName: "layer6")], afterDelay: 0.3)
        //            updateTutorialReadStatus()
        //        }
        
        let tap = UITapGestureRecognizer.init(target: self, action: #selector(selectPicture))
        profileImgVw.addGestureRecognizer(tap)
        
    }
    //MARK:-ViewWillAppear.
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        ChangeEmailXib.delegate = self
        VerifyEmailXib.delegate = self
    }
    
    //MARK:-IBActions.
    //MARK:-Select UserImage.
    @IBAction func profilePicTapped(_ sender: UIButton) {
        selectPicture()
    }
    
    //MARK:-Back Button Action.
    @IBAction func backTapped(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    @IBAction func continueTutorialTapped(_ sender: UIButton) {
        updatePassword(ChangePasswrodButtonCell())
//        showTutorialPopup()
    }
    
    func showTutorialPopup(){
        let tutorial : SlidingTutorialViewController =  UIStoryboard.init(storyboard: .tutorial).initVC()
        tutorial.modalTransitionStyle = .crossDissolve
        tutorial.modalPresentationStyle = .overCurrentContext
                        tutorial.delegate = self
        var arrToSend = ARR_TUTORIAL
        arrToSend.remove(at: 0) //remove welcome screen
        arrToSend.remove(at: 0) // remove edit profile screen
        tutorial.arrImages = arrToSend
        AppDelegate.shared().window?.rootViewController?.presentedViewController?.dismiss(animated: false, completion: nil)
        AppDelegate.shared().window?.rootViewController?.present(tutorial, animated: true, completion: nil)
    }
    //MARK:-Private Functions.
    //MARK:-Set UserData.
    private func createuserExperience(){
        indicator.startAnimating()
        indicator.backgroundColor = .darkGray
        constraintHeightViewNavBar.constant = fromTutorial == true ? 0 : 80
        btnContinue.isHidden = !(fromTutorial ??  false)
        if fromTutorial == true{
            self.showTutorialToast(message: AppMessages.TutorialMessage.InteractiveThree)
        }
        setData()
        if let url = URL(string: editProfile.profilePicUrl){
            profileImgVw.af_setImage(withURL: url, placeholderImage: #imageLiteral(resourceName: "user"), filter: nil, progress: nil, imageTransition: .crossDissolve(0.7), runImageTransitionIfCached: false) { (image) in
                self.indicator.stopAnimating()
                self.indicator.hidesWhenStopped = true
            }
            profileImgVw.af_setImage(withURL: url, placeholderImage: #imageLiteral(resourceName: "user"), filter: nil, progress: nil, imageTransition: .crossDissolve(0.7), runImageTransitionIfCached: false, completion: nil)
        }
        tableView.register(UINib.init(nibName: Constant.Xib.customTextFieldCell, bundle: nil), forCellReuseIdentifier: Constant.Xib.customTextFieldCell)
        self.tableView.reloadData()
    }
    private func setData(){
        editProfile.profilePicUrl = User.sharedInstance.profilePicUrl ?? ""
        editProfile.firstName = User.sharedInstance.firstName ?? ""
        editProfile.lastName = User.sharedInstance.lastName ?? ""
        editProfile.email = User.sharedInstance.email ?? ""
        if let gen = Gender(rawValue: User.sharedInstance.gender ?? 1)?.getTextAndInt(){
            editProfile.gender = gen.str
        }
        editProfile.dob = User.sharedInstance.dateOfBirth ?? ""//?.convertDate(.preDefined, toFormat: .preDefined) ?? ""
        editProfile.location = User.sharedInstance.location ?? ""
        editProfile.latLong = User.sharedInstance.latLong ?? [Double]()
    }
    func getDictionary() -> [String:Any]{
        
        var dict = [String:Any]()
        dict["firstName"] = editProfile.firstName.trim
        dict["lastName"] = editProfile.lastName.trim
        dict["profile_pic"] = editProfile.profilePicUrl
        if editProfile.dob == "NA" {
            dict["dob"] = ""
        }else{
            dict["dob"] = editProfile.dob
        }
        dict["address"] = editProfile.location
        dict["gender"] = Utility.getRawOnly(editProfile.gender)
        dict["longlat"] = editProfile.latLong
        return dict
        
    }
    
    //    private func updateTutorialReadStatus(){
    //
    //        if let readStatus = TutorialStatus.getCurrentStatus(){
    //            readStatus.editprofileScreen = true
    //            TutorialStatus.saveCurrentStatus(status: readStatus)
    //        }else{
    //            let status = TutorialStatus.sharedInstance
    //            status.editprofileScreen = true
    //            TutorialStatus.saveCurrentStatus(status: status)
    //        }
    //    }
    
    //MARK:- Check Form Validations.
    fileprivate func isFormValid() -> Bool {
        var message : String?
        if (editProfile.firstName.isBlank) {
            message = AppMessages.UserName.emptyNameSignup
        }
        else if !(editProfile.firstName.isValidInRange(minLength: 0,maxLength: Constant.Validations.limitName)) {
            message = AppMessages.UserName.maxLengthFirstname
        }
        else if (editProfile.lastName.isBlank) {
            message = AppMessages.UserName.emptyLNameSignup
        }
        else if !(editProfile.lastName.isValidInRange(minLength: 0, maxLength: Constant.Validations.limitName)) {
            message = AppMessages.UserName.maxLengthLastname
        }
        else if (editProfile.email.isBlank) {
            message = AppMessages.Email.emptyEmailSignup
        }
        else if !(editProfile.email.trim.isValidEmail) {
            message = AppMessages.Email.invalidEmailSignup
        } else if (editProfile.gender.isBlank) {
            message = AppMessages.EditProfile.gender
        }
        //        else if (editProfile.location.isBlank) {
        //            message = AppMessages.EditProfile.location
        //        }
        if message != nil {
            self.showAlert( message: message)
            return false
        }
        return true
    }
    
    
    func addChangeEmailXib() {
        let window = UIApplication.shared.keyWindow
        let  nibView = Bundle.main.loadNibNamed("ChangeEmailXib" , owner: self, options: nil)?.first as? ChangeEmailXib
        nibView?.frame = CGRect(x: 0, y: 0, width: (window?.frame.width)!, height: (window?.frame.height)!)
        nibView?.alpha = 0.0
        nibView?.setData()
        nibView?.dimView.alpha = 0.0
        self.view.addSubview(nibView!)
        //window?.addSubview(nibView!)
        UIView.animate(withDuration: 0.2, animations: {
            nibView?.transform = .identity
            nibView?.dimView.alpha = 0.4
            nibView?.alpha = 1.0
            
            self.view.layoutIfNeeded()
        }) { (true) in
            
        }
    }
    func addVerifyEmailXib() {
        let window = UIApplication.shared.keyWindow
        let  nibView = Bundle.main.loadNibNamed("VerifyEmailXib" , owner: self, options: nil)?.first as? VerifyEmailXib
        nibView?.frame = CGRect(x: 0, y: 0, width: (window?.frame.width)!, height: (window?.frame.height)!)
        nibView?.alpha = 0.0
        nibView?.dimView.alpha = 0.0
        nibView?.setData()
        
        self.view.addSubview(nibView!)
        //  window?.addSubview(nibView!)
        UIView.animate(withDuration: 0.2, animations: {
            nibView?.transform = .identity
            nibView?.dimView.alpha = 0.4
            nibView?.alpha = 1.0
            
            self.view.layoutIfNeeded()
        }) { (true) in
            
        }
    }
    
    internal func setUpDatePicker() {
        if self.datePicker == nil {
            self.datePicker = UIDatePicker()
        }
        datePicker?.datePickerMode = .date
        let maximumDate = Date()
//            Calendar.current.date(byAdding: .year, value: -18, to: Date())
        datePicker?.maximumDate = maximumDate
    }
    
    override func setData(selectedString:String) {
        editProfile.gender = selectedString
        self.tableView.reloadData()
    }
    
    
    func activityIndicator() {
        indicator = UIActivityIndicatorView(frame:  CGRect(x: 0.0, y: 0.0, width: 40.0, height: 40.0))
        indicator.activityIndicatorViewStyle = UIActivityIndicatorViewStyle.gray
        indicator.center = self.view.center
        self.profileImgVw.addSubview(indicator)
    }
    
    
    func setDataToModel() {
        User.sharedInstance.profilePicUrl = editProfile.profilePicUrl
        User.sharedInstance.firstName = editProfile.firstName
        User.sharedInstance.lastName = editProfile.lastName
        User.sharedInstance.location = editProfile.location
        User.sharedInstance.email = editProfile.email
        User.sharedInstance.dateOfBirth = editProfile.dob
        User.sharedInstance.gender = Utility.getRawOnly(editProfile.gender)
        
        //        if editProfile.gender == "Male" {
        //             User.sharedInstance.gender = 1
        //        }else{
        //             User.sharedInstance.gender = 2
        //        }
    }
    
    @objc func selectPicture(){
        photoManager = PhotoManager(navigationController: self.navigationController!, allowEditing: true, currentView: self.view, callback: { (image) in
            self.profileImgVw.image = image
        })
    }
    
//    @objc func showPicture(){
//        let controller = UIStoryboard.init(storyboard: .networks).instantiateViewController(withIdentifier: "FullPictureViewController") as! FullPictureViewController
//        controller.modalTransitionStyle = .crossDissolve
//        controller.modalPresentationStyle  = .overCurrentContext
//        controller.imgToLoad = profileImgVw.image
//        AppDelegate.shared().window?.rootViewController?.present(controller, animated: true, completion: nil)
//    }
    
    internal func uploadUserProfilePic(){
        if profileImgVw.image == #imageLiteral(resourceName: "user") {
          updateProfile()
            return
        }
        guard let email = User.sharedInstance.email else{
            return
        }
        LoginFirebaseUser.signIn(email: email , password: email , success: {[weak self] (finished, error) in
            if (error.code == nil) {
                return
            }
            
            //Check will set a new profile image name on firebase, if uploading on first time, but in update time, it will use old imagename and reupload on firebase
            if User.sharedInstance.firebaseProfileImageName == "" || User.sharedInstance.firebaseProfileImageName == nil{
                User.sharedInstance.firebaseProfileImageName = ((email ).replace("@", replacement: "")) + (User.sharedInstance.id ?? String(Constant.Time().now()))
//                    String(Constant.Time().now())
                User.sharedInstance.firebaseProfileImageName = User.sharedInstance.firebaseProfileImageName?.replace(".", replacement: "")
            }
            let firImageName = User.sharedInstance.firebaseProfileImageName ?? ""
            if let image = self?.profileImgVw.image {
                DIFirebaseImageManager().uploadImage(image: image, withQuality:0.8 , withName: firImageName, completion: { (apiResponse, imageUrl, error) in
                    switch apiResponse{
                    case .success:
                        self?.imageUrl = imageUrl ?? ""
                        self?.editProfile.profilePicUrl = imageUrl ?? ""
                        self?.updateProfile()
                    case .failure :
                        self?.hideLoader()
                        self?.showAlert( message: error.message ?? "")
                        break
                    }
                })
            }
        })
    }
    
    internal func updateProfile(){
        self.showLoader()
        DIWebLayerCategoriesDetailApi().updateProfile(parameter: getDictionary(), success: {[weak self] (message) in
            self?.hideLoader()
            User.sharedInstance.setHomeCity()
            self?.setDataToModel()
            if self?.fromTutorial == true{
                self?.showTutorialPopup()
                return
            }
            self?.showAlert(withTitle: "", message: message, okayTitle: AppMessages.AlertTitles.Ok,  okCall: {
                self?.navigationController?.popViewController(animated: true)
            })
        }) {[weak self] (error) in
            self?.hideLoader()
            self?.showAlert(message:error.message)
        }
    }
}


//MARK:-ChangeEmailDelegate
extension EditProfileViewController:ChangeEmailDelegate {
    func showAlertMessage(message: String) {
        self.showAlert(message:message)
    }
    func showLoaderView() {
        self.showLoader()
    }
    func hideLoaderView() {
        self.hideLoader()
    }
    func getEmailOtp() {
        addVerifyEmailXib()
    }  
}

//MARK:-VerifyEmailDelegate
extension EditProfileViewController:VerifyEmailDelegate {
    func verifyEmail() {
        editProfile.email = User.sharedInstance.email ?? ""
        self.tableView.reloadData()
    }
    
    
}

//MARK:-UITabelViewDelegate and Datasource.
extension EditProfileViewController:UITableViewDelegate,UITableViewDataSource{
    func numberOfSections(in tableView: UITableView) -> Int {
        return EditProfileSection.caseCount
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let header = UIView(frame: CGRect(x: 0, y: 0, width: tableView.frame.size.width, height: 1))
        header.backgroundColor = .clear
        return header
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if let editSection = EditProfileSection(rawValue: indexPath.section) {
            switch editSection {
            case .firstLastName:
                guard let cell = tableView.dequeueReusableCell(withIdentifier: Constant.TableviewCellIdentifier.editProfileTableViewCell) as? EditProfileTableViewCell else {return UITableViewCell()}
                cell.initCell(section:indexPath.section,editProfile:editProfile)
                cell.selectionStyle = .none
                cell.firstNameLbl.delegate = self
                cell.lastNameLbl.delegate = self
                return cell
            case .updateBtn:
                guard let cell = tableView.dequeueReusableCell(withIdentifier: Constant.TableviewCellIdentifier.changePasswrodButtonCell) as? ChangePasswrodButtonCell else {return UITableViewCell()}
                cell.delegate = self
                cell.selectionStyle = .none
                cell.updateBtn.isHidden = fromTutorial ?? false
                cell.constraintHeightUpdateBtn.constant = fromTutorial == true ? 0 : 55
                return cell
            default:
                guard let cell = tableView.dequeueReusableCell(withIdentifier: Constant.Xib.customTextFieldCell) as? CustomTextFieldCell else {return UITableViewCell()}
                cell.textFld.tag = indexPath.section + 1
                cell.configureEditProfileCell(indexPath.section,editProfile:editProfile)
                cell.textFld.delegate = self
                cell.selectionStyle = .none
                return cell
            }
        }
        return UITableViewCell()
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        switch section {
        case EditProfileSection.firstLastName.rawValue:return 1
        case EditProfileSection.updateBtn.rawValue:return 30
        default:return 20
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    
    func scrollViewWillBeginDragging(_ scrollView: UIScrollView) {
        height = 0
    }
    
}
extension EditProfileViewController:ChangePasswordButtonDelegate{
    func updatePassword(_ cell: ChangePasswrodButtonCell) {
        self.view.endEditing(true)
        if isFormValid(){
            if !Utility.isConnectedToInternet {
                self.showAlert(message:AppMessages.ErrorAlerts.noInternet)
                return
            }
            showLoader()
            uploadUserProfilePic()
        }
    }
    func cancelUpdate() {
        self.view.endEditing(true)
    }
}

extension EditProfileViewController : UITextFieldDelegate{
    func textFieldDidEndEditing(_ textField: UITextField) {
        if let currentFileds = EditProfileSection(rawValue: textField.tag - 1){
            switch currentFileds{
            case .firstLastName:
                editProfile.lastName = textField.text ?? ""
            case .email:
                editProfile.email = textField.text ?? ""
            case .dob:
                editProfile.dob = Utility.localDateFormatter(dateFormat:"yyyy-MM-dd'T'HH:mm:ss.000'Z'").string(from: datePicker?.date ?? Date())
                self.tableView.reloadData()
            default:break
            }
        } else {
            editProfile.firstName = textField.text ?? ""
        }
    }
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        textField.inputView = nil
        textField.inputAccessoryView = nil
        if let currentField = EditProfileSection(rawValue: textField.tag - 1) {
            switch currentField {
            case .firstLastName:
                return true
            case .email:
                self.addChangeEmailXib()
                return false
            case .gender:
                
                self.addTableView(title:"Select Gender",tableData: [ Gender(rawValue: Gender.male.rawValue)?.getTextAndInt().str ?? "", Gender(rawValue: Gender.female.rawValue)?.getTextAndInt().str ?? "", Gender(rawValue: Gender.others.rawValue)?.getTextAndInt().str ?? ""])
                return false
            case .dob:
//                print("dob")
                setUpDatePicker()
                textField.inputView = self.datePicker
                return true
            case .location:
                let autocompleteController = GMSAutocompleteViewController()
                autocompleteController.delegate = self
                present(autocompleteController, animated: true, completion: nil)
                return false
            case .updateBtn:
                return true
            }
        } else {
            return true
        }
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if let currentField = EditProfileSection(rawValue: textField.tag - 1) {
            switch currentField {
            case .firstLastName:
                let cs = NSCharacterSet(charactersIn: ACCEPTABLE_CHARACTERS).inverted
                let filtered = string.components(separatedBy: cs).joined(separator: "")
                let maxLength = Constant.Validations.limitName-1
                let currentString: NSString = textField.text! as NSString
                let newString: NSString =
                    currentString.replacingCharacters(in: range, with: string) as NSString
                
//                print("Compare string \(string)========= \(filtered)")
                return  (newString.length <= maxLength && (string == filtered))
            case .email:
                let maxLength = Constant.Validations.limitEmail-1
                let currentString: NSString = textField.text! as NSString
                let newString: NSString =
                    currentString.replacingCharacters(in: range, with: string) as NSString
                return newString.length <= maxLength
            case .gender:
                return false
            case .dob:
                return false
            case .location:
                return false
            case .updateBtn:
                return true
            }
        } else {
            let cs = NSCharacterSet(charactersIn: ACCEPTABLE_CHARACTERS).inverted
            let filtered = string.components(separatedBy: cs).joined(separator: "")
            
            let maxLength = Constant.Validations.limitName-1
            let currentString: NSString = textField.text! as NSString
            let newString: NSString =
                currentString.replacingCharacters(in: range, with: string) as NSString
            return  (newString.length <= maxLength && (string == filtered))
        }
        
    }
    
}

extension EditProfileViewController:GMSAutocompleteViewControllerDelegate {
    
    func viewController(_ viewController: GMSAutocompleteViewController, didAutocompleteWith place: GMSPlace) {
        currentAddress = Address(gmsPlace: place)
        editProfile.location = currentAddress?.formatted ?? ""
        editProfile.latLong = [currentAddress?.lat ?? 0.0,currentAddress?.long ?? 0.0]
        self.tableView.reloadData()
        //self.tableView.reloadRows(at: [IndexPath(row: 4, section: 0)], with: .none)
        viewController.dismiss(animated: true, completion: nil)
    }
    
    func viewController(_ viewController: GMSAutocompleteViewController, didFailAutocompleteWithError error: Error) {
//        print(error.localizedDescription)
    }
    
    func wasCancelled(_ viewController: GMSAutocompleteViewController) {
        viewController.dismiss(animated: true, completion: nil)
    }
    
    func didRequestAutocompletePredictions(_ viewController: GMSAutocompleteViewController) {
        UIApplication.shared.isNetworkActivityIndicatorVisible = true
    }
    
    func didUpdateAutocompletePredictions(_ viewController: GMSAutocompleteViewController) {
        UIApplication.shared.isNetworkActivityIndicatorVisible = false
    }
}

extension EditProfileViewController: SlidingTutorialDelegate{
    
    func finishWithTutorial(sender: Bool) {
        if sender == true{
            AppDelegate.shared().navigateToDashboard()
        }
    }
}

