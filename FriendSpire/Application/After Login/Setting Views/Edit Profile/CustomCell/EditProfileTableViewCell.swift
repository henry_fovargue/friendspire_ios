//
//  EditProfileTableViewCell.swift
//  FriendSpire
//
//  Created by shubam on 30/08/18.
//  Copyright © 2018 openkey. All rights reserved.
//

import UIKit

class EditProfileTableViewCell: UITableViewCell {
    @IBOutlet weak var firstNameLbl: UITextField!
    @IBOutlet weak var lastNameLbl: UITextField!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
    func initCell(section:Int,editProfile:EditProfile){
        firstNameLbl.text = editProfile.firstName
        lastNameLbl.text = editProfile.lastName
    }
    
}
