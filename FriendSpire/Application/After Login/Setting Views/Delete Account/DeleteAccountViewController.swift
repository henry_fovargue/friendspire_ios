//
//  DeleteAccountViewController.swift
//  FriendSpire
//
//  Created by shubam on 28/08/18.
//  Copyright © 2018 openkey. All rights reserved.
//

import UIKit

class DeleteAccountViewController: DIBaseController {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
    }
    
    
    @IBAction func backBtnTapped(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func deleteAccTapped(_ sender: GradientButton) {
        DispatchQueue.main.async {
            self.showAlert(message: AppMessages.AlertTitles.deleteAccount, okayTitle: AppMessages.AlertTitles.Yes, cancelTitle: AppMessages.AlertTitles.No, okCall: {
                if !Utility.isConnectedToInternet {
                    self.showAlert(message:AppMessages.ErrorAlerts.noInternet)
                    return
                }
                self.showLoader()
                DIWebLayerUserAPI().deleteAccount(success: { (response) in
                    self.hideLoader()
                    UserManager.logout()
                    appDelegate.navigateToLogin()
                }) { (error) in
                    self.hideLoader()
                    self.showAlert(withError:error)
                }
            })
            return
        }
        
    }
}
