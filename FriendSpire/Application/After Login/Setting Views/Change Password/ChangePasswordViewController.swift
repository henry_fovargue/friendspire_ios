//
//  ChangePasswordViewController.swift
//  FriendSpire
//
//  Created by shubam on 28/08/18.
//  Copyright © 2018 openkey. All rights reserved.
//

import UIKit
enum ChangePasswordFields:Int{
    case currentPassword=0,
    newPassword,
    confirmPassword,
    updateAndCancelBtn
}

class ChangePassword {
    var currentPassword = ""
    var newPassword = ""
    var confirmPassword = ""
}



class ChangePasswordViewController: DIBaseController {
    
    
    //MARK:-Variables.
    var changePassword = ChangePassword()
    
    //MARK:-IBOutlets.
    @IBOutlet weak var tableView: UITableView!
    
    
    //MARK:-ViewLifeCycle.
    override func viewDidLoad() {
        super.viewDidLoad()
        createUserExperience()
    }
    
    
    
    //MARK:-Private Functions.
    //MARK:- Function to set UI Data.
    private func createUserExperience(){
        tableView.register(UINib.init(nibName: Constant.Xib.customTextFieldCell, bundle: nil), forCellReuseIdentifier: Constant.Xib.customTextFieldCell)
    }
    //MARK:-Check Form Validations.
    func isValidChangePassForm() -> Bool{
        var message:String?
        if changePassword.currentPassword.isBlank{
            message = AppMessages.Password.currentEmpty
        }else if !(changePassword.currentPassword.trimmed.isValidPassword) {
            message = AppMessages.Password.invalidPassword
        }else if changePassword.newPassword.isBlank{
            message = AppMessages.Password.newEmpty
        }else if !(changePassword.newPassword.trimmed.isValidPassword) {
            message = AppMessages.Password.invalidPassword
        }else if changePassword.confirmPassword.isBlank{
            message = AppMessages.Password.emptyConfirmPassword
        }else if changePassword.newPassword != changePassword.confirmPassword{
            message = AppMessages.Password.newConfirmMismatch
        }
        if message != nil{
            self.showAlert( message: message)
            return false
        }
        return true
    }
    
    //MARK:-Function to Change User Password.
    func updatePassword(){
        if !Utility.isConnectedToInternet {
            self.showAlert(message:AppMessages.ErrorAlerts.noInternet)
            return
        }
        self.showLoader()
        let paramas =  ["password":changePassword.currentPassword,
                        "new_password":changePassword.newPassword]
        DIWebLayerUserAPI().changePassword(parameters: paramas, success: { (response) in
            self.hideLoader()
            let message = response["message"] as? String ?? ""
            self.showAlert(withTitle: "", message: message, okayTitle: AppMessages.AlertTitles.Ok,  okCall: {
                self.navigationController?.popViewController(animated: true)
            })
        }) { (error) in
            self.hideLoader()
            self.showAlert(withError: error)
        }
    }
    
    
    @IBAction func backButtonTapped(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
}

//MARK:-Extension for UITableViewDelegate&UITableViewDataSource.
extension ChangePasswordViewController:UITableViewDelegate,UITableViewDataSource{
    func numberOfSections(in tableView: UITableView) -> Int {
        return 4
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let header = UIView(frame: CGRect(x: 0, y: 0, width: tableView.frame.size.width, height: 1))
        header.backgroundColor = .clear
        return header
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.section < 3{
            guard let cell = tableView.dequeueReusableCell(withIdentifier: Constant.Xib.customTextFieldCell) as? CustomTextFieldCell else {return UITableViewCell()}
            cell.configureChangePasswordCell(indexPath.section)
            cell.textFld.delegate = self
            return cell
        }
        guard let cell = tableView.dequeueReusableCell(withIdentifier: Constant.TableviewCellIdentifier.changePasswrodButtonCell) as? ChangePasswrodButtonCell else {return UITableViewCell()}
        cell.delegate = self
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        if section < 3{
            return 15
        } else {
            return 40
        }
    }
}


//MARK:-Extension to Override UITextFieldDelegate.
extension ChangePasswordViewController : UITextFieldDelegate{
    
    
    
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        let maxLength = Constant.Validations.limitPass-1
        let currentString: NSString = textField.text! as NSString
        let newString: NSString =
            currentString.replacingCharacters(in: range, with: string) as NSString
        return newString.length <= maxLength
        
    }
    
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        switch textField.tag{
        case ChangePasswordFields.currentPassword.rawValue:
            changePassword.currentPassword = textField.text ?? ""
        case ChangePasswordFields.newPassword.rawValue:
            changePassword.newPassword = textField.text ?? ""
        case ChangePasswordFields.confirmPassword.rawValue:
            changePassword.confirmPassword = textField.text ?? ""
        default:
            break
        }
    }
    
    
}

//MARK:-Extension to Override ChangePasswordButtonDelegate
extension ChangePasswordViewController:ChangePasswordButtonDelegate{
    func updatePassword(_ cell: ChangePasswrodButtonCell) {
        self.view.endEditing(true)
        if isValidChangePassForm() {
            self.updatePassword()
        }
    }
    func cancelUpdate() {
        self.view.endEditing(true)
        self.navigationController?.popViewController(animated: true)
    }
}
