//
//  ChangePasswrodButtonCell.swift
//  FriendSpire
//
//  Created by shubam on 28/08/18.
//  Copyright © 2018 openkey. All rights reserved.
//

import UIKit
protocol ChangePasswordButtonDelegate {
    func updatePassword(_ cell:ChangePasswrodButtonCell)
    func cancelUpdate()
}

class ChangePasswrodButtonCell: UITableViewCell {

    var delegate : ChangePasswordButtonDelegate?
    @IBOutlet weak var cancelBtn: UIButton!
    @IBOutlet weak var updateBtn: GradientButton!
    @IBOutlet weak var constraintHeightUpdateBtn: NSLayoutConstraint!

    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    @IBAction func updateBtnTapped(_ sender: GradientButton) {
        delegate?.updatePassword(self)
    }
    
    
    @IBAction func cancelBtnTapped(_ sender: UIButton) {
        delegate?.cancelUpdate()
    }
    
}
