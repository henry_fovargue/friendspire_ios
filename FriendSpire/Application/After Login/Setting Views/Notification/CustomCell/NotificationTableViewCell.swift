//
//  NotificationTableViewCell.swift
//  FriendSpire
//
//  Created by shubam on 28/08/18.
//  Copyright © 2018 openkey. All rights reserved.
//

import UIKit

class NotificationTableViewCell: UITableViewCell {
    @IBOutlet weak var profileImgVw: UIImageView!
    
    @IBOutlet weak var nameLbl: UILabel!
    @IBOutlet weak var notificationtextLbl: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
