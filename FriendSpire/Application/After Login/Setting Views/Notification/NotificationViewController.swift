//
//  NotificationViewController.swift
//  FriendSpire
//
//  Created by shubam on 28/08/18.
//  Copyright © 2018 openkey. All rights reserved.
//

import UIKit

class NotificationViewController: DIBaseController {

    override func viewDidLoad() {
        super.viewDidLoad()
         createUserExperience()
        // Do any additional setup after loading the view.
    }
    
    func createUserExperience(){
        self.showLoader()
        getNotificationList()
    }
    
    
    func getNotificationList(){
        DIWebLayerUserAPI().getNotificationList(parameters: nil, pageNo: 1, success: { (response) in
            self.hideLoader()
            print(response)
        }) { (error) in
            self.hideLoader()
            self.showAlert(withError:error)
        }
    }

}
extension NotificationViewController:UITableViewDelegate,UITableViewDataSource{
   
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 4
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: Constant.TableviewCellIdentifier.notificationTableViewCell) as? NotificationTableViewCell else {return UITableViewCell()}
        cell.selectionStyle = .none
        return cell
    }
   
}
 
