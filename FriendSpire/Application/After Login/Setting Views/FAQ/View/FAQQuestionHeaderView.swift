//
//  FAQQuestionHeaderView.swift
//  BottleDriver
//
//  Created by shilpa on 14/08/18.
//  Copyright © 2018 Debut. All rights reserved.
//

import UIKit
protocol FAQHeaderViewDelegate: class {
    func didClickOnHeader(sender: UIButton)
}
class FAQQuestionHeaderView: UITableViewHeaderFooterView {

    //MARK:- Properties
    weak var delegate: FAQHeaderViewDelegate?
    
    //MARK:- IBOutlets
    @IBOutlet weak var headerButton: UIButton!
    @IBOutlet weak var dropDownButton: UIButton!
    @IBOutlet weak var headerLabel: UILabel!
    @IBOutlet weak var headerBottomView: UIView!
    
    
    //MARK:- IBActions
    @IBAction func headerTapped(_ sender: UIButton) {
        DispatchQueue.main.async {
            self.delegate?.didClickOnHeader(sender: sender)
        }
    }
    
    //MARK:- Initialization
    func initializeWith(faq: FAQ, atSection section: Int) {
        headerButton.tag = section
        self.headerLabel.text = faq.heading?.trim
        if faq.isExpanded {
            dropDownButton.setImage(#imageLiteral(resourceName: "down"), for: .normal)
        } else {
            dropDownButton.setImage(#imageLiteral(resourceName: "Disclosure Indicator"), for: .normal)
        }
        headerBottomView.isHidden = faq.isExpanded
    }
}
