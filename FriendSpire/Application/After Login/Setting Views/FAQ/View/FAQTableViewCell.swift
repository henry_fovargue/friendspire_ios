//
//  FAQTableViewCell.swift
//  BottleDriver
//
//  Created by shilpa on 14/08/18.
//  Copyright © 2018 Debut. All rights reserved.
//

import UIKit

class FAQTableViewCell: UITableViewCell {

    //MARK:-
    @IBOutlet weak var faqContentLabel: UILabel!
    @IBOutlet weak var separatorView: UIView!
    @IBOutlet weak var txtView: UITextView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    //MARK:- Initialize
    func initializeWith(faq: FAQ) {
        if let desc = faq.desc {
            self.txtView.attributedText = desc.trim.htmlToAttributedString
            let string = self.txtView.text.components(separatedBy: .newlines)
            self.txtView.text = string.joined(separator: "")
        }
        separatorView.isHidden = !faq.isExpanded
    }
}
