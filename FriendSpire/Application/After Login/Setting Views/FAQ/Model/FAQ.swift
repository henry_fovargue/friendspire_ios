//
//  FAQ.swift
//  BottleDriver
//
//  Created by shilpa on 14/08/18.
//  Copyright © 2018 Debut. All rights reserved.
//

import Foundation
struct FAQ: Codable {
    var heading: String?
    var desc: String?
    var isExpanded = false
    var id: String?
    
    enum CodingKeys: String, CodingKey {
        case heading
        case desc = "description"
        case id = "_id"
    }
}
