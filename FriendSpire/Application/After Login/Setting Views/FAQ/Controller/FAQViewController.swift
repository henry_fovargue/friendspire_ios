//
//  FAQViewController.swift
//  BottleDriver
//
//  Created by shilpa on 14/08/18.
//  Copyright © 2018 Debut. All rights reserved.
//

import UIKit

class FAQViewController: DIBaseController {

    //MARK:- Properties
    internal var faqs: [FAQ]?
    
    //MARK:- IBOutlets
    @IBOutlet weak var tableView: UITableView!
    
    //MARK:- IBActions
    @IBAction func backTapped(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    //MARK:- View Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.delegate = self
        tableView.dataSource = self
        tableView.estimatedRowHeight = 60.0
        tableView.rowHeight = UITableViewAutomaticDimension
        tableView.estimatedSectionHeaderHeight = 60.0
        tableView.sectionHeaderHeight = UITableViewAutomaticDimension
        self.registerXibs()
        self.getFaqs()
        // Do any additional setup after loading the view.
    }

    //MARK:- Helpers
    private func registerXibs() {
        self.tableView.register(UINib(nibName: FAQQuestionHeaderView.identifier, bundle: nil), forHeaderFooterViewReuseIdentifier: FAQQuestionHeaderView.identifier)
    }
    
    //MARK:- Server hit
    private func getFaqs() {
        guard Utility.isConnectedToInternet else {
            self.tableView.showEmptyScreen(withMessage: AppMessages.ErrorAlerts.noInternet)
            return
        }
        self.showLoader()
        DIWebLayerUserAPI().faqsList(parameters: nil, success: { (faqs) in
            self.hideLoader()
            self.faqs = [FAQ]()
            self.faqs?.append(contentsOf: faqs)
            if self.faqs!.count == 0 {
                self.tableView.showEmptyScreen(withMessage: "No FAQs found".localized)
            }
            self.tableView.reloadData()
        }) { (error) in
            self.hideLoader()
            self.showAlert(withError:error)
        }
        
    }
}

//MARK:- UITableViewDataSource
extension FAQViewController: UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        guard let faqs = self.faqs else {
            return 0
        }
        return faqs.count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if let faqs = faqs,
            faqs[section].isExpanded {
            return 1
        }
        return 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: FAQTableViewCell.identifier, for: indexPath) as? FAQTableViewCell else {
            return UITableViewCell()
        }
        if let faqs = self.faqs {
            cell.initializeWith(faq: faqs[indexPath.section])
        }
        return cell
    }
}

//MARK:- UITableViewDelegate
extension FAQViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        guard let faqs = self.faqs else {
            return nil
        }
        let headerView = tableView.dequeueReusableHeaderFooterView(withIdentifier: FAQQuestionHeaderView.identifier) as! FAQQuestionHeaderView
        headerView.delegate = self
        headerView.initializeWith(faq: faqs[section], atSection: section)
        return headerView
    }
}

//MARK:- FAQHeaderViewDelegate
extension FAQViewController: FAQHeaderViewDelegate {
    func didClickOnHeader(sender: UIButton) {
        if let faqs = self.faqs {
            self.faqs?[sender.tag].isExpanded = !faqs[sender.tag].isExpanded
            self.tableView.reloadData()
        }
    }
}
