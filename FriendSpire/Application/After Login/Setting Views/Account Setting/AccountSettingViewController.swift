//
//  AccountSettingViewController.swift
//  FriendSpire
//
//  Created by Mac Test on 19/07/18.
//  Copyright © 2018 openkey. All rights reserved.
//

import UIKit
import UserNotifications
enum AccountSetting:Int{
    case changePassword=0,deleteAccount,logout
}

class AccountSettingViewController: DIBaseController {
    
    
    //MARK:-IBOutlets.
    @IBOutlet weak var tableView: UITableView!
    
    //MARK:-Variables.
    let arrTitle = [Titles.SettingScreenTitles.AccountSettingTitles.changePassword,Titles.SettingScreenTitles.AccountSettingTitles.deleteAccount,Titles.SettingScreenTitles.AccountSettingTitles.logout]
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    // MARK: - IBAction
    @IBAction func backButtonTapped(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
}
// MARK: -
extension AccountSettingViewController : UITableViewDataSource,UITableViewDelegate {
    // MARK: - TableView Delegate
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrTitle.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell  = tableView.dequeueReusableCell(withIdentifier: CellIdentifiers.SettingScreen.AccountSetting.accountSettingCell) as! SettingsCell
        cell.configureAccountCell(arrTitle[indexPath.row], showArrow: indexPath.row == 0 ? true : false)
        
        return cell
    }
  
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        switch indexPath.row{
        case AccountSetting.changePassword.rawValue:
            let changePasswordVc:ChangePasswordViewController = UIStoryboard.init(storyboard: .setting).initVC()
            self.navigationController?.pushViewController(changePasswordVc, animated: true)
        case AccountSetting.deleteAccount.rawValue:
            let deleteAccVc:DeleteAccountViewController = UIStoryboard.init(storyboard: .setting).initVC()
            self.navigationController?.pushViewController(deleteAccVc, animated: true)
        case AccountSetting.logout.rawValue:
            DispatchQueue.main.async {
                self.showAlert(message: AppMessages.AlertTitles.logoutMessage, okayTitle: AppMessages.AlertTitles.Yes, cancelTitle: AppMessages.AlertTitles.No, okCall: {
                    if !Utility.isConnectedToInternet {
                        self.showAlert(message:AppMessages.ErrorAlerts.noInternet)
                        return
                    }
                    self.showLoader()
                    DIWebLayerCategoriesDetailApi().logout(success: { (message) in
                        self.hideLoader()
                        UserManager.logout()
                        if #available(iOS 10.0, *) {
                            let center = UNUserNotificationCenter.current()
                            center.removeAllPendingNotificationRequests() // To remove all pending notifications which are not delivered yet but scheduled.
                            center.removeAllDeliveredNotifications() // To remove all delivered notifications
                        }
//                        _ = self.navigationController?.popToRootViewController(animated: true)
                        UserManager.resetNavigation()
                    }, failure: { (error) in
                        self.hideLoader()
                        self.showAlert(message:error.message)
                    })
                    
                })
                return
            }
        default:break
        }
    }
}
