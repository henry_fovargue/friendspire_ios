//
//  ContactAdministrator.swift
//  FriendSpire
//
//  Created by Harpreet_kaur on 04/09/18.
//  Copyright © 2018 openkey. All rights reserved.
//
import UIKit
class ContactField{
    var id:String?
    var name:String?
    init(){}
    convenience init(json:Parameters){
        self.init()
        self.id = json["_id"] as? String
        self.name = json["name"] as? String
    }
}

class ContactAdministrator: DIBaseController {
    
    var contactCategories = [ContactField]()
    var selecteduserId:String?
    
    @IBOutlet weak var descriptionCharactersLabel: UILabel!
    @IBOutlet weak var descriptionTextView: UITextView!
    @IBOutlet weak var subjectTextField: UITextField!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setTextView()
        getContactCategories()
    }
    @IBAction func backTapped(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func showCategoriesAction(_ sender: UIButton) {
        var string = [String]()
        for (_,title) in contactCategories.enumerated(){
            string.append(title.name ?? "")
        }
        self.addTableView(title:"Choose Category",tableData: string)
    }
    
    @IBAction func submitButtonAction(_ sender: UIButton) {
        self.view.endEditing(true)
        if isFormValid(){
            if !Utility.isConnectedToInternet {
                self.showAlert(message:AppMessages.ErrorAlerts.noInternet)
                return
            }
            self.showLoader()
            DIWebLayerCategoriesDetailApi().contactAdministrator(parameter: ["subject_id":selecteduserId ?? "" ,"message":descriptionTextView.text.trim], success: { (message) in
                self.hideLoader()
                self.showAlert(withTitle: "", message:message , okayTitle: "OK", okCall: {
                    self.navigationController?.popViewController(animated: true)
                })
            }) { (error) in
                self.hideLoader()
                self.showAlert(withError: error)
            }
        }
    }
    
    private func isFormValid() -> Bool {
        var message:String?
        if (subjectTextField.text?.isBlank)! {
            message = "emptyContactSubject".localized
        }else if (descriptionTextView.text?.isBlank)!{
            message = "emptyContactDescription".localized
        }
        if message != nil {
            self.showAlert(message:message)
            return false
        }
        return true
    }
    func setTextView() {
        //  descriptionTextView.textContainerInset = UIEdgeInsets(top: 16, left: 16, bottom: 16, right: 16)
        descriptionTextView.layer.shadowRadius = 5.0
        descriptionTextView.layer.shadowColor = UIColor.gray.cgColor
        descriptionTextView.layer.shadowOffset = CGSize(width: 2.0, height: 2.0)
        descriptionTextView.layer.shadowOpacity = 0.8
    }
    
    
    func getContactCategories(){
        showLoader()
        DIWebLayerCategoriesDetailApi().getContactCategory(success: { (nsArray) in
            self.hideLoader()
            for each in nsArray{
                let dict = each as? [String:Any] ?? [:]
//                print(dict)
                self.contactCategories.append(ContactField(json: dict))
            }
            if self.contactCategories.count > 0{
                self.subjectTextField.text = "    \(self.contactCategories[0].name ?? "")"
                self.selecteduserId = self.contactCategories[0].id
            }
        }) { (error) in
            self.hideLoader()
            self.showAlert(withError: error)
        }
    }
    override func setData(selectedString:String) {
        self.subjectTextField.text = "    \(selectedString)"
        for (index,data) in contactCategories.enumerated() {
            if data.name == selectedString {
                self.selecteduserId = self.contactCategories[index].id
            }
        }
    }
}

extension ContactAdministrator:UITextViewDelegate {
    func textViewDidChange(_ textView: UITextView) {
        descriptionCharactersLabel.text = "\(textView.text.count)/1000"
    }
    
    
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        if textView.text.characters.count + text.characters.count > 1000 {
            return false
        }
        if textView.text.characters.count <  1000 || text == "" {
            return true
        }else {
            return  false
        }
    }
    
}

