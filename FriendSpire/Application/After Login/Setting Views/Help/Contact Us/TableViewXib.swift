//
//  TableViewXib.swift
//  Noah
//
//  Created by Harpreet on 16/01/18.
//  Copyright © 2018 Harpreet. All rights reserved.
//

import UIKit
protocol TableViewXibDelegate {
    func setData(selectedString:String)
}

class TableViewXib: UIView {
    
    
    //MARK:-Variables
    var xibTableArray = [String]()
    var tableCellIdentifier:String = "cell"
    var delegate:TableViewXibDelegate?
    var tap:UITapGestureRecognizer!
    var selectedString:String?
    
    
    //MARK:-IBOulets.
    @IBOutlet weak var dimView: UIView!
    @IBOutlet weak var mainView: UIView!
    @IBOutlet weak var xibTableViewHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var xibTableView: UITableView!
    @IBOutlet weak var titleLabel: UILabel!
    
    
    
    //MARK:-UITableViewFunctions
    override func awakeFromNib() {
        xibTableView.tableFooterView = UIView(frame: CGRect(x: 0, y: 0, width: xibTableView.frame.size.width, height: 1))
    }
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    //Mark:-Custom Functions.
    //MARK:-Function to set view data.
    func setData(title:String,tableData:[String]) {
        titleLabel.text = title
        xibTableView.delegate = self
        xibTableView.dataSource = self
        xibTableView.register(UITableViewCell.self, forCellReuseIdentifier: tableCellIdentifier)
        xibTableArray = tableData
        xibTableViewHeightConstraint.constant = CGFloat(xibTableArray.count*44)
        if xibTableArray.count == 0 {
            xibTableViewHeightConstraint.constant = CGFloat(44)
        }
        let height = UIApplication.shared.keyWindow?.frame.height ?? 0.0
        if height == 812 {
            if CGFloat(xibTableArray.count*44) > height - 100 {
                xibTableViewHeightConstraint.constant = height - 150
                xibTableView.isScrollEnabled = true
            }
//            else{
//                xibTableViewHeightConstraint.constant = CGFloat(xibTableArray.count*44)
//            }
        }else{
            if CGFloat(xibTableArray.count*44) > height - 40 {
                xibTableViewHeightConstraint.constant = height - 100
                xibTableView.isScrollEnabled = true
            }
        }
        setTapGesture()
    }
    
    //MARK:-CreateTapGesture to remove Xib from its superview.
    func setTapGesture() {
        tap = UITapGestureRecognizer(target: self, action: #selector(tapFunction(sender:)))
        dimView.isUserInteractionEnabled = true
        dimView.addGestureRecognizer(tap)
    }
    
    //MARK:-Function to remove Xib from its superview.
    func removeXib() {
        UIView.animate(withDuration: 0.2, animations: {
            self.removeFromSuperview()
        }) { (true) in
        }
    }
    
    
    @objc func tapFunction(sender:UITapGestureRecognizer) {
        if tap != nil{
            dimView.removeGestureRecognizer(tap)
        }
        removeXib()
    }
}

//MARK:-UITableViewDelegate&UITableViewDataSource
extension TableViewXib:UITableViewDelegate,UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if xibTableArray.count == 0 {
            return 1
        }
        return xibTableArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: tableCellIdentifier) else {
            return  UITableViewCell()
        }
        cell.selectionStyle = .none
        if xibTableArray.count == 0 {
             cell.textLabel?.text = "Oops Something went wrong!\n We don't have any data to show"
        }else{
        cell.textLabel?.text = xibTableArray[indexPath.row]
        }
        cell.textLabel?.font = UIFont.init(name: "SFUIText-Regular", size: 13.0)
        cell.textLabel?.textColor = #colorLiteral(red: 0.2392156863, green: 0.2509803922, blue: 0.3294117647, alpha: 1)
        cell.textLabel?.numberOfLines = 0
        cell.textLabel?.textAlignment = .center
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if xibTableArray.count == 0 {
            return
        }
        selectedString = xibTableArray[indexPath.row]
        delegate?.setData(selectedString: selectedString ?? "")
        UIView.animate(withDuration: 0.2, animations: {
            self.alpha = 0.0
            
        }) { (true) in
            self.removeXib()
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
}




