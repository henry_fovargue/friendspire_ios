//
//  HelpViewController.swift
//  FriendSpire
//
//  Created by Mac Test on 19/07/18.
//  Copyright © 2018 openkey. All rights reserved.
//
enum HelpSection:Int,CaseCountable{
    case faq,contactUs,tutorial
    func getName()->String{
        switch self{
        case .faq:
            return "Frequently asked questions"
        case .contactUs:
            return "Contact Us"
        case .tutorial:
            return "Tutorial"
        }
    }
}

import UIKit

class HelpViewController: DIBaseController {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
    }
    
    // MARK: - IBAction
    
    @IBAction func backButtonTapped(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }

}

extension HelpViewController:UITableViewDelegate,UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return HelpSection.caseCount
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: CellIdentifiers.SettingScreen.settingsCell) as? SettingsCell else {return UITableViewCell()}
        cell.configureHelpCell(indexPath)
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 60.0
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        switch indexPath.row {
        case HelpSection.faq.rawValue:
            let faqVC: FAQViewController = UIStoryboard(storyboard: .setting).initVC()
            self.navigationController?.pushViewController(faqVC, animated: true)
            break
        case HelpSection.contactUs.rawValue:
            let contactAdministratorVC: ContactAdministrator = UIStoryboard(storyboard: .setting).initVC()
            self.navigationController?.pushViewController(contactAdministratorVC, animated: true)
            break
        case HelpSection.tutorial.rawValue:
            showTutorialPopup()
            break
            
        default:
            break
        }
    }
    
    func showTutorialPopup(){
            TutorialStatus.resetTutorial()
            let tutorial : SlidingTutorialViewController =  UIStoryboard.init(storyboard: .tutorial).initVC()
            tutorial.modalTransitionStyle = .crossDissolve
            tutorial.modalPresentationStyle = .overCurrentContext
            tutorial.delegate = self
            tutorial.arrImages = [ARR_TUTORIAL[0]]
            AppDelegate.shared().window?.rootViewController?.present(tutorial, animated: true, completion: nil)
   }
    
}

extension HelpViewController: SlidingTutorialDelegate{
    
    func finishWithTutorial(sender: Bool) {
        if sender == false{
           AppDelegate.shared().navigateToFriendsTab()
//            let  controller: NetworkViewController = UIStoryboard.init(storyboard: .networks).instantiateViewController(withIdentifier: "userNetwork") as! NetworkViewController
//            controller.indexToShow = 2
//            controller.fromTutorial = true
//             AppDelegate.shared().window?.rootViewController?.presentedViewController?.dismiss(animated: false, completion: nil)
//            AppDelegate.shared().window?.rootViewController?.present(controller, animated: true, completion: nil)
        }else{
            AppDelegate.shared().navigateToDashboard()
        }
    }
}
