//
//  PrivacyPolicyViewController.swift
//  FriendSpire
//
//  Created by Mac Test on 19/07/18.
//  Copyright © 2018 openkey. All rights reserved.
//

import UIKit
import WebKit


enum webViewType {
    case Privacy
    case About
    case Terms
}


class PrivacyPolicyViewController: DIBaseController {
    
    var loadViewFor : webViewType?
    
    //IBOutlets
    
    @IBOutlet weak var lblTitle: UILabel!
    
    
    //Local Variable
    fileprivate var strUrl : String?
    fileprivate  var webView:WKWebView!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        
        if loadViewFor != nil{   loadScreen(for: loadViewFor!) }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func loadScreen(for type: webViewType){
        
        switch type {
        case .Privacy:
            self.lblTitle.text = AppMessages.ScreenTitles.privacy
            
            strUrl =  APIHandler().absolutePath(forApi: "privacy_policy")
            break
            
        case .About:
            
            self.lblTitle.text = AppMessages.ScreenTitles.about
            strUrl = APIHandler().absolutePath(forApi: "aboutUs")
            break
            
        case .Terms:
            self.lblTitle.text = AppMessages.ScreenTitles.terms
            strUrl = APIHandler().absolutePath(forApi: "termsConditions")
            break
            
        }
        
        let frame:CGRect = CGRect(x: self.view.frame.origin.x, y: self.view.frame.origin.y + 80, width: self.view.frame.width, height: self.view.frame.height - 80.0)
        self.webView = WKWebView.init(frame: frame)
        self.view.addSubview(webView)
        self.webView.navigationDelegate = self
        self.webView.uiDelegate = self
        
        if let url = URL(string: strUrl ?? "") {
            let request:URLRequest = URLRequest(url: url)
            self.webView.load(request)
            self.showLoader()
        }
    }
    
    
    
    // MARK: - IBAction
    
    @IBAction func backButtonTapped(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
}

// MARK: - WKWebView Delegation
extension PrivacyPolicyViewController:WKNavigationDelegate {
    
    func webView(_ webView: WKWebView, didFinish navigation: WKNavigation!) {
        self.hideLoader()
    }
    
    func webView(_ webView: WKWebView, didFail navigation: WKNavigation!, withError error: Error) {
        self.hideLoader()
    }
}

extension PrivacyPolicyViewController:WKUIDelegate {
    
}
