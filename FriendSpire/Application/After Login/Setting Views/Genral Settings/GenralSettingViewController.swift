//
//  GenralSettingViewController.swift
//  FriendSpire
//
//  Created by Mac Test on 19/07/18.
//  Copyright © 2018 openkey. All rights reserved.
//

import UIKit



class GenralSettingViewController: DIBaseController {
    
    @IBOutlet weak var tableView: UITableView!
    
    var location = true
    fileprivate let arrTitle = [Titles.SettingScreenTitles.GenralSettingTitles.distance,Titles.SettingScreenTitles.GenralSettingTitles.privateAccount,Titles.SettingScreenTitles.GenralSettingTitles.locations,Titles.SettingScreenTitles.GenralSettingTitles.push]
    
    fileprivate var settingValue = SettingsModal()
    var mileSelected = true
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        settingValue.isLocation = User.sharedInstance.isLocationEnabled ?? true
        settingValue.distanceUnit = DistanceUnit.Miles.rawValue
        tableView.tableFooterView = UIView()
        showLoader()
        DispatchQueue.global(qos: .background).async {
            self.getSettingDetail()
        }
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    // MARK: - IBAction
    
    @IBAction func backButtonTapped(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
}
// MARK: -
extension GenralSettingViewController : UITableViewDataSource,UITableViewDelegate{
    // MARK: - TableView Delegate
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrTitle.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        var cell : GenralSettingCell?
        var cellIdentifier = ""
        
        if indexPath.row == 0{cellIdentifier = CellIdentifiers.SettingScreen.GenralSetting.milesCell }
        else{cellIdentifier = CellIdentifiers.SettingScreen.GenralSetting.privateCell}
        
        cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier) as? GenralSettingCell
        cell?.configureCell(title: arrTitle[indexPath.row],setting: settingValue, index: indexPath.row)
        cell?.switch_Status?.addTarget(self, action: #selector(changeStatus(_:)), for: .valueChanged)
        cell?.btn_Miles?.addTarget(self, action: #selector(distanceSelected(_:)), for: .touchUpInside)
        cell?.btn_Kms?.addTarget(self, action: #selector(distanceSelected(_:)), for: .touchUpInside)
        
        return cell!
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        if indexPath.row == 1{
            return 120.0
        }
        
        return 60.0
    }
    
    // MARK: -  Custom Methods
    func distanceSelected(_ sender : UIButton){
        
        if !Utility.isConnectedToInternet {
            self.showAlert( message: AppMessages.ErrorAlerts.noInternet)
            return
        }
        
        let index =  IndexPath.init(row: 0, section: 0)
        
        if sender.tag == 10{
            User.sharedInstance.objAttributes?.distanceUnit = DistanceUnit.Miles
            settingValue.distanceUnit = DistanceUnit.Miles.rawValue
            distanceUnitUpdate(status: DistanceUnit.Miles)
        }else {
            
            User.sharedInstance.objAttributes?.distanceUnit = DistanceUnit.Kms
            settingValue.distanceUnit = DistanceUnit.Kms.rawValue
            distanceUnitUpdate(status: DistanceUnit.Kms)
        }
        User.sharedInstance.distanceUnit = settingValue.distanceUnit
        self.tableView.reloadRows(at: [index], with: .fade)
    }
    
    func changeStatus(_ sender : UISwitch){
        
        let index =  IndexPath.init(row: sender.tag, section: 0)
        
        switch sender.tag {
        case 1:
            if settingValue.isPrivate == 1{
                settingValue.isPrivate = 0
            }else {
                settingValue.isPrivate = 1
            }
            privateStatusUpdate(status: settingValue.isPrivate ?? 0)
            break
            
        case 2:
            if settingValue.isLocation == true{
                settingValue.isLocation = false
                User.sharedInstance.isLocationEnabled = false
                
            }else {
                settingValue.isLocation = true
                User.sharedInstance.isLocationEnabled = true
            }
            let user = User.sharedInstance
            UserManager.saveCurrentUser(user: user)
            break
            
        case 3:
            if settingValue.pushNotification == 1{
                settingValue.pushNotification = 0
            }else {
                settingValue.pushNotification = 1
            }
            pushStatusUpdate(status: settingValue.pushNotification ?? 0)
            break
        default:
            DILog.print(items: "Nothing to do")
        }
        //self.tableView.reloadData()
        self.tableView.reloadRows(at: [index], with: .fade)
    }
}


extension GenralSettingViewController{
    
    
    fileprivate func getSettingDetail(){
        
        if !Utility.isConnectedToInternet {
            self.hideLoader()
            self.showAlert( message: AppMessages.ErrorAlerts.noInternet)
            return
        }
        DIWebLayerUserAPI().getSettings(success: { [weak self](modal) in
            
            self?.settingValue = modal
            self?.settingValue.isLocation = User.sharedInstance.isLocationEnabled ?? true
            
            User.sharedInstance.distanceUnit = modal.distanceUnit
            DispatchQueue.main.async {
                self?.hideLoader()
                self?.tableView.reloadData()
            }
            
            
        }) { [weak self] (error) in
            DILog.print(items: error.message ?? "")
            DispatchQueue.main.async {
                self?.showAlert( message: error.message ?? AppMessages.ErrorAlerts.unknownError)
                
                self?.hideLoader()
                self?.tableView.reloadData()
            }
        }
    }
    
    fileprivate func pushStatusUpdate(status : Int){
        var dict = PushUpdate()
        dict.push = status
        
        if !Utility.isConnectedToInternet {
            self.hideLoader()
            self.showAlert( message: AppMessages.ErrorAlerts.noInternet)
            return
        }
        showLoader()
        DIWebLayerUserAPI().updatePushStatus(parameters: dict.getDictonary(), success: { [weak self](finish) in
            
            if finish == false {
                if self?.settingValue.pushNotification == 1{
                    self?.settingValue.pushNotification = 0
                }else {
                    self?.settingValue.pushNotification = 1
                }
            }
            
            DispatchQueue.main.async {
                self?.hideLoader()
                self?.tableView.reloadData()
            }
            
            
        }) { [weak self] (error) in
            DILog.print(items: error.message ?? "")
            DispatchQueue.main.async {
                self?.hideLoader()
                self?.showAlert( message: error.message ?? AppMessages.ErrorAlerts.unknownError)
                self?.tableView.reloadData()
            }
        }
    }
    
    fileprivate func distanceUnitUpdate(status : DistanceUnit){
        var dict = DistanceUpdate()
        dict.distanceUnit = status
        
        if !Utility.isConnectedToInternet {
            self.hideLoader()
            self.showAlert( message: AppMessages.ErrorAlerts.noInternet)
            return
        }
        showLoader()
        DIWebLayerUserAPI().updateDistanceStatus(parameters: dict.getDictonary(), success: { [weak self](finish) in
            
            if finish == false {
                if self?.settingValue.distanceUnit == DistanceUnit.Miles.rawValue{
                    self?.settingValue.distanceUnit = DistanceUnit.Kms.rawValue
                     User.sharedInstance.objAttributes?.distanceUnit = DistanceUnit.Kms
                }else {
                    self?.settingValue.distanceUnit = DistanceUnit.Miles.rawValue
                     User.sharedInstance.objAttributes?.distanceUnit = DistanceUnit.Miles
                }
                User.sharedInstance.distanceUnit =  self?.settingValue.distanceUnit
                
            }
            
            DispatchQueue.main.async {
                self?.hideLoader()
                self?.tableView.reloadData()
            }
            
            
        }) { [weak self] (error) in
            DILog.print(items: error.message ?? "")
            DispatchQueue.main.async {
                self?.showAlert( message: error.message ?? AppMessages.ErrorAlerts.unknownError)
                self?.hideLoader()
                self?.tableView.reloadData()
            }
        }
    }
    
    fileprivate func privateStatusUpdate(status : Int){
        var dict = PrivateAccountUpdate()
        dict.isPrivate = status
        if !Utility.isConnectedToInternet {
            self.hideLoader()
            self.showAlert( message: AppMessages.ErrorAlerts.noInternet)
            return
        }
        showLoader()
        DIWebLayerUserAPI().updatePrivateStatus(parameters: dict.getDictonary(), success: { [weak self](finish) in
            if finish == false {
                if self?.settingValue.isPrivate == 1{
                    self?.settingValue.isPrivate = 0
                }else {
                    self?.settingValue.isPrivate = 1
                }
            }
            
            DispatchQueue.main.async {
                self?.hideLoader()
                self?.tableView.reloadData()
            }
            
            
        }) { [weak self] (error) in
            DILog.print(items: error.message ?? "")
            DispatchQueue.main.async {
                self?.showAlert( message: error.message ?? AppMessages.ErrorAlerts.unknownError)
                self?.hideLoader()
                self?.tableView.reloadData()
            }
        }
    }
    
}
