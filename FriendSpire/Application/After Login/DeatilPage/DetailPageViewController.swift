//
//  DetailPageViewController.swift
//  FriendSpire
//
//  Created by Harpreet_kaur on 01/08/18.
//  Copyright © 2018 openkey. All rights reserved.
//


/* let parameters = ["lat": "\(lat.debugDescription)",
 "long": "\(long.debugDescription)"
 ]*/


import UIKit
import GoogleMaps
import GooglePlaces
import CoreLocation


class DetailPageViewController: DIBaseController {
    
    
    //MARK:-Varibles
    //MARK:-Variables.
    var isThirdPartySearchOn : Bool?
    var latlong = [NSNumber]()
    var locationManager = CLLocationManager()
    var currentAddress:Address?
    var category:Int = 1
    var modalRef : FeedModal?
    var isComingFromLink:Bool = false
    var postId:String = ""
    var openReplyFor: String?
    var openCommentTo: String?
    var fromAntherUserLibrary : Bool?
    
    //MARK:-IBOutlets.
    var objRestaurantDetail = RestaurantModelData()
    
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var addresssLabel: UILabel!
    @IBOutlet weak var pageControl: UIPageControl!
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var backView: UIView!
    @IBOutlet weak var navigationBarView: UIView!
    @IBOutlet weak var mapView: GMSMapView!
    @IBOutlet weak var cuisinesLabel: UILabel!
    @IBOutlet weak var detailTabelView: UITableView!
    @IBOutlet weak var coverImageView: UIImageView!
    @IBOutlet weak var friendsRatingImageView: UIImageView!
    @IBOutlet weak var everyOneImageView: UIImageView!
    @IBOutlet weak var friendsRatingLabel: UILabel!
    @IBOutlet weak var everyOneRatingLabel: UILabel!
    @IBOutlet weak var fourSquareRatingLabel: UILabel!
    @IBOutlet weak var mondayTimingLabel: UILabel!
    @IBOutlet weak var tuesdayTimingLabel: UILabel!
    @IBOutlet weak var wednesdayTimingLabel: UILabel!
    @IBOutlet weak var thursdayTimingLabel: UILabel!
    @IBOutlet weak var fridayTimingLabel: UILabel!
    @IBOutlet weak var staurdayTimingLabel: UILabel!
    @IBOutlet weak var sundayTimingLabel: UILabel!
    @IBOutlet weak var mobileNumberLabel: UILabel!
    @IBOutlet weak var emailLabel: UILabel!
    @IBOutlet weak var descriptionLabel: UILabel!
    @IBOutlet weak var stackViewTimings: UIStackView!
    @IBOutlet weak var constraintHeightTimings: NSLayoutConstraint!
    @IBOutlet weak var lblHours: UILabel!
    @IBOutlet weak var viewFooter: UIView!
    @IBOutlet weak var viewHeader: UIView!
    @IBOutlet weak var imgViewWeb: UIImageView!
    @IBOutlet weak var imgViewMobile: UIImageView!
    @IBOutlet weak var lblFriendReviewCount: UILabel!
    @IBOutlet weak var lblEveryoneReviewCount: UILabel!
    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var reviewLabel: UILabel!
    @IBOutlet weak var userImageView: UIImageView!
    @IBOutlet weak var bookmarkButton: UIButton!
    @IBOutlet weak var addRatingButton: UIButton!
    @IBOutlet weak var constraintWidthBookmark: NSLayoutConstraint!
    @IBOutlet weak var userRatingView: UIView!
    @IBOutlet weak var constraintWidthStackView: NSLayoutConstraint!    
    @IBOutlet weak var btnReply: UIButton!
    @IBOutlet weak var imgViewCategory: UIImageView!
    
    fileprivate var currentMapIcon = #imageLiteral(resourceName: "mapRes")
    
    
    //MARK:-ViewLifeCycle.
    //MARK:-ViewDidLoad.
    override func viewDidLoad() {
        super.viewDidLoad()
        //pageControl.numberOfPages = 0
        pageControl.isHidden = true
        
        //change color
        changeCategoryColor()
        pageControl.currentPage = 1
        detailTabelView.register(UINib(nibName: Constant.Xib.reviewsTabelCellXib, bundle: nil), forCellReuseIdentifier: Constant.TableviewCellIdentifier.reviewsTabelCellXib)
        detailTabelView.register(UINib(nibName: Constant.Xib.emptyTableCell, bundle: nil), forCellReuseIdentifier: Constant.TableviewCellIdentifier.emptyTableCell)
        if self.isThirdPartySearchOn == true{
            self.showLoader()
        }
        detailTabelView.estimatedSectionHeaderHeight = 100
        getUserCurrentLcoation()
        
        
        if openReplyFor != nil{
            self.perform(#selector(presentReviewController(recommendationId:)), with: openReplyFor, afterDelay: 0.3)
        }//show Tutorial screen to user
        //        else if TutorialStatus.getCurrentStatus() == nil ||  TutorialStatus.getCurrentStatus()?.detailScreen == nil{
        //            self.perform(#selector(showTutorialPopup(arr:)), with: [#imageLiteral(resourceName: "layer10")], afterDelay: 0.3)
        //            updateTutorialReadStatus()
        //        }
    }
    
    //MARK:-ViewWillAppear.
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        //        UserReviewHeader.delegate = self
        //        ReviewsTabelCellXib.delegate = self
        
        
        if   User.sharedInstance.isLocationEnabled  == false && ( category == Constant.CategoryTypeBackend.restraunt.rawValue ||  category == Constant.CategoryTypeBackend.bars.rawValue){
            DispatchQueue.main.asyncAfter(deadline: .now() + .milliseconds(100)) {
                
                self.showAlert(withTitle: AppMessages.Location.title, message: AppMessages.AlertTitles.locationOff, okayTitle: AppMessages.AlertTitles.Ok, cancelTitle: nil, okCall: {
                    self.navigationController?.popViewController(animated: true)
                }, cancelCall: {
                })
            }
        }
    }
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(true)
        self.hideLoader()
        
        if self.isMovingFromParentViewController{
            mapView?.clear()
            mapView?.removeFromSuperview()
        }
    }
    
    
    //MARK:-Back Button Action.
    @IBAction func backButtonAction(_ sender: UIButton) {
        
        //        mapView?.clear()
        //        mapView?.removeFromSuperview()
        if isComingFromLink {
            appDelegate.navigateToDashboard()
        }else{
            self.navigationController?.popViewController(animated: true)
        }
    }
    
    @IBAction func replyButtonTapped(_ sender: UIButton) {
        if let recommendationId = objRestaurantDetail.userRecommendationId{
            presentReviewController(recommendationId: recommendationId)
        }
    }
    
    @IBAction func callTapped(_ sender: UIButton) {
        
        DispatchQueue.main.async {
            
            guard let number1  = self.mobileNumberLabel.text?.onlyDigits() else {
                self.showAlert( message: "Phone number not available")
                return
            }
            
            if number1.count > 0 {
                guard let number = URL(string: "tel://" + number1) else {
                    self.showAlert( message: "Wrong number")
                    return
                }
                UIApplication.shared.open(number, options: [:], completionHandler: nil)
            }
        }
    }
    
    
    @IBAction func openUrlTapped(_ sender: UIButton) {
        
        if let url = URL(string: emailLabel.text ?? ""), UIApplication.shared.canOpenURL(url) {
            UIApplication.shared.open(url, options: [:], completionHandler: nil)
        }
    }
    
    @IBAction func openRefrenceLinkButton(_ sender: UIButton) {
        if let url = URL(string: objRestaurantDetail.refrenceUrl ?? ""), UIApplication.shared.canOpenURL(url) {
            if #available(iOS 10.0, *) {
                UIApplication.shared.open(url, options: [:], completionHandler: nil)
            } else {
                UIApplication.shared.openURL(url)
            }
        }
    }
    @IBAction func shareButtonAction(_ sender: UIButton) {
        self.shareData(id: objRestaurantDetail.id, type: category, notificationType: 1, title: objRestaurantDetail.title ?? "")
    }
    @IBAction func bookmarkButtonAction(_ sender: UIButton) {
        addToLibrary(slected: objRestaurantDetail.isBookMarked ?? false)
    }
    @IBAction func addRatingButtonAction(_ sender: UIButton) {
        openRatingPopup()
    }
    
    
    func addToLibrary(slected:Bool) {
        var ratingModal = RatingRequest()
        ratingModal.referenceid = postId
        ratingModal.type = category
        objRestaurantDetail.isBookMarked = !(objRestaurantDetail.isBookMarked ?? false)
        
        if slected == false { // addBookmark
            showLoader()
            self.addBookmark(modal: ratingModal) { (finish) in
                self.hideLoader()
                if finish == false{
                    self.objRestaurantDetail.isBookMarked = false  // failure, change back to previous state
                    // store for local change
                    self.modalRef?.isBookmarked = false
                    self.modalRef?.myBookmarkStatus = 0
                }else{
                    self.objRestaurantDetail.isBookMarked = true
                    self.modalRef?.isBookmarked = true
                    self.modalRef?.myBookmarkStatus = 1
                }
                self.bookmarkButton.isSelected = self.objRestaurantDetail.isBookMarked ?? false
                self.detailTabelView.reloadData()
            }
        }else{//remove bookmark
            self.showAlert(withTitle: appName.capitalized, message: AppMessages.AlertTitles.removeBookMark, okayTitle: AppMessages.AlertTitles.Yes, cancelTitle: AppMessages.AlertTitles.No, okCall: {
                self.showLoader()
                self.removeBookmark(modal: ratingModal) { (finish) in
                    self.hideLoader()
                    if finish == false{
                        self.objRestaurantDetail.isBookMarked = true //failure, change back to previous state
                        // store for local change
                        self.modalRef?.isBookmarked = true
                        self.modalRef?.myBookmarkStatus = 1
                    }else{
                        self.objRestaurantDetail.isBookMarked = false
                        self.modalRef?.isBookmarked = false
                        self.modalRef?.myBookmarkStatus = 0
                    }
                    self.bookmarkButton.isSelected = self.objRestaurantDetail.isBookMarked ?? false
                    self.detailTabelView.reloadData()
                }
            }) {
                //Nothing to do
                //  sender.isSelected = !sender.isSelected
                // store for local change
                self.modalRef?.isBookmarked = slected
                self.modalRef?.myBookmarkStatus = slected == true ? 1 : 0
            }
        }
    }
    func openRatingPopup() {
        self.addRating(using:category, referenceID: postId,modalRef,fromAntherUserLibrary)
    }
    
    func redirectToUserProfile() {
        let  controller: UserProfileViewController = UIStoryboard.init(storyboard: .networks).initVC()
        controller.userId =  User.sharedInstance.id
        self.navigationController?.pushViewController(controller, animated: true)
    }
    
    //MARK:-Get Data From Server.
    func getData() {
        
        if !Utility.isConnectedToInternet {
            self.hideLoader()
            //            self.showAlert( message: AppMessages.ErrorAlerts.noInternet)
            self.showAlert(withTitle: appName.capitalized, message:  AppMessages.ErrorAlerts.noInternet, okayTitle: AppMessages.AlertTitles.Ok, okCall: {
                self.navigationController?.popViewController(animated: true)
            })
            
            return
        }
        
        if User.sharedInstance.userCurrentLocation == nil{
            self.openSetting()
            self.hideLoader()
            return
        }
        
        DIWebLayerCategoriesDetailApi().getRestaurantCategoryDeatils(category: category, postID: postId, latLong: [User.sharedInstance.userCurrentLocation?.latitude.debugDescription ?? "0.0" , User.sharedInstance.userCurrentLocation?.longitude.debugDescription ?? "0.0"]
            , success: { [weak self](data,latlong) in
                self?.hideLoader()
                self?.objRestaurantDetail = data
                self?.latlong = latlong
                self?.setData()
                self?.initMap()
                self?.detailTabelView.reloadData()
        }) { [weak self](error) in
            self?.hideLoader()
            self?.showAlert(withTitle: appName.capitalized, message: error.message, okayTitle: AppMessages.AlertTitles.Ok, okCall: {
                self?.navigationController?.popViewController(animated: true)
            })
        }
    }
    //MARK:-SetData.
    func setData() {
        setUserRatingViewData()
        if objRestaurantDetail.image.count >= 1 {
            self.pageControl.numberOfPages = objRestaurantDetail.image.count > 5 ? 5 : objRestaurantDetail.image.count
            self.collectionView.reloadData()
        }else{
            coverImageView.image = #imageLiteral(resourceName: "placeholderPoster")
        }
        addresssLabel.text = objRestaurantDetail.address.joined(separator: ",")
        
        if objRestaurantDetail.image.count > 1 {
            pageControl.isHidden = false
        } else {
            pageControl.isHidden = true
        }
        titleLabel.text = (objRestaurantDetail.title ?? "")
        cuisinesLabel.text = "\((objRestaurantDetail.distance ?? 0).getDistanceString()) | " + Utility.getModifiedJoinString(using: objRestaurantDetail.cuisine) + " | " + Utility.getPriceLevel(value: objRestaurantDetail.priceTier ?? 0)
        friendsRatingLabel.text = String(format: "%.1f", arguments: [objRestaurantDetail.friendsRating ?? 0.0]).removeZero()
        everyOneRatingLabel.text = String(format: "%.1f", arguments: [objRestaurantDetail.friendspireRating ?? 0.0]).removeZero()
        fourSquareRatingLabel.text = String(format: "%.1f", arguments: [objRestaurantDetail.rating ?? 0.0]).removeZero()
        friendsRatingImageView.image = Utility.getEmojiAccordindToRating(rating: objRestaurantDetail.friendsRating ?? 0.0)
        everyOneImageView.image = Utility.getEmojiAccordindToRating(rating: objRestaurantDetail.friendspireRating ?? 0.0)
        lblFriendReviewCount.text = objRestaurantDetail.countFriends?.countFormationWithoutReview
        lblEveryoneReviewCount.text = objRestaurantDetail.everyoneCount?.countFormationWithoutReview
        descriptionLabel.text = objRestaurantDetail.descriptionField ?? ""
        if objRestaurantDetail.phone != ""{
            mobileNumberLabel.text = objRestaurantDetail.phone ?? ""
        }else {
            mobileNumberLabel.text = ""
            imgViewMobile.image = nil
        }
        if objRestaurantDetail.url != ""{
            emailLabel.text = objRestaurantDetail.url ?? ""
        }else {
            emailLabel.text =  ""
            imgViewWeb.image = nil
        }
        
        let timeArray = objRestaurantDetail.timings
        
        for (index,data) in (timeArray.enumerated()) {
            var value = ""
            for (index,data1) in (data.enumerated()) {
                let start = (data1["start"] ?? "").replace("+", replacement: "")
                let end = (data1["end"] ?? "").replace("+", replacement: "")
                if index > 0{
                    value.append(", ")
                }
                
                value.append("\(Utility.openHours(value: start)) - \(Utility.openHours(value: end))")
                value = value.replace(":00 ", replacement: "")
            }
            switch index {
            case 0:
                mondayTimingLabel.text = value
            case 1:
                tuesdayTimingLabel.text = value
            case 2:
                wednesdayTimingLabel.text = value
            case 3:
                thursdayTimingLabel.text = value
            case 4:
                fridayTimingLabel.text = value
            case 5:
                staurdayTimingLabel.text = value
            case 6:
                sundayTimingLabel.text = value
            default :
                break;
            }
        }
        
        if timeArray.count == 0{
            stackViewTimings.isHidden = true
            constraintHeightTimings.constant = 0
            lblHours.isHidden = true
        }
        
        descriptionLabel.text = objRestaurantDetail.descriptionField
        detailTabelView.reloadData()
        self.perform(#selector(sizeHeaderToFit), with: self, afterDelay: 0.2)
        self.perform(#selector(sizeFooterToFit), with: self, afterDelay: 0.3)
    }
    
    func setUserRatingViewData() {
        let tap = UITapGestureRecognizer(target: self, action: #selector(redirectToUserProfile))
        userImageView?.addGestureRecognizer(tap)
        let tapReview = UITapGestureRecognizer(target: self, action: #selector(openRatingPopup))
        reviewLabel.addGestureRecognizer(tapReview)
        if let imageUrl = URL.init(string: User.sharedInstance.profilePicUrl ?? "") {
            userImageView.af_setImage(withURL: imageUrl, placeholderImage: #imageLiteral(resourceName: "user") , filter: nil, progress: nil, imageTransition: .crossDissolve(0.7), runImageTransitionIfCached: false, completion: nil)
        }
        dateLabel.text = objRestaurantDetail.userUpdatedAt?.convertDateLocal(.serverFormat, toFormat: .reviewsDate)
        bookmarkButton.isSelected = objRestaurantDetail.isBookMarked ?? false
        if objRestaurantDetail.isBookMarked == true {
            bookmarkButton.setImage(#imageLiteral(resourceName: "bookmark-black-shape-active"), for: .normal)
        }else{
            bookmarkButton.setImage(#imageLiteral(resourceName: "bookmark-black-shape"), for: .normal)
        }
        let color = Utility.getColor(using: Utility.getBackendTypeString(using: category))
        addRatingButton.backgroundColor = color
        if objRestaurantDetail.userRating == 0.0 {
            addRatingButton.setImage(#imageLiteral(resourceName: "add -neq-white"), for: .normal)
            bookmarkButton.isHidden = false
            constraintWidthBookmark.constant = 40
            constraintWidthStackView.constant = 110
            reviewLabel.text = "Click + to leave a review"
            reviewLabel.isUserInteractionEnabled = false
            btnReply.isHidden = true
        }else{
            let img = Utility.getEmojiAccordindToRating(rating: objRestaurantDetail.userRating ?? 0.0)
            addRatingButton.setImage(Utility.resizeImage(image: img, newWidth: 20.0), for: .normal)
            bookmarkButton.isHidden = true
            constraintWidthBookmark.constant = 0
            constraintWidthStackView.constant = 70
            if objRestaurantDetail.userReview != nil{
                reviewLabel.isUserInteractionEnabled = true
                reviewLabel.text = objRestaurantDetail.userReview  == "" ? "You have not left a comment. " : objRestaurantDetail.userReview
            }else{
                reviewLabel.isUserInteractionEnabled = false
            }
            btnReply.isHidden = false
            let count = objRestaurantDetail.userCommentCount ?? 0 > 0 ? "(\(objRestaurantDetail.userCommentCount ?? 0))" : ""
            
            if objRestaurantDetail.userCommentCount ?? 0 > 1 {
                btnReply.setTitle("Comments \(count)", for: .normal)
            }else{
                btnReply.setTitle("Comment \(count)", for: .normal)
            }
        }
    }
    
    //MARK:-PrivateFunction.
    //    private func updateTutorialReadStatus(){
    //
    //        if let readStatus = TutorialStatus.getCurrentStatus(){
    //            readStatus.detailScreen = true
    //            TutorialStatus.saveCurrentStatus(status: readStatus)
    //        }else{
    //            let status = TutorialStatus.sharedInstance
    //            status.detailScreen = true
    //            TutorialStatus.saveCurrentStatus(status: status)
    //        }
    //    }
    
    func initMap() {
        mapView.isMyLocationEnabled = true
        mapView.settings.myLocationButton = true
        //FIXME: code to start map view rendering
        //        mapView.delegate = self
        mapView.startRendering()
        //        mapView.padding = UIEdgeInsets(top: 0, left: 0, bottom: 20, right: 0)
        updateMap()
    }
    
    private func changeCategoryColor(){
        currentMapIcon = category == Constant.CategoryTypeBackend.restraunt.rawValue ? #imageLiteral(resourceName: "mapRes") : #imageLiteral(resourceName: "mapBar")
        
        let strCategory = Utility.getBackendTypeString(using: category)
        let color = Utility.getColor(using: strCategory)
        backView.backgroundColor = color
        navigationBarView.backgroundColor = color
        lblEveryoneReviewCount.textColor = color
        lblFriendReviewCount.textColor = color
        imgViewCategory.image = Utility.getIcon(using: strCategory)
        imgViewCategory.backgroundColor = color
        self.colorToUse = color
    }
    
    //MARK:-UpdateMap.
    func updateMap() -> Void {
        mapView.clear()
        if (self.latlong.count ) >= 2 {
            let camera = GMSCameraPosition.camera(withLatitude: CLLocationDegrees(self.latlong[0] ), longitude: CLLocationDegrees(self.latlong[1]), zoom: 13)
            self.mapView.animate(to: camera)
        }
        self.showMarkers()
    }
    
    //MARK:-ShowMarkerOnGoogleMapForSelectedLocation.
    private func showMarkers() -> Void {
        if objRestaurantDetail.location?.count ?? 0 >= 2  {
            let position = CLLocationCoordinate2DMake(CLLocationDegrees(objRestaurantDetail.location?[0] ?? 0.0), CLLocationDegrees(objRestaurantDetail.location?[1] ?? 0.0))
            let marker = GMSMarker(position: position)
            marker.infoWindowAnchor = CGPoint(x: 0.5, y: 0.2)
            marker.map = self.mapView
            marker.icon = currentMapIcon
            marker.userData = index
        }
    }
    
    //MARK:-Get UserCurrentLocation.
    fileprivate func getUserCurrentLcoation() -> Void {
        if LocationManager.shared.permissionStatus == true{
            self.showLoader()
        }else{
            self.openSetting()
            return
        }
        LocationManager.shared.start()
        LocationManager.shared.success = { address,timestamp in
            self.hideLoader()
            User.sharedInstance.userCurrentLocation = CLLocationCoordinate2D.init(latitude: address.lat ?? 0.0, longitude: address.long ?? 0.0)
            if self.isThirdPartySearchOn == nil || self.isThirdPartySearchOn == false   {
                self.showLoader()
                self.getData()
            }
        }
        LocationManager.shared.failure = {
            self.hideLoader()
            if $0.code == .locationPermissionDenied {
                self.openSetting()
            } else {
                self.showAlert(withError: $0)
            }
        }
    }
    
    @objc fileprivate  func openSetting() {
        self.showAlert(withTitle: AppMessages.Location.title, message: AppMessages.Location.off, okayTitle: "Settings".localized, cancelTitle: "Cancel", okCall: {
            UIApplication.shared.open(URL(string: UIApplicationOpenSettingsURLString)! as URL)
        }, cancelCall: {
            
            guard let nav = self.navigationController else{
                UIApplication.topViewController()?.navigationController?.popViewController(animated: true)
                return
            }
            nav.popViewController(animated: true)
        })
    }
    
    func reportReview(indexPath:IndexPath){
        if (objRestaurantDetail.reviews[indexPath.row].userId?.id ?? "") == User.sharedInstance.id{
            self.showAlert(message:"You can't report your own review.".localized)
            return
        }
        self.showLoader()
        let params = ["recommendationid":objRestaurantDetail.reviews[indexPath.row].id]
        DIWebLayerCategoriesDetailApi().reportReview(parameter: params , success: { (value) in
            self.hideLoader()
            let message = value["message"] as? String ?? ""
            self.showAlert(message:message)
        }) { (error) in
            self.hideLoader()
            self.showAlert(message:error.message)
        }
    }
    
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        var visibleRect = CGRect()
        visibleRect.origin = self.collectionView.contentOffset
        visibleRect.size = self.collectionView.bounds.size
        let bounds = UIScreen.main.bounds
        let width = bounds.size.width
        let indexPoint =  visibleRect.origin.x / width
        self.updateTextAndController(index: Int(indexPoint))
    }
    
    func updateTextAndController(index:Int){
        self.pageControl.currentPage = index
    }
}

//MARK:-UITabelViewDelegate&UITabelViewDataSource.
extension DetailPageViewController : UITableViewDelegate , UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if objRestaurantDetail.reviews.count == 0 {
            return 0
        }
        return objRestaurantDetail.reviews.count == 6 ? 5 : objRestaurantDetail.reviews.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if objRestaurantDetail.reviews.count == 0 {
            guard let cell = detailTabelView.dequeueReusableCell(withIdentifier: Constant.TableviewCellIdentifier.emptyTableCell, for: indexPath) as? EmptyTableCell else {
                return UITableViewCell()
            }
            return cell
        }
        guard let cell = detailTabelView.dequeueReusableCell(withIdentifier: Constant.TableviewCellIdentifier.reviewsTabelCellXib, for: indexPath) as? ReviewsTabelCellXib else {
            return UITableViewCell()
        }
        cell.contentView.backgroundColor = .white
        cell.userImageView.tag = indexPath.row
        cell.userNameLabel.tag = indexPath.row
        cell.setData(reviewsData: objRestaurantDetail.reviews[indexPath.row])
        cell.btnReply.tag = indexPath.row
        cell.btnReply.addTarget(self, action: #selector(replyOnReview(sender:)), for: .touchUpInside)
        cell.delegate = self
        return cell
    }
    
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let nibView = Bundle.main.loadNibNamed("UserReviewHeader" , owner: self, options: nil)?.first as? UserReviewHeader
        nibView?.delegate = self
        let tap = UITapGestureRecognizer(target: self, action: #selector(redirectToUserProfile))
        nibView?.imgViewUser.addGestureRecognizer(tap)
        let tapReview = UITapGestureRecognizer(target: self, action: #selector(openRatingPopup))
        nibView?.lblReviews.addGestureRecognizer(tapReview)
        if let imageUrl = URL.init(string: User.sharedInstance.profilePicUrl ?? "") {
            nibView?.imgViewUser.af_setImage(withURL: imageUrl, placeholderImage: #imageLiteral(resourceName: "user") , filter: nil, progress: nil, imageTransition: .crossDissolve(0.7), runImageTransitionIfCached: false, completion: nil)
        }
        nibView?.lblName.text = objRestaurantDetail.userUpdatedAt?.convertDateLocal(.serverFormat, toFormat: .reviewsDate)
        nibView?.bookmarkButton.isSelected = objRestaurantDetail.isBookMarked ?? false
        if objRestaurantDetail.isBookMarked == true {
            nibView?.bookmarkButton.setImage(#imageLiteral(resourceName: "bookmark-black-shape-active"), for: .normal)
        }else{
            nibView?.bookmarkButton.setImage(#imageLiteral(resourceName: "bookmark-black-shape"), for: .normal)
        }
        let color = Utility.getColor(using: Utility.getBackendTypeString(using: category))
        nibView?.addRatingButton.backgroundColor = color
        if objRestaurantDetail.userRating == 0.0 {
            nibView?.addRatingButton.setImage(#imageLiteral(resourceName: "add -neq-white"), for: .normal)
            nibView?.bookmarkButton.isHidden = false
            nibView?.constraintWidthBookmark.constant = 40
            nibView?.constraintWidthStackView.constant = 110
            nibView?.lblReviews.text = "Click + to leave a review"
            nibView?.lblReviews.isUserInteractionEnabled = false
            nibView?.btnReply.isHidden = true
            //            let count = objRestaurantDetail.userReview
            nibView?.btnReply.setTitle("Comment", for: .normal)
            
        }else{
            let img = Utility.getEmojiAccordindToRating(rating: objRestaurantDetail.userRating ?? 0.0)
            nibView?.addRatingButton.setImage(Utility.resizeImage(image: img, newWidth: 20.0), for: .normal)
            nibView?.bookmarkButton.isHidden = true
            nibView?.constraintWidthBookmark.constant = 0
            nibView?.constraintWidthStackView.constant = 70
            if objRestaurantDetail.userReview != nil{
                nibView?.lblReviews.isUserInteractionEnabled = true
                nibView?.lblReviews.text = objRestaurantDetail.userReview  == "" ? "You have not left a comment. " : objRestaurantDetail.userReview
            }else{
                nibView?.lblReviews.isUserInteractionEnabled = false
            }
            nibView?.btnReply.isHidden = false
            let count = objRestaurantDetail.userCommentCount ?? 0 > 0 ? "(\(objRestaurantDetail.userCommentCount ?? 0))" : ""
            
            if objRestaurantDetail.userCommentCount ?? 0 > 1 {
                nibView?.btnReply.setTitle("Comments \(count)", for: .normal)
            }else{
                nibView?.btnReply.setTitle("Comment \(count)", for: .normal)
            }
        }
        nibView?.btnReply.addTarget(self, action: #selector(replyButtonTapped(_:)), for: .touchUpInside)
        
        if objRestaurantDetail.reviews.count <= 5 {
            nibView?.viewAllButton.isHidden = true
            nibView?.viewAllButton.isUserInteractionEnabled = false
        }else {
            nibView?.viewAllButton.isHidden = false
            nibView?.viewAllButton.isUserInteractionEnabled = true
        }
        if objRestaurantDetail.reviews.count == 0 {
            nibView?.lblNoReview.isHidden = false
        }else{
            nibView?.lblNoReview.isHidden = true
        }
        return nibView
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        
        if objRestaurantDetail.reviews.count == 0 {
            return false
        }
        return true
    }
    
    func tableView(_ tableView: UITableView, editActionsForRowAt indexPath: IndexPath) -> [UITableViewRowAction]? {
        
        let report = UITableViewRowAction(style: UITableViewRowActionStyle.destructive, title: AppMessages.AlertTitles.Report){(UITableViewRowAction,NSIndexPath) -> Void in
            self.reportReview(indexPath:indexPath)
        }
        return [report]
    }
    
    func sizeHeaderToFit() {
        let headerView = detailTabelView.tableHeaderView!
        headerView.setNeedsLayout()
        headerView.layoutIfNeeded()
        var frame = headerView.frame
        frame.size.height = headerView.systemLayoutSizeFitting(UILayoutFittingCompressedSize).height
        headerView.frame = frame
        detailTabelView.tableHeaderView = headerView
    }
    
    func sizeFooterToFit() {
        
        let footerView = detailTabelView.tableFooterView!
        footerView.setNeedsLayout()
        footerView.layoutIfNeeded()
        var frame = footerView.frame
        let minHeight = footerView.systemLayoutSizeFitting(UILayoutFittingCompressedSize).height + 50
        frame.size.height = minHeight
        footerView.frame = frame
        detailTabelView.tableFooterView = footerView
    }
    
    
    @objc func replyOnReview(sender:UIButton){
        guard let recommendationId = objRestaurantDetail.reviews[sender.tag].id else {
            return
        }
        presentReviewController(recommendationId: recommendationId)
    }
    
    @objc fileprivate  func presentReviewController(recommendationId : String){
        let controller: CommentsViewController = UIStoryboard(storyboard: .inspiration).initVC()
        //        controller.modalPresentationStyle = .overCurrentContext
        //        controller.modalTransitionStyle = .crossDissolve
        //        controller.category = self.category
        //        controller.postId = self.postId
        controller.colorToUse = self.colorToUse
        controller.commentID = self.openCommentTo
        controller.delegate = self
        controller.reviewId = recommendationId
        self.navigationController?.pushViewController(controller, animated: true)
        //        AppDelegate.shared().window?.rootViewController?.present(controller, animated: true, completion: nil)
    }
}

extension DetailPageViewController : AddRatingDelegate{
    func refreshViewDetail(rated: Bool) {
        if rated == true{
            showLoader()
            getData()
        }
    }    
}
extension DetailPageViewController:ReviewsTabelCellXibDelegate {
    func redirectToProfile(tag: Int,section:Int) {
        let  controller: UserProfileViewController = UIStoryboard.init(storyboard: .networks).initVC()
        controller.userId =  objRestaurantDetail.reviews[tag].userId?.id ?? ""
        self.navigationController?.pushViewController(controller, animated: true)
    }
    
    func refresh() {
        getData()
    }
}
extension DetailPageViewController : UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int{
        
        if objRestaurantDetail.image.count == 0{
            pageControl.numberOfPages = 1
            return 1
        }
        
        let count = objRestaurantDetail.image.count > 5 ? 5 : objRestaurantDetail.image.count
        pageControl.numberOfPages = count
        return count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if objRestaurantDetail.image.count == 0{
            guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "DetailImagesCollectionViewCell", for: indexPath) as? DetailImagesCollectionViewCell else {return UICollectionViewCell()}
            cell.imageView.image = #imageLiteral(resourceName: "placeholderPoster")
            return cell
        }
        guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "DetailImagesCollectionViewCell", for: indexPath) as? DetailImagesCollectionViewCell else {return UICollectionViewCell()}
        var strURl =  objRestaurantDetail.image[indexPath.item]
        strURl = strURl.replacingOccurrences(of: "size", with: "\(Int(collectionView.frame.size.width))x\(Int(collectionView.frame.size.height))")
        if let imageUrl = URL.init(string:strURl) {
            cell.imageView.af_setImage(withURL: imageUrl, placeholderImage: #imageLiteral(resourceName: "placeholderPoster") , filter: nil, progress: nil, imageTransition: .crossDissolve(0.7), runImageTransitionIfCached: false, completion: nil)
        }
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: self.collectionView.frame.width, height: self.collectionView.frame.height)
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        if scrollView == collectionView {
            let pageWidth = scrollView.frame.width
            let currentPage = Int((scrollView.contentOffset.x + pageWidth / 2)/pageWidth)
            
            pageControl.currentPage = currentPage
        }
        if scrollView == detailTabelView {
            if scrollView.contentOffset.y >= 290.0 {
                userRatingView.isHidden = false
            }else{
                userRatingView.isHidden = true
            }
        }
    }
}

extension DetailPageViewController : ReviewsDelegate{
    func moveToProfileController(using userId: String?) {
        
        let  controller: UserProfileViewController = UIStoryboard.init(storyboard: .networks).initVC()
        controller.userId = userId
        self.navigationController?.pushViewController(controller, animated: true)
    }
}


extension DetailPageViewController:UserActions {
    func userAction(tag: Int, selected: Bool) {
        switch tag {
        case 0:
            self.shareData(id: objRestaurantDetail.id, type: category, notificationType: 1, title: objRestaurantDetail.title ?? "")
        case 1:
            addToLibrary(slected: selected)
        case 2:
            openRatingPopup()
        case 3:
            let controller: ReviewsViewController = UIStoryboard(storyboard: .inspiration).initVC()
            controller.colorToUse = self.colorToUse
            controller.category = self.category
            controller.postId = self.postId
            controller.delegate = self
            self.navigationController?.pushViewController(controller, animated: true)
        default:
            break
        }
    }
}
