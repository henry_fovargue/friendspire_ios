//
//  DetailTabelCell.swift
//  FriendSpire
//
//  Created by Harpreet_kaur on 01/08/18.
//  Copyright © 2018 openkey. All rights reserved.
//

import UIKit

class DetailTabelCell: UITableViewCell {

    
    //MARK:-IBOutlets.
    @IBOutlet weak var userImageView: UIImageView!
    @IBOutlet weak var userNameLabel: UILabel!
    @IBOutlet weak var emojiImageView: UIImageView!
    @IBOutlet weak var userRatingDescLabel: UILabel!
    @IBOutlet weak var userRatingLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }

}
