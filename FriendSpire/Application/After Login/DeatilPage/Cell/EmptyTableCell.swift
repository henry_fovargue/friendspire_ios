//
//  EmptyTableCell.swift
//  FriendSpire
//
//  Created by Harpreet_kaur on 17/08/18.
//  Copyright © 2018 openkey. All rights reserved.
//

import UIKit

class EmptyTableCell: UITableViewCell {

    
    //MARK:-IBOutlets.
    @IBOutlet weak var messageLabel: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    
}
}
