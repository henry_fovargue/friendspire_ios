//
//  UserReviewHeader.swift
//  FriendSpire
//
//  Created by Harpreet_kaur on 23/10/18.
//  Copyright © 2018 openkey. All rights reserved.
//

import UIKit
protocol UserActions : class{
    func userAction(tag:Int,selected:Bool)
}

class UserReviewHeader: UIView {
    
    //MARK:-Variables.
   weak var delegate:UserActions?
    
    //MARK:-IBOutlets.
    @IBOutlet weak var imgViewUser: UIImageView!
    @IBOutlet weak var lblNoReview: UILabel!
    @IBOutlet weak var viewAllButton: UIButton!
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var lblReviews: UILabel!
    @IBOutlet weak var addRatingButton: UIButton!
    @IBOutlet weak var bookmarkButton: UIButton!
    @IBOutlet weak var constraintWidthBookmark: NSLayoutConstraint!
    @IBOutlet weak var constraintWidthStackView: NSLayoutConstraint!
    
    @IBOutlet weak var btnReply: UIButton!
    
    //MARK:-IBActions.
    @IBAction func allButtonsAction(_ sender: UIButton) {
       delegate?.userAction(tag: sender.tag, selected: sender.isSelected)
    }
    
    
}
