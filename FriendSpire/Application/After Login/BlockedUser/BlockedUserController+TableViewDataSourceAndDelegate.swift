//
//  BlockedUserController+TableViewDataSourceAndDelegate.swift
//  Noah
//
//  Created by Harpreet on 16/03/18.
//  Copyright © 2018 Harpreet. All rights reserved.
//

import Foundation
import UIKit
import SwipeCellKit

//MARK:-UITableViewDelegate And UITableViewDataSource.
extension BlockedUserController: UITableViewDelegate,UITableViewDataSource,SwipeTableViewCellDelegate{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        blockedUserArray.count == 0 ? tableView.setEmptyMessage("No Blocked Users Found") : tableView.setEmptyMessage("")
        return blockedUserArray.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 71
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return 99
    }
    
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        return true
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell:BlockedUserTableCell = blockedUserTableView.dequeueReusableCell(withIdentifier: Constant.TableviewCellIdentifier.blockedUserTableCell) as! BlockedUserTableCell
        cell.setData(user:self.blockedUserArray[indexPath.row])
        cell.delegate = self
        return cell
    }
    
    //Library delegates
    func tableView(_ tableView: UITableView, editActionsForRowAt indexPath: IndexPath, for orientation: SwipeActionsOrientation) -> [SwipeAction]? {
        guard orientation == .right else { return nil }
        
        
        let unBlockAction = SwipeAction(style: .default, title: nil) { action, indexPath in
           self.unblockUser(id:self.blockedUserArray[indexPath.row].id ?? "",tag:indexPath.row)
        }
        // customize the action appearance
        unBlockAction.backgroundColor = #colorLiteral(red: 0.1294117719, green: 0.2156862766, blue: 0.06666667014, alpha: 1)
        unBlockAction.image = UIImage(named: "ic_unblock")  //UIImage(named: "ic_block_black_24px")
        return [unBlockAction]
    }

    
    func unblockUser(id:String,tag:Int) {
        self.showLoader()
        DIWebLayerUserAPI().unBlockUser(parameters: ["user_id":id], success: { (message) in
            self.hideLoader()
            self.blockedUserArray.remove(at: tag)
            self.blockedUserTableView.reloadData()
            //Refresh friends list on network view controller
            
            let dict = [RefreshNetworkKey.id.rawValue:id,RefreshNetworkKey.type.rawValue:RefreshNetwork.unblock.rawValue] as [String : Any]
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: notificationNetwork), object: dict)
        }) { (error) in
            self.hideLoader()
            self.blockedUserTableView.reloadData()
            self.showAlert(message:error.message)
        }
        
    }
}
