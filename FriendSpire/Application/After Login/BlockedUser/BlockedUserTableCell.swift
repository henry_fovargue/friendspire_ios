//
//  BlockedUserTableCell.swift
//  Noah
//
//  Created by Narinder on 16/03/18.
//  Copyright © 2018 Harpreet. All rights reserved.
//

import UIKit
import SwipeCellKit

class BlockedUserTableCell: SwipeTableViewCell {

    
    //MARK:-IBOutlets.
    @IBOutlet weak var userImageView: UIImageView!
    @IBOutlet weak var userNameLabel: UILabel!
    
    
    //MARK:-UITableViewCell
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    func setData(user:BlockedUser){
        if let imageUrl = URL.init(string: user.profilePic ?? "") {
            userImageView.af_setImage(withURL: imageUrl, placeholderImage: #imageLiteral(resourceName: "user") , filter: nil, progress: nil, imageTransition: .crossDissolve(0.7), runImageTransitionIfCached: false, completion: nil)
        }
        userNameLabel.text = "\(user.firstName ?? "") \(user.lastName ?? "")"
    }

}
