//
//  BlockedUserController.swift
//  Noah
//
//  Created by Narinder on 15/03/18.
//  Copyright © 2018 Harpreet. All rights reserved.
//

import UIKit

class BlockedUserController: DIBaseController {

    
    //MARK:-Variables.
    var blockedUserArray = [BlockedUser]()
    var currentPageNumber = 1
    var previousPageNumber = 1
    var refreshControl: UIRefreshControl!
    
    
    //MARK:-IBOutlets.
    @IBOutlet weak var blockedUserTableView: UITableView!
    
    
    //MARK:-IBActions.
    //MARK:-IBActions
    @IBAction func backButtonAction(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
    //MARK:-ViewLifeCycle.
    //MARK:-ViewDidLoad.
    override func viewDidLoad() {
        super.viewDidLoad()
        blockedUserTableView.tableFooterView = UIView()
        addRefreshController()
        self.showLoader()
        getBlockedUser()
    }
    //MARK:-ViewWillAppear.
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        self.navigationController?.isNavigationBarHidden = true
    }
    
    //MARK:-PrivateFunction.
    //MARK:-Function to fetch Blocked User.
    func getBlockedUser() {
        if !Utility.isConnectedToInternet {
            self.showAlert( message: AppMessages.ErrorAlerts.noInternet)
            return
        }
            DIWebLayerUserAPI().getBlockedUser(page:currentPageNumber, success: { (data) in
                self.hideLoader()
                if self.currentPageNumber == 1{
                    self.blockedUserArray.removeAll()
                }
                self.blockedUserArray += data
                if data.count == 15 {
                    self.currentPageNumber += 1
                }
                self.refreshControl.endRefreshing()
                self.blockedUserTableView.reloadData()
            }) { (error) in
                 self.hideLoader()
                self.refreshControl.endRefreshing()
//                self.showAlert(message:error.message)
            }
    }
    
    //MARK:- AddRefreshController To TableView.
    private func addRefreshController() {
        refreshControl = UIRefreshControl()
        refreshControl.addTarget(self, action: #selector(refreshControlAction(sender:)) , for: .valueChanged)
        blockedUserTableView.addSubview(refreshControl)
    }
    
    //MARK:- Function To Refresh News Tableview Data.
    @objc func refreshControlAction(sender:AnyObject) {
        if Utility.isConnectedToInternet {
            currentPageNumber = 1
            previousPageNumber = 1
        }
        self.refreshControl.beginRefreshing()
        getBlockedUser()
    }
}
