//
//  BookmarksLibraryViewController+GMSAutocompleteViewControllerDelegate.swift
//  FriendSpire
//
//  Created by Harpreet_kaur on 23/10/18.
//  Copyright © 2018 openkey. All rights reserved.
//

import Foundation
import GooglePlaces

extension BookmarksLibraryViewController:GMSAutocompleteViewControllerDelegate {
    
    func viewController(_ viewController: GMSAutocompleteViewController, didAutocompleteWith place: GMSPlace) {
        self.currentAddress = Address(gmsPlace: place)
        RestuarantFilterModel.objFeedListRequest.latLong = "[\(String(describing: self.currentAddress?.lat ?? 0.0)),\(String(describing: self.currentAddress?.long ?? 0.0))]"
        RestuarantFilterModel.objFeedListRequest.filterLatLong = 1
        self.txtLocation.text = self.currentAddress?.city
        self.customLocationBookmark = place.coordinate
        self.filterApplied()
        viewController.dismiss(animated: true, completion: nil)
    }
    
    func viewController(_ viewController: GMSAutocompleteViewController, didFailAutocompleteWithError error: Error) {
//        print(error.localizedDescription)
    }
    
    func wasCancelled(_ viewController: GMSAutocompleteViewController) {
        viewController.dismiss(animated: true, completion: nil)
    }
    
    func didRequestAutocompletePredictions(_ viewController: GMSAutocompleteViewController) {
        UIApplication.shared.isNetworkActivityIndicatorVisible = true
    }
    
    func didUpdateAutocompletePredictions(_ viewController: GMSAutocompleteViewController) {
        UIApplication.shared.isNetworkActivityIndicatorVisible = false
    }
}
