//
//  RestrauntsLibraryOnMapViewController.swift
//  FriendSpire
//
//  Created by abhishek on 03/08/18.
//  Copyright © 2018 openkey. All rights reserved.
//

import UIKit
import GoogleMaps
import CoreLocation
import GooglePlaces

 protocol  RestrauntsLibraryOnMapViewControllerDelegate: class{
    func changeIndexSelection(index:Int)
    func changeLocation(address:Address)
    func shouldProceedWith(filter:Bool,_ cusines:[String]?)
}

class RestrauntsLibraryOnMapViewController: DIBaseController {
    
    //variable from another view controller
    var arrRestauntFriends = [FeedModal]()
    var arrRestauntEveryone = [FeedModal]()
    var navToUse : UINavigationController?
    var selectedIndexListType : Int?
    var delegate : RestrauntsLibraryOnMapViewControllerDelegate?
    var type : Constant.CategoryTypeBackend?
    var currentAddress:Address?
    var cuisinesArrey:[String] = [String]()
    var selectedCuisinesArrey:[String] = [String]()
    var fromLibrary : Bool?
    var userId : String?
    
    //IBOutlets
    @IBOutlet weak var viewTopBar: UIView!
    @IBOutlet weak var mapView: GMSMapView!
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblBadgeCount: UILabel!
    @IBOutlet weak var segmentUserType: UISegmentedControl!
    @IBOutlet weak var cuisinesCollectionView: UICollectionView!
    @IBOutlet weak var collectionViewPost: UICollectionView!   
    @IBOutlet weak var collectionHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var txtLocation: CustomTextField!
    
    fileprivate var arrMarker = [GMSMarker]()
    fileprivate var arrSelectedFeed = [FeedModal]()
    var currentUserLocation : CLLocationCoordinate2D?
    fileprivate var newUserLocation : CLLocationCoordinate2D?
    fileprivate var ratingToChange : String?
    fileprivate var customLocationSelected = false
    fileprivate var currentMapIcon = #imageLiteral(resourceName: "mapRes")
    fileprivate lazy var currenSelection : Int = 0
    var initalLocationChanged = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        getCuisines()
        currentMapIcon = type?.rawValue == Constant.CategoryTypeBackend.restraunt.rawValue ? #imageLiteral(resourceName: "mapRes") : #imageLiteral(resourceName: "mapBar")
        initalSetup()
        initMap()
        if fromLibrary == true{
            initalSetupForLibraryMap()
        }
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        if RestuarantFilterModel.sharedInstance.filterApplied == true{
            filterApplied()
        }else{
            if fromLibrary == true{
                let modal = getRequestModal()
                showLoader()
                DispatchQueue.global(qos: .background).async {
                    self.getBookmark(modal: modal)
                    self.getReview(modal:modal)
                }
            }else{
                
                var modal = getRequestModal()
                showLoader()
                DispatchQueue.global(qos: .background).async {
                    modal.friends = 1 // only friends review
                    self.getFeeds(modal: modal)
                    modal.friends = 0 // everyone review
                    self.getFeeds(modal: modal)
                }
            }
        }
        if User.sharedInstance.isLocationEnabled  == false{
            DispatchQueue.main.asyncAfter(deadline: .now() + .milliseconds(100)) {
                self.hideLoader()
                self.showAlert(withTitle: AppMessages.Location.title, message: AppMessages.AlertTitles.locationOff, okayTitle: AppMessages.AlertTitles.Ok, cancelTitle: nil, okCall: {
                    self.navToUse?.popViewController(animated: false)
                    self.dismiss(animated: true, completion: nil)
                    
                }, cancelCall: {
                })
            }
        }
        
        plotOnMap()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //MARK:- Custom Method
    
    func initalSetup(){
        if selectedIndexListType != nil{
            segmentUserType.selectedSegmentIndex = selectedIndexListType ?? 0
        }
        
        if title != nil {
            lblTitle.text = title
        }
        if colorToUse != nil {
            viewTopBar.backgroundColor = colorToUse
        }
    }
    
    /**
     give modified request modal after segment selection and page factors calculation
     */
    fileprivate func getRequestModal() -> FeedListRequest{
        var modal = FeedListRequest()
        
        modal.latLong = "[\(currentUserLocation?.latitude ?? User.sharedInstance.userCurrentLocation?.latitude ?? 0.0),\(currentUserLocation?.longitude ?? User.sharedInstance.userCurrentLocation?.longitude ?? 0.0)]"
        
        if RestuarantFilterModel.sharedInstance.filterApplied == true{
            modal = RestuarantFilterModel.sharedInstance.getDictionarForRestaurant()
            
            // if user doesn't select location send current location
            if modal.latLong == "" || customLocationSelected == false{
                modal.latLong = "[\(currentUserLocation?.latitude ?? User.sharedInstance.userCurrentLocation?.latitude ?? NEWYORK_LAT),\(currentUserLocation?.longitude ?? User.sharedInstance.userCurrentLocation?.longitude ?? NEWYORK_LNG)]"
            }
            
            // if user selected the distance neglect filterLatLong
            if modal.distance != "" {
                modal.filterLatLong = 0
            }
        }
        modal.category = self.type?.rawValue ?? 0
        modal.fromMap = 1
        modal.page = 1
        
        //No need to add further request return now
        if fromLibrary == true {
            if userId != nil{ // for other user only
                modal.userID = userId ?? ""
            }
            return modal
        }
        
        if self.segmentUserType.selectedSegmentIndex == ListTypeCategory.friends.rawValue{
            modal.friends = 1 // only friends review
        }else {
            modal.friends = 0 // everyone review
        }
        return modal
    }
    
    fileprivate func initalSetupForLibraryMap(){
        segmentUserType.setTitle(AppMessages.ListType.bookmarks, forSegmentAt: 1)
        segmentUserType.setTitle(AppMessages.ListType.reviews, forSegmentAt: 0)
    }
    
    fileprivate func initMap(){
        mapView.isMyLocationEnabled = true
        //        mapView.settings.myLocationButton = true
        mapView.padding = UIEdgeInsets(top: 0, left: 0, bottom: 240, right: 10)
        mapView.delegate = self
        if currentUserLocation != nil{
            mapView.camera = GMSCameraPosition.camera(withTarget: currentUserLocation!, zoom: 13.0)
            self.txtLocation.text = self.currentAddress?.city
        }else if User.sharedInstance.userCurrentLocation != nil{
            currentUserLocation = User.sharedInstance.userCurrentLocation!
            mapView.camera = GMSCameraPosition.camera(withTarget: User.sharedInstance.userCurrentLocation!, zoom: 13.0)
            self.txtLocation.text = self.currentAddress?.city
        }else{
            mapView.animate(toZoom: 13.0)
            setCurrentLocationTapped(UIButton())
        }
    }
    
    fileprivate func plotOnMap(){
        
        // reset all view on map
        //        customLocationSelected = false
        
        if cuisinesArrey.count == 0{
            getCuisines()
        }
        mapView.clear()
        arrMarker.removeAll()
        arrSelectedFeed.removeAll()
        
        if segmentUserType.selectedSegmentIndex == ListTypeCategory.friends.rawValue{
            
            for obj in arrRestauntFriends.enumerated(){
                
                if obj.element.location?.count ?? 0 > 1{
                    guard let lng = obj.element.location?[1]else {return}
                    guard let lat = obj.element.location?[0] else {return}
                    let coordinate = CLLocationCoordinate2D.init(latitude: lat, longitude: lng)
                    addAnotation(title: obj.element.title,cordinate: coordinate)
                    arrSelectedFeed.append(obj.element)
                    
                }
                // if user rated a Restraunt/bar
                if ratingToChange != nil {
                    if ratingToChange == obj.element.id{
                        self.collectionViewPost.reloadData() // to make sure collection has specfic index
                        self.collectionViewPost.scrollToItem(at: IndexPath.init(row: obj.offset, section: 0), at: .centeredHorizontally, animated: true)
                        let marker = arrMarker.last
                        marker?.icon = currentMapIcon
                    }
                }
            }
            //pick first element if not selected for rating
            if ratingToChange == nil{
                let marker = arrMarker.first
                marker?.icon = currentMapIcon
            }
            
            //            if arrRestauntFriends.count == 0{
            //                self.showAlert(message:AppMessages.ErrorAlerts.noRecords)
            //            }
        }else {
            for obj in arrRestauntEveryone.enumerated(){
                
                if obj.element.location?.count ?? 0 > 1{
                    guard let lng = obj.element.location?[1]else {return}
                    guard let lat = obj.element.location?[0] else {return}
                    let coordinate = CLLocationCoordinate2D.init(latitude: lat, longitude: lng)
                    addAnotation(title: obj.element.title,cordinate: coordinate)
                    arrSelectedFeed.append(obj.element)
                    
                }
                // if user rated a Restraunt/bar
                if ratingToChange != nil {
                    if ratingToChange == obj.element.id{
                        self.collectionViewPost.reloadData() // to make sure collection has specfic index
                        self.collectionViewPost.scrollToItem(at: IndexPath.init(row: obj.offset, section: 0), at: .centeredHorizontally, animated: true)
                        let marker = arrMarker.last
                        marker?.icon = currentMapIcon
                    }
                }
            }
            //pick first element if not selected for rating
            if ratingToChange == nil {
                let marker = arrMarker.first
                marker?.icon = currentMapIcon
            }
            //            if arrRestauntEveryone.count == 0{
            //                self.showAlert(message:AppMessages.ErrorAlerts.noRecords)
            //            }
        }
        collectionViewPost.reloadData()
        if currentUserLocation != nil{
            mapView.animate(toLocation: currentUserLocation!)
        }
        mapView.sizeToFit()
    }
    
    
    func addAnotation(title:String? = "",cordinate : CLLocationCoordinate2D){
        
        let marker = GMSMarker(position: cordinate)
        marker.title = title
        marker.icon = #imageLiteral(resourceName: "map")
        marker.map = mapView
        arrMarker.append(marker)
        
    }
    
    
    // MARK: - IBAction
    
    @IBAction func backTapped(_ sender: UIButton) {
        Utility.resetFilter()
        navToUse?.popViewController(animated: false)
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func addRecomendationTapped(_ sender: UIButton) {
        self.redirectToAddRecommendation(category: Utility.getBackendTypeString(using: type?.rawValue ?? 1))
    }
    
    @IBAction func listButtonTapped(_ sender: UIButton) {
        //        Utility.resetFilter()
        
        // to manage filter updation
        delegate?.shouldProceedWith(filter: RestuarantFilterModel.sharedInstance.filterApplied ?? false,selectedCuisinesArrey)
        delegate?.changeIndexSelection(index: segmentUserType.selectedSegmentIndex)
        if initalLocationChanged == true && self.currentAddress != nil{
            delegate?.changeLocation(address: self.currentAddress!)
        }
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func setCurrentLocationTapped(_ sender: UIButton) {
        if LocationManager.shared.permissionStatus == false{
            self.hideLoader()
            self.openSetting()
            return
        }
        
        LocationManager.shared.start()
        LocationManager.shared.success = { address,timestamp in
            self.initalLocationChanged = true
            self.currentAddress = address
            RestuarantFilterModel.objFeedListRequest.latLong = "[\(String(describing: self.currentAddress?.lat ?? 0.0)),\(String(describing: self.currentAddress?.long ?? 0.0))]"
            RestuarantFilterModel.objFeedListRequest.filterLatLong = 1
            self.filterApplied()
            self.txtLocation.text = AppMessages.Location.current
        }
        LocationManager.shared.failure = {
            if $0.code == .locationPermissionDenied {
                self.openSetting()
            } else {
                self.showAlert(withError: $0)
            }
        }
    }
    
    fileprivate  func openSetting() {
        self.showAlert(withTitle: AppMessages.Location.title, message: AppMessages.Location.off, okayTitle: "Settings".localized, cancelTitle: "Cancel", okCall: {
            UIApplication.shared.open(URL(string: UIApplicationOpenSettingsURLString)! as URL)
        }, cancelCall: {
        })
    }
    
    @IBAction func searchBarButtonAction(_ sender: UIButton) {
        self.view.endEditing(true)
        let autocompleteController = GMSAutocompleteViewController()
        autocompleteController.delegate = self
        let filter = GMSAutocompleteFilter()
        filter.type = .city
        autocompleteController.autocompleteFilter = filter
        present(autocompleteController, animated: true, completion: nil)
    }
    
    @IBAction func filterButtonTapped(_ sender: UIButton) {
        let controller: RestaurantsFiltersViewController = UIStoryboard(storyboard: .filter).initVC()
        controller.modalPresentationStyle = .overCurrentContext
        controller.modalTransitionStyle = .crossDissolve
        controller.category = type?.rawValue ?? 1
        controller.comingFromLibrary = false
        controller.delegate = self
        self.present(controller, animated: true, completion: nil)
    }
    
    @IBAction func userTypeTapped(_ sender: UISegmentedControl) {
        showLoader()
        if fromLibrary == true {
            currenSelection = sender.selectedSegmentIndex
            if sender.selectedSegmentIndex == ListTypeLibrary.reviews.rawValue{
                getReview(modal: getRequestModal())
            }
            else {
                getBookmark(modal: getRequestModal())
            }
            
        }else{
            self.getFeeds(modal: getRequestModal())
        }
    }
}


extension RestrauntsLibraryOnMapViewController {
    
    //MARK:- API Integration
    fileprivate func getFeeds(modal : FeedListRequest){
        
        if !Utility.isConnectedToInternet {
            self.hideLoader()
            self.showAlert( message: AppMessages.ErrorAlerts.noInternet)
            return
        }
        
        DIWebLayerUserAPI().getListOfFeeds(parameters: modal.getDictonary(), success: { [weak self](response) in
            self?.fillDataInArray( with: response.0, using: modal)
            if modal.friends == 1{
                self?.segmentUserType.setTitle("\(AppMessages.ListType.friends) (\(response.1.count))", forSegmentAt: 0)
            }else {
                self?.segmentUserType.setTitle("\(AppMessages.ListType.everyone) (\(response.1.count))", forSegmentAt: 1)
            }
            
            self?.hideLoader()
            
        }) { [weak self] (error) in
            DILog.print(items: error.message ?? "")
            self?.hideLoader()
            self?.showAlert( message: error.message ?? AppMessages.ErrorAlerts.unknownError)
        }
    }
    
    //fecth Cuisines.
    func getCuisines() {
        self.showLoader()
        DIWebLayerCategoriesDetailApi().getCuisines(type:type?.rawValue ?? 1, success: { (data) in
            self.cuisinesArrey = data
            if self.cuisinesArrey.count == 0{
                self.collectionHeightConstraint.constant = 0
            }else{
                self.collectionHeightConstraint.constant = 50
            }
            self.cuisinesCollectionView.reloadData()
            self.hideLoader()
        }) { (error) in
            self.hideLoader()
        }
    }
    
    //MARK:- API Integration
    fileprivate func getBookmark(modal : FeedListRequest){
        
        if !Utility.isConnectedToInternet {
            self.hideLoader()
            self.showAlert( message: AppMessages.ErrorAlerts.noInternet)
            return
        }
        
        DIWebLayerUserAPI().getBookmarkListing(for: modal.getDictonary(), success: { [weak self](response) in
            
            self?.fillLibraryDataInArray(with: response.0, review: false, pageno: modal.page)
            self?.segmentUserType.setTitle("\(AppMessages.ListType.bookmarks) (\(response.1.count))", forSegmentAt: 1)
            self?.hideLoader()
            
        }) { [weak self] (error) in
            DILog.print(items: error.message ?? "")
            DispatchQueue.main.async {
                self?.showAlert( message: error.message ?? AppMessages.ErrorAlerts.unknownError)
                self?.hideLoader()
            }
        }
    }
    
    fileprivate func getReview(modal : FeedListRequest){
        
        if !Utility.isConnectedToInternet {
            self.hideLoader()
            self.showAlert( message: AppMessages.ErrorAlerts.noInternet)
            return
        }
        
        DIWebLayerUserAPI().getReviewListing(for: modal.getDictonary(), success:  { [weak self](response) in
            
            self?.fillLibraryDataInArray(with: response.0, review: true, pageno: modal.page)
            self?.segmentUserType.setTitle("\(AppMessages.ListType.reviews) (\(response.1.count))", forSegmentAt: 0)
            self?.hideLoader()
            
        }) { [weak self] (error) in
            DILog.print(items: error.message ?? "")
            DispatchQueue.main.async {
                self?.showAlert( message: error.message ?? AppMessages.ErrorAlerts.unknownError)
                self?.hideLoader()
            }
        }
    }
    
    /**
     manage local array according to  category and parameters response from server ,page is current offset
     */
    fileprivate func fillDataInArray( with response : [FeedModal], using modal : FeedListRequest){
        
        if modal.friends == 1{
            arrRestauntFriends.removeAll()
            if response.count > 0 {
                arrRestauntFriends = modifyArrayContent(arr: arrRestauntFriends, with: response, using: modal.page)
            }else{
                if segmentUserType.selectedSegmentIndex == ListTypeCategory.friends.rawValue{
                    self.showAlert(message:AppMessages.ErrorAlerts.noRecords)
                }
            }
        }else {
            arrRestauntEveryone.removeAll()
            if response.count > 0 {
                arrRestauntEveryone = modifyArrayContent(arr: arrRestauntEveryone, with: response, using: modal.page)
            }else{
                if segmentUserType.selectedSegmentIndex == ListTypeCategory.everyone.rawValue{
                    self.showAlert(message:AppMessages.ErrorAlerts.noRecords)
                }
            }
        }
        plotOnMap()
    }
    
    /**
     manage local array according to  category and parameters response from server ,page is current offset
     */
    fileprivate func fillLibraryDataInArray( with response : [FeedModal], review : Bool , pageno: Int){
        
        if review{
            arrRestauntFriends.removeAll()
            if response.count > 0 {
                arrRestauntFriends = modifyArrayContent(arr: arrRestauntFriends, with: response, using: pageno)
            }else{
                if segmentUserType.selectedSegmentIndex == ListTypeLibrary.reviews.rawValue{
                    self.showAlert(message:AppMessages.ErrorAlerts.noRecords)
                }
            }
        }else {
            arrRestauntEveryone.removeAll()
            if response.count > 0 {
                arrRestauntEveryone = modifyArrayContent(arr: arrRestauntEveryone, with: response, using: pageno)
            }else{
                if segmentUserType.selectedSegmentIndex == ListTypeLibrary.bookmark.rawValue{
                    self.showAlert(message:AppMessages.ErrorAlerts.noRecords)
                }
            }
        }
        plotOnMap()
    }
    
    
    
    /**
     check local array  current offset and add or remove accordingly ,
     Parameters : arr is reference of array to do operation on,response (server response casted in Modal),page is current offset
     */
    fileprivate func modifyArrayContent( arr : [FeedModal], with response : [FeedModal], using page : Int) -> [FeedModal]{
        var arrRef = arr
        
        if page == 1{
            arrRef.removeAll()
        }
        arrRef.append(contentsOf: response)
        return arrRef
    }
}

extension RestrauntsLibraryOnMapViewController : AddRatingDelegate{
    func refreshViewDetail(rated: Bool) {
        if rated == true{
            var modal = getRequestModal()
            showLoader()
            DispatchQueue.global(qos: .background).async {
                if self.fromLibrary == true {
                    self.getReview(modal: self.getRequestModal())
                    self.getBookmark(modal: self.getRequestModal())
                }else{
                    modal.page = 1
                    modal.friends = 1 // only friends review
                    self.getFeeds(modal: modal)
                    modal.friends = 0 // everyone review
                    self.getFeeds(modal: modal)
                }
            }
        }
    }
}

extension RestrauntsLibraryOnMapViewController : BookFilterDelegate{
    
    func filterApplied() {
        RestuarantFilterModel.sharedInstance.filterApplied = true
        
        if RestuarantFilterModel.filterCount > 0{
            lblBadgeCount.isHidden = false
            lblBadgeCount.text = "\(RestuarantFilterModel.filterCount)"
        }else {
            lblBadgeCount.isHidden = true
        }
        let modal1 = RestuarantFilterModel.sharedInstance.getDictionarForRestaurant()
        customLocationSelected = modal1.filterLatLong == 1 ? true : false
        if customLocationSelected == true{
            var temp = modal1.latLong
            temp = temp.replace("[", replacement: "")
            temp = temp.replace("]", replacement: "")
            let arr = temp.components(separatedBy: ",")
            if arr.count > 1{
                newUserLocation = CLLocationCoordinate2D.init(latitude: Double(arr[0]) ?? NEWYORK_LAT , longitude: Double(arr[1]) ?? NEWYORK_LNG)
                self.perform(#selector(locationChanged), with: nil, afterDelay: 0.5)
                
            }
        }
        
        if fromLibrary == true{
            let modal = getRequestModal()
            showLoader()
            DispatchQueue.global(qos: .background).async {
                self.getBookmark(modal: modal)
                self.getReview(modal:modal)
            }
        }else{
            var modal = getRequestModal()
            showLoader()
            DispatchQueue.global(qos: .background).async {
                modal.friends = 1 // only friends review
                self.getFeeds(modal: modal)
                modal.friends = 0 // everyone review
                self.getFeeds(modal: modal)
            }
        }
    }
    
    //This will animate to new location 
    @objc func locationChanged(){
        mapView.animate(toLocation:newUserLocation!)
    }
}

extension RestrauntsLibraryOnMapViewController: GMSMapViewDelegate{
    
    func mapView(_ mapView: GMSMapView, didTap marker: GMSMarker) -> Bool {
        
        for mark in arrMarker{
            mark.icon = #imageLiteral(resourceName: "map")
        }
        marker.icon = currentMapIcon
        
        //filter the tapped marker
        if let indexTapped = arrMarker.index(of: marker){
            
            var currentSelection : FeedModal?
            if segmentUserType.selectedSegmentIndex == ListTypeCategory.friends.rawValue{
                currentSelection = arrRestauntFriends[indexTapped]
            }else {
                currentSelection = arrRestauntEveryone[indexTapped]
            }
            
            if currentSelection != nil {  // selected pin detail will shown on collection
                self.collectionViewPost.reloadData()
                self.collectionViewPost.scrollToItem(at: IndexPath.init(row: indexTapped, section: 0), at: .centeredHorizontally, animated: true)
            }
        }
        return true
    }
    
    func mapView(_ mapView: GMSMapView, idleAt position: GMSCameraPosition) {
        
        if customLocationSelected == false{
            self.newUserLocation =  position.target // map view moved to new position
        }
        if self.currentUserLocation != nil && self.newUserLocation != nil{
            let distanceCalulated =  CLLocation.init(latitude: (newUserLocation?.latitude)!, longitude:  (newUserLocation?.longitude)!).distance(from: CLLocation.init(latitude: (currentUserLocation?.latitude)!, longitude: (currentUserLocation?.longitude)!))
            
            DILog.print(items: distanceCalulated)
            if distanceCalulated > RadiusLimit {
                currentUserLocation = newUserLocation
                NSObject.cancelPreviousPerformRequests(withTarget: self)
                self.perform(#selector(self.getDataWithDelay), with: nil, afterDelay: 0.5)
            }
            customLocationSelected = false
        }else {
            self.currentUserLocation = position.target  // save user current location as well
        }
        
    }
    
    @objc fileprivate func getDataWithDelay(){
        showLoader()
        if fromLibrary == true {
            if segmentUserType.selectedSegmentIndex == ListTypeLibrary.reviews.rawValue{
                getReview(modal: getRequestModal())
            }
            else {
                getBookmark(modal: getRequestModal())
            }
            
        }else{
            self.getFeeds(modal: getRequestModal())
        }
        customLocationSelected = false
    }
}


extension RestrauntsLibraryOnMapViewController : UICollectionViewDataSource,UICollectionViewDelegate,UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        if collectionView == collectionViewPost{
            if arrSelectedFeed.count > 0 {
                mapView.padding = UIEdgeInsets(top: 0, left: 0, bottom: 240, right: 10)
                collectionViewPost.isUserInteractionEnabled = true
            }else{
                collectionViewPost.isUserInteractionEnabled = false
                mapView.padding = UIEdgeInsets(top: 0, left: 0, bottom: 10, right: 10)
            }
            return arrSelectedFeed.count
        }
        return cuisinesArrey.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        //***********************Post collection view Configuration************************/
        
        if collectionView == collectionViewPost{
            guard let cell:RestrauntOnMapCell = collectionView.dequeueReusableCell(withReuseIdentifier: "RestrauntOnMapCell", for: indexPath) as? RestrauntOnMapCell else{
                return UICollectionViewCell()
            }
            if fromLibrary == true{
                let onReviewTab = self.currenSelection == ListTypeLibrary.reviews.rawValue ? true : false
                let ownProfile = userId != nil ? false : true
                cell.configureCell(modal: arrSelectedFeed[indexPath.row], with: indexPath, with: Utility.getBackendTypeString(using: type?.rawValue ?? 0),onReviewTab,ownProfile)
                
            }else {
                cell.configureCell(modal: arrSelectedFeed[indexPath.row], with: indexPath, with: Utility.getBackendTypeString(using: type?.rawValue ?? 0))
            }
            cell.btnShare.addTarget(self, action: #selector(share(_ :)), for: .touchUpInside)
            cell.btnBookmark.addTarget(self, action: #selector(bookmark(_ :)), for: .touchUpInside)
            cell.btnAddRating.addTarget(self, action: #selector(rate(_ :)), for: .touchUpInside)
            return cell
        }
        
        
        //***********************Cuisine collection view Configuration************************/
        guard let cell:CuisinesGenerCollectionCell = collectionView.dequeueReusableCell(withReuseIdentifier: "CuisinesGenerCollectionCell", for: indexPath) as? CuisinesGenerCollectionCell else{
            return UICollectionViewCell()
        }
        cell.cuisinesGenerLabel.text = cuisinesArrey[indexPath.item]
        if selectedCuisinesArrey.contains(cuisinesArrey[indexPath.item]) {
            cell.cuisinesGenerLabel.textColor = colorToUse
        }else{
            cell.cuisinesGenerLabel.textColor = .black
        }
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        //***********************Post collection view selected************************/
        if collectionView == collectionViewPost{
            let modal = arrSelectedFeed[indexPath.row]
            if   User.sharedInstance.isLocationEnabled  == false{
                self.showAlert(message:AppMessages.AlertTitles.locationOff)
                return
            }
            ratingToChange  = modal.id ?? ""
            let  controllerLocal : DetailPageViewController = UIStoryboard.init(storyboard: .inspiration).initVC()
            controllerLocal.category =  modal.type ?? 0
            controllerLocal.postId = modal.id ?? ""
            self.navigationController?.pushViewController(controllerLocal, animated: true)
            return
        }
        
        //***********************Cusines collection view selected************************/
        
        if selectedCuisinesArrey.contains(cuisinesArrey[indexPath.item]) {
            for (index,data) in  selectedCuisinesArrey.enumerated() {
                if data == cuisinesArrey[indexPath.item] {
                    selectedCuisinesArrey.remove(at: index)
                }
            }
        }else{
            selectedCuisinesArrey.append(cuisinesArrey[indexPath.item])
        }
        cuisinesCollectionView.reloadData()
        if selectedCuisinesArrey.count > 0 {
            RestuarantFilterModel.objFeedListRequest.cuisine = "[\"\(selectedCuisinesArrey.joined(separator: "\",\""))\"]"
        }else {
            RestuarantFilterModel.objFeedListRequest.cuisine.removeAll()
        }
        filterApplied()
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        if collectionView == collectionViewPost{
            
            var str = ""
            if fromLibrary == true{
                
                str = arrSelectedFeed[indexPath.item].userReview ?? ""
                let heightToShow = str.height(withConstrainedWidth: self.view.frame.size.width, font: UIFont.init(name: "SFUIText-Regular", size: 13.0)!)
                //**Check if user view someone else library and height is less than required and user rating is not null**/
                let addtionalHeight : CGFloat = (arrSelectedFeed[indexPath.item].userRating ?? 0 > 0 && heightToShow < 21 && userId != nil) ? 30 : 0
                return CGSize(width: self.view.frame.size.width, height: (195.0 + addtionalHeight + (heightToShow > 20 ? 40 : heightToShow) ))
            }else{
                
                str = arrSelectedFeed[indexPath.item].description ?? ""
                let heightToShow = str.height(withConstrainedWidth: self.view.frame.size.width, font: UIFont.init(name: "SFUIText-Regular", size: 13.0)!)
                return CGSize(width: self.view.frame.size.width, height: (195.0 + (heightToShow > 45 ? 45 : heightToShow) ))
            }
        }
        
        let str = cuisinesArrey[indexPath.item]
        let widthToShow = str.width(withConstrainedHeight: 40.0, font: UIFont.init(name: "SFUIText-Regular", size: 17.0)!)
        return CGSize(width: widthToShow + 10.0 , height: 50.0)
    }
    
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        
        var visibleRect = CGRect()
        visibleRect.origin = collectionViewPost.contentOffset
        visibleRect.size = collectionViewPost.bounds.size
        let visiblePoint = CGPoint(x: visibleRect.midX, y: visibleRect.midY)
        guard let indexPath = collectionViewPost.indexPathForItem(at: visiblePoint) else { return }
        for mark in arrMarker.enumerated(){
            mark.element.icon = #imageLiteral(resourceName: "map")
            if mark.offset == indexPath.item{
                mark.element.icon = currentMapIcon
                mapView.animate(toLocation: mark.element.position)
            }
        }
    }
    
    //MARK:-  Custom Methods
    @objc func share(_ sender : UIButton){
        //        self.shareData(data: "Friendspire")
        let modal : FeedModal = arrSelectedFeed[sender.tag]
        self.shareData(id:modal.id , type: modal.type ?? 1, notificationType: 1, title: modal.title ?? "")
    }
    
    @objc func bookmark(_ sender : UIButton){
        let modalFeed : FeedModal = arrSelectedFeed[sender.tag]
        sender.isSelected = !sender.isSelected
        var ratingModal = RatingRequest()
        ratingModal.referenceid = modalFeed.id ?? ""
        ratingModal.type = Utility.getBackendType(using: self.title ?? "")
        
        
        if sender.isSelected == true{ // addBookmark
            self.showLoader()
            self.addBookmark(modal: ratingModal) { [weak self](finish) in
                self?.hideLoader()
                if finish == false{
                    sender.isSelected = !sender.isSelected  // failure, change back to previous state
                }else {
                    modalFeed.isBookmarked = true
                    self?.updateModalLocally(using: modalFeed, at: sender.tag)
                }
            }
        }else{ //remove bookmark
            
            //Show pop up
            self.showAlert(withTitle: appName.capitalized, message: AppMessages.AlertTitles.removeBookMark, okayTitle: AppMessages.AlertTitles.Yes, cancelTitle: AppMessages.AlertTitles.No, okCall: {
                self.showLoader()
                self.removeBookmark(modal: ratingModal) { [weak self](finish) in
                    self?.hideLoader()
                    if finish == false{
                        sender.isSelected = !sender.isSelected //failure, change back to previous state
                    }else {
                        modalFeed.isBookmarked = false
                        self?.updateModalLocally(using: modalFeed, at: sender.tag)
                    }
                }
            }) {
                //Nothing to do
                sender.isSelected = !sender.isSelected
            }
        }
    }
    
    @objc func rate(_ sender : UIButton){
        //Already present on main window present this controller locally
        let modal : FeedModal = arrSelectedFeed[sender.tag]
        ratingToChange  = modal.id ?? ""
        let controller = UIStoryboard.init(storyboard: .inspiration).instantiateViewController(withIdentifier: "AddRatingViewController") as! AddRatingViewController
        controller.modalTransitionStyle = .crossDissolve
        controller.modalPresentationStyle  = .overCurrentContext
        controller.categoryType = modal.type ?? 0
        controller.referenceID = ratingToChange
        controller.modalRef = modal
        controller.fromAnotherUserLibrary = userId != nil ? true : false // coming from library and which user's library
        controller.delegate = self
        self.present(controller, animated: true, completion: nil)
    }
    
    
    func updateModalLocally(using modal:FeedModal ,at index: Int){
        self.arrSelectedFeed.remove(at: index)
        self.arrSelectedFeed.insert(modal, at: index)
        DispatchQueue.main.async {
            self.collectionViewPost.reloadData()
        }
    }
    
}


