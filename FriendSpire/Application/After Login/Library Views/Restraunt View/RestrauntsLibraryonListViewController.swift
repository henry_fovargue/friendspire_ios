//
//  RestrauntsLibraryViewController.swift
//  FriendSpire
//
//  Created by abhishek on 03/08/18.
//  Copyright © 2018 openkey. All rights reserved.
//

import UIKit
import CoreLocation
import GooglePlaces
class RestrauntsLibraryonListViewController: DIBaseController {
    
    //variable from another controller user for data passing
    var type : Constant.CategoryTypeBackend?
    var currentAddress:Address?
    var cuisinesArrey:[String] = [String]()
    var selectedCuisinesArrey:[String] = [String]()
    
    
    //IBOutlets
    
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var btnMap: UIButton!
    @IBOutlet weak var btnFilter: UIButton!
    @IBOutlet weak var segmentUserType: UISegmentedControl!
    @IBOutlet weak var viewTopBar: UIView!
    @IBOutlet weak var tableViewLib: UITableView!
    @IBOutlet weak var lblBadgeCount: UILabel!
    @IBOutlet weak var cuisinesCollectionView: UICollectionView!
    
    @IBOutlet weak var txtLocation: CustomTextField!
    @IBOutlet weak var collectionHeightConstraint: NSLayoutConstraint!
    //Local variables
    fileprivate var arrRestauntFriends = [FeedModal]()
    fileprivate var arrRestauntEveryone = [FeedModal]()
    fileprivate var reachedEndOfFriendFeed = false
    fileprivate var reachedEndOfEveryoneFeed = false
    fileprivate var indexToRefresh : Int?
    fileprivate var currentSelection = ListTypeCategory.friends.rawValue
    var customLocationListing : CLLocationCoordinate2D?

    let refreshControl = UIRefreshControl()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        getCuisines()
        // Do any additional setup after loading the view.
        addPagination()
        tableViewLib.refreshControl = refreshControl
        refreshControl.addTarget(self, action: #selector(pullToRefresh), for: UIControlEvents.valueChanged)
        //REgister nib with table
        tableViewLib.register(UINib.init(nibName: CellIdentifiers.libraryScreen.CategoryList.movieNib, bundle: nil), forCellReuseIdentifier: CellIdentifiers.libraryScreen.CategoryList.restrauntCell)
        
        if title != nil {
            lblTitle.text = title
        }
        if colorToUse != nil {
            viewTopBar.backgroundColor = colorToUse
        }
        showLoader()
        var modal = self.getRequestModal(nextPage: false)
        DispatchQueue.global(qos: .background).async {
            self.resetAllCheck() // allow api hit
            modal.page = 1
            modal.friends = 1 // only friends review
            self.getFeeds(modal: modal)
            modal.friends = 0 // everyone review
            self.getFeeds(modal: modal)
        }
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        if User.sharedInstance.userCurrentLocation == nil{
            showLoader(message: AppMessages.Location.fetch)
            self.performSelector(onMainThread: #selector(getUserCurrentLcoation), with: nil, waitUntilDone: true)
        }
        refreshViewManaged()
        if User.sharedInstance.isLocationEnabled  == false{
            DispatchQueue.main.asyncAfter(deadline: .now() + .milliseconds(100)) {
                self.hideLoader()
                self.showAlert(withTitle: AppMessages.Location.title, message: AppMessages.AlertTitles.locationOff, okayTitle: AppMessages.AlertTitles.Ok, cancelTitle: nil, okCall: {
                    self.navigationController?.popViewController(animated: true)
                }, cancelCall: {
                })
            }
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //MARK:-Function To fecth Cuisines.
    func getCuisines() {
//        self.showLoader()
        DIWebLayerCategoriesDetailApi().getCuisines(type:type?.rawValue ?? 1, success: { (data) in
            self.cuisinesArrey = data
            if self.cuisinesArrey.count == 0{
                self.collectionHeightConstraint.constant = 0
            }else{
                self.collectionHeightConstraint.constant = 50
            }
            self.cuisinesCollectionView.reloadData()
//            self.hideLoader()
        }) { (error) in
        //    self.showAlert(message:error.message)
//            self.hideLoader()
        }
    }
    
    // MARK: - Custom Methods
    
   fileprivate func refreshViewManaged(){
    if RestuarantFilterModel.sharedInstance.filterApplied == true{
        setFilterCount()
    }
        self.tableViewLib.reloadData()
        if self.tableViewLib.numberOfRows(inSection: 0) > 0{
            let indexPath = IndexPath(row: indexToRefresh ?? 0, section: 0)
            self.tableViewLib.scrollToRow(at: indexPath, at: .top, animated: false)
            //check if same post is on another tab
            if indexToRefresh != nil{
                if let modal = getModal(index: indexToRefresh!){
                    self.updatePostOnOtherTab(modal: modal) // update the same post
                }
            }
            indexToRefresh = nil
        }
    }
    //Get UserCurrentLocation.
    @objc fileprivate func getUserCurrentLcoation() -> Void {
        if LocationManager.shared.permissionStatus == false{
            self.hideLoader()
            self.openSetting()
            return
        }
        
        LocationManager.shared.start()
        LocationManager.shared.success = { address,timestamp in
            User.sharedInstance.userCurrentLocation = CLLocationCoordinate2D.init(latitude: address.lat ?? 0.0, longitude: address.long ?? 0.0)
            self.hideLoader()
        }
        LocationManager.shared.failure = {
            self.hideLoader()
            if $0.code == .locationPermissionDenied {
                User.sharedInstance.userCurrentLocation = nil
                self.openSetting()
            } else {
                self.showAlert(withError: $0)
            }
        }
    }
    
    fileprivate  func openSetting() {
        self.showAlert(withTitle: AppMessages.Location.title, message: AppMessages.Location.off, okayTitle: "Settings".localized, cancelTitle: "Cancel", okCall: {
            UIApplication.shared.open(URL(string: UIApplicationOpenSettingsURLString)! as URL)
        }, cancelCall: {
        })
    }
    
    fileprivate func getModal(index:Int) -> FeedModal?{
        if self.segmentUserType.selectedSegmentIndex == ListTypeCategory.friends.rawValue{
            if index < arrRestauntFriends.count {
                return arrRestauntFriends[index]
            }
        }else {
            if index < arrRestauntEveryone.count {
                return arrRestauntEveryone[index]
            }
        }
        return nil
    }
    
    func pullToRefresh(){
        resetAllCheck()
        if cuisinesArrey.count == 0{
            getCuisines()
        }
        var modal = getRequestModal(nextPage: false)
        modal.page = 1
        self.getFeeds(modal: modal)
    }
    
    func resetAllCheck(){
        reachedEndOfFriendFeed = false
        reachedEndOfEveryoneFeed = false
    }
    
    /**
     Check if user reached to end of list to a particular list
     */
    fileprivate func shouldProceedWithFetch() -> Bool{
        if currentSelection == ListTypeCategory.friends.rawValue{
            return !reachedEndOfFriendFeed
        }else {
            return !reachedEndOfEveryoneFeed
        }
    }
    
    /**
     each time table view reaches to its limit callback will allow you to manage api Hits
     */
    func addPagination(){
        self.tableViewLib.infiniteScrolling(self) { [weak self] in
            
            guard let modal = self?.getRequestModal(nextPage: true)else{
                self?.hideAllLoaders(count: 0)
                return
            }
            self?.getFeeds(modal: modal)
        }
    }
    
    
    /**
     give modified request modal after segment selection and page factors calculation
     */
    fileprivate  func getRequestModal(nextPage:Bool) -> FeedListRequest{
        var modal = FeedListRequest()
        
        modal.latLong = "[\(User.sharedInstance.userCurrentLocation?.latitude ?? 0.0),\(User.sharedInstance.userCurrentLocation?.longitude ?? 0.0)]"
        
        if RestuarantFilterModel.sharedInstance.filterApplied == true{
            modal = RestuarantFilterModel.sharedInstance.getDictionarForRestaurant()
            // if user doesn't select location send current location
            if modal.latLong == ""{
                modal.latLong = "[\(User.sharedInstance.userCurrentLocation?.latitude ?? 0.0),\(User.sharedInstance.userCurrentLocation?.longitude ?? 0.0)]"
                
            }
            
            // if user selected the distance neglect filterLatLong
            if modal.distance != "" {
                modal.filterLatLong = 0
            }
        }
        
        modal.category = self.type?.rawValue ?? 0
        if self.segmentUserType.selectedSegmentIndex == ListTypeCategory.friends.rawValue{
            let currentOffset = Utility.getCurrentOffset(count: self.arrRestauntFriends.count)
            if nextPage == true{
                modal.page = currentOffset+1
            }else {
                modal.page = currentOffset
            }
            modal.friends = 1 // only friends review
        }else {
            let currentOffset = Utility.getCurrentOffset(count: self.arrRestauntEveryone.count)
            if nextPage == true{
                modal.page = currentOffset+1
            }else {
                modal.page = currentOffset
            }
            modal.friends = 0 // everyone review
        }
        return modal
    }
    
    /**
     get count to use according to category
     */
    fileprivate func getCount()->Int {
        
        if currentSelection == ListTypeCategory.friends.rawValue{
            return arrRestauntFriends.count
        } else {
            return arrRestauntEveryone.count
        }
    }
    
    
    // MARK: - IBActions
    
    @IBAction func addRecomendationTapped(_ sender: UIButton) {
        self.redirectToAddRecommendation(category: Utility.getBackendTypeString(using: type?.rawValue ?? 1))

    }
    
    
    @IBAction func setCurrentLocationTapped(_ sender: UIButton) {
        if LocationManager.shared.permissionStatus == false{
            self.hideLoader()
            self.openSetting()
            return
        }
        
        LocationManager.shared.start()
        LocationManager.shared.success = { address,timestamp in
            self.currentAddress = address
            RestuarantFilterModel.objFeedListRequest.latLong = "[\(String(describing: self.currentAddress?.lat ?? 0.0)),\(String(describing: self.currentAddress?.long ?? 0.0))]"
            RestuarantFilterModel.objFeedListRequest.filterLatLong = 1
            self.filterApplied()
            self.txtLocation.text = AppMessages.Location.current
            self.customLocationListing = nil
        }
        LocationManager.shared.failure = {
            if $0.code == .locationPermissionDenied {
                self.openSetting()
            } else {
                self.showAlert(withError: $0)
            }
        }
    }
    
    @IBAction func searchBarButtonAction(_ sender: UIButton) {
        self.view.endEditing(true)
        let autocompleteController = GMSAutocompleteViewController()
        autocompleteController.delegate = self
        let filter = GMSAutocompleteFilter()
        filter.type = .city
        autocompleteController.autocompleteFilter = filter
        present(autocompleteController, animated: true, completion: nil)
    }
    @IBAction func backTapped(_ sender: UIButton) {
        Utility.resetFilter()
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func mapButtonTapped(_ sender: UIButton) {
//        Utility.resetFilter()
        let controller = UIStoryboard.init(storyboard: .library).instantiateViewController(withIdentifier: "RestrauntsLibraryOnMapViewController") as! RestrauntsLibraryOnMapViewController
        controller.title = self.title
        controller.colorToUse = self.colorToUse
        controller.navToUse = self.navigationController // this will use to keep the stack on presented controller as well
        controller.modalTransitionStyle = .crossDissolve
        controller.modalPresentationStyle = .overCurrentContext
        controller.arrRestauntFriends = self.arrRestauntFriends
        controller.arrRestauntEveryone = self.arrRestauntEveryone
        controller.selectedCuisinesArrey = self.selectedCuisinesArrey
        controller.selectedIndexListType = self.segmentUserType.selectedSegmentIndex
        controller.type = self.type
        controller.delegate = self
        controller.currentAddress = self.currentAddress
        controller.currentUserLocation = self.customLocationListing
        // start new stack management for presented controller
        let navController = UINavigationController.init(rootViewController: controller)
        navController.navigationBar.isHidden = true
        AppDelegate.shared().window?.rootViewController?.present(navController, animated: true, completion: nil)
    }
    
    @IBAction func filterButtonTapped(_ sender: UIButton) {
        self.showFilterBar(using: type?.rawValue ?? 1,fromLibrary: false)
    }
    
    @IBAction func userTypeTapped(_ sender: UISegmentedControl) {
        currentSelection = sender.selectedSegmentIndex
        
        if currentSelection == ListTypeCategory.friends.rawValue{
            tableViewLib.enableInfiniteScrolling(status: !reachedEndOfFriendFeed)
        }else{
            tableViewLib.enableInfiniteScrolling(status: !reachedEndOfEveryoneFeed)
        }
        DispatchQueue.main.asyncAfter(deadline: .now() + .milliseconds(300)) {
            if self.tableViewLib.numberOfRows(inSection: 0) > 0{
                let indexPath = IndexPath(row: 0, section: 0)
                self.tableViewLib.scrollToRow(at: indexPath, at: .top, animated: true)
            }
        }
        tableViewLib.reloadData()
    }
    
    
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
}
extension RestrauntsLibraryonListViewController : UITableViewDataSource,UITableViewDelegate{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        var count = 0
        if segmentUserType.selectedSegmentIndex == ListTypeCategory.friends.rawValue{
            count = arrRestauntFriends.count
        }else {
            count = arrRestauntEveryone.count
        }
        if count > 0 {
            tableViewLib.setEmptyMessage("")
        }else {
            tableViewLib.setEmptyMessage(AppMessages.ErrorAlerts.noRecords)
        }
        
        return count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
//        let cell = tableView.dequeueReusableCell(withIdentifier: CellIdentifiers.libraryScreen.CategoryList.restrauntCell) as! RestrauntCell
                let cell = tableView.dequeueReusableCell(withIdentifier: CellIdentifiers.libraryScreen.CategoryList.restrauntCell) as! RecomendationCell

        guard let modal = getModal(index: indexPath.row) else {return UITableViewCell()}
        cell.configureCellAddRecomendationList(modal: modal, with: indexPath, with: self.title ?? AppMessages.Categories.restruant)
//        cell.configureCellLibrary(modal, with: indexPath, with: self.title ?? AppMessages.Categories.restruant)
        cell.btnShare.addTarget(self, action: #selector(share(_ :)), for: .touchUpInside)
        cell.btnBookmark.addTarget(self, action: #selector(bookmark(_ :)), for: .touchUpInside)
        cell.btnAddRating.addTarget(self, action: #selector(rate(_ :)), for: .touchUpInside)
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        guard let modal :  FeedModal = getModal(index: indexPath.row) else {return}
        if   User.sharedInstance.isLocationEnabled  == false{
            self.showAlert(message:AppMessages.AlertTitles.locationOff)
            return
        }
        indexToRefresh = indexPath.row
        let  controllerLocal : DetailPageViewController = UIStoryboard.init(storyboard: .inspiration).initVC()
        controllerLocal.category =  modal.type ?? 0
        controllerLocal.postId = modal.id ?? ""
        controllerLocal.modalRef = modal
        self.navigationController?.pushViewController(controllerLocal, animated: true)
    }
    
    //MARK:-  Custom Methods
    @objc func share(_ sender : UIButton){
        
        guard let modal : FeedModal = getModal(index: sender.tag) else {return}
        self.shareData(id:modal.id , type: modal.type ?? 1, notificationType: 1, title: modal.title ?? "")
    }
    
    @objc func bookmark(_ sender : UIButton){
        guard let modal : FeedModal = getModal(index: sender.tag) else {return}
        modal.isBookmarked = !sender.isSelected
        sender.isSelected = !sender.isSelected
        var ratingModal = RatingRequest()
        ratingModal.referenceid = modal.id ?? ""
        ratingModal.type = Utility.getBackendType(using: self.title ?? "")
        
        
        if sender.isSelected == true{ // addBookmark
            self.showLoader()
            self.addBookmark(modal: ratingModal) {[weak self] (finish) in
                self?.hideLoader()
                if finish == false{
                    sender.isSelected = !sender.isSelected  // failure, change back to previous state
                    modal.isBookmarked = !sender.isSelected
                }
                self?.updatePostOnOtherTab(modal: modal)
            }
        }else{ //remove bookmark
            
            //Show pop up
            self.showAlert(withTitle: appName.capitalized, message: AppMessages.AlertTitles.removeBookMark, okayTitle: AppMessages.AlertTitles.Yes, cancelTitle: AppMessages.AlertTitles.No, okCall: {
                self.showLoader()
                self.removeBookmark(modal: ratingModal) {[weak self] (finish) in
                    self?.hideLoader()
                    if finish == false{
                        sender.isSelected = !sender.isSelected //failure, change back to previous state
                        modal.isBookmarked = !sender.isSelected
                    }
                    self?.updatePostOnOtherTab(modal: modal)
                }
            }) {
                //Nothing to do
                sender.isSelected = !sender.isSelected
                modal.isBookmarked = !sender.isSelected
            }
        }
    }
    
    @objc func rate(_ sender : UIButton){
        guard let modal : FeedModal = getModal(index: sender.tag)else {return}
        indexToRefresh = sender.tag
        self.addRating(using: modal.type ?? 0, referenceID: modal.id ?? "",modal)
    }
    
    fileprivate func updatePostOnOtherTab(modal:FeedModal){
        
        if segmentUserType.selectedSegmentIndex == ListTypeCategory.everyone.rawValue{
            if let index : Int = arrRestauntFriends.index(where: {$0.id == modal.id}) {
                arrRestauntFriends.remove(at: index)
                arrRestauntFriends.insert(modal, at: index)
            }
            
        }else{
            if let index : Int = arrRestauntEveryone.index(where: {$0.id == modal.id}){
                arrRestauntEveryone.remove(at: index)
                arrRestauntEveryone.insert(modal, at: index)
            }
        }
    }
    
}

extension RestrauntsLibraryonListViewController {
    
    //MARK:- API Integration
    fileprivate func getFeeds(modal : FeedListRequest){
        
        if !Utility.isConnectedToInternet {
            self.hideAllLoaders(count: self.getCount())
            self.showAlert( message: AppMessages.ErrorAlerts.noInternet)
            return
        }
        
        // check if more no more records left
        if self.shouldProceedWithFetch() == false{
            self.hideAllLoaders(count: self.getCount())
            return
        }
        DIWebLayerUserAPI().getListOfFeeds(parameters: modal.getDictonary(), success: { [weak self](response) in
            
            self?.fillDataInArray( with: response.0, using: modal)
            if modal.friends == 1{
                self?.segmentUserType.setTitle("\(AppMessages.ListType.friends) (\(response.1.count))", forSegmentAt: 0)
            }else {
                self?.segmentUserType.setTitle("\(AppMessages.ListType.everyone) (\(response.1.count))", forSegmentAt: 1)
            }
            
        }) { [weak self] (error) in
            DILog.print(items: error.message ?? "")
            self?.hideAllLoaders(count: self?.getCount() ?? 0)
            self?.showAlert( message: error.message ?? AppMessages.ErrorAlerts.unknownError)
            
        }
    }
    /**
     manage local array according to  category and parameters response from server ,page is current offset
     */
    fileprivate func fillDataInArray( with response : [FeedModal], using modal : FeedListRequest ){
        
        if modal.friends == 1{
            if modal.page == 1{
                arrRestauntFriends.removeAll()
            }else{
                
                DispatchQueue.main.asyncAfter(deadline: .now() + .milliseconds(100)) {
                    if self.tableViewLib.numberOfRows(inSection: 0) > 0{
                        self.tableViewLib.scrollToRow(at: IndexPath.init(row: self.arrRestauntFriends.count - (response.count+1) , section: 0), at: .top, animated: false)
                    }
                }
                
            }
            if response.count > 0 {
                reachedEndOfFriendFeed  = false
                arrRestauntFriends = modifyArrayContent(arr: arrRestauntFriends, with: response, using: modal.page)
                if response.count < RECORDS_PER_PAGE{
                    reachedEndOfFriendFeed = true
                }
            }else{
                reachedEndOfFriendFeed  = true
            }
        }else {
            if modal.page == 1{
                arrRestauntEveryone.removeAll()
            }else{
                //*********** Make sure to scroll to last feed user was, when user request for new feeds
                DispatchQueue.main.asyncAfter(deadline: .now() + .milliseconds(100)) {
                    if self.tableViewLib.numberOfRows(inSection: 0) > 0{
                        self.tableViewLib.scrollToRow(at: IndexPath.init(row: self.arrRestauntEveryone.count - (response.count+1) , section: 0), at: .top, animated: false)
                    }
                }
                
            }
            if response.count > 0 {
                reachedEndOfEveryoneFeed  = false
                arrRestauntEveryone = modifyArrayContent(arr: arrRestauntEveryone, with: response, using: modal.page)
                if response.count < RECORDS_PER_PAGE{
                    reachedEndOfEveryoneFeed = true
                }
            }else{
                reachedEndOfEveryoneFeed  = true
            }
        }
        self.hideAllLoaders(count: response.count)
        self.tableViewLib.reloadData()
    }
    
    /**
     check local array  current offset and add or remove accordingly ,
     Parameters : arr is reference of array to do operation on,response (server response casted in Modal),page is current offset
     */
    fileprivate func modifyArrayContent( arr : [FeedModal], with response : [FeedModal], using page : Int) -> [FeedModal]{
        var arrRef = arr
        if page == 1{
            arrRef.removeAll()
        }
        arrRef.append(contentsOf: response)
        return arrRef
    }
    
    /**
     This will hide all loaders used in this class
     and also scroll table view on top if required
     */
    
    fileprivate func hideAllLoaders(count : Int ){
        DispatchQueue.main.async {
            self.hideLoader()
            self.tableViewLib.endPull2RefreshAndInfiniteScrolling(count: count)
            self.refreshControl.endRefreshing()
            self.tableViewLib.reloadData()
        }
    }
}


extension RestrauntsLibraryonListViewController : BookFilterDelegate{
    
    func filterApplied() {
        resetAllCheck()
        RestuarantFilterModel.sharedInstance.filterApplied = true
        setFilterCount()
        var modal = getRequestModal(nextPage: false)
        showLoader()
        DispatchQueue.global(qos: .background).async {
            modal.page = 1
            modal.friends = 1 // only friends review
            self.getFeeds(modal: modal)
            modal.friends = 0 // everyone review
            self.getFeeds(modal: modal)
        }
    }
    
    fileprivate func setFilterCount(){
        if RestuarantFilterModel.filterCount > 0{
            lblBadgeCount.isHidden = false
            lblBadgeCount.text = "\(RestuarantFilterModel.filterCount)"
        }else {
            lblBadgeCount.isHidden = true
        }
    }
}

extension RestrauntsLibraryonListViewController : AddRatingDelegate{
    func refreshViewDetail(rated: Bool) {
        if rated == true{
            refreshViewManaged()
        }
    }
}


extension RestrauntsLibraryonListViewController: RestrauntsLibraryOnMapViewControllerDelegate{
    
    func changeIndexSelection(index: Int) {
        segmentUserType.selectedSegmentIndex = index
        tableViewLib.reloadData()
    }
    
    func changeLocation(address: Address) {
        self.txtLocation.text = address.city
        self.currentAddress = address
        RestuarantFilterModel.objFeedListRequest.latLong = "[\(String(describing: self.currentAddress?.lat ?? 0.0)),\(String(describing: self.currentAddress?.long ?? 0.0))]"
        RestuarantFilterModel.objFeedListRequest.filterLatLong = 1
        self.filterApplied()
    }
    
    
    func shouldProceedWith(filter: Bool,_ cusines: [String]?) {
        if filter == true{
            filterApplied()
        }
        
        guard (cusines?.count) != nil else {
            return
        }
        selectedCuisinesArrey = cusines ?? []
        self.cuisinesCollectionView.reloadData()
        
    }
}


extension RestrauntsLibraryonListViewController : UICollectionViewDataSource,UICollectionViewDelegate ,UICollectionViewDelegateFlowLayout{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return cuisinesArrey.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        guard let cell:CuisinesGenerCollectionCell = collectionView.dequeueReusableCell(withReuseIdentifier: "CuisinesGenerCollectionCell", for: indexPath) as? CuisinesGenerCollectionCell else{
            return UICollectionViewCell()
        }
        cell.cuisinesGenerLabel.text = cuisinesArrey[indexPath.item]
        if selectedCuisinesArrey.contains(cuisinesArrey[indexPath.item]) {
            cell.cuisinesGenerLabel.textColor = colorToUse
        }else{
            cell.cuisinesGenerLabel.textColor = .black
        }
        return cell
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if selectedCuisinesArrey.contains(cuisinesArrey[indexPath.item]) {
            for (index,data) in  selectedCuisinesArrey.enumerated() {
                if data == cuisinesArrey[indexPath.item] {
                    selectedCuisinesArrey.remove(at: index)
                }
            }
        }else{
            selectedCuisinesArrey.append(cuisinesArrey[indexPath.item])
        }
        cuisinesCollectionView.reloadData()
        if selectedCuisinesArrey.count > 0 {
            RestuarantFilterModel.objFeedListRequest.cuisine = "[\"\(selectedCuisinesArrey.joined(separator: "\",\""))\"]"
        }else {
            RestuarantFilterModel.objFeedListRequest.cuisine.removeAll()
        }
        filterApplied()
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let str = cuisinesArrey[indexPath.item]
        let widthToShow = str.width(withConstrainedHeight: 40.0, font: UIFont.init(name: "SFUIText-Regular", size: 17.0)!)
        return CGSize(width: widthToShow + 10.0 , height: 50.0)
    }
}


