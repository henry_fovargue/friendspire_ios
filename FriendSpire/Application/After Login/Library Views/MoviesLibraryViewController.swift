//
//  MoviesLibraryViewController.swift
//  FriendSpire
//
//  Created by abhishek on 03/08/18.
//  Copyright © 2018 openkey. All rights reserved.
//

import UIKit
import CoreLocation
import GooglePlaces

enum ListTypeCategory : Int {
    case friends = 0
    case everyone
}

class MoviesLibraryViewController: DIBaseController {
    
    var type : Constant.CategoryTypeBackend?
    var currentAddress:Address?
    var generArrey:[String] = [String]()
    var selectedGenerArrey:[String] = [String]()
    
    
    //IBOutlet
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var viewTopBar: UIView!
    @IBOutlet weak var tableViewLib: UITableView!
    @IBOutlet weak var segmentUserType: UISegmentedControl!
    @IBOutlet weak var lblBadgeCount: UILabel!
    @IBOutlet weak var generCollectionView: UICollectionView!
    
    @IBOutlet weak var collectionHeightConstraint: NSLayoutConstraint!
    fileprivate var arrMoviesFriends = [FeedModal]()
    fileprivate var arrMoviesEveryone = [FeedModal]()
    fileprivate var reachedEndOfFriendFeed = false
    fileprivate var reachedEndOfEveryoneFeed = false
    let refreshControl = UIRefreshControl()
    
    fileprivate var currentSelection = ListTypeCategory.friends.rawValue
    fileprivate var indexToRefresh : Int?
    var category : Int?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        getGenre()
        // Do any additional setup after loading the view.
        
        addPagination()
        tableViewLib.refreshControl = refreshControl
        refreshControl.addTarget(self, action: #selector(pullToRefresh), for: UIControlEvents.valueChanged)
        tableViewLib.register(UINib.init(nibName: CellIdentifiers.libraryScreen.CategoryList.movieNib, bundle: nil), forCellReuseIdentifier: CellIdentifiers.libraryScreen.CategoryList.movieCell)
        
        if title != nil {
            lblTitle.text = title //Change title accordingly
        }
        if colorToUse != nil {
            viewTopBar.backgroundColor = colorToUse //Change top bar color accordingly
        }
        
        showLoader()
        var modal = self.getDefaultRequestModal()
        //        var modal = self.getModifiedRequestModal(nextPage: false)
        DispatchQueue.global(qos: .background).async {
            self.resetAllCheck() // allow api hit
            //                        var modal = self.getDefaultRequestModal()
            
            modal.friends = 1 // only friends review
            self.getFeeds(modal: modal)
            modal.friends = 0 // everyone review
            self.getFeeds(modal: modal)
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        refreshViewManaged()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: - Custom Methods
    func refreshViewManaged(){
        if RestuarantFilterModel.sharedInstance.filterApplied == true{
//            _ = RestuarantFilterModel.objFeedListRequest.getDictonary()
//         RestuarantFilterModel.filterCount = RestuarantFilterModel.filterCountKey
            setFilterCount()
        }
        self.tableViewLib.reloadData()
        if self.tableViewLib.numberOfRows(inSection: 0) > 0{
            let indexPath = IndexPath(row: indexToRefresh ?? 0, section: 0)
            self.tableViewLib.scrollToRow(at: indexPath, at: .top, animated: false)
            //check if same post is on another tab
            if indexToRefresh != nil{
                let modal = getModal(index: indexToRefresh!)
                self.updatePostOnOtherTab(modal: modal) // update the same post
            }
            indexToRefresh = nil
        }
    }
    
    func pullToRefresh(){
        resetAllCheck()
        if self.generArrey.count == 0{
            getGenre()
        }
        var modal = self.getDefaultRequestModal()
        if RestuarantFilterModel.sharedInstance.filterApplied == true{
            modal = getModifiedRequestModal(nextPage: false)
        }
        modal.page = 1
        if segmentUserType.selectedSegmentIndex == ListTypeCategory.friends.rawValue{
            modal.friends = 1 // only friends review
            self.getFeeds(modal: modal)
        }else {
            modal.friends = 0 // everyone review
            self.getFeeds(modal: modal)
        }
    }
    
    func resetAllCheck(){
        reachedEndOfFriendFeed = false
        reachedEndOfEveryoneFeed = false
    }
    
    /**
     Check if user reached to end of list to a particular list
     */
    fileprivate func shouldProceedWithFetch() -> Bool{
        
        if currentSelection == ListTypeCategory.friends.rawValue{
            return !reachedEndOfFriendFeed
        }else {
            return !reachedEndOfEveryoneFeed
        }
        
    }
    
    //MARK:-Function To fecth Gener.
    func getGenre() {
        DIWebLayerCategoriesDetailApi().getGenre(type:type?.rawValue ?? 3, success: { (data) in
            self.generArrey = data
            if self.generArrey.count == 0{
                self.collectionHeightConstraint.constant = 0
            }else{
                self.collectionHeightConstraint.constant = 50
            }
            self.generCollectionView.reloadData()
        }) { (error) in
        }
    }
    
    /**
     each time table view reaches to its limit callback will allow you to manage api Hits
     */
    func addPagination(){
        self.tableViewLib.infiniteScrolling(self) { [weak self] in
            
            guard let modal = self?.getModifiedRequestModal(nextPage: true) else {return}
            self?.getFeeds(modal: modal,true)
            
        }
    }
    
    /**
     get count to use according to category
     */
    fileprivate func getCount()->Int {
        
        if self.segmentUserType.selectedSegmentIndex == ListTypeCategory.friends.rawValue{
            return arrMoviesFriends.count
        } else {
            return arrMoviesEveryone.count
        }
        
    }
    
    /**
     This will hide all loaders used in this class
     and also scroll table view on top if required
     */
    
    fileprivate func hideAllLoaders(count : Int , _ selectedIndex:Int? = 0){
        DispatchQueue.main.async {
            self.hideLoader()
            self.tableViewLib.endPull2RefreshAndInfiniteScrolling(count: count)
            self.refreshControl.endRefreshing()
            if self.segmentUserType.selectedSegmentIndex == selectedIndex {
                self.tableViewLib.reloadData()
            }
        }
    }
    
    
    // MARK: - IBAction
    
    @IBAction func backTapped(_ sender: UIButton) {
        Utility.resetFilter()
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func addRecomendationTapped(_ sender: UIButton) {
        
        self.redirectToAddRecommendation(category: Utility.getBackendTypeString(using: type?.rawValue ?? 3))
    }
    
    @IBAction func filterButtonTapped(_ sender: UIButton) {
        showFilterMovie(using: type?.rawValue ?? 3,fromLibrary: false)
    }
    
    @IBAction func userTypeTapped(_ sender: UISegmentedControl) {
        
        currentSelection = sender.selectedSegmentIndex
        
        if currentSelection == ListTypeCategory.friends.rawValue{
            tableViewLib.enableInfiniteScrolling(status: !reachedEndOfFriendFeed)
        }else{
            tableViewLib.enableInfiniteScrolling(status: !reachedEndOfEveryoneFeed)
        }
        DispatchQueue.main.asyncAfter(deadline: .now() + .milliseconds(300)) {
            if self.tableViewLib.numberOfRows(inSection: 0) > 0{
                let indexPath = IndexPath(row: 0, section: 0)
                self.tableViewLib.scrollToRow(at: indexPath, at: .top, animated: true)
            }
        }
        tableViewLib.reloadData()
    }
}

extension MoviesLibraryViewController : UITableViewDataSource,UITableViewDelegate{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        var count = 0
        if segmentUserType.selectedSegmentIndex == ListTypeCategory.friends.rawValue{
            count = arrMoviesFriends.count
        }else {
            count = arrMoviesEveryone.count
        }
        if count > 0 {
            tableViewLib.setEmptyMessage("")
        }else {
            tableViewLib.setEmptyMessage(AppMessages.ErrorAlerts.noRecords)
        }
        
        return count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: CellIdentifiers.libraryScreen.CategoryList.movieCell) as! RecomendationCell
        let modal = getModal(index: indexPath.row)
        let strCategory = Utility.getBackendTypeString(using: modal.type ?? 3)
        cell.configureCellAddRecomendationList(modal: modal, with: indexPath, with: strCategory)
        cell.btnShare.addTarget(self, action: #selector(share(_ :)), for: .touchUpInside)
        cell.btnBookmark.addTarget(self, action: #selector(bookmark(_ :)), for: .touchUpInside)
        cell.btnAddRating.addTarget(self, action: #selector(rate(_ :)), for: .touchUpInside)
        return cell
    }
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let modal :  FeedModal = getModal(index: indexPath.row)
        indexToRefresh = indexPath.row
        let controller : UIViewController?
        if modal.type ?? 0 == Constant.CategoryTypeBackend.movie.rawValue ||  modal.type ?? 0 == Constant.CategoryTypeBackend.tv.rawValue || modal.type ?? 0 == Constant.CategoryTypeBackend.books.rawValue{
            let controllerLocal : MovieDetailViewController = UIStoryboard.init(storyboard: .inspiration).initVC()
            controllerLocal.category =  modal.type ?? 0
            controllerLocal.postId = modal.id ?? ""
            controllerLocal.modalRef = modal
            controller = controllerLocal
        }else {
            let  controllerLocal : DetailPageViewController = UIStoryboard.init(storyboard: .inspiration).initVC()
            controllerLocal.category =  modal.type ?? 0
            controllerLocal.postId = modal.id ?? ""
            controllerLocal.modalRef = modal
            controller = controllerLocal
        }
        if controller != nil{
            self.navigationController?.pushViewController(controller!, animated: true)
        }
    }
    
    /**
     fetch modal from locally managed array
     */
    fileprivate func getModal(index:Int)->FeedModal {
        if segmentUserType.selectedSegmentIndex == ListTypeCategory.friends.rawValue{
            if index >= arrMoviesFriends.count{return FeedModal()}
            return arrMoviesFriends[index]
        }else {
            if index >= arrMoviesEveryone.count{return FeedModal()}
            return  arrMoviesEveryone[index]
        }
    }
    
    //MARK:- Custom Methods
    fileprivate func getDefaultRequestModal() ->FeedListRequest{
        var modal = FeedListRequest()
        modal.category = self.type?.rawValue ?? 0   // 0 refer to all
        modal.page = 1
        return modal
    }
    
    
    fileprivate func getModifiedRequestModal(nextPage :Bool)-> FeedListRequest{
        
        var modal = FeedListRequest()
        
        //check if filter applied
        if RestuarantFilterModel.sharedInstance.filterApplied == true{
            
            //get filter values to request along with api
            if self.type?.rawValue == Constant.CategoryTypeBackend.movie.rawValue ||  self.type?.rawValue == Constant.CategoryTypeBackend.tv.rawValue || self.type?.rawValue == Constant.CategoryTypeBackend.books.rawValue{
                modal =  RestuarantFilterModel.sharedInstance.getDictionarForBooks()
            }
        }
        
        modal.category = self.type?.rawValue ?? 0
        
        //for friends
        if self.segmentUserType.selectedSegmentIndex == ListTypeCategory.friends.rawValue{
            let currentOffset = Utility.getCurrentOffset(count: self.arrMoviesFriends.count)
            if nextPage == true{
                modal.page = currentOffset+1
            }else {
                modal.page = currentOffset
            }
            modal.friends = 1
        }else { //for everyone
            let currentOffset = Utility.getCurrentOffset(count: self.arrMoviesEveryone.count)
            if nextPage == true{
                modal.page = currentOffset+1
            }else {
                modal.page = currentOffset
            }
            modal.friends = 0
        }
        return modal
    }
    
    //MARK:-  Custom Methods
    @objc func share(_ sender : UIButton){
        let modal : FeedModal = getModal(index: sender.tag)
        self.shareData(id:modal.id , type: modal.type ?? 1, notificationType: 1, title: modal.title ?? "")
    }
    
    @objc func bookmark(_ sender : UIButton){
        
        let modal : FeedModal = getModal(index: sender.tag)
        modal.isBookmarked = !sender.isSelected
        sender.isSelected = !sender.isSelected
        var ratingModal = RatingRequest()
        ratingModal.referenceid = modal.id ?? ""
        ratingModal.type = type?.rawValue ?? 3
        
        if sender.isSelected == true{ // addBookmark
            showLoader()
            self.addBookmark(modal: ratingModal) { [weak self](finish) in
                self?.hideLoader()
                if finish == false{
                    sender.isSelected = !sender.isSelected  // failure, change back to previous state
                    modal.isBookmarked = !sender.isSelected
                }
                self?.updatePostOnOtherTab(modal: modal)
            }
        }else{ //remove bookmark
            //Show pop up
            self.showAlert(withTitle: appName.capitalized, message: AppMessages.AlertTitles.removeBookMark, okayTitle: AppMessages.AlertTitles.Yes, cancelTitle: AppMessages.AlertTitles.No, okCall: {
                self.showLoader()
                self.removeBookmark(modal: ratingModal) {[weak self] (finish) in
                    self?.hideLoader()
                    if finish == false{
                        sender.isSelected = !sender.isSelected //failure, change back to previous state
                        modal.isBookmarked = !sender.isSelected
                    }
                    self?.updatePostOnOtherTab(modal: modal)
                }
            }) {
                //Nothing to do
                sender.isSelected = !sender.isSelected
                modal.isBookmarked = !sender.isSelected
            }
        }
    }
    
    @objc func rate(_ sender : UIButton){
        let modal : FeedModal = getModal(index: sender.tag)
        indexToRefresh = sender.tag
        self.addRating(using: modal.type ?? 0, referenceID: modal.id ?? "",modal)
    }
    
    fileprivate func updatePostOnOtherTab(modal:FeedModal){
        
        if segmentUserType.selectedSegmentIndex == ListTypeCategory.everyone.rawValue{
            if let index : Int = arrMoviesFriends.index(where: {$0.id == modal.id}) {
                arrMoviesFriends.remove(at: index)
                arrMoviesFriends.insert(modal, at: index)
            }
            
        }else{
            if let index : Int = arrMoviesEveryone.index(where: {$0.id == modal.id}){
                arrMoviesEveryone.remove(at: index)
                arrMoviesEveryone.insert(modal, at: index)
            }
        }
    }
}

extension MoviesLibraryViewController {
    
    //MARK:- API Integration
    fileprivate func getFeeds(modal : FeedListRequest , _ nextPage: Bool? = false){
        
        if !Utility.isConnectedToInternet {
            self.hideAllLoaders( count: self.getCount(),modal.friends)
            self.showAlert( message: AppMessages.ErrorAlerts.noInternet)
            return
        }
        
        // check if more no more records left
        if self.shouldProceedWithFetch() == false{
            self.hideAllLoaders(count: self.getCount(),modal.friends)
            return
        }
        
        DIWebLayerUserAPI().getListOfFeeds(parameters: modal.getDictonary(), success: { [weak self](response) in
            
            
            self?.fillDataInArray( with: response.0, using: modal)
            if modal.friends == 1{
                self?.segmentUserType.setTitle("Friends (\(response.1.count))", forSegmentAt: 0)
            }else {
                self?.segmentUserType.setTitle("Everyone (\(response.1.count))", forSegmentAt: 1)
            }
            
        }) { [weak self] (error) in
            DILog.print(items: error.message ?? "")
            DispatchQueue.main.async {
                self?.showAlert( message: error.message ?? AppMessages.ErrorAlerts.unknownError)
                self?.hideAllLoaders(count: self?.getCount() ?? 0,modal.friends)
            }
        }
    }
    /**
     manage local array according to  category and parameters response from server ,page is current offset
     */
    fileprivate func fillDataInArray( with response : [FeedModal], using modal : FeedListRequest ){
        
        if modal.friends == 1{
            if modal.page == 1{
                arrMoviesFriends.removeAll()
            }else{
                //*********** Make sure to scroll to last feed user was, when user request for new feeds
                DispatchQueue.main.asyncAfter(deadline: .now() + .milliseconds(100)) {
                    if self.tableViewLib.numberOfRows(inSection: 0) > 0{
                        self.tableViewLib.scrollToRow(at: IndexPath.init(row: self.arrMoviesFriends.count - (response.count+1) , section: 0), at: .top, animated: false)
                    }
                }
                
            }
            if response.count > 0 {
                reachedEndOfFriendFeed  = false
                arrMoviesFriends = modifyArrayContent(arr: arrMoviesFriends, with: response, using: modal.page)
                if response.count < RECORDS_PER_PAGE{
                    reachedEndOfFriendFeed = true
                }
            }else{
                reachedEndOfFriendFeed  = true
            }
        }else {
            if modal.page == 1{
                arrMoviesEveryone.removeAll()
            }else{
                
                DispatchQueue.main.asyncAfter(deadline: .now() + .milliseconds(100)) {
                    if self.tableViewLib.numberOfRows(inSection: 0) > 0{
                        self.tableViewLib.scrollToRow(at: IndexPath.init(row: self.arrMoviesEveryone.count - (response.count+1) , section: 0), at: .top, animated: false)
                    }
                }
                
            }
            if response.count > 0 {
                reachedEndOfEveryoneFeed  = false
                arrMoviesEveryone = modifyArrayContent(arr: arrMoviesEveryone, with: response, using: modal.page)
                if response.count < RECORDS_PER_PAGE{
                    reachedEndOfEveryoneFeed = true
                }
            }else{
                reachedEndOfEveryoneFeed  = true
            }
        }
        self.hideAllLoaders(count: response.count, modal.friends)
        
    }
    
    /// check local array  current offset and add or remove accordingly
    ///
    /// - Parameters:
    ///   - arr: reference of array to do operation on
    ///   - response: server response casted in Modal
    ///   - page: current offset
    /// - Returns: FeedModal array
    
    fileprivate func modifyArrayContent( arr : [FeedModal], with response : [FeedModal], using page : Int) -> [FeedModal]{
        var arrRef = arr
        
        if page == 1{
            arrRef.removeAll()
        }
        arrRef.append(contentsOf: response)
        return arrRef
    }
}


extension MoviesLibraryViewController : BookFilterDelegate{
    
    func filterApplied() {
        
        resetAllCheck()
        RestuarantFilterModel.sharedInstance.filterApplied = true
        setFilterCount()
        var modal = getModifiedRequestModal(nextPage: false)
        modal.page = 1
        showLoader()
        DispatchQueue.global(qos: .background).async {
            modal.page = 1
            modal.friends = 1 // only friends review
            self.getFeeds(modal: modal)
            modal.friends = 0 // everyone review
            self.getFeeds(modal: modal)
        }
    }
    
    fileprivate func setFilterCount(){
        if RestuarantFilterModel.filterCount > 0{
            lblBadgeCount.isHidden = false
            lblBadgeCount.text = "\(RestuarantFilterModel.filterCount)"
        }else {
            lblBadgeCount.isHidden = true
        }
    }
}


extension MoviesLibraryViewController : AddRatingDelegate{
    func refreshViewDetail(rated: Bool) {
        if rated == true{
            refreshViewManaged()
        }
    }
}


extension MoviesLibraryViewController : UICollectionViewDataSource,UICollectionViewDelegate,UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return generArrey.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        guard let cell:CuisinesGenerCollectionCell = collectionView.dequeueReusableCell(withReuseIdentifier: "CuisinesGenerCollectionCell", for: indexPath) as? CuisinesGenerCollectionCell else{
            return UICollectionViewCell()
        }
        cell.cuisinesGenerLabel.text = generArrey[indexPath.item]
        if selectedGenerArrey.contains(generArrey[indexPath.item]) {
            cell.cuisinesGenerLabel.textColor = colorToUse
        }else{
            cell.cuisinesGenerLabel.textColor = .black
        }
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if selectedGenerArrey.contains(generArrey[indexPath.item]) {
            for (index,data) in  selectedGenerArrey.enumerated() {
                if data == generArrey[indexPath.item] {
                    selectedGenerArrey.remove(at: index)
                }
            }
        }else{
            selectedGenerArrey.append(generArrey[indexPath.item])
        }
        generCollectionView.reloadData()
        if selectedGenerArrey.count > 0{
            let str = "\""
            RestuarantFilterModel.objFeedListRequest.genre = "[\(str)\(selectedGenerArrey.joined(separator: "\(str),\(str)"))\(str)]"
        }else {
            RestuarantFilterModel.objFeedListRequest.genre.removeAll()
        }
        filterApplied()
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let str = generArrey[indexPath.item]
        let widthToShow = str.width(withConstrainedHeight: 40.0, font: UIFont.init(name: "SFUIText-Regular", size: 17.0)!)
        return CGSize(width: widthToShow + 10.0 , height: 50.0)
    }
}

