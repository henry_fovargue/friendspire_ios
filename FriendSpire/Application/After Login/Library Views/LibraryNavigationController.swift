//
//  LibraryNavigationController.swift
//  FriendSpire
//
//  Created by abhishek on 26/07/18.
//  Copyright © 2018 openkey. All rights reserved.

//

import UIKit

class LibraryNavigationController: UINavigationController {
  
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        
        //Set root view controller for library tab
        let controller = UIStoryboard.init(storyboard: .networks).instantiateViewController(withIdentifier: "UserProfileViewController") as! UserProfileViewController
        controller.fromLibrary = true
        setViewControllers([controller], animated: false)
 
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
 }
