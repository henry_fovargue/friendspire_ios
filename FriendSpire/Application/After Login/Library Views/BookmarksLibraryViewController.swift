//
//  BookmarksLibraryViewController.swift
//  FriendSpire
//
//  Created by abhishek on 09/08/18.
//  Copyright © 2018 openkey. All rights reserved.
//

import UIKit
import CoreLocation
import GooglePlaces

enum ListTypeLibrary : Int {
    case reviews = 0
    case bookmark
}
class BookmarksLibraryViewController: DIBaseController {
    
    //variable from another view controller
    var type : Constant.CategoryTypeBackend?
    var userID : String?
    var comingFromLibrary:Bool = false
    var img : UIImage? = #imageLiteral(resourceName: "user")
    var currentAddress:Address?
    var cuisinesGenerArrey:[String] = [String]()
    var selectedCuisinesGenerArrey:[String] = [String]()
    
    //IBOutlets
    @IBOutlet weak var searchBarViewHeightCostraint: NSLayoutConstraint!
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var tableViewLib: UITableView!
    @IBOutlet weak var segmentListingType: UISegmentedControl!
    @IBOutlet weak var imgViewUser: UIImageView!
    @IBOutlet weak var lblBadgeCount: UILabel!
    @IBOutlet weak var viewTopBar: GradientView!
    @IBOutlet weak var cuisinesGenerCollectionView: UICollectionView!
    @IBOutlet weak var txtLocation: UITextField!
    @IBOutlet weak var btnMap: UIButton!
    @IBOutlet weak var collectionHeightConstraint: NSLayoutConstraint!
    
    @IBOutlet weak var constraintWidthbtnMap: NSLayoutConstraint!
    
    fileprivate var indexToRefresh : Int?
    fileprivate var arrReviews = [FeedModal]()
    fileprivate var arrBookmarks = [FeedModal]()
    fileprivate var reachedEndOfReviewFeed = false
    fileprivate var reachedEndOfBookmarkFeed = false
    fileprivate var currentSelection = ListTypeLibrary.reviews.rawValue
    var customLocationBookmark : CLLocationCoordinate2D?
    var totalReviewCount = 0 ,totalBookmarkCount = 0
    let refreshControl = UIRefreshControl()
    
    //    fileprivate var currentListType  = ListTypeLibrary.reviews
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        
        
        if type?.rawValue == Constant.CategoryTypeBackend.bars.rawValue || type?.rawValue == Constant.CategoryTypeBackend.restraunt.rawValue {
            getCuisines()
            searchBarViewHeightCostraint.constant = 50
            btnMap.isHidden = false
            constraintWidthbtnMap.constant = 40
        }else{
            btnMap.isHidden = true
            constraintWidthbtnMap.constant = -10
            searchBarViewHeightCostraint.constant = 0
            getGenre()
        }
        
        
        addPagination()
        tableViewLib.refreshControl = refreshControl
        refreshControl.addTarget(self, action: #selector(pullToRefresh), for: UIControlEvents.valueChanged)
        tableViewLib.register(UINib.init(nibName: CellIdentifiers.libraryScreen.CategoryList.movieNib, bundle: nil), forCellReuseIdentifier: CellIdentifiers.libraryScreen.CategoryList.bookmarkCell)
        let viewFooter = UIView()
        viewFooter.backgroundColor = .clear
        tableViewLib.tableFooterView = viewFooter
        imgViewUser.image = img
        
        if title != nil {
            lblTitle.text = (title ?? "")
            //                + "-" + AppMessages.ScreenTitles.library  //Change title accordingly
        }
        if colorToUse != nil {
            viewTopBar.backgroundColor = colorToUse //Change top bar color accordingly
        }
        
        showLoader()
//        let modal = self.getModifiedRequestModal(nextPage: false)
        let modal = self.getDefaultRequestModal()
        DispatchQueue.global(qos: .background).async {
            self.resetAllCheck() // allow api hit
            //            let modal = self.getDefaultRequestModal()
            self.getReview(modal: modal, nextPage: false)
            self.getBookmark(modal: modal, nextPage: false)
        }
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        if User.sharedInstance.userCurrentLocation == nil {
            Utility.fetchUserLocation {[weak self] (finish) in
                if finish == true{
                    self?.refreshViewManaged()
                }
            }
        }else{
            self.refreshViewManaged()
        }
        
        
        if User.sharedInstance.isLocationEnabled  == false && (self.title ?? AppMessages.Categories.movie == AppMessages.Categories.restruant || self.title ?? AppMessages.Categories.movie == AppMessages.Categories.bars){
            DispatchQueue.main.asyncAfter(deadline: .now() + .milliseconds(100)) {
                self.hideLoader()
                self.showAlert(withTitle: AppMessages.Location.title, message: AppMessages.AlertTitles.locationOff, okayTitle: AppMessages.AlertTitles.Ok, cancelTitle: nil, okCall: {
                    self.navigationController?.popViewController(animated: true)
                }, cancelCall: {
                })
            }
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    
    
    // MARK: - IBAction
    
    @IBAction func addRecomendationTapped(_ sender: UIButton) {
        self.redirectToAddRecommendation(category: title)
    }
    
    @IBAction func mapTapped(_ sender: UIButton) {
        
        //        Utility.resetFilter()
        let controller = UIStoryboard.init(storyboard: .library).instantiateViewController(withIdentifier: "RestrauntsLibraryOnMapViewController") as! RestrauntsLibraryOnMapViewController
        controller.title = self.title
        controller.colorToUse = self.colorToUse
        controller.navToUse = self.navigationController // this will use to keep the stack on presented controller as well
        controller.modalTransitionStyle = .crossDissolve
        controller.modalPresentationStyle = .overCurrentContext
        controller.arrRestauntFriends = arrReviews
        controller.arrRestauntEveryone = arrBookmarks
        controller.selectedIndexListType = segmentListingType.selectedSegmentIndex
        controller.selectedCuisinesArrey = self.selectedCuisinesGenerArrey
        controller.currentAddress = self.currentAddress
        controller.currentUserLocation = self.customLocationBookmark
        controller.fromLibrary = true
        controller.type = self.type
        if self.userID != nil && self.userID != User.sharedInstance.id {
            controller.userId = self.userID ?? ""
        }
        controller.delegate = self
        // start new stack management for presented controller
        let navController = UINavigationController.init(rootViewController: controller)
        navController.navigationBar.isHidden = true
        AppDelegate.shared().window?.rootViewController?.present(navController, animated: true, completion: nil)
    }
    
    @IBAction func backTapped(_ sender: UIButton) {
        
        Utility.resetFilter()
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func setCurrentLocationTapped(_ sender: UIButton) {
        LocationManager.shared.start()
        LocationManager.shared.success = { address,timestamp in
            self.currentAddress = address
            RestuarantFilterModel.objFeedListRequest.latLong = "[\(String(describing: address.lat ?? 0.0)),\(String(describing: address.long ?? 0.0))]"
            RestuarantFilterModel.objFeedListRequest.filterLatLong = 1
            self.filterApplied()
            self.txtLocation.text = AppMessages.Location.current
            self.customLocationBookmark = nil
        }
        LocationManager.shared.failure = {
            if $0.code == .locationPermissionDenied {
                self.openSetting()
            } else {
                self.showAlert(withError: $0)
            }
        }
    }
    
    fileprivate  func openSetting() {
        self.showAlert(withTitle: AppMessages.Location.title, message: AppMessages.Location.off, okayTitle: "Settings".localized, cancelTitle: "Cancel", okCall: {
            UIApplication.shared.open(URL(string: UIApplicationOpenSettingsURLString)! as URL)
        }, cancelCall: {
        })
    }
    
    @IBAction func searchBarButtonAction(_ sender: UIButton) {
        self.view.endEditing(true)
        let autocompleteController = GMSAutocompleteViewController()
        autocompleteController.delegate = self
        let filter = GMSAutocompleteFilter()
        filter.type = .city
        autocompleteController.autocompleteFilter = filter
        present(autocompleteController, animated: true, completion: nil)
    }
    
    @IBAction func filterButtonTapped(_ sender: UIButton) {
        
        if type?.rawValue  == Constant.CategoryTypeBackend.movie.rawValue || type?.rawValue == Constant.CategoryTypeBackend.tv.rawValue || type?.rawValue  == Constant.CategoryTypeBackend.books.rawValue{
            showFilterMovie(using: type?.rawValue ?? 1,fromLibrary: true)
        }else {
            showFilterBar(using: type?.rawValue ?? 1,fromLibrary: true)
        }
    }
    
    @IBAction func listTypeTapped(_ sender: UISegmentedControl) {
        
        currentSelection = sender.selectedSegmentIndex
        if currentSelection == ListTypeLibrary.reviews.rawValue{
            tableViewLib.enableInfiniteScrolling(status: !reachedEndOfReviewFeed)
        }else{
            tableViewLib.enableInfiniteScrolling(status: !reachedEndOfBookmarkFeed)
        }
        
        DispatchQueue.main.asyncAfter(deadline: .now() + .milliseconds(300)) {
            if self.tableViewLib.numberOfRows(inSection: 0) > 0{
                let indexPath = IndexPath(row: 0, section: 0)
                self.tableViewLib.scrollToRow(at: indexPath, at: .top, animated: true)
            }
        }
        tableViewLib.reloadData()
    }
    
    //MARK:- Custom Methods
    
    func refreshViewManaged(){
        
        //check if filter applied
        if RestuarantFilterModel.sharedInstance.filterApplied == true{
            
            // check particular type to get exact count
//            if self.type?.rawValue == Constant.CategoryTypeBackend.movie.rawValue ||  self.type?.rawValue == Constant.CategoryTypeBackend.tv.rawValue || self.type?.rawValue == Constant.CategoryTypeBackend.books.rawValue{
//                 _ = RestuarantFilterModel.sharedInstance.getDictionarForBooks().getDictonary()
//            }else {
//                _ = RestuarantFilterModel.sharedInstance.getDictionarForRestaurant().getDictonary()
//            }
//            _ = RestuarantFilterModel.objFeedListRequest.getDictonary()
//            RestuarantFilterModel.filterCount = RestuarantFilterModel.filterCountKey
            setFilterCount()
        }
        if segmentListingType.selectedSegmentIndex == ListTypeLibrary.bookmark.rawValue{
            
            if txtLocation.text == "" || txtLocation.text == AppMessages.Location.current{
                swapBookmark()
                self.tableViewLib.reloadData()
            }
        }else {
            swapReviewBookmark()
            self.tableViewLib.reloadData()
        }
        let rowCount = self.tableViewLib.numberOfRows(inSection: 0)
        if rowCount > 0{
            //check if index removed now set it to last one
            if  indexToRefresh ?? 0 >= rowCount { indexToRefresh = rowCount-1}
            let indexPath = IndexPath(row: indexToRefresh ?? 0, section: 0)
            self.tableViewLib.scrollToRow(at: indexPath, at: .top, animated: false)
        }
        
    }
    
    /**
     get count to use according to category
     */
    fileprivate func getCount()->Int {
        
        if currentSelection == ListTypeLibrary.reviews.rawValue{
            return arrReviews.count
        } else {
            return arrBookmarks.count
        }
    }
    
    fileprivate func swapReviewBookmark(){
        if self.userID != User.sharedInstance.id {return}
        for obj in arrReviews.enumerated(){
            
            //check if review bookmarked now
            if obj.element.isBookmarked == true{
                arrBookmarks.append(obj.element)
                arrReviews.remove(at: obj.offset)
                self.totalBookmarkCount = self.totalBookmarkCount + 1
                self.totalReviewCount = self.totalReviewCount - 1
            }//check if review deleted
            else if obj.element.userRating ?? 0 < 1{
                arrReviews.remove(at: obj.offset)
                self.totalReviewCount = self.totalReviewCount - 1
            }
        }
        
        segmentListingType.setTitle("Reviews (\(totalReviewCount))", forSegmentAt: 0)
        segmentListingType.setTitle("Bookmarks (\(totalBookmarkCount))", forSegmentAt: 1)
    }
    
    fileprivate func swapBookmark(){
        if self.userID != User.sharedInstance.id {return}
        for obj in arrBookmarks.enumerated(){
            
            
            //check if  bookmarked removed
            if obj.element.isBookmarked == false{
                arrBookmarks.remove(at: obj.offset)
                self.totalBookmarkCount = self.totalBookmarkCount - 1
                //check if bookmark change to review now
                if obj.element.userRating ?? 0 > 0 {
                    arrReviews.append(obj.element)
                    self.totalReviewCount = self.totalReviewCount + 1
                }
            }
            //check if bookmark change to review now
            //            else if obj.element.userRating ?? 0 > 0 {
            //                arrReviews.append(obj.element)
            //                arrBookmarks.remove(at: obj.offset)
            //                self.totalReviewCount = self.totalReviewCount + 1
            //                 self.totalBookmarkCount = self.totalBookmarkCount - 1
            //            }
        }
        
        segmentListingType.setTitle("Reviews (\(totalReviewCount))", forSegmentAt: 0)
        segmentListingType.setTitle("Bookmarks (\(totalBookmarkCount))", forSegmentAt: 1)
    }
    
    
    fileprivate func getDefaultRequestModal() ->FeedListRequest{
        var modal = FeedListRequest()
        modal.category = self.type?.rawValue ?? 0   // 0 refer to all
        modal.page = 1
        if self.userID != nil && self.userID != User.sharedInstance.id {
            modal.userID = self.userID ?? ""
        }
        if self.type?.rawValue == Constant.CategoryTypeBackend.restraunt.rawValue ||  self.type?.rawValue == Constant.CategoryTypeBackend.bars.rawValue {
            modal.latLong = "[\(User.sharedInstance.userCurrentLocation?.latitude ?? 0.0),\(User.sharedInstance.userCurrentLocation?.longitude ?? 0.0)]"
        }
        return modal
    }
    
    
    fileprivate   func getModifiedRequestModal(nextPage :Bool)-> FeedListRequest{
        var modal = FeedListRequest()
        modal.latLong = "[\(User.sharedInstance.userCurrentLocation?.latitude ?? 0.0),\(User.sharedInstance.userCurrentLocation?.longitude ?? 0.0)]"
        
        if RestuarantFilterModel.sharedInstance.filterApplied == true{
            
            if self.type?.rawValue == Constant.CategoryTypeBackend.movie.rawValue ||  self.type?.rawValue == Constant.CategoryTypeBackend.tv.rawValue || self.type?.rawValue == Constant.CategoryTypeBackend.books.rawValue{
                modal =  RestuarantFilterModel.sharedInstance.getDictionarForBooks()
            }else {
                modal = RestuarantFilterModel.sharedInstance.getDictionarForRestaurant()
                // if user doesn't select location send current location
                if modal.latLong == ""{
                    modal.latLong = "[\(User.sharedInstance.userCurrentLocation?.latitude ?? 0.0),\(User.sharedInstance.userCurrentLocation?.longitude ?? 0.0)]"
                }
            }
            
            // if user selected the distance neglect filterLatLong
            if modal.distance != "" {
                modal.filterLatLong = 0
            }
            
        }
        
        modal.category = self.type?.rawValue ?? 0
        
        if self.userID != nil && self.userID != User.sharedInstance.id {
            modal.userID = self.userID ?? ""
        }
        if self.segmentListingType.selectedSegmentIndex == ListTypeLibrary.bookmark.rawValue{
            let currentOffset = Utility.getCurrentOffset(count: self.arrBookmarks.count)
            if nextPage == true{
                modal.page = currentOffset+1
            }else {
                modal.page = currentOffset
            }
        }else {
            let currentOffset = Utility.getCurrentOffset(count: self.arrReviews.count)
            if nextPage == true{
                modal.page = currentOffset+1
            }else {
                modal.page = currentOffset
            }
        }
        return modal
    }
    
    func pullToRefresh(){
        resetAllCheck()
        if self.cuisinesGenerArrey.count == 0{
            if type?.rawValue == Constant.CategoryTypeBackend.bars.rawValue || type?.rawValue == Constant.CategoryTypeBackend.restraunt.rawValue {
                getCuisines()
            }else{
                getGenre()
            }
        }
        var modal = getDefaultRequestModal()
        if RestuarantFilterModel.sharedInstance.filterApplied == true{
            modal = getModifiedRequestModal(nextPage: false)
        }
        modal.page = 1 // setDefault
        self.getReview(modal: modal, nextPage: false)
        self.getBookmark(modal: modal, nextPage: false)
    }
    
    func resetAllCheck(){
        reachedEndOfReviewFeed = false
        reachedEndOfBookmarkFeed = false
    }
    
    
    /**
     each time table view reaches to its limit callback will allow you to manage api Hits
     */
    func addPagination(){
        self.tableViewLib.infiniteScrolling(self) {
            
            let modal = self.getModifiedRequestModal(nextPage: true)
            if self.segmentListingType.selectedSegmentIndex == ListTypeLibrary.bookmark.rawValue{
                self.getBookmark(modal: modal ,nextPage: true)
            }else {
                self.getReview(modal: modal ,nextPage: true)
            }
            
        }
    }
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
}

extension BookmarksLibraryViewController : UITableViewDataSource,UITableViewDelegate{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        var count = 0
        if currentSelection == ListTypeLibrary.bookmark.rawValue{
            count = arrBookmarks.count
        }else {
            count = arrReviews.count
        }
        if count > 0 {
            tableView.setEmptyMessage("")
        }else {
            tableView.setEmptyMessage(AppMessages.ErrorAlerts.noRecords)
        }
        return count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: CellIdentifiers.libraryScreen.CategoryList.bookmarkCell) as! RecomendationCell
        let modal : FeedModal = getModal(index: indexPath.row) ?? FeedModal()
        let checkReview = segmentListingType.selectedSegmentIndex == ListTypeLibrary.bookmark.rawValue ? false : true
        cell.configureCellLibrary(modal, with: indexPath, with: self.title ?? AppMessages.Categories.movie,isReview: checkReview,isLoginUser: self.userID  != User.sharedInstance.id ? false : true)
        cell.btnShare.addTarget(self, action: #selector(share(_ :)), for: .touchUpInside)
        cell.btnBookmark.addTarget(self, action: #selector(bookmark(_ :)), for: .touchUpInside)
        cell.btnAddRating.addTarget(self, action: #selector(rate(_ :)), for: .touchUpInside)
        cell.commentButton.addTarget(self, action: #selector(rate(_ :)), for: .touchUpInside)
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let modal :  FeedModal?
        indexToRefresh = indexPath.row
        if segmentListingType.selectedSegmentIndex == ListTypeLibrary.reviews.rawValue{
            modal = arrReviews[indexPath.row]
        }else {
            modal =  arrBookmarks[indexPath.row]
        }
        
        let controller : UIViewController?
        if modal?.type ?? 0 == Constant.CategoryTypeBackend.movie.rawValue ||  modal?.type ?? 0 == Constant.CategoryTypeBackend.tv.rawValue || modal?.type ?? 0 == Constant.CategoryTypeBackend.books.rawValue{
            let controllerLocal : MovieDetailViewController = UIStoryboard.init(storyboard: .inspiration).initVC()
            controllerLocal.category =  modal?.type ?? 0
            controllerLocal.postId = modal?.id ?? ""
            controllerLocal.modalRef = modal
            controllerLocal.fromAntherUserLibrary = (self.userID != User.sharedInstance.id ? true : false )
            controller = controllerLocal
        }else {
            let  controllerLocal : DetailPageViewController = UIStoryboard.init(storyboard: .inspiration).initVC()
            controllerLocal.category =  modal?.type ?? 0
            controllerLocal.postId = modal?.id ?? ""
            controllerLocal.modalRef = modal
            controllerLocal.fromAntherUserLibrary = (self.userID != User.sharedInstance.id ? true : false)
            controller = controllerLocal
        }
        if controller != nil{
            self.navigationController?.pushViewController(controller!, animated: true)
        }
    }
    
    //MARK:-  Custom Methodsfg
    @objc func share(_ sender : UIButton){
        let modal : FeedModal = getModal(index: sender.tag) ?? FeedModal()
        self.shareData(id:modal.id , type: modal.type ?? 1, notificationType: 1, title: modal.title ?? "")
    }
    
    @objc func bookmark(_ sender : UIButton){
        /********Not allowed to take action on another user profile*********/
        //        if self.userID  != User.sharedInstance.id{
        //            return
        //        }
        
        if segmentListingType.selectedSegmentIndex == ListTypeLibrary.reviews.rawValue && self.userID == User.sharedInstance.id{
            self.showAlert( message:"Bookmark not allowed on rated Recommendations")
            return
        }
        let modal : FeedModal = getModal(index: sender.tag) ?? FeedModal()
        sender.isSelected = !sender.isSelected
        modal.myBookmarkStatus = sender.isSelected == true ? 1 : 0
        var ratingModal = RatingRequest()
        ratingModal.referenceid = modal.id ?? ""
        ratingModal.type = Utility.getBackendType(using: self.title ?? "")
        
        
        if sender.isSelected == true{ // addBookmark
            showLoader()
            self.addBookmark(modal: ratingModal) { (finish) in
                self.hideLoader()
                if finish == false{
                    sender.isSelected = !sender.isSelected  // failure, change back to previous state
                    modal.myBookmarkStatus = sender.isSelected == true ? 1 : 0
                    
                }
            }
        }else{ //remove bookmark
            
            //Show pop up
            self.showAlert(withTitle: appName.capitalized, message: AppMessages.AlertTitles.removeBookMark, okayTitle: AppMessages.AlertTitles.Yes, cancelTitle: AppMessages.AlertTitles.No, okCall: {
                self.showLoader()
                self.removeBookmark(modal: ratingModal) { (finish) in
                    self.hideLoader()
                    if finish == false{
                        sender.isSelected = !sender.isSelected //failure, change back to previous state
                        modal.myBookmarkStatus = sender.isSelected == true ? 1 : 0
                        
                    }else {
                        self.resetAllCheck()
                        self.getBookmark(modal: self.getModifiedRequestModal(nextPage: false), nextPage: false)
                    }
                }
            }) {
                //Nothing to do
                sender.isSelected = !sender.isSelected
                modal.myBookmarkStatus = sender.isSelected == true ? 1 : 0
            }
        }
    }
    
    @objc func rate(_ sender : UIButton){
        //        if segmentListingType.selectedSegmentIndex == ListTypeLibrary.bookmark.rawValue{
        //            self.showAlert( message:"Rating not allowed on Bookmarked Recomendations")
        //            return
        //        }
        /********Not allowed to take action on another user profile*********/
        //        if self.userID  != User.sharedInstance.id{
        //            return
        //        }
        indexToRefresh = sender.tag
        let modal : FeedModal = getModal(index: sender.tag) ?? FeedModal()
        self.addRating(using: modal.type ?? 0, referenceID: modal.id ?? "",modal,self.userID != User.sharedInstance.id ? true : false )
    }
    
    
    fileprivate func getModal(index:Int) -> FeedModal?{
        if currentSelection == ListTypeLibrary.bookmark.rawValue{
            if index < self.arrBookmarks.count{
                return self.arrBookmarks[index]
            }
        }
        else {
            if index < self.arrReviews.count{
                return self.arrReviews[index]
            }
        }
        return nil
    }
}

//MARK:- API
extension BookmarksLibraryViewController {
    //MARK:- API Integration
    
    
    
    //MARK:-Function To fecth Cuisines.
    func getCuisines() {
        //        self.showLoader()
        DIWebLayerCategoriesDetailApi().getCuisines(type:type?.rawValue ?? 1, success: { (data) in
            self.cuisinesGenerArrey = data
            if self.cuisinesGenerArrey.count == 0{
                self.collectionHeightConstraint.constant = 0
            }else{
                self.collectionHeightConstraint.constant = 50
            }
            self.cuisinesGenerCollectionView.reloadData()
            //            self.hideLoader()
        }) { (error) in
            //      self.showAlert(message:error.message)
            //            self.hideLoader()
        }
    }
    
    //MARK:-Function To fecth Genre.
    func getGenre() {
        //        self.showLoader()
        DIWebLayerCategoriesDetailApi().getGenre(type:type?.rawValue ?? 0 , success: { (data) in
            self.cuisinesGenerArrey = data
            if self.cuisinesGenerArrey.count == 0{
                self.collectionHeightConstraint.constant = 0
            }else{
                self.collectionHeightConstraint.constant = 50
            }
            self.cuisinesGenerCollectionView.reloadData()
            //            self.hideLoader()
        }) { (error) in
            //  self.showAlert(message:error.message)
            //            self.hideLoader()
        }
    }
    
    
    
    fileprivate func getBookmark(modal : FeedListRequest, nextPage : Bool){
        
        
        if !Utility.isConnectedToInternet {
            self.hideAllLoader(nextPage: nextPage,ListTypeLibrary.bookmark.rawValue)
            self.showAlert( message: AppMessages.ErrorAlerts.noInternet)
            return
        }
        
        if reachedEndOfBookmarkFeed == true{
            self.hideAllLoader(nextPage: nextPage,ListTypeLibrary.bookmark.rawValue)
            return
        }
        
        //        print(modal.page)
        DIWebLayerUserAPI().getBookmarkListing(for: modal.getDictonary(), success: { [weak self](response) in
            if modal.page == 1{
                self?.arrBookmarks.removeAll()
            }
            
            if response.0.count > 0{
                self?.arrBookmarks.append(contentsOf: response.0)
                self?.reachedEndOfBookmarkFeed = false
                if response.0.count < RECORDS_PER_PAGE{
                    self?.reachedEndOfBookmarkFeed = true
                }
            }else {
                self?.reachedEndOfBookmarkFeed = true
            }
            self?.totalBookmarkCount = response.1.count
            self?.segmentListingType.setTitle("\(AppMessages.ListType.bookmarks) (\(self?.totalBookmarkCount ?? 0))", forSegmentAt: 1)
            self?.hideAllLoader(nextPage: nextPage,ListTypeLibrary.bookmark.rawValue)
        }) { [weak self] (error) in
            DILog.print(items: error.message ?? "")
            DispatchQueue.main.async {
                self?.showAlert( message: error.message ?? AppMessages.ErrorAlerts.unknownError)
                self?.hideAllLoader(nextPage: nextPage,ListTypeLibrary.bookmark.rawValue)
            }
        }
    }
    
    fileprivate func getReview(modal : FeedListRequest, nextPage : Bool){
        
        
        if !Utility.isConnectedToInternet {
            self.hideAllLoader(nextPage: nextPage,ListTypeLibrary.reviews.rawValue)
            self.showAlert( message: AppMessages.ErrorAlerts.noInternet)
            return
        }
        
        if reachedEndOfReviewFeed == true{
            self.hideAllLoader(nextPage: nextPage,ListTypeLibrary.reviews.rawValue)
            return
        }
        
        //        print(modal.page)
        DIWebLayerUserAPI().getReviewListing(for: modal.getDictonary(), success:  { [weak self](response) in
            
            if modal.page == 1{
                self?.arrReviews.removeAll()
            }
            
            if response.0.count > 0{
                self?.arrReviews.append(contentsOf:  response.0)
                self?.reachedEndOfReviewFeed = false
                if response.0.count < RECORDS_PER_PAGE{
                    self?.reachedEndOfReviewFeed = true
                }
            }else {
                self?.reachedEndOfReviewFeed = true
            }
            self?.totalReviewCount = response.1.count
            self?.segmentListingType.setTitle("\(AppMessages.ListType.reviews) (\(self?.totalReviewCount ?? 0))", forSegmentAt: 0)
            self?.hideAllLoader(nextPage: nextPage,ListTypeLibrary.reviews.rawValue)
            
            
        }) { [weak self] (error) in
            DILog.print(items: error.message ?? "")
            DispatchQueue.main.async {
                self?.showAlert( message: error.message ?? AppMessages.ErrorAlerts.unknownError)
                self?.hideAllLoader(nextPage: nextPage,ListTypeLibrary.reviews.rawValue)
            }
        }
    }
    
    fileprivate func hideAllLoader(nextPage:Bool,_ selectedIndex : Int? = 0){
        DispatchQueue.main.async {
            self.hideLoader()
            self.refreshControl.endRefreshing()
            self.tableViewLib.endPull2RefreshAndInfiniteScrolling(count: self.getCount())
            if self.segmentListingType.selectedSegmentIndex == selectedIndex {
                self.tableViewLib.reloadData()
            }
        }
    }
}

extension BookmarksLibraryViewController : BookFilterDelegate{
    
    func filterApplied() {
        resetAllCheck()
        RestuarantFilterModel.sharedInstance.filterApplied = true
        var modal = getModifiedRequestModal(nextPage: false)
        setFilterCount()
        modal.page = 1
        showLoader()
        arrBookmarks.removeAll()
        arrReviews.removeAll()
        self.getBookmark(modal: modal ,nextPage: false)
        self.getReview(modal: modal ,nextPage: false)
    }
    
    fileprivate func setFilterCount(){
        if RestuarantFilterModel.filterCount > 0{
            lblBadgeCount.isHidden = false
            lblBadgeCount.text = "\(RestuarantFilterModel.filterCount)"
        }else {
            lblBadgeCount.isHidden = true
        }
    }
}

extension BookmarksLibraryViewController : AddRatingDelegate{
    func refreshViewDetail(rated: Bool) {
        if rated == true{
            refreshViewManaged()
        }
    }
}

extension BookmarksLibraryViewController : UICollectionViewDataSource,UICollectionViewDelegate ,UICollectionViewDelegateFlowLayout{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return cuisinesGenerArrey.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        guard let cell:CuisinesGenerCollectionCell = collectionView.dequeueReusableCell(withReuseIdentifier: "CuisinesGenerCollectionCell", for: indexPath) as? CuisinesGenerCollectionCell else{
            return UICollectionViewCell()
        }
        cell.cuisinesGenerLabel.text = cuisinesGenerArrey[indexPath.item]
        if selectedCuisinesGenerArrey.contains(cuisinesGenerArrey[indexPath.item]) {
            cell.cuisinesGenerLabel.textColor = colorToUse
        }else{
            cell.cuisinesGenerLabel.textColor = .black
        }
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if selectedCuisinesGenerArrey.contains(cuisinesGenerArrey[indexPath.item]) {
            for (index,data) in  selectedCuisinesGenerArrey.enumerated() {
                if data == cuisinesGenerArrey[indexPath.item] {
                    selectedCuisinesGenerArrey.remove(at: index)
                }
            }
        }else{
            selectedCuisinesGenerArrey.append(cuisinesGenerArrey[indexPath.item])
        }
        cuisinesGenerCollectionView.reloadData()
        if type?.rawValue == Constant.CategoryTypeBackend.bars.rawValue || type?.rawValue == Constant.CategoryTypeBackend.restraunt.rawValue {
            if selectedCuisinesGenerArrey.count > 0{
                RestuarantFilterModel.objFeedListRequest.cuisine = "[\"\(selectedCuisinesGenerArrey.joined(separator: "\",\""))\"]"
            }else {
                RestuarantFilterModel.objFeedListRequest.cuisine.removeAll()
            }
        }else{
            if selectedCuisinesGenerArrey.count > 0{
                let str = "\""
                RestuarantFilterModel.objFeedListRequest.genre = "[\(str)\(selectedCuisinesGenerArrey.joined(separator: "\(str),\(str)"))\(str)]"
            }else {
                RestuarantFilterModel.objFeedListRequest.genre.removeAll()
            }
        }
        filterApplied()
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let str = cuisinesGenerArrey[indexPath.item]
        let widthToShow = str.width(withConstrainedHeight: 40.0, font: UIFont.init(name: "SFUIText-Regular", size: 17.0)!)
        return CGSize(width: widthToShow + 10.0 , height: 50.0)
    }
}


extension BookmarksLibraryViewController: RestrauntsLibraryOnMapViewControllerDelegate{
    
    func changeIndexSelection(index: Int) {
        segmentListingType.selectedSegmentIndex = index
        currentSelection = index
        tableViewLib.reloadData()
    }
    
    func changeLocation(address: Address) {
        self.txtLocation.text = address.city
        self.currentAddress = address
        self.customLocationBookmark = CLLocationCoordinate2DMake(self.currentAddress?.lat ?? 0.0, self.currentAddress?.long ?? 0.0)
        //            self.currentAddress?.lat
        RestuarantFilterModel.objFeedListRequest.latLong = "[\(String(describing: self.currentAddress?.lat ?? 0.0)),\(String(describing: self.currentAddress?.long ?? 0.0))]"
        RestuarantFilterModel.objFeedListRequest.filterLatLong = 1
        self.filterApplied()
    }
   
    func shouldProceedWith(filter: Bool, _ cusines: [String]?) {
        if filter == true {
            filterApplied()
        }
        guard (cusines?.count) != nil else {
            return
        }
        selectedCuisinesGenerArrey = cusines ?? []
        self.cuisinesGenerCollectionView.reloadData()
    }
}

