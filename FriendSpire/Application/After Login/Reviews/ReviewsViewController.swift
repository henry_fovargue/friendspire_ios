//
//  ReviewsViewController.swift
//  FriendSpire
//
//  Created by Harpreet_kaur on 02/08/18.
//  Copyright © 2018 openkey. All rights reserved.
//

import UIKit
import IQKeyboardManagerSwift

protocol ReviewsDelegate {
    func moveToProfileController(using userId : String?)
    func refresh()
}

class ReviewsViewController: DIBaseController {
    
    //MARK:-Variables
    var category:Int = 1
    var postId:String = ""
    var pageNo = 1
    //    var reviewsArray:[ReviewsModel] = [ReviewsModel]()
    var everyoneArray:[ReviewsModel] = [ReviewsModel]()
    var friendsArray:[ReviewsModel] = [ReviewsModel]()
    var friendsData:Bool = true
    var refreshControl: UIRefreshControl!
    var sectionToLoad : Int?
    var delegate : ReviewsDelegate?
    
    var scrollToRecommendationID : String?
    var commentID:String?
    
    var editComment:String?
    //    var totalCount : Int = {
    //       friendsArray.count
    //    }()
    
    //MARK:-IBOutlets.
    @IBOutlet weak var reviewsTabelView: UITableView!
    @IBOutlet weak var topView: UIView!
    @IBOutlet weak var mainViewHeight: NSLayoutConstraint!
    @IBOutlet weak var viewTappbleArea: UIView!
    @IBOutlet weak var btnReply: UIButton!
    @IBOutlet weak var viewReply: UIView!
    @IBOutlet weak var txtReply: UITextField!
    
    @IBOutlet weak var constraintBottomViewReply: NSLayoutConstraint!
    
    //MARK:-ViewLifeCycle.
    //MARK:-ViewDidLoad.
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //        reviewsTabelView.keyboardDismissMode = .onDrag
        
        let viewLoc = UIView()
        viewLoc.backgroundColor = #colorLiteral(red: 0.959019959, green: 0.9754132628, blue: 0.9938805699, alpha: 1)
        reviewsTabelView.tableFooterView = viewLoc
        self.view.backgroundColor = self.colorToUse
        addRefreshController()
        addPagination()
        showLoader()
        getReviews()
        addGestureToViewToDismissPresentController()
        reviewsTabelView.register(UINib(nibName: Constant.Xib.reviewsTabelCellXib, bundle: nil), forCellReuseIdentifier: Constant.TableviewCellIdentifier.reviewsTabelCellXib)
        
        let tap = UITapGestureRecognizer(target: self, action: #selector(dismissCurrentController))
        viewTappbleArea.addGestureRecognizer(tap)
        //  setHeightOfView()
        
        viewReply.isHidden = true
        btnReply.isUserInteractionEnabled = false
        btnReply.alpha = 0.5
        //        txtReply.delegate = self
        
    }
    //MARK:-ViewWillAppear.
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        IQKeyboardManager.shared.enable = false
        IQKeyboardManager.shared.shouldResignOnTouchOutside = false
        
        //        txtReply.keyboardToolbar.doneBarButton.setTarget(self, action: #selector(dismissKeyboard))
        //        NotificationCenter.default.addObserver(self, selector: #selector(liftReply(notification:)), name: NSNotification.Name.UIKeyboardWillShow, object: nil)
        //        NotificationCenter.default.addObserver(self, selector: #selector(resetReply), name: NSNotification.Name.UIKeyboardWillHide, object: nil)
        
        //        ReviewsTabelCellXib.delegate = self
    }
    
    //MARK:-ViewWillAppear.
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        IQKeyboardManager.shared.enable = true
        IQKeyboardManager.shared.shouldResignOnTouchOutside = true
        NotificationCenter.default.removeObserver(self)
        delegate?.refresh()
    }
    
    //MARK:-Private Functions.
    //MARK:-AddGesture to View.
    func addGestureToViewToDismissPresentController() {
        let swipeDown = UISwipeGestureRecognizer(target: self, action: #selector(dismissCurrentController))
        swipeDown.direction = UISwipeGestureRecognizerDirection.down
        self.topView.addGestureRecognizer(swipeDown)
    }
    //MARK:-Function to Dismiss Present Controller.
    func dismissCurrentController(sender: UISwipeGestureRecognizer) {
        delegate?.refresh()
        
        self.navigationController?.popViewController(animated: false)
        //        self.dismiss(animated: true, completion: nil)
    }
    
    //    func dismissKeyboard(){
    ////        resetReply()
    ////        replyInitate()
    //    }
    
    //MARK:-Function to Set the height of current view.
    func setHeightOfView() {
        reviewsTabelView.reloadData()
        if #available(iOS 11.0, *) {
            self.reviewsTabelView.performBatchUpdates(nil, completion: { (true) in
                let height = self.reviewsTabelView.contentSize.height
                if height <= UIScreen.main.bounds.height-50 {
                    self.mainViewHeight.constant = height+70
                    self.reviewsTabelView.isScrollEnabled = false
                }else{
                    self.mainViewHeight.constant = UIScreen.main.bounds.height-50
                }
            })
        } else {
            self.mainViewHeight.constant = UIScreen.main.bounds.height-50
        }
    }
    
    //MARK:- AddRefreshController To TableView.
    private func addRefreshController() {
        refreshControl = UIRefreshControl()
        refreshControl.tintColor = .black
        refreshControl.addTarget(self, action: #selector(refreshReviews) , for: .valueChanged)
        reviewsTabelView.addSubview(refreshControl)
    }
    
    //MARK:- Button ACtion
    
    @IBAction func replyButtonTapped(_ sender: UIButton) {
        //        replyInitate()
    }
    
    //MARK:- Custom Methods
    
    //    func replyInitate(){
    //        if sectionToLoad != nil {
    //
    //            var modalToUse = ReplyRequest()
    //            var post = ReviewsModel()
    //            if friendsArray.count != 0 && sectionToLoad! < friendsArray.count  {
    //                modalToUse.recommendation_id = friendsArray[sectionToLoad!].id ?? ""
    //                post = friendsArray[sectionToLoad!]
    //            }else{
    //                modalToUse.recommendation_id = everyoneArray[sectionToLoad!-friendsArray.count].id ?? ""
    //                post = everyoneArray[sectionToLoad!-friendsArray.count]
    //            }
    //
    //            modalToUse.comment = txtReply.text ?? ""
    //            //            self.view.endEditing(true)
    //
    //
    //            constraintBottomViewReply.constant = 0.0
    //            showLoader()
    //            if editComment != nil{
    //                modalToUse.comment_id = editComment ?? ""
    //                editComment(post: post, request: modalToUse)
    //            }else{
    //                replyOnPost(post: post, request: modalToUse)
    //            }
    //        }
    //    }
    
    /**
     each time table view reaches to its limit callback will allow you to manage api Hits
     */
    func addPagination(){
        self.reviewsTabelView.infiniteScrolling(self) {[weak self] in
            self?.getReviews()
        }
    }
    
    //MARK:- Function To Refresh review Tableview Data.
    @objc func refreshReviews() {
        if !(Utility.isConnectedToInternet) {
            self.showAlert(message:AppMessages.ErrorAlerts.noInternet)
            return
        }
        pageNo = 1
        getReviews()
        self.refreshControl.endRefreshing()
    }
    
    //MARK:- API Integration
    //    func replyOnPost(post : ReviewsModel, request:ReplyRequest) {
    //        if !Utility.isConnectedToInternet {
    //            self.hideLoader()
    //            self.view.endEditing(true)
    //            self.showAlert( message: AppMessages.ErrorAlerts.noInternet)
    //            return
    //        }
    //        DIWebLayerUserAPI().addComment(parameters: request.getDictonary(), success: { [weak self](response) in
    //            self?.hideLoader()
    //            self?.txtReply.text = ""
    //            self?.view.endEditing(true)
    //            self?.btnReply.isUserInteractionEnabled = false
    //            self?.btnReply.alpha = 0.5
    //            self?.scrollToRecommendationID = nil
    //            self?.commentID = response.id
    //            if post.comments?.count ?? 0 > 0{
    //                post.comments?.append(response)
    //            }else{
    //                post.comments = [response]
    //            }
    //            self?.reviewsTabelView.reloadData()
    //
    //        }) { [weak self](error) in
    //            self?.hideLoader()
    //            self?.view.endEditing(true)
    //            self?.showAlert(message:error.message)
    //        }
    //    }
    
    //    func editComment(post : ReviewsModel, request:ReplyRequest) {
    //        if !Utility.isConnectedToInternet {
    //            self.hideLoader()
    //            self.view.endEditing(true)
    //            self.showAlert( message: AppMessages.ErrorAlerts.noInternet)
    //            return
    //        }
    //        DIWebLayerUserAPI().updateComment(parameters: request.getDictonary(), success: { [weak self](response) in
    //            self?.hideLoader()
    //            self?.txtReply.text = ""
    //            self?.view.endEditing(true)
    //            self?.btnReply.isUserInteractionEnabled = false
    //            self?.btnReply.alpha = 0.5
    //            self?.commentID = self?.editComment
    //            if let index = post.comments?.index(where: {$0.id == self?.editComment})
    //            {
    //                post.comments![index].comment = request.comment
    //            }
    //            self?.editComment = nil
    //            //            print(index)
    //            //            if post.comments?.count ?? 0 > 0{
    //            //                post.comments?.append(response)
    //            //            }else{
    //            //                post.comments = [response]
    //            //            }
    //            self?.reviewsTabelView.reloadData()
    //
    //        }) { [weak self](error) in
    //            self?.hideLoader()
    //            self?.view.endEditing(true)
    //            self?.showAlert(message:error.message)
    //        }
    //    }
    
    func getReviews() {
        if !Utility.isConnectedToInternet {
            self.hideLoader()
            self.showAlert( message: AppMessages.ErrorAlerts.noInternet)
            return
        }
        DIWebLayerCategoriesDetailApi().getReviewsBasedOnCategory(postID: postId, page: pageNo, success: { (response) in
            if self.pageNo == 1{
                //                self.reviewsArray.removeAll()
                self.friendsArray.removeAll()
                self.everyoneArray.removeAll()
            }
            var reviewsData:[ReviewsModel] = [ReviewsModel]()
            let partition = response["partition"] as? Int ?? 0
            if let responseDict = response["data"] as? [[String:Any]] {
                let decoder = JSONDecoder()
                for obj in responseDict{
                    if let jasonData = try? JSONSerialization.data(withJSONObject: obj, options: .prettyPrinted){
                        let modal = try? decoder.decode(ReviewsModel.self, from: jasonData)
                        reviewsData.append(modal ?? ReviewsModel())
                    }
                }
                if reviewsData.count == 0 {
                    self.pageNo = 1
                }
                //                self.reviewsArray.append(contentsOf: reviewsData)
            }
            if partition == 1 || (reviewsData.count % RECORDS_PER_PAGE == 0 && reviewsData.count > 0)  {
                self.pageNo += 1
                self.getReviews()
            }
            self.hideLoader()
            let pageFriends = Utility.getCurrentOffset(count: self.friendsArray.count)
            
            if pageFriends == self.pageNo-1{
                self.everyoneArray.removeAll()
            }
            let everyArray =  reviewsData.filter({$0.friends == 0})
            let frndArray = reviewsData.filter({$0.friends == 1})
            self.friendsArray.append(contentsOf: frndArray)
            self.everyoneArray.append(contentsOf: everyArray)
            //            self.everyoneArray = reviewsData.filter({$0.friends == 0})
            //            self.friendsArray = reviewsData.filter({$0.friends == 1})
            self.reviewsTabelView.endReviewRefeshing(partion:partition,count:reviewsData.count)
            self.refreshControl.endRefreshing()
            self.reviewsTabelView.reloadData()
        }) { (error) in
            self.hideLoader()
            if self.pageNo >= 1 {
                self.pageNo -= 1
            }
            self.refreshControl.endRefreshing()
            self.reviewsTabelView.endReviewRefeshing(partion:0,count:0)
            self.showAlert(message:error.message)
        }
    }
    
    func reportReview(_ reviewModel:ReviewsModel){
        if reviewModel.userId?.id == User.sharedInstance.id{
            self.showAlert(message:"You can't report your own review.".localized)
            return
        }
        self.showLoader()
        let params : Parameters = ["recommendationid":reviewModel.id ?? ""]
        DIWebLayerCategoriesDetailApi().reportReview(parameter: params , success: { (value) in
            self.hideLoader()
            let message = value["message"] as? String ?? ""
            self.showAlert(message:message)
        }) { (error) in
            self.hideLoader()
            self.showAlert(message:error.message)
        }
    }
    
    //    func deleteReply(on post: ReviewsModel,_ recomendationId:String,commentIndex:Int){
    //
    //        //Show pop up
    //        self.showAlert(withTitle: appName.capitalized, message: AppMessages.AlertTitles.deleteReply, okayTitle: AppMessages.AlertTitles.Yes, cancelTitle: AppMessages.AlertTitles.No, okCall: {
    //
    //            //call api now
    //            if !Utility.isConnectedToInternet {
    //                self.hideLoader()
    //                self.showAlert( message: AppMessages.ErrorAlerts.noInternet)
    //                return
    //            }
    //            self.showLoader()
    //            let params = ["comment_id":recomendationId]
    //            DIWebLayerUserAPI().deleteComment(parameters: params, success: { (value) in
    //                self.hideLoader()
    //                post.comments?.remove(at: commentIndex)
    //                self.reviewsTabelView.reloadData()
    //            }) { (error) in
    //                self.hideLoader()
    //                self.showAlert(message:error.message)
    //            }
    //        }) {
    //            //Nothing to do
    //        }
    //    }
    
    
    @IBAction func getReviewsSegmentControlAction(_ sender: UISegmentedControl) {
        if sender.selectedSegmentIndex == 0 {
            friendsData = true
        }else{
            friendsData = false
        }
        getReviews()
    }
}

//MARK:-UITabelViewDelegate&UITabelViewDataSource.
extension ReviewsViewController : UITableViewDelegate , UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        
        if everyoneArray.count != 0 {
            return (friendsArray.count + everyoneArray.count)
        }
        return friendsArray.count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if scrollToRecommendationID != nil {
            if let sec = friendsArray.index(where: {$0.id == scrollToRecommendationID}){
                
                //                let user = friendsArray[sec].userId
                //                txtReply.placeholder = "Comment to \(user?.firstName ?? "") \(user?.lastName ?? "")"
                sectionToLoad =  sec
            }
            else if let sec = everyoneArray.index(where: {$0.id == scrollToRecommendationID}){
                
                //                let user = everyoneArray[sec].userId
                //                txtReply.placeholder = "Comment to \(user?.firstName ?? "") \(user?.lastName ?? "")"
                sectionToLoad =  sec+friendsArray.count
            }
            //            viewReply.isHidden = false
            //            txtReply.becomeFirstResponder()
            
        }
        
        //        if sectionToLoad != nil {
        //            if section == sectionToLoad{
        //                //                DispatchQueue.main.asyncAfter(deadline: .now() + .milliseconds(500)) {
        //                //                    if self.reviewsTabelView.numberOfRows(inSection: 0) > 0{
        //                //                        self.reviewsTabelView.scrollToRow(at: IndexPath.init(row: 0, section: self.sectionToLoad!), at: .top, animated: false)
        //                //                    }
        //                //                }
        //
        //                if (section < friendsArray.count && friendsArray.count != 0){
        //
        //                    if let commentToScroll = friendsArray[sectionToLoad!].comments?.index(where: {$0.id == commentID ?? ""}){
        //                        scrollToSpecficComment(section: section, index: commentToScroll+1)
        //                    }else{
        //                        scrollToSpecficComment(section: section, index: 0)
        //                    }
        //
        //                    return (friendsArray[sectionToLoad!].comments?.count ?? 0) + 1
        //                }else {
        //                    if let commentToScroll = everyoneArray[sectionToLoad!-friendsArray.count].comments?.index(where: {$0.id == commentID ?? ""}){
        //                        scrollToSpecficComment(section: section, index: commentToScroll+1)
        //                    }else{
        //                        scrollToSpecficComment(section: section, index: 0)
        //                    }
        //                    return (everyoneArray[sectionToLoad!-friendsArray.count].comments?.count ?? 0) + 1
        //                }
        //
        //            }else{
        //                return 1
        //            }
        //        }
        return 1
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        
        if everyoneArray.count != 0 && section == friendsArray.count {
            guard let header = tableView.dequeueReusableCell(withIdentifier: "HeaderTableViewCell") as? HeaderTableViewCell else {return UIView()}
            header.backgroundColor = .white
            return header
        }
        return nil
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        
        //only friends review
        if indexPath.section < friendsArray.count && friendsArray.count != 0{
            guard let cell = reviewsTabelView.dequeueReusableCell(withIdentifier: Constant.TableviewCellIdentifier.reviewsTabelCellXib, for: indexPath) as? ReviewsTabelCellXib else {
                return UITableViewCell()
            }
            let sectionIndex =  (indexPath.section+ReviewArrayReference.friendRef.rawValue)
            
            if sectionToLoad == indexPath.section && indexPath.row > 0{
                //  get reply array and populate here
                cell.configureAsReply(reviewsData: friendsArray[sectionToLoad!].comments?[indexPath.row-1] ?? Comments(), indexPath: indexPath,using: sectionIndex)
                
            }else {
                //            especially enable click event
                cell.configureasHeader(reviewsData: friendsArray[indexPath.section], indexPath: indexPath,using: sectionIndex)
                cell.buttonHeader.addTarget(self, action: #selector(headerTap), for: .touchUpInside)
                
            }
            cell.delegate = self
            return cell
        }
        
        
        // populate data related to everyone here
        guard let cell = reviewsTabelView.dequeueReusableCell(withIdentifier: Constant.TableviewCellIdentifier.reviewsTabelCellXib, for: indexPath) as? ReviewsTabelCellXib else {
            return UITableViewCell()
        }
        let sectionIndex = ((indexPath.section-friendsArray.count)+ReviewArrayReference.everyoneRef.rawValue)
        if sectionToLoad == indexPath.section && indexPath.row > 0{
            // get reply array and populate here
            cell.configureAsReply(reviewsData: everyoneArray[sectionToLoad!-friendsArray.count].comments?[indexPath.row-1] ?? Comments(), indexPath: indexPath,using: sectionIndex)
            
        }else {
            //            especially enable click event
            let index = indexPath.section-friendsArray.count // remove friends array count
            cell.configureasHeader(reviewsData: everyoneArray[index > 0 ? index : 0], indexPath: indexPath,using: sectionIndex)
            cell.buttonHeader.addTarget(self, action: #selector(headerTap), for: .touchUpInside)
        }
        cell.delegate = self
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        return true
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForHeaderInSection section: Int) -> CGFloat {
        return 45.0
    }
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        if everyoneArray.count != 0 && section == friendsArray.count{
            return 45.0
        }
        return 0.0
    }
    
    //    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
    //
    //        if indexPath.row == 0 {return}
    //
    //        if indexPath.section < friendsArray.count && friendsArray.count != 0{
    //
    //
    //            if self.friendsArray[indexPath.section].comments?[indexPath.row-1].commentedUserData?.id == User.sharedInstance.id{
    //                editComment = self.friendsArray[indexPath.section].comments?[indexPath.row-1].id ?? ""
    //                let comment = self.friendsArray[indexPath.section].comments?[indexPath.row-1].comment ?? ""
    //                txtReply.becomeFirstResponder()
    //                txtReply.text = comment
    //
    //            }
    //
    //        } else {
    //            if self.everyoneArray[indexPath.section-friendsArray.count].comments?[indexPath.row-1].commentedUserData?.id == User.sharedInstance.id{
    //
    //                editComment = self.everyoneArray[indexPath.section-friendsArray.count].comments?[indexPath.row-1].id ?? ""
    //                let comment = self.everyoneArray[indexPath.section-friendsArray.count].comments?[indexPath.row-1].comment ?? ""
    //                txtReply.becomeFirstResponder()
    //                txtReply.text = comment
    //            }
    //        }
    //
    //    }
    
    func tableView(_ tableView: UITableView, editActionsForRowAt indexPath: IndexPath) -> [UITableViewRowAction]? {
        var review = ReviewsModel()
        
        if indexPath.section < friendsArray.count && friendsArray.count != 0{
            if indexPath.row == 0 {
                review = self.friendsArray[indexPath.row]
            }
            //            else {
            //                let id = self.friendsArray[indexPath.section].comments?[indexPath.row-1].id ?? ""
            //                let post = self.friendsArray[indexPath.section]
            //                let deleteReply = UITableViewRowAction(style: .destructive, title: AppMessages.AlertTitles.delete) { (action, index) in
            //                    self.deleteReply(on: post, id, commentIndex: indexPath.row-1)
            //                }
            //                if self.friendsArray[indexPath.section].comments?[indexPath.row-1].commentedUserData?.id == User.sharedInstance.id{
            //                    return [deleteReply]
            //                }else{
            //                    return []
            //                }
            //            }
        } else {
            if indexPath.row == 0 {
                review = self.everyoneArray[indexPath.row]
            }
            //            else {
            //                let id = self.everyoneArray[indexPath.section-friendsArray.count].comments?[indexPath.row-1].id ?? ""
            //                let post = self.everyoneArray[indexPath.section-friendsArray.count]
            //                let deleteReply = UITableViewRowAction(style: .destructive, title: AppMessages.AlertTitles.delete) { (action, index) in
            //                    self.deleteReply(on: post, id, commentIndex: indexPath.row-1)
            //                }
            //                if self.everyoneArray[indexPath.section-friendsArray.count].comments?[indexPath.row-1].commentedUserData?.id == User.sharedInstance.id{
            //                    return [deleteReply]
            //                }else{
            //                    return []
            //                }
            //            }
        }
        
        let report = UITableViewRowAction(style: UITableViewRowActionStyle.destructive, title: AppMessages.AlertTitles.Report){(UITableViewRowAction,NSIndexPath) -> Void in
            self.reportReview(review)
        }
        return [report]
    }
    
    //Custom Method
    @objc func headerTap(_ sender: UIButton) {
        scrollToRecommendationID = nil
        sectionToLoad = sender.tag
        if friendsArray.count != 0 && sectionToLoad! < friendsArray.count  {
            
            guard let id = friendsArray[sectionToLoad!].id else {return}
            //            txtReply.placeholder = "Comment to \(user?.firstName ?? "") \(user?.lastName ?? "")"
            presentCommentsController(recommendationId: id,ref:friendsArray[sectionToLoad!])
        }else{
            guard let id = everyoneArray[sectionToLoad!-friendsArray.count].id else {return}
            //            txtReply.placeholder = "Comment to \(user?.firstName ?? "") \(user?.lastName ?? "")"
            presentCommentsController(recommendationId: id,ref:everyoneArray[sectionToLoad!-friendsArray.count])
            //            let user = everyoneArray[sectionToLoad!-friendsArray.count].userId
            //            txtReply.placeholder = "Comment to \(user?.firstName ?? "") \(user?.lastName ?? "")"
        }
        
        //        viewReply.isHidden = false
        //        txtReply.becomeFirstResponder()
        //        reviewsTabelView.reloadData()
    }
    
    
    fileprivate func presentCommentsController(recommendationId : String,ref:ReviewsModel){
        let controller: CommentsViewController = UIStoryboard(storyboard: .inspiration).initVC()
        //        controller.modalPresentationStyle = .overCurrentContext
        //        controller.modalTransitionStyle = .crossDissolve
        //        controller.category = self.category
        //        controller.postId = self.postId
        //        controller.commentID = self.openCommentTo
        controller.delegate = self
        controller.refModal = ref
        controller.colorToUse = self.colorToUse
        controller.reviewId = recommendationId
        self.navigationController?.pushViewController(controller, animated: true)
        //        AppDelegate.shared().window?.rootViewController?.present(controller, animated: true, completion: nil)
    }
    
    //    func scrollToSpecficComment(section:Int, index : Int? = 0){
    //        DispatchQueue.main.asyncAfter(deadline: .now() + .milliseconds(500)) {
    //            if self.reviewsTabelView.numberOfRows(inSection: section) > 0{
    //                self.reviewsTabelView.scrollToRow(at: IndexPath.init(row: index!, section: self.sectionToLoad!), at: .top, animated: false)
    //            }
    //            self.commentID = nil
    //        }
    //    }
}

extension ReviewsViewController:ReviewsTabelCellXibDelegate {
    func redirectToProfile(tag: Int,section: Int) {
        var idToSend : String?
        
        //this will redirect from header (Aka first cell of section)
        if tag == section{
            if section >= ReviewArrayReference.everyoneRef.rawValue{
                let index = section - ReviewArrayReference.everyoneRef.rawValue
                idToSend = everyoneArray[(index)].userId?.id ?? ""
            }else {
                let index =  section - ReviewArrayReference.friendRef.rawValue
                idToSend = friendsArray[index].userId?.id ?? ""
            }
        }else{
            //this will redirect from reply (Aka remaining cell of section)
            
            let originalSection = (section/2)
            if originalSection >= ReviewArrayReference.everyoneRef.rawValue{
                let index =  tag - originalSection
                idToSend = everyoneArray[(originalSection-ReviewArrayReference.everyoneRef.rawValue)].comments?[index].commentedUserData?.id ?? ""
            }else {
                let index =  tag - originalSection
                idToSend = friendsArray[(originalSection-ReviewArrayReference.friendRef.rawValue)].comments?[index].commentedUserData?.id ?? ""
            }
        }
        let  controller: UserProfileViewController = UIStoryboard.init(storyboard: .networks).initVC()
        controller.userId = idToSend
        self.navigationController?.pushViewController(controller, animated: true)
        
        //        delegate?.moveToProfileController(using: idToSend)
        //        AppDelegate.shared().window?.rootViewController?.presentedViewController?.dismiss(animated: true, completion: nil)
    }
}
extension ReviewsViewController:ReviewsDelegate{
    func moveToProfileController(using userId: String?) {
        
    }
    
    func refresh() {
        self.reviewsTabelView.reloadData()
    }
}
//extension ReviewsViewController :  UITextFieldDelegate{
//
//
//    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
//        if txtReply == textField{
//            let currentString: NSString = textField.text! as NSString
//            let newString: NSString =
//                currentString.replacingCharacters(in: range, with: string) as NSString
//            if newString.length > 0{
//                btnReply.isUserInteractionEnabled = true
//                btnReply.alpha = 1.0
//            }else{
//                btnReply.isUserInteractionEnabled = false
//                btnReply.alpha = 0.5
//            }
//            return  newString.length >= 0
//        }
//        return true
//    }
//
////    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
////        resetReply()
////        return true
////    }
//
////    func liftReply(notification: NSNotification){
////        if let keyboardSize = notification.userInfo?[UIKeyboardFrameEndUserInfoKey] as? CGRect {
////            constraintBottomViewReply.constant = keyboardSize.height
////        }
////    }
//
////    func resetReply(){
////        constraintBottomViewReply.constant = 0.0
////        if txtReply.text?.isEmpty == false{
////            replyInitate()
////        }else{
////            txtReply.resignFirstResponder()
////        }
////    }
//
//}

