//
//  CommentsViewController.swift
//  FriendSpire
//
//  Created by abhishek on 27/11/18.
//  Copyright © 2018 openkey. All rights reserved.
//

import UIKit
import IQKeyboardManagerSwift

enum BottomSpace:CGFloat{
    case iPhone10 = 82.0
    case others = 50.0
}

class CommentsViewController: DIBaseController {
    
    //MARK:-Variables
    var refModal : ReviewsModel?
    var reviewId:String?
    var reviewsArray:[ReviewsModel] = [ReviewsModel]()
    var refreshControl: UIRefreshControl!
    var sectionToLoad : Int?
    var delegate : ReviewsDelegate?
    var commentID:String?
    var editComment:String?
    fileprivate var scrollToRecommendationID : String?
//    fileprivate let bottomSpace : CGFloat = 50.0
    fileprivate var strPlaceHolder = ""
    
    //MARK:-IBOutlets.
    @IBOutlet weak var reviewsTabelView: UITableView!
    @IBOutlet weak var topView: UIView!
    //    @IBOutlet weak var mainViewHeight: NSLayoutConstraint!
    @IBOutlet weak var viewTappbleArea: UIView!
    @IBOutlet weak var btnReply: UIButton!
    @IBOutlet weak var viewReply: UIView!
    @IBOutlet weak var txtReply: UITextField!
    @IBOutlet weak var constraintBottomViewReply: NSLayoutConstraint!
    
    //MARK:-ViewLifeCycle.
    //MARK:-ViewDidLoad.
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //        reviewsTabelView.keyboardDismissMode = .onDrag
        
        scrollToRecommendationID = reviewId
        let viewLoc = UIView()
        viewLoc.backgroundColor = #colorLiteral(red: 0.959019959, green: 0.9754132628, blue: 0.9938805699, alpha: 1)
        reviewsTabelView.tableFooterView = viewLoc
        self.view.backgroundColor = colorToUse
        addRefreshController()
        
        addGestureToViewToDismissPresentController()
        reviewsTabelView.register(UINib(nibName: Constant.Xib.reviewsTabelCellXib, bundle: nil), forCellReuseIdentifier: Constant.TableviewCellIdentifier.reviewsTabelCellXib)
        
        let tap = UITapGestureRecognizer(target: self, action: #selector(dismissCurrentController))
        viewTappbleArea.addGestureRecognizer(tap)
        //  setHeightOfView()
        
        viewReply.isHidden = true
        btnReply.isUserInteractionEnabled = false
        btnReply.alpha = 0.5
        txtReply.delegate = self
        
        showLoader()
        DispatchQueue.global(qos: .background).async {
            self.getReviewAndComment()
        }
        
    }
    //MARK:-ViewWillAppear.
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        IQKeyboardManager.shared.enable = false
        IQKeyboardManager.shared.shouldResignOnTouchOutside = false
        NotificationCenter.default.addObserver(self, selector: #selector(liftReply(notification:)), name: NSNotification.Name.UIKeyboardWillShow, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(resetReply), name: NSNotification.Name.UIKeyboardWillHide, object: nil)
    }
    
    //MARK:-ViewWillAppear.
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        IQKeyboardManager.shared.enable = true
        IQKeyboardManager.shared.shouldResignOnTouchOutside = true
        NotificationCenter.default.removeObserver(self)
        delegate?.refresh()
    }
    
    //MARK:-Private Functions.
    //MARK:-AddGesture to View.
    func addGestureToViewToDismissPresentController() {
        let swipeDown = UISwipeGestureRecognizer(target: self, action: #selector(dismissCurrentController))
        swipeDown.direction = UISwipeGestureRecognizerDirection.down
        self.topView.addGestureRecognizer(swipeDown)
    }
    //MARK:-Function to Dismiss Present Controller.
    func dismissCurrentController(sender: UISwipeGestureRecognizer) {
        delegate?.refresh()
        self.navigationController?.popViewController(animated: false)
        //        self.dismiss(animated: true, completion: nil)
    }
    
    @objc fileprivate func showReply(){
        viewReply.isHidden = false
        txtReply.becomeFirstResponder()
    }
    
    //MARK:- AddRefreshController To TableView.
    private func addRefreshController() {
        refreshControl = UIRefreshControl()
        refreshControl.tintColor = .black
        refreshControl.addTarget(self, action: #selector(refreshReviews) , for: .valueChanged)
        reviewsTabelView.addSubview(refreshControl)
    }
    
    //MARK:- Button ACtion
    
    @IBAction func replyButtonTapped(_ sender: UIButton) {
        replyInitate()
    }
    
    //MARK:- Custom Methods
    
    func replyInitate(){
        if reviewsArray.count == 0 || btnReply.isUserInteractionEnabled == false {
            txtReply.resignFirstResponder()
            return
        }
        var modalToUse = ReplyRequest()
        modalToUse.recommendation_id = reviewsArray[0].id ?? ""
        let post : ReviewsModel = reviewsArray[0]
        modalToUse.comment = txtReply.text ?? ""
        if UIScreen.main.nativeBounds.height > iPHONE10{
            constraintBottomViewReply.constant = BottomSpace.iPhone10.rawValue
        }else{
            constraintBottomViewReply.constant = BottomSpace.others.rawValue
        }
        
        showLoader()
        if editComment != nil{
            modalToUse.comment_id = editComment ?? ""
            editComment(post: post, request: modalToUse)
        }else{
            replyOnPost(post: post, request: modalToUse)
        }
    }

    //MARK:- Function To Refresh review Tableview Data.
    @objc func refreshReviews() {
        if !(Utility.isConnectedToInternet) {
            self.showAlert(message:AppMessages.ErrorAlerts.noInternet)
            return
        }
        getReviewAndComment()
        self.refreshControl.endRefreshing()
    }
    
    //MARK:- API Integration
    func replyOnPost(post : ReviewsModel, request:ReplyRequest) {
        if !Utility.isConnectedToInternet {
            self.hideLoader()
            self.view.endEditing(true)
            self.showAlert( message: AppMessages.ErrorAlerts.noInternet)
            return
        }
        DIWebLayerUserAPI().addComment(parameters: request.getDictonary(), success: { [weak self](response) in
            self?.hideLoader()
            self?.txtReply.text = ""
            self?.view.endEditing(true)
            self?.btnReply.isUserInteractionEnabled = false
            self?.btnReply.alpha = 0.5
            self?.scrollToRecommendationID = nil
            self?.commentID = response.id
            if post.comments?.count ?? 0 > 0{
                post.comments?.append(response)
                post.commentCount = (post.commentCount  ?? 0) + 1
            }else{
                 post.commentCount = 1
                 post.comments = [response]
            }
            self?.refModal?.commentCount = post.commentCount
            self?.reviewsTabelView.reloadData()
            
        }) { [weak self](error) in
            self?.hideLoader()
            self?.view.endEditing(true)
            self?.showAlert(message:error.message)
        }
    }
    
    func editComment(post : ReviewsModel, request:ReplyRequest) {
        if !Utility.isConnectedToInternet {
            self.hideLoader()
            self.view.endEditing(true)
            self.showAlert( message: AppMessages.ErrorAlerts.noInternet)
            return
        }
        DIWebLayerUserAPI().updateComment(parameters: request.getDictonary(), success: { [weak self](response) in
            self?.hideLoader()
            self?.txtReply.text = ""
            self?.view.endEditing(true)
            self?.btnReply.isUserInteractionEnabled = false
            self?.btnReply.alpha = 0.5
            self?.commentID = self?.editComment
            if let index = post.comments?.index(where: {$0.id == self?.editComment})
            {
                post.comments![index].comment = request.comment
            }
            self?.editComment = nil
            self?.txtReply.placeholder = self?.strPlaceHolder
            self?.reviewsTabelView.reloadData()
            
        }) { [weak self](error) in
            self?.hideLoader()
            self?.view.endEditing(true)
            self?.showAlert(message:error.message)
        }
    }
    
    func getReviewAndComment() {
        if !Utility.isConnectedToInternet {
            self.hideLoader()
            self.showAlert( message: AppMessages.ErrorAlerts.noInternet)
            return
        }
        DIWebLayerCategoriesDetailApi().getReviewAndComment(recommendationID:reviewId!, success: { (response) in
            var reviewsData:[ReviewsModel] = [ReviewsModel]()
            if let responseDict = response["data"] as? [[String:Any]] {
                let decoder = JSONDecoder()
                for obj in responseDict{
                    if let jasonData = try? JSONSerialization.data(withJSONObject: obj, options: .prettyPrinted){
                        let modal = try? decoder.decode(ReviewsModel.self, from: jasonData)
                        reviewsData.append(modal ?? ReviewsModel())
                    }
                }
                self.reviewsArray = reviewsData
            }
            DispatchQueue.main.asyncAfter(deadline: .now() + .milliseconds(300)) {
                self.hideLoader()
                self.refreshControl.endRefreshing()
                self.reviewsTabelView.reloadData()
            }
        }) { (error) in
            self.hideLoader()
            self.refreshControl.endRefreshing()
            self.showAlert(message:error.message)
        }
    }
    
    func reportReview(_ reviewModel:ReviewsModel){
        if reviewModel.userId?.id == User.sharedInstance.id{
            self.showAlert(message:"You can't report your own review.".localized)
            return
        }
        self.showLoader()
        let params : Parameters = ["recommendationid":reviewModel.id ?? ""]
        DIWebLayerCategoriesDetailApi().reportReview(parameter: params , success: { (value) in
            self.hideLoader()
            let message = value["message"] as? String ?? ""
            self.showAlert(message:message)
        }) { (error) in
            self.hideLoader()
            self.showAlert(message:error.message)
        }
    }
    
    func deleteReply(on post: ReviewsModel,_ recomendationId:String,commentIndex:Int){
        
        //Show pop up
        self.showAlert(withTitle: appName.capitalized, message: AppMessages.AlertTitles.deleteReply, okayTitle: AppMessages.AlertTitles.Yes, cancelTitle: AppMessages.AlertTitles.No, okCall: {
            
            //call api now
            if !Utility.isConnectedToInternet {
                self.hideLoader()
                self.showAlert( message: AppMessages.ErrorAlerts.noInternet)
                return
            }
            self.showLoader()
            let params = ["comment_id":recomendationId]
            DIWebLayerUserAPI().deleteComment(parameters: params, success: { [weak self](value) in
                self?.hideLoader()
                self?.scrollToRecommendationID = nil
                post.commentCount = (post.commentCount ?? 1) - 1
                post.comments?.remove(at: commentIndex)
                self?.refModal?.commentCount = post.commentCount

                self?.reviewsTabelView.reloadData()
            }) {[weak self] (error) in
                self?.hideLoader()
                self?.showAlert(message:error.message)
            }
        }) {
            //Nothing to do
        }
    }
    
}



//MARK:-UITabelViewDelegate&UITabelViewDataSource.
extension CommentsViewController : UITableViewDelegate , UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return reviewsArray.count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if commentID != nil {
            if let commentToScroll = reviewsArray[section].comments?.index(where: {$0.id == commentID ?? ""}){
                scrollToSpecficComment(section: section, index: commentToScroll+1)
                    let user = reviewsArray[section].userId
                    configureCommentPlaceHolder(user: user)
                self.perform(#selector(showReply), with: nil, afterDelay: 0.3)
            }else{
                scrollToSpecficComment(section: section, index: 0)
            }
            

        } else if scrollToRecommendationID != nil {
            if let sec = reviewsArray.index(where: {$0.id == scrollToRecommendationID}){
                let user = reviewsArray[sec].userId
                configureCommentPlaceHolder(user: user)
            }
            self.perform(#selector(showReply), with: nil, afterDelay: 0.3)
            
        }
        return reviewsArray.count > 0 ? (reviewsArray[0].comments?.count ?? 0) + 1 : 1
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        guard let cell = reviewsTabelView.dequeueReusableCell(withIdentifier: Constant.TableviewCellIdentifier.reviewsTabelCellXib, for: indexPath) as? ReviewsTabelCellXib else {
            return UITableViewCell()
        }
        let sectionIndex =  (indexPath.section+ReviewArrayReference.friendRef.rawValue)
        
        if indexPath.row > 0{
            //  get reply array and populate here
            cell.configureAsReply(reviewsData: reviewsArray[indexPath.section].comments?[indexPath.row-1] ?? Comments(), indexPath: indexPath,using: sectionIndex)
            
        }else {
            //            especially enable click event
            cell.configureasHeader(reviewsData: reviewsArray[indexPath.section], indexPath: indexPath,using: sectionIndex)
            cell.buttonHeader.addTarget(self, action: #selector(headerTap), for: .touchUpInside)
            
        }
        cell.delegate = self
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        return true
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForHeaderInSection section: Int) -> CGFloat {
        return 45.0
    }
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 0.0
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if indexPath.row == 0 {return}
        guard let commentToProcess = self.reviewsArray[indexPath.section].comments?[indexPath.row-1] else {
            return
        }
        if commentToProcess.commentedUserData?.id == User.sharedInstance.id{
            txtReply.placeholder = "Edit comment"
            txtReply.toolbarPlaceholder = "Edit comment"
            editComment = commentToProcess.id ?? ""
            let comment = commentToProcess.comment ?? ""
            txtReply.becomeFirstResponder()
            txtReply.text = comment
            scrollToSpecficComment(section: indexPath.section, index: indexPath.row)
            
        }
        
    }
    
    func tableView(_ tableView: UITableView, editActionsForRowAt indexPath: IndexPath) -> [UITableViewRowAction]? {
        var review = ReviewsModel()
        
        if indexPath.row == 0 {
            review = self.reviewsArray[indexPath.row]
        }else {
            let id = self.reviewsArray[indexPath.section].comments?[indexPath.row-1].id ?? ""
            let post = self.reviewsArray[indexPath.section]
            let deleteReply = UITableViewRowAction(style: .destructive, title: AppMessages.AlertTitles.delete) { (action, index) in
                self.deleteReply(on: post, id, commentIndex: indexPath.row-1)
            }
            if self.reviewsArray[indexPath.section].comments?[indexPath.row-1].commentedUserData?.id == User.sharedInstance.id{
                return [deleteReply]
            }else{
                return []
            }
        }
        let report = UITableViewRowAction(style: UITableViewRowActionStyle.destructive, title: AppMessages.AlertTitles.Report){(UITableViewRowAction,NSIndexPath) -> Void in
            self.reportReview(review)
        }
        return [report]
    }
    
    //Custom Method
    @objc func headerTap(_ sender: UIButton) {
        editComment = nil
        let user = reviewsArray[sender.tag].userId
        configureCommentPlaceHolder(user: user)
//        strPlaceHolder = "Comment to \(user?.firstName ?? "") \(user?.lastName ?? "")"
//        txtReply.placeholder = strPlaceHolder
//        txtReply.toolbarPlaceholder = strPlaceHolder
        showReply()
        reviewsTabelView.reloadData()
    }
    
    func configureCommentPlaceHolder(user: UserId?){
        strPlaceHolder = "Comment to \(user?.firstName ?? "") \(user?.lastName ?? "")"
        txtReply.placeholder = strPlaceHolder
        txtReply.toolbarPlaceholder = strPlaceHolder
    }
    
    func scrollToSpecficComment(section:Int, index : Int? = 0){
        DispatchQueue.main.asyncAfter(deadline: .now() + .milliseconds(500)) {
            if self.reviewsTabelView.numberOfRows(inSection: section) > 0{
                self.reviewsTabelView.scrollToRow(at: IndexPath.init(row: index!, section: section), at: .top, animated: false)
            }
            self.commentID = nil
        }
    }
}

extension CommentsViewController:ReviewsTabelCellXibDelegate {
    func redirectToProfile(tag: Int,section: Int) {
        var idToSend : String?
        
        //this will redirect from header (Aka first cell of section)
        if tag == section{
            let index =  section - ReviewArrayReference.friendRef.rawValue
            idToSend = reviewsArray[index].userId?.id ?? ""
        }else{
            //this will redirect from reply (Aka remaining cell of section)
            
            let originalSection = (section/2)
            let index =  tag - originalSection
            idToSend = reviewsArray[(originalSection-ReviewArrayReference.friendRef.rawValue)].comments?[index].commentedUserData?.id ?? ""
        }
        delegate?.moveToProfileController(using: idToSend)
        AppDelegate.shared().window?.rootViewController?.presentedViewController?.dismiss(animated: true, completion: nil)
    }
}

extension CommentsViewController :  UITextFieldDelegate{
    
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if txtReply == textField{
            let currentString: NSString = textField.text! as NSString
            let newString: NSString =
                currentString.replacingCharacters(in: range, with: string) as NSString
            if newString.length > 0{
                btnReply.isUserInteractionEnabled = true
                btnReply.alpha = 1.0
            }else{
                btnReply.isUserInteractionEnabled = false
                btnReply.alpha = 0.5
            }
            return  newString.length >= 0
        }
        return true
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        resetReply()
        return true
    }
    
    func liftReply(notification: NSNotification){
        if let keyboardSize = notification.userInfo?[UIKeyboardFrameEndUserInfoKey] as? CGRect {
            constraintBottomViewReply.constant = keyboardSize.height
        }
    }
    
    func resetReply(){
        if UIScreen.main.nativeBounds.height > iPHONE10{
            constraintBottomViewReply.constant = BottomSpace.iPhone10.rawValue
        }else{
            constraintBottomViewReply.constant = BottomSpace.others.rawValue
        }
        if txtReply.text?.isEmpty == false{
            replyInitate()
        }else{
            txtReply.resignFirstResponder()
        }
    }
    
}
