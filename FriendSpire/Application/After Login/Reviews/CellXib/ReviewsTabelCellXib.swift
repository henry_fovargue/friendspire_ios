//
//  ReviewsTabelCellXib.swift
//  FriendSpire
//
//  Created by Harpreet_kaur on 02/08/18.
//  Copyright © 2018 openkey. All rights reserved.
//

import UIKit
protocol ReviewsTabelCellXibDelegate {
    func redirectToProfile(tag:Int,section:Int)
}

enum ReviewArrayReference : Int{
     case friendRef = 100000
     case everyoneRef = 1000000
}

class ReviewsTabelCellXib: UITableViewCell {
    
    //MARK:-Variables.
     var delegate:ReviewsTabelCellXibDelegate?
    
    //MARK:-IBOutlets.
    @IBOutlet weak var userImageView: UIImageView!
    @IBOutlet weak var userNameLabel: UILabel!
    @IBOutlet weak var emojiImageView: UIImageView!
    @IBOutlet weak var userRatingDescLabel: UILabel!
    @IBOutlet weak var userRatingLabel: UILabel!
    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var buttonHeader: UIButton!
    @IBOutlet weak var constraintLeadingImgViewUser: NSLayoutConstraint!
    @IBOutlet weak var btnReply: UIButton!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    func setData(reviewsData:ReviewsModel) {
        
        self.userImageView.af_cancelImageRequest()
        userImageView.image = #imageLiteral(resourceName: "user")
        let tap = UITapGestureRecognizer(target: self, action: #selector(redirectToProfile))
        userImageView.addGestureRecognizer(tap)
        let tap1 = UITapGestureRecognizer(target: self, action: #selector(redirectToProfileOnNameClick))
        userNameLabel.addGestureRecognizer(tap1)
        userRatingLabel.text = "\(reviewsData.rating ?? 0)"
        emojiImageView.image = Utility.getEmojiAccordindToRating(rating:Float(reviewsData.rating ?? 0))
        userNameLabel.text = "\(reviewsData.userId?.firstName ?? "") \(reviewsData.userId?.lastName ?? "")"
        if let reviews = reviewsData.review {
            userRatingDescLabel.text = reviews
        }
        dateLabel.text = reviewsData.updatedAt?.convertDate(.serverFormat, toFormat: .reviewsDate)
        if let url = URL(string: reviewsData.userId?.picture ?? "") {
            userImageView.af_setImage(withURL: url, placeholderImage: #imageLiteral(resourceName: "user"), filter: nil, progress: nil, imageTransition: .crossDissolve(0.7), runImageTransitionIfCached: false, completion: nil)
        }
        
        let count = reviewsData.commentCount ?? 0 > 0 ? "(\(reviewsData.commentCount ?? 0))" : ""
        if reviewsData.commentCount ?? 0 > 1 {
            btnReply.setTitle("Comments \(count)", for: .normal)
        }else{
            btnReply.setTitle("Comment \(count)", for: .normal)
        }
    }
    
    
    func setCommentsData(reviewsData:ReviewsModel) {
         self.userImageView.af_cancelImageRequest()
        userImageView.image = #imageLiteral(resourceName: "user")
        let tap = UITapGestureRecognizer(target: self, action: #selector(redirectToProfile))
        userImageView.addGestureRecognizer(tap)
        let tap1 = UITapGestureRecognizer(target: self, action: #selector(redirectToProfileOnNameClick))
        userNameLabel.addGestureRecognizer(tap1)
        userRatingLabel.text = "\(reviewsData.rating ?? 0)"
        emojiImageView.image = Utility.getEmojiAccordindToRating(rating:Float(reviewsData.rating ?? 0))
        userNameLabel.text = "\(reviewsData.userId?.firstName ?? "") \(reviewsData.userId?.lastName ?? "")"
        if let reviews = reviewsData.review {
            userRatingDescLabel.text = reviews
        }
        dateLabel.text = reviewsData.updatedAt?.convertDate(.serverFormat, toFormat: .reviewsDate)
        if let url = URL(string: reviewsData.userId?.picture ?? "") {
            userImageView.af_setImage(withURL: url, placeholderImage: #imageLiteral(resourceName: "user"), filter: nil, progress: nil, imageTransition: .crossDissolve(0.7), runImageTransitionIfCached: false, completion: nil)
        }
        
    }
    
    func redirectToProfile() {
        delegate?.redirectToProfile(tag: userImageView.tag, section: userRatingDescLabel.tag)
    }
    func redirectToProfileOnNameClick() {
       delegate?.redirectToProfile(tag: userNameLabel.tag, section: userRatingDescLabel.tag)
    }
    
    
    //FIXME: LatestCR
    
    func configureasHeader(reviewsData:ReviewsModel,indexPath:IndexPath, using computedIndex : Int){
        constraintLeadingImgViewUser.constant = 10.0
        buttonHeader.isUserInteractionEnabled = true
        buttonHeader.tag = indexPath.section
        
//        let count = reviewsData.comments?.count ?? 0 > 0 ? "(\(reviewsData.comments?.count ?? 0))" : ""
         let count = reviewsData.commentCount ?? 0 > 0 ? "(\(reviewsData.commentCount ?? 0))" : ""
        if reviewsData.commentCount ?? 0 > 1 {
        btnReply.setTitle("Comments \(count)", for: .normal)
        }else{
            btnReply.setTitle("Comment \(count)", for: .normal)
        }
        userImageView.tag = computedIndex
        userNameLabel.tag = computedIndex
        userRatingDescLabel.tag = computedIndex
        btnReply.isHidden = false
        userRatingLabel.isHidden = false
        emojiImageView.isHidden = false
        setCommentsData(reviewsData: reviewsData)
    }
    
    func configureAsReply(reviewsData:Comments,indexPath:IndexPath, using computedIndex : Int){
        constraintLeadingImgViewUser.constant = 30.0
        buttonHeader.isUserInteractionEnabled = false
        btnReply.isHidden = true
        userRatingLabel.isHidden = true
        emojiImageView.isHidden = true
        btnReply.setTitle("Edit", for: .normal)
        userImageView.tag = computedIndex + indexPath.row-1
        userNameLabel.tag = computedIndex + indexPath.row-1
        userRatingDescLabel.tag = computedIndex*2
        
        userImageView.image = #imageLiteral(resourceName: "user")
        let tap = UITapGestureRecognizer(target: self, action: #selector(redirectProfile(sender:)))
        userImageView.addGestureRecognizer(tap)
        let tap1 = UITapGestureRecognizer(target: self, action: #selector(redirectToProfileOnNameClick))
        userNameLabel.addGestureRecognizer(tap1)
        userNameLabel.text = "\(reviewsData.commentedUserData?.firstName ?? "") \(reviewsData.commentedUserData?.lastName ?? "")"
        if let comment = reviewsData.comment {
            userRatingDescLabel.text = comment
        }
        dateLabel.text = reviewsData.updatedAt?.convertDate(.serverFormat, toFormat: .reviewsDate)
        if let url = URL(string: reviewsData.commentedUserData?.picture ?? "") {
            userImageView.af_setImage(withURL: url, placeholderImage: #imageLiteral(resourceName: "user"), filter: nil, progress: nil, imageTransition: .crossDissolve(0.7), runImageTransitionIfCached: false, completion: nil)
        }
    }
    
    func redirectProfile(sender: UITapGestureRecognizer) {
        if let tag = sender.view?.tag{
        delegate?.redirectToProfile(tag: tag, section: userRatingDescLabel.tag)
        }
    }
    
}
