//
//  FullPictureViewController.swift
//  FriendSpire
//
//  Created by abhishek on 12/11/18.
//  Copyright © 2018 openkey. All rights reserved.
//

import UIKit


let WIDTH = UIScreen.main.bounds.size.width
let HEIGHT = UIScreen.main.bounds.size.height
let BUTTON_SIZE : CGFloat = 10.0

class FullPictureViewController: DIBaseController {
    
    var imgToLoad : UIImage?
    var ssMiniMeView : SSUIViewMiniMe?
    var row : NSInteger = 0
    var col : NSInteger = 0
    
    
    @IBOutlet weak var btnClose: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        
        let tempView = UIView.init(frame: self.view.frame)
        let imgViewLoc = UIImageView.init(image: imgToLoad)
        imgViewLoc.frame = self.view.frame
        imgViewLoc.contentMode = .scaleAspectFit
        tempView.addSubview(imgViewLoc)
        
        ssMiniMeView = SSUIViewMiniMe.init(view: tempView, withRatio: 4)
        ssMiniMeView?.delegate = self
        
        self.view.addSubview(ssMiniMeView!)
        self.view.bringSubview(toFront:btnClose)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func closeButtonTapped(_ sender: UIButton) {
        self.dismiss(animated: true, completion: nil)
    }
    
    
}

extension FullPictureViewController : SSUIViewMiniMeDelegate{
    func enlargedView(_ enlargedView: SSUIViewMiniMe!, didScroll scrollView: UIScrollView!) {
        
    }
    func enlargedView(_ enlargedView: SSUIViewMiniMe!, willBeginDragging scrollView: UIScrollView!) {
        DILog.print(items: "hey there")
    }
}
