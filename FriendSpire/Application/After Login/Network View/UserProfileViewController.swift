
//
//  UserProfileViewController.swift
//  FriendSpire
//
//  Created by abhishek on 24/07/18.
//  Copyright © 2018 openkey. All rights reserved.
//

import UIKit

fileprivate let SCROLL_TAG = 1000
class UserProfileViewController: DIBaseController {
    
    
    //Variable from another view controller To change presentation of view
    
    var fromTutorial : Bool?
    var fromLibrary : Bool?
    var userId : String?
    var isComingFromLink:Bool = false
    
    var modalFbRef : FbModal?
    var modalFolloweRef : FollowerModal?
    
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var imgVIew_Profile: UIImageView!
    @IBOutlet weak var lbl_Name: UILabel!
    @IBOutlet weak var btn_Follow: UIButton!
    @IBOutlet weak var lbl_FollowerCount: UILabel!
    @IBOutlet weak var lbl_FollowingCount: UILabel!
    @IBOutlet weak var btn_Share: UIButton!
    @IBOutlet weak var constraintHeight_BtnFollow: NSLayoutConstraint!
    @IBOutlet weak var btnBack: UIButton!
    @IBOutlet weak var lblNameOnTop: UILabel!
    @IBOutlet weak var viewTop: UIView!
    
    //All Categories
    
    @IBOutlet weak var lbl_RatingAll: UILabel!
    @IBOutlet weak var lbl_BookmarkAll: UILabel!
    
    //All Movie
    
    @IBOutlet weak var lbl_RatingMovie: UILabel!
    @IBOutlet weak var lbl_BookmarkMovie: UILabel!
    
    //All Tv
    
    @IBOutlet weak var lbl_RatingTv: UILabel!
    @IBOutlet weak var lbl_BookmarkTv: UILabel!
    
    //All Book
    
    @IBOutlet weak var lbl_RatingBook: UILabel!
    @IBOutlet weak var lbl_BookmarkBook: UILabel!
    
    //All Restraunt
    
    @IBOutlet weak var lbl_RatingRestraunt: UILabel!
    @IBOutlet weak var lbl_BookmarkRestaunt: UILabel!
    
    //All Bars
    
    @IBOutlet weak var lbl_RatingBar: UILabel!
    @IBOutlet weak var lbl_BookmarkBar: UILabel!
    
    //Latest Recomendations
    
    @IBOutlet weak var tableView_Recomendation: UITableView!
    @IBOutlet weak var constraintHeight_TableView: NSLayoutConstraint!
    
    //view to hide all things behind
    @IBOutlet weak var view_PrivateAccount: UIView!
    
    
    fileprivate var arrReviews = [ProfileRecomendation]()
    fileprivate var paginationHitInProcess = false
    fileprivate var reviewLimit = 0
    var currentUser : UserModal?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        scrollView.delegate = self
        self.scrollView.tag = SCROLL_TAG
        
        //        check if user visit own profile or from library
        if fromLibrary == true || UserManager.getCurrentUser()?.id == userId{

            //show Tutorial screen to user
//            if TutorialStatus.getCurrentStatus() == nil || TutorialStatus.getCurrentStatus()?.libraryScreen == nil{
//                self.perform(#selector(showTutorialPopup(arr:)), with: [#imageLiteral(resourceName: "layer11")], afterDelay: 0.3)
//                updateTutorialReadStatus()
//            }
            
            btn_Share.isHidden = false
            btn_Follow.isHidden = true
            constraintHeight_BtnFollow.constant = 0
            btnBack.isHidden = fromLibrary == true ? true : false
        }else {
            showLoader()
        }
        
        //register nib for cell
        tableView_Recomendation.register(UINib.init(nibName: CellIdentifiers.libraryScreen.CategoryList.movieNib, bundle: nil), forCellReuseIdentifier: CellIdentifiers.NetworksScreen.UserProfile.latestRecomendationCell)
        
        tableView_Recomendation.estimatedRowHeight = 170.0
        tableView_Recomendation.rowHeight = UITableViewAutomaticDimension
        self.isUserPrivate(status:true)
        
//        let tap = UITapGestureRecognizer.init(target: self, action: #selector(showFullPicture))
//        imgVIew_Profile.addGestureRecognizer(tap)
        if fromTutorial == true{
            viewTop.alpha = 0.0
            scrollView.scrollRectToVisible(self.view.bounds, animated: false)
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        if userId != nil {
            getProfile() //get user profile
        }
        else if fromLibrary == true{  // to get
            userId = UserManager.getCurrentUser()?.id
            getProfile()
        }
        
        var modal = SearchRecomendationRequest()
        modal.type = 0   // 0 refer to all
        modal.page = 1
        modal.userID = userId ?? ""
        modal.latLng = Utility.userCurrentLocationString() ?? ""
//        showLoader()
        getReview(modal: modal)
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
       
    }
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: - IBAction
    
    @IBAction func shareButtonTapped(_ sender: UIButton) {
        self.shareData(id: currentUser?.id , type: nil, notificationType: 2, title: currentUser?.firstName ?? "Friendpire user")
    }
    
    
    @IBAction func followButtonTapped(_ sender: UIButton) {
        //Change state of user
//        sender.isSelected = !sender.isSelected
        
        //Modal for api hit
        var modal = FollowRequest()
        modal.friendId = userId ?? ""
        
        if sender.isSelected == false{  // user want to send follow request
//            sender.isSelected = !sender.isSelected
            
            requestToFollow(modal: modal) { (finish) in
                if finish {
                    self.getProfile()
                    //                    self.btn_Follow.backgroundColor = .clear
                    //                    self.btn_Follow.isUserInteractionEnabled = false
                }else{
                    self.relationWithUser()
                    
                }
            }
            
            
        }else{ //user want to send unfollow request
//            sender.isSelected = !sender.isSelected
            
            requestToUnfollow(modal: modal, success: { (finish) in
                if finish {
                    self.getProfile()
                    //                    self.btn_Follow.backgroundColor = .white
                }else{
                    self.relationWithUser()
                }
            })
            
        }
    }
    
    @IBAction func showFollowerTapped(_ sender: UIButton) {
        
        //************ check if user view in tutorial restrict the actions
                if fromTutorial == true {return}
        
        //        show only if user is public or in following list
        if currentUser?.isPrivate == 0 || (self.currentUser?.isPrivate == 1 && self.currentUser?.followStatus == 1){
            self.performSegue(withIdentifier: SegueIdentifiers.NetworkScreen.UserProfile.showUserNetwork, sender: 0)
        }
    }
    
    @IBAction func showFollowingTapped(_ sender: UIButton) {
        //************ check if user view in tutorial restrict the actions
        if fromTutorial == true {return}
        
        //        show only if user is public or in following list
        if currentUser?.isPrivate == 0 || (self.currentUser?.isPrivate == 1 && self.currentUser?.followStatus == 1){
            self.performSegue(withIdentifier: SegueIdentifiers.NetworkScreen.UserProfile.showUserNetwork, sender: 1)
        }
    }
    
    @IBAction func backButtonTapped(_ sender: UIButton) {
        if isComingFromLink {
           appDelegate.navigateToDashboard()
        }else if fromTutorial == true{
            self.dismiss(animated: true, completion: nil)
        }else{
            self.navigationController?.popViewController(animated: true)
        }
    }
    
    @IBAction func categoryTapped(_ sender: UIButton) {

        //************ check if user view in tutorial restrict the actions
                if fromTutorial == true {return}
        
        let controller = UIStoryboard.init(storyboard: .library).instantiateViewController(withIdentifier: "BookmarksLibraryViewController" ) as! BookmarksLibraryViewController
        controller.comingFromLibrary = true
        
        switch sender.tag {
            
        case Constant.CategoriesAvailableEnum.movie.rawValue:
            controller.title = AppMessages.Categories.movie
            controller.type = Constant.CategoryTypeBackend.movie
//            controller.totalReviewCount = self.currentUser?.profileData?.movie?[0] ?? 0
//            controller.totalBookmarkCount = self.currentUser?.profileData?.movie?[1] ?? 0
            controller.colorToUse = #colorLiteral(red: 0.5461447239, green: 0.3740770221, blue: 0.7494846582, alpha: 1)
            break
            
        case Constant.CategoriesAvailableEnum.tv.rawValue:
            controller.title = AppMessages.Categories.tv
            controller.type = Constant.CategoryTypeBackend.tv
//            controller.totalReviewCount = self.currentUser?.profileData?.tv?[0] ?? 0
//            controller.totalBookmarkCount = self.currentUser?.profileData?.tv?[1] ?? 0
            controller.colorToUse = #colorLiteral(red: 0.2260053158, green: 0.6834762096, blue: 0.9318693876, alpha: 1)
            break
            
        case Constant.CategoriesAvailableEnum.book.rawValue:
            controller.title = AppMessages.Categories.books
            controller.type = Constant.CategoryTypeBackend.books
//            controller.totalReviewCount = self.currentUser?.profileData?.books?[0] ?? 0
//            controller.totalBookmarkCount = self.currentUser?.profileData?.books?[1] ?? 0
            controller.colorToUse = #colorLiteral(red: 0.1082813069, green: 0.59888798, blue: 0.5456139445, alpha: 1)

            break
            
        case Constant.CategoriesAvailableEnum.restraunt.rawValue:
            if   User.sharedInstance.isLocationEnabled  == false{
                self.showAlert(message:AppMessages.AlertTitles.locationOff)
                return
            }
//            controller.totalReviewCount = self.currentUser?.profileData?.restaurant?[0] ?? 0
//            controller.totalBookmarkCount = self.currentUser?.profileData?.restaurant?[1] ?? 0
            controller.title = AppMessages.Categories.restruant
            controller.type = Constant.CategoryTypeBackend.restraunt
            controller.colorToUse = #colorLiteral(red: 0.9959798455, green: 0.6115841269, blue: 0.3324229121, alpha: 1)

            break
            
        case Constant.CategoriesAvailableEnum.bar.rawValue:
            if   User.sharedInstance.isLocationEnabled  == false{
                self.showAlert(message:AppMessages.AlertTitles.locationOff)
                return
            }
//            controller.totalReviewCount = self.currentUser?.profileData?.bar?[0] ?? 0
//            controller.totalBookmarkCount = self.currentUser?.profileData?.bar?[1] ?? 0
            controller.title = AppMessages.Categories.bars
            controller.type = Constant.CategoryTypeBackend.bars
            controller.colorToUse = #colorLiteral(red: 0.9861864448, green: 0.3759915829, blue: 0.6583402753, alpha: 1)

            break
            
        default:
            DILog.print(items: "NA")
            return
        }
        
        controller.userID = self.currentUser?.id ?? ""
        controller.img = self.imgVIew_Profile.image
        self.navigationController?.pushViewController(controller, animated: true)
    }
    
    //MARK:- Custom method
    
    /**
     This will set the UI according to user is  private or not
     */
    fileprivate func isUserPrivate(status : Bool){
        if status == false{
            self.view_PrivateAccount.isHidden = true
            self.view_PrivateAccount.sendSubview(toBack: self.view)
            scrollView.isScrollEnabled = true
        }else{
            self.view_PrivateAccount.isHidden = false
            self.view.bringSubview(toFront: self.view_PrivateAccount)
            scrollView.isScrollEnabled = false
        }
    }
    
    /**
     This will set the UI according to user is blocked
     */
    fileprivate func userBlocked(){
        
            self.view_PrivateAccount.isHidden = false
            self.view.bringSubview(toFront: self.view_PrivateAccount)
            scrollView.isScrollEnabled = false
            btn_Share.isHidden = true
            btn_Follow.isHidden = true
            constraintHeight_BtnFollow.constant = 0
    }
    
    /**
     this will show option to follow or unfollow etc
     parameter status : Int use to define the user relation in your network
     
     */
    
    fileprivate func relationWithUser(){
        
        // check if modal reference is for fb user and update follow status
        if modalFbRef != nil{
            modalFbRef?.status = self.currentUser?.followStatus
        }else if modalFolloweRef != nil {
            //update normal user follow status
            modalFolloweRef?.status = self.currentUser?.followStatus
        }
        
        switch (self.currentUser?.followStatus ?? 0) {

        case FollowStatus.NotConnected.rawValue: //not in your network
            btn_Follow.isUserInteractionEnabled = true
            btn_Follow.isSelected = false // show follow Option
            self.btn_Follow.backgroundColor = .white
            btn_Follow.setTitle("Follow", for: .normal)
            btn_Follow.setTitleColor( #colorLiteral(red: 1, green: 0.2352941176, blue: 0.3921568627, alpha: 1), for: .normal)
            
            break
        case FollowStatus.Following.rawValue: //you follow this user
            btn_Follow.isUserInteractionEnabled = true
            btn_Follow.isSelected = true // show Unfollow Option
            btn_Follow.setTitle("Following", for: .normal)
            self.btn_Follow.backgroundColor = .clear
            btn_Follow.setTitleColor(.white, for: .normal)
            
            break
        case FollowStatus.Requested.rawValue: // waiting for user response
            btn_Follow.isUserInteractionEnabled = false
            
            btn_Follow.setTitle("Requested", for: .normal)
            btn_Follow.setTitleColor(.white, for: .normal)
            self.btn_Follow.backgroundColor = .clear
            
            break
            
        case FollowStatus.FollowBack.rawValue: // user follows you but you didn't
            btn_Follow.isUserInteractionEnabled = true

            btn_Follow.isSelected = false // show follow  back Option
            self.btn_Follow.backgroundColor = .white
            btn_Follow.setTitle("Follow", for: .normal)
            btn_Follow.setTitleColor( #colorLiteral(red: 1, green: 0.2352941176, blue: 0.3921568627, alpha: 1), for: .normal)
            
            break
            
        default:
            DILog.print(items: "nothing to do")
            break
        }
    }
    
    
    /**
     Populate whole counts for categories
     */
    fileprivate func showCategoriesCount(data: ProfileData?) {
        
        reviewLimit = data?.all?[0] ?? 0
        
        lbl_RatingAll.text = "\(data?.all?[0] ?? 0)"
        lbl_BookmarkAll.text = "\(data?.all?[1] ?? 0)"
        
        lbl_RatingMovie.text = "\(data?.movie?[0] ?? 0)"
        lbl_BookmarkMovie.text = "\(data?.movie?[1] ?? 0)"
        
        lbl_RatingTv.text = "\(data?.tv?[0] ?? 0)"
        lbl_BookmarkTv.text = "\(data?.tv?[1] ?? 0)"
        
        lbl_RatingBook.text = "\(data?.books?[0] ?? 0)"
        lbl_BookmarkBook.text = "\(data?.books?[1] ?? 0)"
        
        lbl_RatingRestraunt.text = "\(data?.restaurant?[0] ?? 0)"
        lbl_BookmarkRestaunt.text = "\(data?.restaurant?[1] ?? 0)"
        
        lbl_RatingBar.text = "\(data?.bar?[0] ?? 0)"
        lbl_BookmarkBar.text = "\(data?.bar?[1] ?? 0)"
        
    }
    
//    private func updateTutorialReadStatus(){
//        
//        if let readStatus = TutorialStatus.getCurrentStatus(){
//            readStatus.libraryScreen = true
//             TutorialStatus.saveCurrentStatus(status: readStatus)
//        }else{
//            let status = TutorialStatus.sharedInstance
//            status.libraryScreen = true
//         TutorialStatus.saveCurrentStatus(status: status)
//        }
//        
//    }
    
//    @objc func showFullPicture(){
//
//        let controller = UIStoryboard.init(storyboard: .networks).instantiateViewController(withIdentifier: "FullPictureViewController") as! FullPictureViewController
//        controller.modalTransitionStyle = .crossDissolve
//        controller.modalPresentationStyle  = .overCurrentContext
//        controller.imgToLoad = imgVIew_Profile.image
//
//        //************ check if user view in tutorial
//        if fromTutorial == true {
//             self.present(controller, animated: true, completion: nil)
//            return
//        }
//        AppDelegate.shared().window?.rootViewController?.present(controller, animated: true, completion: nil)
//    }
    
    //MARK:- API Calling
    fileprivate func getProfile(){
        if !Utility.isConnectedToInternet {
            self.hideLoader()
            if UserManager.getCurrentUser()?.id == userId{
                currentUser = UserModal.shared
            }
            setUserData()
            self.showAlert(withTitle: appName.capitalized, message: AppMessages.ErrorAlerts.noInternet, okayTitle: AppMessages.AlertTitles.Ok, okCall: {
                self.navigationController?.popViewController(animated: true)
            })
            
//            self.showAlert( message: AppMessages.ErrorAlerts.noInternet)
            return
        }
        guard let userID = userId else {return} // get user ID
//        showLoader()
        DIWebLayerUserAPI().getOtherUserProfile(for: userID, success: { [weak self](response) in
            self?.currentUser = response
            DispatchQueue.main.async {
                self?.hideLoader()
                //Show data on view
                self?.setUserData()
              
            }
            
        }) { [weak self] (error) in
            DispatchQueue.main.async {
                self?.hideLoader()
                self?.showAlert(withTitle: appName.capitalized, message: error.message, okayTitle: AppMessages.AlertTitles.Ok, okCall: {
                    self?.navigationController?.popViewController(animated: true)
                })
                
//                self?.showAlert( message: error.message ?? AppMessages.ErrorAlerts.unknownError)
            }
            DILog.print(items: error.message ?? "")
        }
    }
    
    func setUserData() {
        if let imageUrl = URL.init(string: self.currentUser?.profilePic ?? "") {
            self.imgVIew_Profile.af_setImage(withURL: imageUrl, placeholderImage: #imageLiteral(resourceName: "frndWhite") , filter: nil, progress: nil, imageTransition: .crossDissolve(0.7), runImageTransitionIfCached: false, completion: nil)
        }
        self.lbl_Name.text = "\(self.currentUser?.firstName ?? "") \(self.currentUser?.lastName ?? "")"
        self.lblNameOnTop.text = self.lbl_Name.text
        self.lbl_FollowerCount.text = "\(self.currentUser?.followers ?? 0)"
        self.lbl_FollowingCount.text = "\(self.currentUser?.followings ?? 0)"
        
        self.relationWithUser() // check for user relation with you
        
        if UserManager.getCurrentUser()?.id == userId{
            self.currentUser?.isPrivate = 0  // user profile is public as he/she visit own profile
        }
        
        //check if one able to see user whole profile
        if self.currentUser?.followStatus == 1 && self.currentUser?.isPrivate  == 1{
            self.isUserPrivate(status:false)
        }else if self.currentUser?.isBlocked == 1 {
            self.userBlocked()
        }else{
            self.isUserPrivate(status: self.currentUser?.isPrivate == 0 ? false : true)
        }
        self.showCategoriesCount(data: self.currentUser?.profileData) // set counts
    }
    
    fileprivate func getReview(modal : SearchRecomendationRequest){
        
        
        if !Utility.isConnectedToInternet {
            self.hideLoader()
            self.tableView_Recomendation.tableFooterView = UIView()
            self.showAlert( message: AppMessages.ErrorAlerts.noInternet)
             self.paginationHitInProcess = false
            return
        }
        
        
//        print(modal.page)
        DIWebLayerUserAPI().getReviewListingForProfile(for: modal, success:  { [weak self](response) in
            if modal.page == 1{
                self?.arrReviews.removeAll()
            }
            self?.paginationHitInProcess = false
            self?.arrReviews.append(contentsOf:  response)
            DispatchQueue.main.async {
                self?.hideLoader()
                self?.tableView_Recomendation.tableFooterView = UIView()
                self?.tableView_Recomendation.reloadData()
            }
            
        }) { [weak self] (error) in
            DILog.print(items: error.message ?? "")
             self?.paginationHitInProcess = false
            DispatchQueue.main.async {
                self?.tableView_Recomendation.tableFooterView = UIView()
                self?.hideLoader()
            }
        }
    }
    
    fileprivate func requestToFollow(modal : FollowRequest , success: @escaping (_ finish: Bool)-> ()){
        if !Utility.isConnectedToInternet {
            self.hideLoader()
            self.showAlert( message: AppMessages.ErrorAlerts.noInternet)
            return
        }
        showLoader()
        DIWebLayerUserAPI().followUser(parameters: modal.getDictonary(), success: { (status) in
//            DispatchQueue.main.async {
////                self?.hideLoader()
//            }
            if status.0  == 1 {
                //Refresh friends list on network view controller
                var dict = [RefreshNetworkKey.id.rawValue:modal.friendId,RefreshNetworkKey.type.rawValue:RefreshNetwork.follow.rawValue] as [String : Any]
//                if status.1 != nil{
                    if let modal : FollowerModal = status.1{
                        if  modal.status == FollowStatus.Requested.rawValue{
                         dict[RefreshNetworkKey.type.rawValue] = RefreshNetwork.requested.rawValue
                        }
                    }
//                }
//                dict[RefreshNetworkKey.id.rawValue] = status.1.stat
                NotificationCenter.default.post(name: NSNotification.Name(rawValue: notificationNetwork), object: dict)
                success(true)
            }else {
                success(false)
            }
        }) { [weak self] (error) in
            DispatchQueue.main.async {
                self?.hideLoader()
                self?.showAlert( message: error.message ?? AppMessages.ErrorAlerts.unknownError)
            }
            DILog.print(items: error.message ?? "")
            success(false)
        }
    }
    
    fileprivate func requestToUnfollow(modal : FollowRequest , success: @escaping (_ finish: Bool)-> ()){
        
        //Show pop up
        self.showAlert(withTitle: appName.capitalized, message: AppMessages.AlertTitles.unfollow, okayTitle: AppMessages.AlertTitles.Yes, cancelTitle: AppMessages.AlertTitles.No, okCall: {
            
            //call api now
            if !Utility.isConnectedToInternet {
                self.hideLoader()
                
                self.showAlert( message: AppMessages.ErrorAlerts.noInternet)
                return
            }
            self.showLoader()
            DIWebLayerUserAPI().unfollowUser(parameters: modal.getDictonary(), success: { (status) in
//                DispatchQueue.main.async {
////                    self?.hideLoader()
//                }
                if status  == 1 {
                    //Refresh friends list on network view controller
                    let dict = [RefreshNetworkKey.id.rawValue:modal.friendId,RefreshNetworkKey.type.rawValue:RefreshNetwork.unfollow.rawValue] as [String : Any]
                    NotificationCenter.default.post(name: NSNotification.Name(rawValue: notificationNetwork), object: dict)
                    success(true)
                }else {
                    success(false)
                }
            }) { [weak self] (error) in
                DispatchQueue.main.async {
                    self?.hideLoader()
                    self?.showAlert( message: error.message ?? AppMessages.ErrorAlerts.unknownError)
                }
                DILog.print(items: error.message ?? "")
                success(false)
            }
        }) {
            //Nothing to do
        }
    }
    
    
    // MARK: - Navigation
    
    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
        
        if segue.identifier == SegueIdentifiers.NetworkScreen.UserProfile.showUserNetwork{
            
            let controller = segue.destination as! NetworkViewController
            controller.anotherUser = userId
            controller.indexToShow = sender as? Int ?? 0
            controller.followCount = [currentUser?.followers ?? 0 , currentUser?.followings ?? 0] // counts to show
        }
    }
    
    
}
// MARK: -
extension UserProfileViewController : UITableViewDataSource,UITableViewDelegate{
    // MARK: -
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
//        constraintHeight_TableView.constant = CGFloat(160*arrReviews.count)
        tableView.backgroundView = nil // reset to default state
        
        if arrReviews.count > 0 {
            tableView.setEmptyMessage("")
        }else {
            tableView.setEmptyMessage("No Records yet!")
        }
        
        return arrReviews.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: CellIdentifiers.NetworksScreen.UserProfile.latestRecomendationCell) as! RecomendationCell
        let modal = arrReviews[indexPath.row]
        let strCategory = Utility.getBackendTypeString(using: modal.type ?? 0)
        cell.configureCellLatestRecom(modal, with: indexPath, with: strCategory)
        cell.btnShare.addTarget(self, action: #selector(share(_ :)), for: .touchUpInside)
        cell.btnBookmark.addTarget(self, action: #selector(bookmark(_ :)), for: .touchUpInside)
        cell.btnAddRating.addTarget(self, action: #selector(rate(_ :)), for: .touchUpInside)
        cell.commentButton.addTarget(self, action: #selector(rate(_ :)), for: .touchUpInside)
        //************ check if user view in tutorial restrict the actions
        if fromTutorial == true {
            cell.isUserInteractionEnabled = false
        }
        return cell
    }
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let modal = arrReviews[indexPath.row]
        
        let controller : UIViewController?
        if modal.type ?? 0 == Constant.CategoryTypeBackend.movie.rawValue ||  modal.type ?? 0 == Constant.CategoryTypeBackend.tv.rawValue || modal.type ?? 0 == Constant.CategoryTypeBackend.books.rawValue{
            let controllerLocal : MovieDetailViewController = UIStoryboard.init(storyboard: .inspiration).initVC()
            controllerLocal.category =  modal.type ?? 0
            controllerLocal.postId = modal.post?.id ?? ""
            controller = controllerLocal
        }else {
            if   User.sharedInstance.isLocationEnabled  == false{
                self.showAlert(message:AppMessages.AlertTitles.locationOff)
                return
            }
            let  controllerLocal : DetailPageViewController = UIStoryboard.init(storyboard: .inspiration).initVC()
            controllerLocal.category =  modal.type ?? 0
            controllerLocal.postId = modal.post?.id ?? ""
            
            controller = controllerLocal
        }
        if controller != nil{
            self.navigationController?.pushViewController(controller!, animated: true)
        }
    }
    
    
    //MARK:-  Custom Methods
    @objc func share(_ sender : UIButton) {
        let modal = arrReviews[sender.tag]
            self.shareData(id: modal.post?.id, type: modal.post?.type ?? 1, notificationType: 1, title: modal.post?.title ?? "")
    }
    
    @objc func bookmark(_ sender : UIButton){
        /********Not allowed to take action on another user profile*********/
        if self.userId  != User.sharedInstance.id{
            return
        }
        
        if self.userId == User.sharedInstance.id{
        self.showAlert( message:"Bookmark not allowed on rated Recommendations")
        }
        else{
            return
            
            /*var modal = arrReviews[sender.tag]

            sender.isSelected = !sender.isSelected
            var ratingModal = RatingRequest()
            ratingModal.referenceid = modal.post?.id ?? ""
            ratingModal.type = modal.type ?? 0
            
            if sender.isSelected == true{ // addBookmark
                showLoader()
                self.addBookmark(modal: ratingModal) { (finish) in
                    self.hideLoader()
                    if finish == false{
                        sender.isSelected = !sender.isSelected  // failure, change back to previous state
                    }else {
                        modal.is_bookmarked = true
                        self.arrReviews.remove(at: sender.tag)
                        self.arrReviews.insert(modal, at: sender.tag)

                    }
                }
            }else{ //remove bookmark
                
                self.showAlert(withTitle: appName.capitalized, message: AppMessages.AlertTitles.removeBookMark, okayTitle: AppMessages.AlertTitles.Yes, cancelTitle: AppMessages.AlertTitles.No, okCall: {
                    self.showLoader()
                    self.removeBookmark(modal: ratingModal) { (finish) in
                        self.hideLoader()
                        if finish == false{
                            sender.isSelected = !sender.isSelected //failure, change back to previous state
                        }else {
                            modal.is_bookmarked = true
                            self.arrReviews.remove(at: sender.tag)
                            self.arrReviews.insert(modal, at: sender.tag)                        }
                    }
                }) {
                    //Nothing to do
                    sender.isSelected = !sender.isSelected
                }
            }*/
        }
    }
    
    @objc func rate(_ sender : UIButton){
        /********Not allowed to take action on another user profile*********/
        if self.userId  != User.sharedInstance.id{
            return
        }
        let modal = arrReviews[sender.tag]
        self.addRating(using: modal.type ?? 0, referenceID: modal.post?.id ?? "")
    }

}

extension UserProfileViewController : UIScrollViewDelegate{
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        //change top bar visiblity
        if scrollView == self.scrollView{
        viewTop.alpha = scrollView.contentOffset.y * 0.01
        }
    }
    
    func scrollViewDidEndDragging(_ scrollView: UIScrollView, willDecelerate decelerate: Bool) {
        
        if ((scrollView.contentOffset.y + scrollView.frame.size.height) >= scrollView.contentSize.height) {
            
            if scrollView.tag == SCROLL_TAG{
//                && tableView_Recomendation.isScrollEnabled == false{
//                tableView_Recomendation.isScrollEnabled = true
                return
            }
            
            if arrReviews.count < reviewLimit && self.paginationHitInProcess == false{
            self.paginationHitInProcess = true
            tableView_Recomendation.tableFooterView = Utility.getPagingSpinner()
            var modal = SearchRecomendationRequest()
            modal.type = 0   // 0 refer to all
            modal.page = Utility.getCurrentOffset(count: arrReviews.count)+1
            modal.userID = userId ?? ""
            modal.latLng = Utility.userCurrentLocationString() ?? ""

            getReview(modal: modal)
            }
        }else if scrollView == self.scrollView{
//            tableView_Recomendation.isScrollEnabled = false
        }
    }
}


extension UserProfileViewController : AddRatingDelegate{
    func refreshViewDetail(rated: Bool) {
        if rated == true{
           self.showLoader()
            var modal = SearchRecomendationRequest()
            modal.type = 0   // 0 refer to all
            modal.page = Utility.getCurrentOffset(count: arrReviews.count)
            modal.userID = userId ?? ""
            modal.latLng = Utility.userCurrentLocationString() ?? ""
            DispatchQueue.global(qos: .background).async {
                self.getReview(modal: modal)
                self.getProfile()
            }
        }
    }
}
