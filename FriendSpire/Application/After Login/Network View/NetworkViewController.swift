//
//  NetworkViewController.swift
//  FriendSpire
//
//  Created by Mac Test on 19/07/18.
//  Copyright © 2018 openkey. All rights reserved.
//

import UIKit
import FBSDKCoreKit
import IQKeyboardManagerSwift

enum RefreshNetwork: Int{
    case follow,unfollow,block,unblock,requested
}

enum RefreshNetworkKey:String{
    case id = "id"
    case type = "type"
}

class NetworkViewController: DIBaseController {
    
    final let CANCEL_BUTTON_WIDTH = CGFloat(45.0)
    final let BACK_BUTTON_WIDTH =  CGFloat(35.0)
    
    //Variable from another view controller
    var fromTutorial : Bool?
    var fromNotification : Bool?
    var anotherUser : String?
    var indexToShow : Int? // to decide show follower or following  of another user
    var followCount = [0,0]  // 0 index value for follower and 1 index value for following
    var isComingFromLink:Bool = false
    
    //IBOutlets
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var btnBack: UIButton!
    @IBOutlet weak var constraintWidthBtnBack: NSLayoutConstraint!
    @IBOutlet weak var txtSearch: UITextField!
    @IBOutlet weak var tableViewSearch: UITableView!
    @IBOutlet weak var btnCancelSearch: UIButton!
    @IBOutlet weak var constraintWidthbtnCancel: NSLayoutConstraint!
    @IBOutlet weak var viewMovingBelowUserType: UIView!
    @IBOutlet weak var constraintLeadingViewMovingBelowUserType: NSLayoutConstraint!
    @IBOutlet weak var stackView: UIStackView!
    @IBOutlet weak var btnInviteFriends: GradientButton!
    @IBOutlet weak var constraintHeightBtnInviteFriends: NSLayoutConstraint!
    
    
    //Follower specfic Outlets
    @IBOutlet weak var viewFollower: UIView!
    @IBOutlet weak var lblFollowerCount: UILabel!
    @IBOutlet weak var btnFollower: UIButton!
    
    //Following specfic Outlets
    @IBOutlet weak var lblFollowingCount: UILabel!
    @IBOutlet weak var btnFollowing: UIButton!
    
    //Facebook specfic Outlets
    @IBOutlet weak var viewFacebook: UIView!
    @IBOutlet weak var lblFbCount: UILabel!
    @IBOutlet weak var btnFb: UIButton!
    @IBOutlet weak var constraintWidthViewFacebook: NSLayoutConstraint!
    @IBOutlet weak var btnConnectFb: UIButton!
    @IBOutlet weak var lbl_facebook: UILabel!
    @IBOutlet weak var lblNotConnectedFB: UILabel!
    
    @IBOutlet weak var viewTutorialAction: UIView!
    //Local variables
    fileprivate  var arrFollower = [FollowerModal]()
    fileprivate  var arrPendingRequest = [FollowerModal]()
    fileprivate  var arrFollowing = [FollowerModal]()
    fileprivate  var arrFb = [FbModal]()
    fileprivate  var arrSearch = [SearchFriendModal]()
    fileprivate var isSearchingOn = false
    fileprivate  var refreshControl = UIRefreshControl()
    fileprivate var followLimitReached = false
    fileprivate var followingLimitReached = false
    fileprivate var pendingCount = 0
//    fileprivate var userEditing : String?
    fileprivate var refreshInProgress = false
    fileprivate var fetchNewUser  = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        //        viewTutorialAction.isHidden = false

          NotificationCenter.default.addObserver(self, selector: #selector(manageFriendList(notification:)), name: NSNotification.Name(rawValue: notificationNetwork), object: nil)
        
        refreshControl.attributedTitle = NSAttributedString(string: "")
        refreshControl.addTarget(self, action: #selector(refresh), for: UIControlEvents.valueChanged)
        tableView.addSubview(refreshControl)
        
        let viewFooter = UIView()
        viewFooter.backgroundColor = .clear
        tableViewSearch.tableFooterView = viewFooter
        btnFollower.isSelected = true
        lbl_facebook.text = "Suggested \n Friends"
        tableViewSearch.keyboardDismissMode = .onDrag
        addPagination() // add pagination
        initalApiFetch()
        if fromNotification == true {
            self.constraintWidthBtnBack.constant = self.BACK_BUTTON_WIDTH
        }
        
        //show Tutorial screen to user
        //        if TutorialStatus.getCurrentStatus() == nil ||  TutorialStatus.getCurrentStatus()?.networkScreen == nil{
        //            self.perform(#selector(showTutorialPopup(arr:)), with: [#imageLiteral(resourceName: "layer1")], afterDelay: 0.3)
        //            updateTutorialReadStatus(suggestion: false,begin: false)
        //        }
        
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
       
        // pagination size changes
        RECORDS_PER_PAGE = Constant.PageLimit.localApi
        
        IQKeyboardManager.shared.toolbarDoneBarButtonItemText = "Search"
        
        if isSearchingOn == true {
            txtSearch.becomeFirstResponder()
            constraintWidthbtnCancel.constant = CANCEL_BUTTON_WIDTH
        }
        
        DispatchQueue.global(qos: .background).async { [weak self] in
           
            if self?.fetchNewUser == true{
                DispatchQueue.main.async { [weak self] in
                    self?.refresh()
                }
                self?.fetchNewUser = false
            }else{
            
            //This is for another user
            if self?.anotherUser != nil && (self?.anotherUser ?? "") != User.sharedInstance.id{
               self?.refreshPageAnother()
                
            }else { //load user's own profile
                 self?.refreshPage(follower: nil)
            }
            }
            DispatchQueue.main.async { [weak self] in
                self?.tableView.reloadData()
            }
        }
    }
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        IQKeyboardManager.shared.toolbarDoneBarButtonItemText = nil
        
    }
    deinit {
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: notificationNetwork), object: nil)
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
  
    func initalApiFetch(){
        self.showLoader()
        DispatchQueue.global(qos: .background).async {
            if self.anotherUser != nil {
                
                DispatchQueue.main.async {
                    self.viewFacebook.isHidden = true
                    //                    self.constraintWidthViewFacebook.constant = 0
                    self.constraintWidthBtnBack.constant = self.BACK_BUTTON_WIDTH
                    self.btnInviteFriends.isHidden = true
                    self.constraintHeightBtnInviteFriends.constant = 0
                    self.lblFollowerCount.text = "\(self.followCount[0])" // get follower count
                    self.lblFollowingCount.text = "\(self.followCount[1] )" // get following count
                    
                }
                self.getProfileCount()
                self.getOthersFollower(limit: 1)
                //                self.getOthersFollowing(limit: 1)
            }
            else {
                
                self.getNetworkDetail()
                self.getFollowers(limit:1)
                //                self.getFollowing (limit: 1)
                self.fetchFromFacebookList()
            }
        }
    }
    // MARK: - IBAction
    
    @IBAction func continueTutorialTapped(_ sender: UIButton) {
        viewTutorialAction.isHidden = true
        showTutorialPopup()
    }
    
    @IBAction func skipTutorialTapped(_ sender: UIButton) {
        viewTutorialAction.isHidden = true
        self.tutorialCompleted()
        AppDelegate.shared().navigateToDashboard()
    }
    
    @IBAction func backButtonTapped(_ sender: UIButton) {
        if isComingFromLink {
            appDelegate.navigateToDashboard()
        }else{
            self.navigationController?.popViewController(animated: true)
        }
    }
    
    @IBAction func cancelSearchButtonTapped(_ sender: UIButton) {
        
        isSearchingOn = false
        arrSearch.removeAll()
        tableViewSearch.reloadData()
        txtSearch.text = ""
        txtSearch.resignFirstResponder()
        tableViewSearch.isHidden = true
        self.view.sendSubview(toBack: tableViewSearch)
        showCancelButton(false)
    }
    
    
    @IBAction func inviteButtonTapped(_ sender: UIButton) {
        //************ check if user view in tutorial restrict the actions
        //        if fromTutorial == true {return}
        self.shareData(id: nil, type: nil, notificationType: 3, title: "")
    }
    
    @IBAction func userTypeListTapped(_ sender: UIButton) {
        
        //Set Default State
        btnFb.isSelected = false
        btnFollowing.isSelected = false
        btnFollower.isSelected = false
        moveSelector(to:sender)
        
        self.btnConnectFb.isHidden = true
        self.lblNotConnectedFB.isHidden = true
        if btnFb.isSelected == true{
            //            check  if fb accesstoken is available
            if (FBSDKAccessToken.current() == nil) {
                self.btnConnectFb.isHidden = false
                self.lblNotConnectedFB.isHidden = false
                self.btnConnectFb.isHidden = self.arrFb.count > 0 ? true : false
                self.lblNotConnectedFB.isHidden = self.arrFb.count > 0 ? true : false
                
                if self.arrFb.count > 0{
                    self.showAlert(withTitle: appName.capitalized, message: "Please login with facebook again to get latest suggestion", okayTitle: AppMessages.AlertTitles.Ok, cancelTitle: AppMessages.AlertTitles.Cancel, okCall: {
                        self.fbLogin()
                    }) {
                        
                    }
                }
                
            }else{
                self.btnConnectFb.isHidden = true
                self.lblNotConnectedFB.isHidden = true
            }
        }
        
        tableView.reloadData()
    }
    
    @IBAction func connectFbTapped(_ sender: UIButton) {
        self.fbLogin()
    }
    
    
    /**
     This index selection managed for another user profile
     */
    fileprivate func indexSelection(){
        if self.indexToShow != nil {
            
            switch self.indexToShow{
            case 0:
                self.userTypeListTapped(self.btnFollower)
                break
            case 1:
                self.userTypeListTapped(self.btnFollowing)
                break
            case 2:
                self.userTypeListTapped(self.btnFb)
                break
            default:
                self.userTypeListTapped(self.btnFollower)
                break
            }
            indexToShow = nil
        }
    }
    
    
    /**
     Move view below selection according to user selection
     */
    fileprivate  func moveSelector(to btn : UIButton){
        
        if btn == btnFollower{
            if (anotherUser == nil || (anotherUser ?? "") == User.sharedInstance.id  ){
                getNetworkDetail()
                getFollowers(limit: 1)
            }else if (anotherUser ?? "") != User.sharedInstance.id  {
                getProfileCount()
                getOthersFollower(limit: 1)
            }
            
            tableView.enableInfiniteScrolling(status: !followLimitReached)
        }else if btn == btnFollowing{
            if (anotherUser == nil || (anotherUser ?? "") == User.sharedInstance.id  ){
                getNetworkDetail()
                getFollowing(limit: 1)
            }else if (anotherUser ?? "") != User.sharedInstance.id {
                getProfileCount()
                getOthersFollowing(limit: 1)
            }
            tableView.enableInfiniteScrolling(status: !followingLimitReached)
        }else{
            if (anotherUser == nil || (anotherUser ?? "") == User.sharedInstance.id  ){
                getFbFriends()
            }
            tableView.enableInfiniteScrolling(status:false)
        }
//        userEditing = nil
        btn.isSelected = true
        let width = btn.frame.size.width
        let offset = CGFloat(btn.tag)
        let xAxis = offset*width+offset*stackView.spacing
        self.constraintLeadingViewMovingBelowUserType.constant = xAxis
    }
    
    fileprivate func showCancelButton(_ status:Bool) {
        btnCancelSearch.isHidden = !status
        constraintWidthbtnCancel.constant =  status == true ? CANCEL_BUTTON_WIDTH : 0
    }
    
    /**
     This method use for only pull to refresh
     */
    @objc func refresh() {
        // Code to refresh table view
        followLimitReached = false
        followingLimitReached = false
        RECORDS_PER_PAGE = Constant.PageLimit.localApi
        refreshInProgress = true
        
        var index = 1
        
        if self.btnFollower.isSelected == true {
            index = 1 // load follower only
            tableView.enableInfiniteScrolling(status: !followLimitReached)
        }else if self.btnFollowing.isSelected == true{
            index = 2 // load following only
            tableView.enableInfiniteScrolling(status: !followingLimitReached)
        }
        let loadFb = self.btnFb.isSelected // to avoid background access warning
        DispatchQueue.global(qos: .background).async {
            
            if self.anotherUser != nil {
                self.getProfileCount()
                if index == 1{
                self.getOthersFollower(limit: 1)
                }else if index == 2{
                self.getOthersFollowing(limit: 1)
                }
            }else {
                if loadFb == true{
                    self.getFbFriends()
                }else {
                    self.getNetworkDetail()
                    if index == 1{
                    self.getFollowers(limit:1)
                      }else if index == 2{
                    self.getFollowing (limit: 1)
                    }
                }
            }
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        if segue.identifier ==  SegueIdentifiers.NetworkScreen.otherUser{
            let controller =  segue.destination as! UserProfileViewController
            
            // send reference modal accordingly
            if let user = sender as? FbModal{
                controller.userId = user.userId?.id
                controller.modalFbRef = user
            }else if let user = sender as? FollowerModal{
                controller.userId = user.id
                controller.modalFolloweRef = user
            }else{// for searching table
            controller.userId = sender as? String ?? ""
            }
            controller.fromTutorial = fromTutorial
        }
    }
    
    /// Update that user read the tutorial
    ///
    /// - Parameter suggestion: This will indicate read status for suggestion tutorial complete flow
    ///   - begin: indicate user just started the suggestion not completed the flow yet
    fileprivate func updateTutorialReadStatus(suggestion:Bool,begin:Bool){
        
        var status = TutorialStatus.sharedInstance
        if suggestion == true{ //suggestion complete flow done
            status.friendSuggestion = true
        }else if begin == true{ // indicate begining of tutorial for suggestion
            status.friendSuggestionBegin = true
        }else{
            status.networkScreen = true
        }
        if let readStatus = TutorialStatus.getCurrentStatus(){
            if suggestion == true{//suggestion complete flow done
                readStatus.friendSuggestion = true
            }else if begin == true{ // indicate begining of tutorial for suggestion
                readStatus.friendSuggestionBegin = true
            }else{
                readStatus.networkScreen = true
            }
            status = readStatus
        }
        TutorialStatus.saveCurrentStatus(status: status)
    }
    
}

//MARK:-
extension NetworkViewController:UITableViewDataSource,UITableViewDelegate{
    //MARK:- Table view delegate
    
    func numberOfSections(in tableView: UITableView) -> Int {
        if tableView == tableViewSearch{
            return 1
        }
        
        if btnFollower.isSelected {
            return 2
        }
        
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        tableView.backgroundView = nil // reset to default state
        tableView.setEmptyMessage("")
        /*  handling for search table  */
        if tableView == tableViewSearch {
            if (arrSearch.count == 0 && (txtSearch.text?.trim.count ?? 0 > 1) ){
                tableViewSearch.showEmptyScreen("No Result", errorIcon: #imageLiteral(resourceName: "no-result"),with: #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1))
            }
            return arrSearch.count
        }
        /*  follower listing table Handling */
        
        var totalCount = 0
        if btnFollower.isSelected {
            if section == 0 {
                totalCount = arrPendingRequest.count
            }else{
                totalCount = arrFollower.count
                
            }
            if (arrPendingRequest.count == 0 && arrFollower.count == 0){
                tableView.setEmptyMessage(AppMessages.TableEmpty.follower)
            }
            return totalCount
        }
        else if btnFollowing.isSelected {
            totalCount = arrFollowing.count
            if totalCount == 0 {tableView.setEmptyMessage(AppMessages.TableEmpty.following)}
            
        }
        else if btnFb.isSelected{
            
            totalCount = arrFb.count
            
            //user doesn't have suggestion and not connected with fb
            if totalCount == 0 && btnConnectFb.isHidden == false{
                
                //show Tutorial screen to user
                if ((TutorialStatus.getCurrentStatus() == nil ||  TutorialStatus.getCurrentStatus()?.friendSuggestionBegin == nil) && User.sharedInstance.tutorialRead == 0) &&  fromTutorial == true {
                    viewTutorialAction.isHidden = false
                    //                    self.perform(#selector(showTutorialPopup(arr:)), with: [#imageLiteral(resourceName: "layer2")], afterDelay: 0.3)
//                    self.showTutorialToast(message: AppMessages.TutorialMessage.InteractiveOne)
                    updateTutorialReadStatus(suggestion: false,begin: true)
                }
                
                //user doesn't have suggestion and connected to fb as well
            }else if totalCount == 0 && btnConnectFb.isHidden == true{
                tableView.setEmptyMessage(AppMessages.TableEmpty.emptyfb)
                //                show Tutorial screen to user  to search using search bar
                if ((TutorialStatus.getCurrentStatus() == nil ||  TutorialStatus.getCurrentStatus()?.friendSuggestion == nil) && User.sharedInstance.tutorialRead == 0 ) &&  fromTutorial == true{
                    
                    //fb user
                    if  User.sharedInstance.snsID != nil{
                        viewTutorialAction.isHidden = false
                        //                        self.perform(#selector(showTutorialPopup(arr:)), with: [#imageLiteral(resourceName: "layer5")], afterDelay: 0.3)
                    }else {//non fb user
                        viewTutorialAction.isHidden = false
                        //                        self.perform(#selector(showTutorialPopup(arr:)), with: [#imageLiteral(resourceName: "layer3")], afterDelay: 0.3)
                    }
                    updateTutorialReadStatus(suggestion: true,begin: false)
                    //                    viewTutorialAction.isHidden = false
                }
                
            }else if totalCount > 0{
                
                //show Tutorial screen to user we have found suggestion for you *fb User only
                if ((TutorialStatus.getCurrentStatus()?.friendSuggestion == nil && User.sharedInstance.snsID != nil) && User.sharedInstance.tutorialRead == 0 ) &&  fromTutorial == true{
                    //                    self.perform(#selector(showTutorialPopup(arr:)), with: [#imageLiteral(resourceName: "layer4")], afterDelay: 0.3)
//                    self.showTutorialToast(message: AppMessages.TutorialMessage.InteractiveTwo)
                    
                    viewTutorialAction.isHidden = false
                    updateTutorialReadStatus(suggestion: true,begin: false)
                    //                    viewTutorialAction.isHidden = false
                }else if (TutorialStatus.getCurrentStatus()?.friendSuggestion == nil && User.sharedInstance.tutorialRead == 0) &&  fromTutorial == true{
                    finishWithTutorial(sender: false)
                    updateTutorialReadStatus(suggestion: true,begin: false)
                }
            }
        }
        
        return totalCount
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        /*  handling for search table  */
        if tableView == tableViewSearch {
            let cell = tableView.dequeueReusableCell(withIdentifier: CellIdentifiers.NetworksScreen.search)
            let modal = arrSearch[indexPath.row]
            cell?.textLabel?.text =  "\(modal.firstName ?? "") \(modal.lastName ?? "")"
            cell?.imageView?.image = #imageLiteral(resourceName: "search")
            return cell!
        }
        
        /*  normal table shown */
        let cell = tableView.dequeueReusableCell(withIdentifier: "FollowerCell") as! FollowerCell
        cell.setDefaultstate() // set all element on default
        
        //Follower configuration
        if btnFollower.isSelected  {
            if indexPath.section == 0{
                if indexPath.row >= arrPendingRequest.count{
                    tableView.reloadData()
                    return cell
                }
                cell.configureCellFollower(user: arrPendingRequest[indexPath.row], index: indexPath)
            }else {
                if indexPath.row >= arrFollower.count{
                    tableView.reloadData()
                    return cell
                }
                cell.configureCellFollower(user: arrFollower[indexPath.row], index: indexPath)
            }
            
            cell.btn_Accept.addTarget(self, action: #selector(accept(_:)), for: .touchUpInside)
            cell.btn_Reject.addTarget(self, action: #selector(reject(_:)), for: .touchUpInside)
            cell.btn_Follow.addTarget(self, action: #selector(follow(_:)), for: .touchUpInside)
            cell.btnBlock.addTarget(self, action: #selector(block(_:)), for: .touchUpInside)
        }
            
            //Following configuration
        else if btnFollowing.isSelected {
            if indexPath.row >= arrFollowing.count{
                tableView.reloadData()
                return cell
            }
            cell.configureCellFollowing(user: arrFollowing[indexPath.row], index: indexPath)
            cell.btn_Follow.addTarget(self, action: #selector(follow(_:)), for: .touchUpInside)
            
        }
            
            //Facebook Users configuration
        else if btnFb.isSelected{
            cell.configureCellFB(user: arrFb[indexPath.row],using: indexPath)
            cell.btn_Follow.addTarget(self, action: #selector(follow(_:)), for: .touchUpInside)
        }
        
        return cell
    }
    
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        
        if tableView == self.tableView{
            let headerView = tableView.dequeueReusableCell(withIdentifier: CellIdentifiers.NetworksScreen.headerView) as! FollowerCell
            
            if section == 0{
                if btnFollower.isSelected == true{// set header value for  pending section
                    headerView.lbl_HeaderTitle.text = Titles.NetworkScreenTitles.HeaderTitle.pendingRequest
                }else if anotherUser != nil{// set header value for Following section another user
                    headerView.lbl_HeaderTitle.text = Titles.NetworkScreenTitles.HeaderTitle.followingAnother
                }else {// set header value for Following section
                    headerView.lbl_HeaderTitle.text = Titles.NetworkScreenTitles.HeaderTitle.following
                }
            }else if btnFollower.isSelected == true{// set header value for  Follower section
                if anotherUser != nil{// set header value for Follower section another user
                    headerView.lbl_HeaderTitle.text = Titles.NetworkScreenTitles.HeaderTitle.followerAnother
                }else {
                    headerView.lbl_HeaderTitle.text = Titles.NetworkScreenTitles.HeaderTitle.follower
                }
            }
            return headerView
        }
        return nil
    }
    
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        if tableView == self.tableView{
            
            if (arrPendingRequest.count > 0 && arrFollower.count > 0 || arrPendingRequest.count > 0 && section == 0 || arrFollower.count > 0 && section == 1) && (btnFollower.isSelected){
                return 30.0
            }
            else if btnFollowing.isSelected && arrFollowing.count > 0{return 30.0}
            return 0.0
        }
        
        return 0.0
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 0.0
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if tableView == tableViewSearch {
            self.performSegue(withIdentifier: SegueIdentifiers.NetworkScreen.otherUser, sender: arrSearch[indexPath.row].id)
            return
        }
        
        if btnFb.isSelected {
            self.performSegue(withIdentifier: SegueIdentifiers.NetworkScreen.otherUser, sender: arrFb[indexPath.row])
            
        }
        else if btnFollower.isSelected{
            if indexPath.section == 0 {
                self.performSegue(withIdentifier: SegueIdentifiers.NetworkScreen.otherUser, sender: arrPendingRequest[indexPath.row])
            }
            else {
                self.performSegue(withIdentifier: SegueIdentifiers.NetworkScreen.otherUser, sender: arrFollower[indexPath.row])
            }
        }
        else if btnFollowing.isSelected {
            self.performSegue(withIdentifier: SegueIdentifiers.NetworkScreen.otherUser, sender: arrFollowing[indexPath.row])
        }
        
    }
    
    // MARK:- Custom Methods
    
    /// this will refresh the whole view accoding to user action
    ///
    /// - Parameter notification:  contain an object of id - userId and type - user action taken
    func manageFriendList(notification:Notification){
     
        // check if list for another user
        if anotherUser != nil && (anotherUser ?? "") != User.sharedInstance.id{
            refreshPageAnother()
        }else{// own profile
            refreshPage(follower: nil)
        }
        
        guard let dict = notification.object as? [String:Any] else{
            return
        }
        guard let idToUse = dict[RefreshNetworkKey.id.rawValue] as? String else{return}
        
        //let's go with user action
        if let type = dict[RefreshNetworkKey.type.rawValue] as? Int{
            switch type{
            case RefreshNetwork.block.rawValue:
                // remove blocked user from all lists
                if let index = self.arrFollower.index(where: {$0.id == idToUse}){
                    self.arrFollower.remove(at: index)
                }
                if let index = self.arrFollowing.index(where: {$0.id == idToUse}){
                    self.arrFollowing.remove(at: index)
                }
                if let index = self.arrFb.index(where: {$0.userId?.id == idToUse}){
                    self.arrFb.remove(at: index)
                }
                break
                
            case RefreshNetwork.unblock.rawValue:
                  //fetch users lists from server
                refresh()
                break
                
            case RefreshNetwork.follow.rawValue:
                
                  // remove user from all lists if found
                if let index = self.arrFollower.index(where: {$0.id == idToUse}){
                    self.arrFollower[index].status = FollowStatus.Following.rawValue
                }
                if let index = self.arrFollowing.index(where: {$0.id == idToUse}){
                    self.arrFollowing[index].status = FollowStatus.Following.rawValue
                }else{ // user not found in list fetch new entry from server
//                    refresh()
                    self.fetchNewUser = true
                }
                break
                
            case RefreshNetwork.unfollow.rawValue:
                  // change user follow status for all lists
                if let index = self.arrFollower.index(where: {$0.id == idToUse}){
                    self.arrFollower[index].status = FollowStatus.NotConnected.rawValue
                }
                if let index = self.arrFollowing.index(where: {$0.id == idToUse}){
                    //if we are on some other user's list change follow status
                     if anotherUser != nil && (anotherUser ?? "") != User.sharedInstance.id{
                     self.arrFollowing[index].status = FollowStatus.NotConnected.rawValue
                     }else{
                        //remove user from list on own profile
                         self.arrFollowing.remove(at: index)
                    }
                }
                break
                
            case RefreshNetwork.requested.rawValue:
                // change user follow status for all lists
                if let index = self.arrFollower.index(where: {$0.id == idToUse}){
                    self.arrFollower[index].status = FollowStatus.Requested.rawValue
                }
                if let index = self.arrFollowing.index(where: {$0.id == idToUse}){
                   self.arrFollowing[index].status = FollowStatus.Requested.rawValue
                }else{ //not found in list fetch new entry from server
//                    refresh()
                    self.fetchNewUser = true
                }
                break
                
            default:
                refresh() // refresh whole view
                break
            }
        }
        
    }
    
    fileprivate func hideAllLoader(){
        DispatchQueue.main.async {
            self.tableView.endRefresh()
            self.hideLoader()
            self.refreshControl.endRefreshing()
            self.tableView.reloadData()
        }
    }
    /**
     each time table view reaches to its limit callback will allow you to manage api Hits
     */
    func addPagination(){
        self.tableView.infiniteScrolling(self) {[weak self] in
            
            //check if true : - not allow to load more as inital refresh in progress
           if self?.refreshInProgress == true{return}

            //Hit api to get follower list
            if self?.btnFollower.isSelected == true {
                //calculate next offset
                
                // if required pagination for pending request
                //                if self?.arrPendingRequest.count ?? 0 < self?.pendingCount ?? 0{
                //                    let offset = self?.getCurrentOffset(count: self?.arrPendingRequest.count ?? 0) ?? 0
                //                    let nextOffset = offset+1
                //                    self?.getPending(limit: nextOffset)
                //                }else{
                let offset = self?.getCurrentOffset(count: self?.arrFollower.count ?? 0) ?? 0
                let nextOffset = offset+1
                if self?.anotherUser != nil {// User view another user's profile
                    self?.getOthersFollower(limit: nextOffset)
                }else{
                    self?.getFollowers(limit: nextOffset) //  own profile
                }
                //                }
            }
                //Hit api to get following list
                
            else if self?.btnFollowing.isSelected == true{
                //calculate next offset
                let offset = self?.getCurrentOffset(count: self?.arrFollowing.count ?? 0) ?? 0
                let nextOffset = offset+1
                if self?.anotherUser != nil {// User view another user's profile
                    self?.getOthersFollowing(limit: nextOffset)
                }else{
                    self?.getFollowing(limit: nextOffset)//  own profile
                }
            }else{
                self?.tableView.endPull2RefreshAndInfiniteScrolling(count: 0)
                self?.hideAllLoader()
            }
        }
    }
    
    /**
     this will return the offset as per calcultaion of page size and array count
     */
    fileprivate func getCurrentOffset(count:Int ) -> Int{
        //check if offset is less than 0 return 1
        return count/RECORDS_PER_PAGE > 0 ? count/RECORDS_PER_PAGE : 1
    }
    /// Accept User request
    ///
    /// - Parameter sender: uibutton (Tag will use to find user from array)
    @objc func accept(_ sender : UIButton){
        
        guard let userId = arrPendingRequest[sender.tag].id else {
            return
        }
        var modal = FollowRequest()
        modal.friendId = userId
        showLoader()
        self.acceptRequest(modal: modal) { [weak self] (finish) in
            if finish{
                self?.showLoader()
                self?.getNetworkDetail()
               self?.getFollowers(limit:1)
            }
        }
    }
    /// Reject User request
    ///
    /// - Parameter sender: uibutton (Tag will use to find user from array)
    @objc func reject(_ sender : UIButton){
        
        guard let userId = arrPendingRequest[sender.tag].id else {
            return
        }
        var modal = FollowRequest()
        modal.friendId = userId
        showLoader()
        self.rejectRequest(modal: modal) { [weak self](finish) in
            if finish{
                self?.showLoader()
                self?.getNetworkDetail()
                self?.getFollowers(limit:1)
            }
        }
    }
    /// Block User
    ///
    /// - Parameter sender: uibutton (Tag will use to find user from array)
    @objc func block(_ sender : UIButton){
        
        //FOr Follower
        if sender.tag - ArrayRefernce.follower.rawValue >= 0 ? true : false{ //calculation as per tag ArrayRefernce index value
            let user = arrFollower[sender.tag - ArrayRefernce.follower.rawValue]
//            userEditing = user.id
            var modal = FollowRequest()
            modal.friendId = user.id ?? ""
            modal.userName = "\(user.firstName ?? "") \(user.lastName ?? "")"
            self.blockUser(modal: modal) { [weak self] (finish) in
                if finish{
                    self?.showLoader()
                    let dict = [RefreshNetworkKey.id.rawValue:modal.friendId,RefreshNetworkKey.type.rawValue:RefreshNetwork.block.rawValue] as [String : Any]
                      self?.manageFriendList(notification: Notification.init(name: Notification.Name.init(notificationNetwork), object: dict, userInfo: nil))
                    
                    //Check if user is on another user's profile
                    if self?.anotherUser != nil {
                        //Refresh friends list on own network view controller instance
                        NotificationCenter.default.post(name: NSNotification.Name(rawValue: notificationNetwork), object: dict)
                    }
                }
                self?.tableView.reloadData()
            }
        }
        
    }
    
    /**
     send follow or unfollow request
     */
    @objc func follow(_ sender : UIButton ){
        
        //******************  check which array should use to get the user modal
        
        //FOr Follower
        if sender.tag - ArrayRefernce.follower.rawValue >= 0 ? true : false{ //calculation as per tag ArrayRefernce contain value
            let user = arrFollower[sender.tag - ArrayRefernce.follower.rawValue]
//            userEditing = user.id
            var modal = FollowRequest()
            modal.friendId = user.id ?? ""
            modal.userName = "\(user.firstName ?? "") \(user.lastName ?? "")"
            if sender.isSelected == true{  // already followed send unfollow request
                self.requestToUnfollow(modal: modal) { [weak self] (finish) in
                    if finish{
                        
                        let dict = [RefreshNetworkKey.id.rawValue:modal.friendId,RefreshNetworkKey.type.rawValue:RefreshNetwork.unfollow.rawValue] as [String : Any]
                        
                        self?.manageFriendList(notification: Notification.init(name: Notification.Name.init(notificationNetwork), object: dict, userInfo: nil))
                        self?.showLoader()
                        //Check if user is on another user's profile
                        if self?.anotherUser != nil {
                            NotificationCenter.default.post(name: NSNotification.Name(rawValue: notificationNetwork), object: dict)
                        }
                    }
                    self?.tableView.reloadData()
                }
            }else {//  send follow request
                showLoader()
                self.requestToFollow(modal: modal) { [weak self] (finish) in
                    if finish.0{
                        self?.showLoader()
                        //Check if user is on another user's profile
                        var dict = [RefreshNetworkKey.id.rawValue:modal.friendId,RefreshNetworkKey.type.rawValue:RefreshNetwork.follow.rawValue] as [String : Any]
                            if  finish.1?.status == FollowStatus.Requested.rawValue{
                                dict[RefreshNetworkKey.type.rawValue] = RefreshNetwork.requested.rawValue
                            }
                        
                        self?.manageFriendList(notification: Notification.init(name: Notification.Name.init(notificationNetwork), object: dict, userInfo: nil))
                        
                        if self?.anotherUser != nil {
                             NotificationCenter.default.post(name: NSNotification.Name(rawValue: notificationNetwork), object: dict)
                        }
                    }
                    self?.tableView.reloadData()
                    
                    DispatchQueue.main.asyncAfter(deadline: .now() + .milliseconds(500)) {
                        if  self?.tableView.numberOfRows(inSection: 0) ?? 0 > 0{
                             self?.tableView.scrollToRow(at: IndexPath.init(row: sender.tag, section: 0), at: .top, animated: false)
                        }
                    }
                }
            }
        }
            //FOr Suggested
            
        else if sender.tag - ArrayRefernce.facebook.rawValue >= 0 ? true : false{//calculation as per tag ArrayRefernce contain value
            let user = arrFb[sender.tag - ArrayRefernce.facebook.rawValue]
//            userEditing = user.userId?.id
            var modal = FollowRequest()
            modal.friendId = user.userId?.id ?? ""
            if sender.isSelected == true{ // already followed send unfollow request
                showLoader()
                self.requestToUnfollow(modal: modal) { [weak self] (finish) in
                    if finish{
                        self?.arrFb.remove(at: (sender.tag - ArrayRefernce.facebook.rawValue))
                        self?.showLoader()
                        self?.refreshPage(follower: nil)
                    }
                    self?.tableView.reloadData()
                }
            }else {//  send follow request
                showLoader()
                self.requestToFollow(modal: modal) { [weak self] (finish) in
                    if finish.0{
                        self?.showLoader()
                        var dict = [RefreshNetworkKey.id.rawValue:modal.friendId,RefreshNetworkKey.type.rawValue:RefreshNetwork.follow.rawValue] as [String : Any]
                        if  finish.1?.status == FollowStatus.Requested.rawValue{
                            dict[RefreshNetworkKey.type.rawValue] = RefreshNetwork.requested.rawValue
                        }
                        
                        self?.manageFriendList(notification: Notification.init(name: Notification.Name.init(notificationNetwork), object: dict, userInfo: nil))
                        
                        
                        if finish.1 != nil{
                            self?.arrFb[sender.tag - ArrayRefernce.facebook.rawValue].status = finish.1?.status
                        }
                        self?.refreshPage(follower: nil)
                    }
                    self?.tableView.reloadData()
                }
            }
        }
            //FOr Following
        else{
            let user = arrFollowing[sender.tag - ArrayRefernce.following.rawValue]
//            userEditing = user.id
            var modal = FollowRequest()
            modal.friendId = user.id ?? ""
            if sender.isSelected == true{  // already followed send unfollow request
                self.requestToUnfollow(modal: modal) { [weak self] (finish) in
                    if finish{
                        let dict = [RefreshNetworkKey.id.rawValue:modal.friendId,RefreshNetworkKey.type.rawValue:RefreshNetwork.unfollow.rawValue] as [String : Any]
                        
                        self?.manageFriendList(notification: Notification.init(name: Notification.Name.init(notificationNetwork), object: dict, userInfo: nil))
                        //Check if user is on another user's profile
                        if self?.anotherUser != nil {
                             NotificationCenter.default.post(name: NSNotification.Name(rawValue: notificationNetwork), object: dict)
                        }
                    }
                    self?.tableView.reloadData()
                }
            }else {//  send follow request
                showLoader()
                self.requestToFollow(modal: modal) { [weak self] (finish) in
                    if finish.0{
                        var dict = [RefreshNetworkKey.id.rawValue:modal.friendId,RefreshNetworkKey.type.rawValue:RefreshNetwork.follow.rawValue] as [String : Any]
                        if  finish.1?.status == FollowStatus.Requested.rawValue{
                            dict[RefreshNetworkKey.type.rawValue] = RefreshNetwork.requested.rawValue
                        }
                        
                        self?.manageFriendList(notification: Notification.init(name: Notification.Name.init(notificationNetwork), object: dict, userInfo: nil))
                        //Check if user is on another user's profile
                        if self?.anotherUser != nil {
                             NotificationCenter.default.post(name: NSNotification.Name(rawValue: notificationNetwork), object: dict)
                            self?.refreshPageAnother()
                        }
                    }
                    self?.tableView.reloadData()
                }
            }
        }
    }
    
    
    /// this method will use to refresh the page once follow button tapped
    ///
    /// - Parameter follower: identify to which section api need to hit
    fileprivate func refreshPage(follower:Bool?){
        DispatchQueue.global(qos: .background).async {[weak self] in
            self?.getNetworkDetail()
            self?.getFbFriends()
        }
    }
    
    /// this method will use to refresh the page once follow button tapped on another user profile
    ///
    fileprivate func refreshPageAnother(){
        DispatchQueue.global(qos: .background).async { [weak self] in
            self?.getProfileCount()
        }
    }
    func showTutorialPopup(){
        
        let  controller: EditProfileViewController = UIStoryboard.init(storyboard: .setting).initVC()
        controller.fromTutorial = true
        AppDelegate.shared().window?.rootViewController?.presentedViewController?.dismiss(animated: false, completion: nil)
        AppDelegate.shared().window?.rootViewController?.present(controller, animated: true, completion: nil)
    }
}

//MARK:-
extension NetworkViewController : UITextFieldDelegate{
    //MARK:- TextField Delegate
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        
        if txtSearch == textField{
            isSearchingOn = true
            tableViewSearch.isHidden = false
            self.view.bringSubview(toFront: tableViewSearch)
            showCancelButton(true)
        }
        return true
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        //**********  Check if textfield is empty retrict user to enter space
        if string.trim.length == 0 && textField.text?.length == 0{
            return false
        }
        
        if string == "" && textField.text?.length == 1 { // back space check
            self.arrSearch.removeAll()
            self.tableViewSearch.reloadData()
            return true
        }
        else {
            //            hit  api now after canceling all previous schedule task
            NSObject.cancelPreviousPerformRequests(withTarget: self)
            if anotherUser != nil {
                self.perform(#selector(self.searchForSelectedTab), with: nil, afterDelay: 0.5)
            }else {
                self.perform(#selector(self.searchUser), with: nil, afterDelay: 0.5)
            }
        }
        return true
    }
    
    
    func textFieldShouldClear(_ textField: UITextField) -> Bool {
        self.perform(#selector(self.reloadWithDelay), with: nil, afterDelay: 0.3)
        
        return true
    }
    
    func reloadWithDelay(){
        self.arrSearch.removeAll()
        self.tableViewSearch.reloadData()
    }
}

//MARK:-
extension NetworkViewController{
    
    
    //MARK:- API Calling
    fileprivate func fetchFromFacebookList(){
        
        if !Utility.isConnectedToInternet {
            self.hideAllLoader()
            self.showAlert( message: AppMessages.ErrorAlerts.noInternet)
            return
        }
        FacebookManager.shared.getFriendList(sucess: { [weak self](response) in
            
            let arrIDs = response.map({$0.id ?? ""})
            if arrIDs.count > 0{
                self?.syncFbData(arrIDs)
            }
            
        }) { (error) in
            DILog.print(items: error.debugDescription)
        }
    }
    
    fileprivate func syncFbData(_ arr : [String]){
        if !Utility.isConnectedToInternet {
            DispatchQueue.main.async {
                self.hideAllLoader()
                self.showAlert( message: AppMessages.ErrorAlerts.noInternet)
                return
            }
        }
        let parameters = ["friends":arr]
        DIWebLayerUserAPI().syncFB(parameters: parameters, success: { [weak self](response) in
            self?.getFbFriends()
        }) { (error) in}
    }
    
    fileprivate func getFbFriends(){
        if !Utility.isConnectedToInternet {
            DispatchQueue.main.async {
                self.hideAllLoader()
                self.showAlert( message: AppMessages.ErrorAlerts.noInternet)
                return
            }
        }
        DIWebLayerUserAPI().getFriends(parameters: nil, success: { [weak self](response) in
            
            self?.arrFb = response
            DispatchQueue.main.async {
                self?.hideAllLoader()
                self?.lblFbCount.text = "\(self?.arrFb.count ?? 0)"
                self?.indexSelection()
            }
            
        }) { [weak self] (error) in
            DispatchQueue.main.async {
                self?.hideAllLoader()
                self?.indexSelection()
                
            }
            DILog.print(items: error.message ?? "")
        }
    }
    
    fileprivate func getFollowing(limit :Int){
        if !Utility.isConnectedToInternet {
            DispatchQueue.main.async {
                self.hideAllLoader()
                self.showAlert( message: AppMessages.ErrorAlerts.noInternet)
                self.tableView.endPull2RefreshAndInfiniteScrolling(count: self.arrFollowing.count )
                return
            }
        }
        DIWebLayerUserAPI().getFollowing(upto: limit,success: { [weak self](response) in
            
            if limit == 1{
                self?.arrFollowing.removeAll()
                
                if self?.btnFollowing.isSelected == true{
                self?.refreshInProgress = false
                }
            }
            if response.count > 0{
                self?.arrFollowing.append(contentsOf: response)
                self?.followingLimitReached = false
                if response.count < RECORDS_PER_PAGE{
                    self?.followingLimitReached = true
                }
            }else {
                self?.followingLimitReached = true
            }
            DispatchQueue.main.async {
                self?.hideAllLoader()
                self?.tableView.endPull2RefreshAndInfiniteScrolling(count: response.count )
            }
            
            
        }) { [weak self] (error) in
            DispatchQueue.main.async {
                self?.hideAllLoader()
                self?.tableView.endPull2RefreshAndInfiniteScrolling(count: self?.arrFollowing.count ?? 0)
            }
            DILog.print(items: error.message ?? "")
        }
    }
    
    fileprivate func getFollowers(limit :Int){
        if !Utility.isConnectedToInternet {
            DispatchQueue.main.async {
                self.hideAllLoader()
                self.showAlert( message: AppMessages.ErrorAlerts.noInternet)
                self.tableView.endPull2RefreshAndInfiniteScrolling(count: self.arrFollower.count)
                return
            }
        }
        
        DIWebLayerUserAPI().getFollowers(upto: limit, success: { [weak self](response) in
            if limit == 1{
                self?.arrFollower.removeAll()
                if self?.btnFollower.isSelected == true{
                    self?.refreshInProgress = false
                }
            }
            if response.count > 0{
                self?.arrFollower.append(contentsOf: response)
                self?.followLimitReached = false
                if response.count < RECORDS_PER_PAGE{
                    self?.followLimitReached = true
                }
            }else {
                self?.followLimitReached = true
            }
//            self?.lblFollowerCount.text = "\(self?.arrFollower.count ?? 0)"
            DispatchQueue.main.async {
                self?.hideAllLoader()
                self?.tableView.endPull2RefreshAndInfiniteScrolling(count: response.count)
            }
            
        }) { [weak self] (error) in
            DispatchQueue.main.async {
                self?.hideAllLoader()
                self?.tableView.endPull2RefreshAndInfiniteScrolling(count: self?.arrFollower.count ?? 0)
            }
            DILog.print(items: error.message ?? "")
        }
    }
    
    @objc fileprivate func searchUser(){
        if !Utility.isConnectedToInternet {
            DispatchQueue.main.async {
                self.hideLoader()
                self.showAlert( message: AppMessages.ErrorAlerts.noInternet)
                return
            }
        }
        DIWebLayerUserAPI().getSearchResult(for: txtSearch.text?.trim.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed) ?? "", success: { [weak self](response) in
            
            self?.arrSearch = response
            DispatchQueue.main.async {
                self?.hideLoader()
                self?.tableViewSearch.reloadData()
            }
        }) { [weak self] (error) in
            DispatchQueue.main.async {
                self?.hideLoader()
                self?.tableViewSearch.reloadData()
            }
            DILog.print(items: error.message ?? "")
        }
    }
    
    fileprivate func getPending(limit :Int){
        if !Utility.isConnectedToInternet {
            DispatchQueue.main.async {
                self.hideAllLoader()
                self.showAlert( message: AppMessages.ErrorAlerts.noInternet)
                return
            }
        }
        DIWebLayerUserAPI().getPendingRequest(upto: limit, success: { [weak self](response) in
            
            self?.arrPendingRequest = response
            DispatchQueue.main.async {
                self?.hideAllLoader()
            }
            
        }) { [weak self] (error) in
            DispatchQueue.main.async {
                self?.hideAllLoader()
            }
            DILog.print(items: error.message ?? "")
        }
    }
    
    fileprivate func getNetworkDetail(){
        if !Utility.isConnectedToInternet {
            DispatchQueue.main.async {
                self.hideAllLoader()
                self.showAlert( message: AppMessages.ErrorAlerts.noInternet)
                return
            }
        }
        
        DIWebLayerUserAPI().getNetworkDetail(parameters: nil, success: { [weak self](modal) in
            
            if (modal.pending ?? 0) > 0{
                self?.pendingCount = modal.pending ?? 0
                self?.getPending(limit: 1)
            }else {
                self?.arrPendingRequest.removeAll()
            }
            DispatchQueue.main.async {
//                print("followers count ****************** \(modal.followers)")
//                print("followings count ****************** \(modal.following)")
                self?.tableView.reloadData()
                self?.lblFollowerCount.text = "\(modal.followers ?? 0)"
                self?.lblFollowingCount.text = "\(modal.following ?? 0)"
            }
            
            
        }) { [weak self] (error) in
            DILog.print(items: error.message ?? "")
            DispatchQueue.main.async {
                self?.refreshControl.endRefreshing()
                self?.tableView.reloadData()
            }
        }
    }
    
    
    fileprivate func requestToFollow(modal : FollowRequest , success: @escaping (_ finish: Bool,_ user:FollowerModal?)-> ()){
        if !Utility.isConnectedToInternet {
            self.hideLoader()
            self.showAlert( message: AppMessages.ErrorAlerts.noInternet)
            return
        }
        
        DIWebLayerUserAPI().followUser(parameters: modal.getDictonary(), success: { [weak self](status) in
            DispatchQueue.main.async {
                self?.hideLoader()
            }
            if status.0  == 1 {
                if (self?.anotherUser ?? "") == User.sharedInstance.id{
                    self?.getNetworkDetail()
                }
                success(true,status.1)
            }else {
                success(false,status.1)
            }
        }) { [weak self] (error) in
            DispatchQueue.main.async {
                self?.hideLoader()
                self?.showAlert( message: error.message ?? AppMessages.ErrorAlerts.unknownError)
            }
            DILog.print(items: error.message ?? "")
            success(false,nil)
        }
    }
    
    fileprivate func requestToUnfollow(modal : FollowRequest , success: @escaping (_ finish: Bool)-> ()){
        
        self.showAlert(withTitle: appName.capitalized, message: AppMessages.AlertTitles.unfollow, okayTitle: AppMessages.AlertTitles.Yes, cancelTitle: AppMessages.AlertTitles.No, okCall: {
            
            //call api now
            if !Utility.isConnectedToInternet {
                self.hideLoader()
                
                self.showAlert( message: AppMessages.ErrorAlerts.noInternet)
                return
            }
            self.showLoader()
            DIWebLayerUserAPI().unfollowUser(parameters: modal.getDictonary(), success: { [weak self](status) in
                DispatchQueue.main.async {
                    self?.hideLoader()
                }
                if status  == 1 {
                    if (self?.anotherUser ?? "") == User.sharedInstance.id{
                        self?.getNetworkDetail()
                    }
                    success(true)
                }else {
                    success(false)
                }
            }) { [weak self] (error) in
                DispatchQueue.main.async {
                    self?.hideLoader()
                    self?.showAlert( message: error.message ?? AppMessages.ErrorAlerts.unknownError)
                }
                DILog.print(items: error.message ?? "")
                success(false)
            }
        }) {
            
        }
    }
    
    fileprivate func acceptRequest(modal : FollowRequest , success: @escaping (_ finish: Bool)-> ()){
        if !Utility.isConnectedToInternet {
            self.hideLoader()
            self.showAlert( message: AppMessages.ErrorAlerts.noInternet)
            return
        }
        
        DIWebLayerUserAPI().acceptRequest(parameters: modal.getDictonary(), success: { [weak self](status) in
            DispatchQueue.main.async {
                self?.hideLoader()
            }
            if status  == 1 {
                success(true)
            }else {
                success(false)
            }
            
            
        }) { [weak self] (error) in
            DispatchQueue.main.async {
                self?.hideLoader()
                self?.showAlert( message: error.message ?? AppMessages.ErrorAlerts.unknownError)
            }
//            DILog.print(items: error.message ?? "")
            success(false)
        }
    }
    
    fileprivate func rejectRequest(modal : FollowRequest , success: @escaping (_ finish: Bool)-> ()){
        if !Utility.isConnectedToInternet {
            self.hideLoader()
            self.showAlert( message: AppMessages.ErrorAlerts.noInternet)
            return
        }
        
        DIWebLayerUserAPI().rejectRequest(parameters: modal.getDictonary(), success: { [weak self](status) in
            DispatchQueue.main.async {
                self?.hideLoader()
            }
            if status  == 1 {
                success(true)
            }else {
                success(false)
            }
            
        }) { [weak self] (error) in
            DispatchQueue.main.async {
                self?.hideLoader()
                self?.showAlert( message: error.message ?? AppMessages.ErrorAlerts.unknownError)
            }
//            DILog.print(items: error.message ?? "")
            success(false)
        }
    }
    
    fileprivate func blockUser(modal : FollowRequest , success: @escaping (_ finish: Bool)-> ()){
        
        self.showAlert(withTitle: appName.capitalized, message: "\(AppMessages.AlertTitles.removeFlowers) \(modal.userName) as a follower ", okayTitle: AppMessages.AlertTitles.Yes, cancelTitle: AppMessages.AlertTitles.No, okCall: {
            
            self.showLoader()
            //call api now
            if !Utility.isConnectedToInternet {
                self.hideLoader()
                self.showAlert( message: AppMessages.ErrorAlerts.noInternet)
                return
            }
            
            DIWebLayerUserAPI().blockUser(parameters: ["user_id":modal.friendId], success: { [weak self](status) in
                self?.hideLoader()
                success(true)
                
            }) { [weak self] (error) in
                self?.hideLoader()
                self?.showAlert( message: error.message ?? AppMessages.ErrorAlerts.unknownError)
                success(false)
            }
            
        }) {
            
        }
        
    }
    
    
    //MARK:-
    //MARK:- Another User APIs
    
    @objc fileprivate func searchForSelectedTab(){
        if !Utility.isConnectedToInternet {
            self.hideLoader()
            self.showAlert( message: AppMessages.ErrorAlerts.noInternet)
            return
        }
        
        guard let userID = anotherUser else {
            self.hideLoader()
            return
        }
        //don't search for white spaces
        if txtSearch.text?.trim.count == 0{
            self.hideLoader()
            return
        }
        
        
        //Search from follower listing of user
        if btnFollower.isSelected == true{
            
            DIWebLayerUserAPI().searchOtherUserFollower(for: userID, searchingFor: txtSearch.text?.trim.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed) ?? "", success: { [weak self](response) in
                
                self?.arrSearch = response
                DispatchQueue.main.async {
                    self?.hideLoader()
                    self?.tableViewSearch.reloadData()
                }
            }) { [weak self] (error) in
                DispatchQueue.main.async {
                    self?.hideLoader()
                    self?.tableViewSearch.reloadData()
                }
                DILog.print(items: error.message ?? "")
            }
        }else {
            //Search from following listing of user
            
            DIWebLayerUserAPI().searchOtherUserFollowing(for: userID, searchingFor: txtSearch.text?.trim.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed) ?? "", success: { [weak self](response) in
                
                self?.arrSearch = response
                DispatchQueue.main.async {
                    self?.hideLoader()
                    self?.tableViewSearch.reloadData()
                }
            }) { [weak self] (error) in
                DispatchQueue.main.async {
                    self?.hideLoader()
                    self?.tableViewSearch.reloadData()
                }
                DILog.print(items: error.message ?? "")
            }
        }
    }
    
    fileprivate func getOthersFollower(limit :Int){
        if !Utility.isConnectedToInternet {
            DispatchQueue.main.async {
                self.hideAllLoader()
                self.showAlert( message: AppMessages.ErrorAlerts.noInternet)
                self.tableView.endPull2RefreshAndInfiniteScrolling(count: self.arrFollower.count)
                return
            }
        }
        
        guard let userID = anotherUser else {
            self.hideAllLoader()
            self.tableView.endPull2RefreshAndInfiniteScrolling(count: self.arrFollower.count)
            return
        }
        
        DIWebLayerUserAPI().getOtherUserFollower(for: userID,upto: limit, success: { [weak self](response) in
            if limit == 1{
                self?.arrFollower.removeAll()
                if self?.btnFollower.isSelected == true{
                    self?.refreshInProgress = false
                }
            }
            
            if response.count > 0{
                self?.arrFollower.append(contentsOf: response)
                self?.followLimitReached = false
                if response.count < RECORDS_PER_PAGE{
                    self?.followLimitReached = true
                }
            }else {
                self?.followLimitReached = true
            }
            DispatchQueue.main.async {
                self?.tableView.endPull2RefreshAndInfiniteScrolling(count: response.count)
                self?.indexSelection()
                self?.hideAllLoader()
                
            }
            
        }) { [weak self] (error) in
            DispatchQueue.main.async {
                self?.tableView.endPull2RefreshAndInfiniteScrolling(count: self?.arrFollower.count ?? 0)
                self?.indexSelection()
                self?.hideAllLoader()
            }
            DILog.print(items: error.message ?? "")
        }
    }
    
    fileprivate func getOthersFollowing(limit :Int){
        
        if !Utility.isConnectedToInternet {
            DispatchQueue.main.async {
                self.hideAllLoader()
                self.showAlert( message: AppMessages.ErrorAlerts.noInternet)
                self.tableView.endPull2RefreshAndInfiniteScrolling(count: self.arrFollowing.count)
                return
            }
        }
        
        guard let userID = anotherUser else {
            self.hideAllLoader()
            self.tableView.endPull2RefreshAndInfiniteScrolling(count: self.arrFollowing.count)
            return
        }
        DIWebLayerUserAPI().getOtherUserFollowing(for: userID, upto: limit, success: { [weak self](response) in
            if limit == 1{
                self?.arrFollowing.removeAll()
                if self?.btnFollowing.isSelected == true{
                    self?.refreshInProgress = false
                }
            }
            if response.count > 0{
                self?.arrFollowing.append(contentsOf: response)
                self?.followingLimitReached = false
                if response.count < RECORDS_PER_PAGE{
                    self?.followingLimitReached = true
                }
            }else {
                self?.followingLimitReached = true
            }
            DispatchQueue.main.async {
                self?.tableView.endPull2RefreshAndInfiniteScrolling(count:response.count)
                self?.indexSelection()
                self?.hideAllLoader()
            }
            
        }) { [weak self] (error) in
            DispatchQueue.main.async {
                self?.tableView.endPull2RefreshAndInfiniteScrolling(count: self?.arrFollowing.count ?? 0)
                self?.indexSelection()
                self?.hideAllLoader()
            }
            DILog.print(items: error.message ?? "")
        }
    }
    
    
    fileprivate func fbLogin(){
        if !Utility.isConnectedToInternet {
            hideLoader()
            self.showAlert( message: AppMessages.ErrorAlerts.noInternet)
            return
        }
        FacebookManager.shared.login([.email, .publicProfile, .birthday, .friends], success: { (user) in
            
            let login = User()
            
            // modal for user exitence
            login.snsType = user.snsType
            login.snsID = user.snsID
            
            if user.snsID != nil {
                DispatchQueue.main.async {
                    self.btnConnectFb.isHidden = true
                    self.lblNotConnectedFB.isHidden = true
                }
            }
            self.fetchFromFacebookList()
            
        }, failure: { (error) in
            self.hideLoader()
        }, onController: self)
        
    }
    
    fileprivate func getProfileCount(){
        if !Utility.isConnectedToInternet {
            self.hideLoader()
            self.showAlert(withTitle: appName.capitalized, message: AppMessages.ErrorAlerts.noInternet, okayTitle: AppMessages.AlertTitles.Ok, okCall: {
                self.navigationController?.popViewController(animated: true)
            })
            
            return
        }
        guard let userID = anotherUser else {return} // get user ID
        //        showLoader()
        DIWebLayerUserAPI().getOtherUserProfile(for: userID, success: { [weak self](response) in
            
            DispatchQueue.main.async {
                self?.hideLoader()
                //Show data on view
                
                self?.followCount[0] = response.followers ?? 0
                self?.followCount[1] = response.followings ?? 0
                
                self?.lblFollowerCount.text = "\(self?.followCount[0] ?? 0)"
                self?.lblFollowingCount.text = "\(self?.followCount[1] ?? 0)"
                
            }
            
        }) { [weak self] (error) in
            DispatchQueue.main.async {
                self?.hideLoader()
                self?.showAlert(withTitle: appName.capitalized, message: error.message, okayTitle: AppMessages.AlertTitles.Ok, okCall: {
                    self?.navigationController?.popViewController(animated: true)
                })
            }
            DILog.print(items: error.message ?? "")
        }
    }
}

extension NetworkViewController : SlidingTutorialDelegate{
    func finishWithTutorial(sender: Bool) {
        
        if sender == true{
            viewTutorialAction.isHidden = true
            AppDelegate.shared().navigateToDashboard()
        }else{
            viewTutorialAction.isHidden = false
        }
    }
}
