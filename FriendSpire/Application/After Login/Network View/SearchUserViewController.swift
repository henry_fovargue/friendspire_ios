//
//  SearchUserViewController.swift
//  FriendSpire
//
//  Created by abhishek on 26/07/18.
//  Copyright © 2018 openkey. All rights reserved.
//

import UIKit

class SearchUserViewController: DIBaseController {
    
    @IBOutlet weak var txtSearch: UITextField!
    @IBOutlet weak var tableViewSearch: UITableView!
    @IBOutlet weak var stackView: UIStackView!
    
    
    fileprivate  var arrSearch = [SearchFriendModal]()
    
    var strSearch:String?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        
     
        
        if strSearch != nil {
            showLoader()
           txtSearch.text = strSearch?.replacingOccurrences(of: "%20", with: " ")
            searchUser()
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func backButtonTapped(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
    
    
    @objc fileprivate func searchUser(){
        if !Utility.isConnectedToInternet {
            self.hideLoader()
            self.showAlert( message: AppMessages.ErrorAlerts.noInternet)
            return
        }
        DIWebLayerUserAPI().getSearchDetailResult(for: strSearch?.trim.addDeleimeter() ?? "", success: { [weak self](response) in
            
            self?.arrSearch = response
            DispatchQueue.main.async {
                self?.hideLoader()
                self?.tableViewSearch.reloadData()
            }
            
            
        }) { [weak self] (error) in
            DispatchQueue.main.async {
                self?.showAlert( message: error.message ?? AppMessages.ErrorAlerts.unknownError)
                self?.hideLoader()
                self?.tableViewSearch.reloadData()
            }
            DILog.print(items: error.message ?? "")
        }
    }
    
    // MARK: - Navigation
    
    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
        
        if segue.identifier == SegueIdentifiers.NetworkScreen.SearchScreen.moveToUser {
            let controller = segue.destination as! UserProfileViewController
            let modal = sender as? SearchFriendModal
            controller.userId = modal?.id
        }
    }
    
    
}
//MARK:-
extension SearchUserViewController : UITextFieldDelegate{
    //MARK:- TextField Delegate
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        
        if txtSearch == textField{
            
        }
        return true
    }
    
    func textFieldShouldEndEditing(_ textField: UITextField) -> Bool {
        
        
        return true
    }
    
    
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        if string.trim.length == 0 && textField.text?.length == 0{
//            print(string)
            return false
        }
        
        if string == "" && textField.text?.length == 1 {
          
            self.arrSearch.removeAll()
            self.tableViewSearch.reloadData()
        }
        else {
            strSearch = textField.text
            NSObject.cancelPreviousPerformRequests(withTarget: self)
            self.perform(#selector(self.searchUser), with: nil, afterDelay: 0.5)
        }
        return true
        
        
    }
    
}

//MARK:-
extension SearchUserViewController:UITableViewDataSource,UITableViewDelegate{
    //MARK:- Table view delegate
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        tableView.backgroundView = nil // reset to default state
        if arrSearch.count > 0 {
            stackView.isHidden = false
        }else {
            stackView.isHidden = true
            tableViewSearch.showEmptyScreen("No Result", errorIcon: #imageLiteral(resourceName: "no-result"),with: #colorLiteral(red: 0.6000000238, green: 0.6000000238, blue: 0.6000000238, alpha: 1))
        }
        
        return arrSearch.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: CellIdentifiers.NetworksScreen.SearchScreen.searchUser) as! SearchUserCell
        cell.configureCell(arrSearch[indexPath.row])
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.performSegue(withIdentifier: SegueIdentifiers.NetworkScreen.SearchScreen.moveToUser, sender: arrSearch[indexPath.row])
    }
    
}
