//
//  TutroialViewController.swift
//  FriendSpire
//
//  Created by abhishek on 29/10/18.
//  Copyright © 2018 openkey. All rights reserved.
//

import UIKit


class TutroialViewController: DIBaseController {
    
    //variable from another controller
    var arrImages = [UIImage]()
    
  weak var delegate : SlidingTutorialDelegate?
    
    @IBOutlet weak var btnSkip: UIButton!
    @IBOutlet weak var imgViewOverlay: UIImageView!
    @IBOutlet weak var btnContinue: UIButton!
    
    fileprivate var currentIndex = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        if arrImages.count > 0{
            imgViewOverlay.image = arrImages[0]
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    @IBAction func continueTapped(_ sender: UIButton) {
        
        if currentIndex < arrImages.count-1 {
            currentIndex += 1
            imgViewOverlay.image = arrImages[currentIndex]
        }else{
            delegate?.finishWithTutorial(sender: false)
            self.dismiss(animated: true, completion: nil)
        }
        
    }
    
    @IBAction func skipTapped(_ sender: UIButton) {
        self.tutorialCompleted()
        delegate?.finishWithTutorial(sender: true)
        self.dismiss(animated: true, completion: nil)
    }
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
}
