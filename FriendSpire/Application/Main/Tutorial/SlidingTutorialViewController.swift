//
//  SlidingTutorialViewController.swift
//  FriendSpire
//
//  Created by abhishek on 13/11/18.
//  Copyright © 2018 openkey. All rights reserved.
//

import UIKit

protocol SlidingTutorialDelegate :class{
    
    func finishWithTutorial(sender:Bool)
    
}

class SlidingTutorialViewController: DIBaseController {
    
    //variable from another controller
    
    var arrImages = [[KeyTutorial:Any]]()
    weak var delegate : SlidingTutorialDelegate?
    
    @IBOutlet weak var btnSkip: UIButton!
    @IBOutlet weak var btnContinue: UIButton!
    @IBOutlet weak var collectioView: UICollectionView!
    @IBOutlet weak var indexPageControl: UIPageControl!
    @IBOutlet weak var constraintBtnContinueCenter: NSLayoutConstraint!
    @IBOutlet weak var constraintBtnContinueWidth: NSLayoutConstraint!
    //    @IBOutlet weak var constraintBottomBtnContinue: NSLayoutConstraint!
    
    fileprivate var currentIndex = 0
    fileprivate var pageChanged = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //        if UIScreen.main.nativeBounds.height > iPHONE10{
        //            constraintBottomBtnContinue.constant = 40
        //        }else{
        //            constraintBottomBtnContinue.constant = 10
        //        }
        
        // Do any additional setup after loading the view.
        indexPageControl.currentPage = currentIndex
        indexPageControl.numberOfPages = arrImages.count
        //            == 1 ? 0 : arrImages.count
        indexPageControl.isHidden = arrImages.count == 1 ? true : false
        indexPageControl.isUserInteractionEnabled = false
        if arrImages.count > 1{
            self.view.removeConstraint(constraintBtnContinueCenter)
            constraintBtnContinueWidth.constant = 80
        }else{
            constraintBtnContinueWidth.constant = 200
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func continueTapped(_ sender: UIButton) {
        
        if currentIndex < arrImages.count-1 {
            currentIndex += 1
            collectioView.scrollToItem(at: IndexPath.init(row: currentIndex, section: 0), at: .centeredHorizontally, animated: true)
            NSObject.cancelPreviousPerformRequests(withTarget: self)
            self.perform(#selector(reloadWithDelay), with: nil, afterDelay: 0.5)
        }else{
            if arrImages.count > 1{
                self.tutorialCompleted()
                delegate?.finishWithTutorial(sender: true)
                return
            }
            delegate?.finishWithTutorial(sender: false)
            self.dismiss(animated: true, completion: nil)
        }
        
    }
    
    @IBAction func skipTapped(_ sender: UIButton) {
        self.tutorialCompleted()
        delegate?.finishWithTutorial(sender: true)
        self.dismiss(animated: true, completion: nil)
    }
    
    func reloadWithDelay(){
        collectioView.reloadData()
    }
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
    
    
    
}

extension SlidingTutorialViewController : UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return arrImages.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "SlidingTutorialCell", for: indexPath) as! SlidingTutorialCell
        cell.configureCell(modal: arrImages[indexPath.row])
        animateZoomforCellremove(zoomCell: cell.imgViewScreen)
        if indexPath.row == currentIndex{
            animateZoomforCell(zoomCell: cell.imgViewScreen)
        }
        return cell
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        
        let pageWidth = scrollView.frame.width
        let currentPage = Int((scrollView.contentOffset.x  + pageWidth / 2)/pageWidth)
        if currentPage != currentIndex{
            indexPageControl.currentPage = currentPage
            currentIndex = currentPage
            pageChanged = true
            btnContinue.setTitle("Next", for: .normal)
            if arrImages.count > 1 && currentIndex == arrImages.count-1{
                btnContinue.setTitle("Begin", for: .normal)
            }
        }
    }
    
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        if pageChanged{
            
            pageChanged = false
            collectioView.reloadData()
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        return CGSize.init(width: self.view.frame.size.width, height: self.collectioView.frame.size.height)
    }
    
    func animateZoomforCell(zoomCell : UIView){
        UIView.animate(withDuration: 1.0,
                       delay: 0,
                       options: UIViewAnimationOptions.curveEaseOut, animations: {
                        zoomCell.transform = CGAffineTransform.init(scaleX: 1.0, y: 1.0)
        }) { (finished) in
        }
    }
    
    func animateZoomforCellremove(zoomCell : UIView){
        zoomCell.transform = CGAffineTransform.init(scaleX: 0.8, y: 0.8)
    }
    
}
