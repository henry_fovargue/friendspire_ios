//
//  TabBarViewController.swift
//  FriendSpire
//
//  Created by abhishek on 14/12/18.
//  Copyright © 2018 openkey. All rights reserved.
//

import UIKit

class TabBarViewController: UITabBarController, UITabBarControllerDelegate {

    //MARK:- IBOutlets
    private var selectedIndexOfTab = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.delegate = self
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
//    func tabBarController(_ tabBarController: UITabBarController, shouldSelect viewController: UIViewController) -> Bool {
//
//        if RestuarantFilterModel.sharedInstance.filterApplied == true{
//
//            showFilterResetWarning()
//            return false
//        }
//        return true
//    }
    
    func tabBarController(_ tabBarController: UITabBarController, didSelect viewController: UIViewController) {
        
        //Prevent double tap
        if tabBarController.selectedIndex == 3 && self.selectedIndexOfTab != 3 {
            if let controllers = tabBarController.viewControllers,
                let navigationController = controllers[tabBarController.selectedIndex] as? UINavigationController {
                if let newController = navigationController.viewControllers.first as? NetworkViewController{
                            newController.initalApiFetch()
                    }
            }
        }
        self.selectedIndexOfTab = tabBarController.selectedIndex
    }
    
    
//    func showFilterResetWarning(){
//        let alert = UIAlertController(title: title, message: "Switch to other tab will leads to filter l", preferredStyle: .alert)
//        alert.addAction(UIAlertAction(title: AppMessages.AlertTitles.Ok, style: .default, handler: { (action) in
//           Utility.resetFilter()
//        }))
//        alert.addAction(UIAlertAction(title: AppMessages.AlertTitles.Cancel, style: .cancel, handler: { (action) in
//
//            }))
//        self.present(alert, animated: true, completion: nil)
//    }
}
