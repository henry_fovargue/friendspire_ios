//
//  AppDelegate.swift
//  BaseProject
//
//  Created by narinder on 28/02/17.
//  varyright © 2017 openkey. All rights reserved.
//

import UIKit
import IQKeyboardManagerSwift
import Firebase
import UserNotifications
import CoreData
import UserNotificationsUI
import FirebaseMessaging
import FirebaseInstanceID
import FirebaseDatabase
import FBSDKCoreKit
import GooglePlaces
import GoogleMaps
import FirebaseDynamicLinks
//import FBSDKLoginKit

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate,MessagingDelegate {
    
    
    var window: UIWindow?
    var fir_instanceID_token:String = "init"
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        
        
        
        //For not printing logs
        DILog.enableLog = false
        GMSPlacesClient.provideAPIKey(GOOGLEAPIKEY)
        GMSServices.provideAPIKey(GOOGLEAPIKEY)
        // GMSServices.provideAPIKey("AIzaSyCQ3Y_uUtx21bj6LvNN4SbwUAMvWE9HItw")
        
        
        if #available(iOS 10.0, *) {
            UNUserNotificationCenter.current().delegate = self
            let authOptions: UNAuthorizationOptions = [.alert, .badge, .sound]
            UNUserNotificationCenter.current().requestAuthorization(
                options: authOptions,
                completionHandler: {_, _ in })
            Messaging.messaging().delegate = self
            let notificationCenter = UNUserNotificationCenter.current()
            notificationCenter.requestAuthorization(options: [.alert, .sound, .badge])  { (granted, error) in
                notificationCenter.delegate = self
                if error == nil{
                    //UIApplication.shared.registerForRemoteNotifications()
                }
            }
        }else {
            let settings: UIUserNotificationSettings =
                UIUserNotificationSettings(types: [.alert, .badge, .sound], categories: nil)
            application.registerUserNotificationSettings(settings)
        }
        application.registerForRemoteNotifications()
        
        // Add observer for InstanceID token refresh callback.
        NotificationCenter.default.addObserver(self, selector: #selector(self.tokenRefreshNotification),name: NSNotification.Name.InstanceIDTokenRefresh, object: nil)
        
        if let  refreshedToken = InstanceID.instanceID().token() {
            self.fir_instanceID_token = refreshedToken
            Defaults.shared.set(value: refreshedToken, forKey: DefaultKey.deviceToken)
        }else{
            self.fir_instanceID_token = "not Found"
        }
        //Check user session
        redirectUserToApp()
        let defaults = UserDefaults.standard
        if let savedPerson = defaults.object(forKey: "SavedPerson") as? Data {
            let decoder = JSONDecoder()
            if let loadedPerson = try? decoder.decode(UserModal.self, from: savedPerson) {
                UserModal.shared = loadedPerson
            }
        }
        
        //Tab bar icon position
        UITabBarItem.appearance().titlePositionAdjustment = UIOffsetMake(0.0, -4.0)
        
        //IQKeyboard Manager Implementation
        self.addKeyboardManager()
        
        // FB Token Sync
        FBSDKApplicationDelegate.sharedInstance().application(application, didFinishLaunchingWithOptions: launchOptions)
        FirebaseOptions.defaultOptions()?.deepLinkURLScheme = "friendspireapp.page.link"
        FirebaseApp.configure()
        googleAnalytics()
        
        return true
    }
    
    //MARK:- Configure Google Analytics
    func googleAnalytics() {
        if let gai = GAI.sharedInstance(),
            let gaConfigValues = Bundle.main.infoDictionary?["GoogleAnalytics"] as? [String: String],
            let trackingId = gaConfigValues["TRACKING_ID"]{
            gai.logger.logLevel = .error
            gai.trackUncaughtExceptions = false
            gai.tracker(withTrackingId: trackingId)
            // gai.dispatchInterval = 120.0
        } else {
            assertionFailure("Google Analytics not configured correctly")
        }
    }
    
    //MARK: Register APNS
    
    func application(_ application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data) {
        DILog.print(items: "DEVICE TOKEN = \(deviceToken)")
        Messaging.messaging().apnsToken = deviceToken
        let token = deviceToken.hexString
        DILog.print(items: token)
        //        DIWebLayerCategoriesDetailApi().updateDeviceToken(parameter: ["device_token":token], success: { (message) in
        //        }) { (error) in
        //        }
    }
    
    func application(_ application: UIApplication, didFailToRegisterForRemoteNotificationsWithError error: Error) {
        DILog.print(items: error)
    }
    
    private func application(application: UIApplication, didReceiveRemoteNotification userInfo: [NSObject : AnyObject]) {
        DILog.print(items: userInfo)
    }
    
    func messaging(_ messaging: Messaging, didReceiveRegistrationToken fcmToken: String) {
        //        DILog.print(items:"Firebase registration token: \(fcmToken)")
        Defaults.shared.set(value: fcmToken, forKey: DefaultKey.deviceToken)
        
        // TODO: If necessary send token to application server.
        // Note: This callback is fired at each app startup and whenever a new token is generated.
    }
    
    func messaging(_ messaging: Messaging, didRefreshRegistrationToken fcmToken: String) {
        //        DILog.print(items:"Firebase registration token: \(fcmToken)")
        Defaults.shared.set(value: fcmToken, forKey: DefaultKey.deviceToken)
    }
    
    func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable : Any]) {
        //        DILog.print(items:userInfo)
    }
    
    @objc func tokenRefreshNotification(_ notification: Notification)
    {
        if let refreshedToken = InstanceID.instanceID().token() {
            Defaults.shared.set(value: refreshedToken, forKey: DefaultKey.deviceToken)
        }
        
        // Connect to FCM since connection may have failed when attempted before having a token.
        connectToFcm()
    }
    func connectToFcm() {
        guard InstanceID.instanceID().token() != nil else {
            return;
        }
        
        // Disconnect previous FCM connection if it exists.
        Messaging.messaging().disconnect()
        Messaging.messaging().connect { (error) in
            if error != nil {
                DILog.print(items:"Unable to connect with FCM. \(String(describing: error))")
            } else {
                DILog.print(items:"Connected to FCM.")
            }
        }
    }
    
    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
    }
    func application(_ application: UIApplication, open url: URL, sourceApplication: String?, annotation: Any) -> Bool {
        
        
        
        return true
    }
    
    
    // Respond to Universal Links
    func application(_ application: UIApplication, continue userActivity: NSUserActivity, restorationHandler: @escaping ([Any]?) -> Void) -> Bool {
        // pass the url to the handle deep link call
        //Branch.getInstance().continue(userActivity)
        
        if let incomingUrl = userActivity.webpageURL {
            DILog.print(items: "Incoming url \(incomingUrl)")
            
            let _ = DynamicLinks.dynamicLinks().handleUniversalLink(incomingUrl, completion: { (link, error) in
                if let url:URL = link?.url {
                    if UserManager.isUserLoggedIn() {
                        self.redirectUserToScreenOnLinkClick(url: url)
                        return
                    }else{
                        LoginViewController.isComingFromDeepLink = true
                        LoginViewController.deepLinkUrl = url
                        self.navigateToLogin()
                    }
                }
            })
        }
        
        return true
    }
    
    func navigateToProfile(id:String) {
        let  controller: UserProfileViewController = UIStoryboard.init(storyboard: .networks).initVC()
        controller.userId =  id
        controller.isComingFromLink = true
        self.setNavigationToRoot(viewContoller: controller)
        
    }
    func redirectUserToApp() {
        if UserManager.isUserLoggedIn() {
            if let user = UserManager.getCurrentUser() {
                User.sharedInstance = user
                User.sharedInstance.setHomeCity()
            }
            navigateToDashboard()
            //Navigate to dashboard.
        }
        else {
            //Navigate to login screen.
            navigateToLogin()
        }
    }
    
    // Respond to URI scheme links
    
    func application(_ app: UIApplication, open url: URL, options: [UIApplicationOpenURLOptionsKey : Any]) -> Bool{
        let fb = FBSDKApplicationDelegate.sharedInstance().application(app, open: url, options: options)
        let deepLink = application(app, open: url,
                                   sourceApplication: options[UIApplicationOpenURLOptionsKey.sourceApplication] as? String,
                                   annotation: "")
        
        return fb||deepLink
    }
    
    
    static func shared() -> AppDelegate {
        return UIApplication.shared.delegate as! AppDelegate
    }
    
    func navigateToDashboard() {
        let dashboardVC = UIStoryboard(storyboard: .inspiration).instantiateViewController(withIdentifier: "InspirationViewController")  as! UITabBarController
        setNavigationToRoot(viewContoller: dashboardVC)
    }
    
    func navigateToFriendsTab() {
        
        let  controller: NetworkViewController = UIStoryboard.init(storyboard: .networks).instantiateViewController(withIdentifier: "userNetwork") as! NetworkViewController
        controller.indexToShow = 2
        controller.fromTutorial = true
        AppDelegate.shared().window?.rootViewController?.presentedViewController?.dismiss(animated: false, completion: nil)
        AppDelegate.shared().window?.rootViewController?.present(controller, animated: true, completion: nil)
    }
    
    func navigateToLogin() {
         UIApplication.shared.applicationIconBadgeNumber = 0
        let loginVC: LoginViewController = UIStoryboard(storyboard: .registration).initVC()
        self.setNavigationToRoot(viewContoller: loginVC)
    }
    
    func setNavigationToRoot(viewContoller: UIViewController) {
        //Setting Login to Root Controller
        let navController = UINavigationController()
        navController.navigationBar.isHidden = true
        navController.interactivePopGestureRecognizer?.isEnabled = true
        navController.pushViewController(viewContoller, animated: true)
        self.window?.rootViewController = navController
        self.window?.makeKeyAndVisible()
    }
    
    func addKeyboardManager() {
        IQKeyboardManager.shared.enable = true
        IQKeyboardManager.shared.shouldResignOnTouchOutside = true
        
    }
    
    func redirectUserToScreenOnLinkClick(url:URL) {
        if let type = url.valueOf("category") {
            let value = Int(type) ?? 10
            switch value {
            case 1,2:
                let  controller: DetailPageViewController = UIStoryboard.init(storyboard: .inspiration).initVC()
                controller.category =  value
                controller.isComingFromLink = true
                controller.postId = url.valueOf("postId") ?? ""
                self.setNavigationToRoot(viewContoller: controller)
            case 3,4,5:
                let  controller: MovieDetailViewController = UIStoryboard.init(storyboard: .inspiration).initVC()
                controller.category =  value
                controller.isComingFromLink = true
                controller.postId = url.valueOf("postId") ?? ""
                self.setNavigationToRoot(viewContoller: controller)
            default:
                if let id  = url.valueOf("profile_id") {
                    self.navigateToProfile(id:id)
                }else{
                    self.redirectUserToApp()
                }
            }
        }else{
            if let id  = url.valueOf("profile_id") {
                self.navigateToProfile(id:id)
            }else{
                self.redirectUserToApp()
            }
        }
        
    }
    
    
    func redirectUserToScreenOnNotificationClick(responseData:[AnyHashable:Any]) {
        if let type = responseData["type"] as? String {
            if type == Constant.pushNotificationType.commentOnPost.rawValue {
                if let category =  responseData["post_type"] as? Int {
                    if category == 1 || category == 2 {
                        let  controller: DetailPageViewController = UIStoryboard.init(storyboard: .inspiration).initVC()
                        controller.category =  category
                        controller.isComingFromLink = true
                        controller.postId = responseData["post_id"] as? String ?? ""
                        self.setNavigationToRoot(viewContoller: controller)
                    }else if category == 3 || category == 4 || category == 5 {
                        let  controller: MovieDetailViewController = UIStoryboard.init(storyboard: .inspiration).initVC()
                        controller.category =  category
                        controller.isComingFromLink = true
                        controller.postId = responseData["post_id"] as? String ?? ""
                        self.setNavigationToRoot(viewContoller: controller)
                    }
                }
                else if let category =  responseData["post_type"] as? String {
                    if category == "1" || category == "2" {
                        let  controller: DetailPageViewController = UIStoryboard.init(storyboard: .inspiration).initVC()
                        controller.category =  Int(category) ?? 1
                        controller.isComingFromLink = true
                        controller.postId = responseData["post_id"] as? String ?? ""
                        self.setNavigationToRoot(viewContoller: controller)
                    }else if category == "3" || category == "4" || category == "5" {
                        let  controller: MovieDetailViewController = UIStoryboard.init(storyboard: .inspiration).initVC()
                        controller.category =  Int(category) ?? 3
                        controller.isComingFromLink = true
                        controller.postId = responseData["post_id"] as? String ?? ""
                        self.setNavigationToRoot(viewContoller: controller)
                    }
                }
            }else if type == Constant.pushNotificationType.accept.rawValue || type == Constant.pushNotificationType.reject.rawValue {
                if let id = responseData["user_id"] as? String {
                    self.navigateToProfile(id:id)
                }else{
                    self.redirectUserToApp()
                }
            }else if type == Constant.pushNotificationType.requestSent.rawValue {
                let dashboardVC = UIStoryboard(storyboard: .inspiration).instantiateViewController(withIdentifier: "InspirationViewController")  as! UITabBarController
                dashboardVC.selectedIndex = 3
                setNavigationToRoot(viewContoller: dashboardVC)
                
            }else if type == Constant.pushNotificationType.dailyReview.rawValue {
                let dashboardVC = UIStoryboard(storyboard: .inspiration).instantiateViewController(withIdentifier: "InspirationViewController")  as! UITabBarController
                dashboardVC.selectedIndex = 0
                setNavigationToRoot(viewContoller: dashboardVC)
            }
            else if type == Constant.pushNotificationType.replyOnComment.rawValue {
                if let category =  responseData["post_type"] as? Int {
                    if category == 1 || category == 2 {
                        let  controller: DetailPageViewController = UIStoryboard.init(storyboard: .inspiration).initVC()
                        controller.category =  category
                        controller.isComingFromLink = true
                        controller.postId = responseData["post_id"] as? String ?? ""
                        controller.openReplyFor = responseData["recommendation_id"] as? String
                        controller.openCommentTo = responseData["comment_id"] as? String 
                        self.setNavigationToRoot(viewContoller: controller)
                    }else if category == 3 || category == 4 || category == 5 {
                        let  controller: MovieDetailViewController = UIStoryboard.init(storyboard: .inspiration).initVC()
                        controller.category =  category
                        controller.isComingFromLink = true
                        controller.postId = responseData["post_id"] as? String ?? ""
                        controller.openReplyFor = responseData["recommendation_id"] as? String
                        controller.openCommentTo = responseData["comment_id"] as? String
                        self.setNavigationToRoot(viewContoller: controller)
                    }
                }
                else if let category =  responseData["post_type"] as? String {
                    if category == "1" || category == "2" {
                        let  controller: DetailPageViewController = UIStoryboard.init(storyboard: .inspiration).initVC()
                        controller.category =  Int(category) ?? 1
                        controller.isComingFromLink = true
                        controller.postId = responseData["post_id"] as? String ?? ""
                        controller.openReplyFor = responseData["recommendation_id"] as? String
                        controller.openCommentTo = responseData["comment_id"] as? String
                        self.setNavigationToRoot(viewContoller: controller)
                    }else if category == "3" || category == "4" || category == "5" {
                        let  controller: MovieDetailViewController = UIStoryboard.init(storyboard: .inspiration).initVC()
                        controller.category =  Int(category) ?? 3
                        controller.isComingFromLink = true
                        controller.postId = responseData["post_id"] as? String ?? ""
                        controller.openReplyFor = responseData["recommendation_id"] as? String
                        controller.openCommentTo = responseData["comment_id"] as? String
                        self.setNavigationToRoot(viewContoller: controller)
                    }
                }
            }
        }
    }
    
    /*func applicationWillResignActive(_ application: UIApplication) {
     // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
     // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
     }
     */
    func applicationDidEnterBackground(_ application: UIApplication) {
        Messaging.messaging().disconnect()
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }
    
    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
        
       let count = UIApplication.shared.applicationIconBadgeNumber
        Defaults.shared.set(value: count, forKey: DefaultKey.notificationCount)
        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "increaseNotificationCount"), object: nil, userInfo: [:])
    }
    
    func applicationDidBecomeActive(_ application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
        
//        AppManager.shared.isUpgradeAvailable { (status, url, error) in
//            switch status {
//            case .available :
//                self.showVersionUpdate()
//                DILog.print(items: "New Update is available for application")
//                break
//            case .none :
//                DILog.print(items: "There is no update available for your application")
//                break
//            case .error :
//                DILog.print(items: "Update Error occured >>>>>>>>>>>", error?.message ?? "Something happen wrong")
//                break
//            }
//        }
        
    }
    
    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    }
    
    
    func showVersionUpdate() {
        let alert = UIAlertController(title: "New Version Available", message: "There is a newer Version available for download! Please update the app by visiting the Apple Store", preferredStyle: UIAlertControllerStyle.alert)
        
        let updateAction = UIAlertAction(title: "UPDATE", style: UIAlertActionStyle.default) {
            UIAlertAction in
            //go to app store
            
            if let url = URL(string: "https://itunes.apple.com/app/id1435691931?mt=8") {
                UIApplication.shared.open(url, options: [:])
            }
        }
        
        let cancel = UIAlertAction(title: "Cancel", style: UIAlertActionStyle.cancel) {
            UIAlertAction in
            
        }
        alert.addAction(updateAction)
        alert.addAction(cancel)
        UIApplication.shared.keyWindow?.rootViewController?.present(alert, animated: true, completion: {
            
        })
        
    }
    
    
    /// This will handle 3d touch action
    ///
    /// - Parameters:
    ///   - application: application it self
    ///   - shortcutItem: action taken by user
    ///   - completionHandler: do rest of handling
    
    func application(_ application: UIApplication, performActionFor shortcutItem: UIApplicationShortcutItem, completionHandler: @escaping (Bool) -> Void) {
        let identifier =  Bundle.main.bundleIdentifier!
        
        if shortcutItem.type == "\(identifier).recommendation"{
            let dashboardVC = UIStoryboard(storyboard: .inspiration).instantiateViewController(withIdentifier: "InspirationViewController")  as! UITabBarController
            dashboardVC.selectedIndex = 2
            self.window?.rootViewController = dashboardVC
            self.window?.makeKeyAndVisible()
        }else if shortcutItem.type == "\(identifier).invite"{
            let dashboardVC = UIStoryboard(storyboard: .inspiration).instantiateViewController(withIdentifier: "InspirationViewController")  as! UITabBarController
            dashboardVC.selectedIndex = 0
            if let navController = dashboardVC.viewControllers?[0] as? UINavigationController {
                if let controller = navController.viewControllers[0] as? InspirationViewController{
                    controller.respondTo3DTouch = true
                }
            }
            self.window?.rootViewController = dashboardVC
            self.window?.makeKeyAndVisible()
        }
    }
    
}

extension UIApplication {
    
    class func topViewController(base: UIViewController? = (UIApplication.shared.delegate as! AppDelegate).window?.rootViewController) -> UIViewController? {
        
        if let nav = base as? UINavigationController {
            return topViewController(base: nav.visibleViewController)
        }
        if let tab = base as? UITabBarController {
            if let selected = tab.selectedViewController {
                return topViewController(base: selected)
            }
        }
        if let presented = base?.presentedViewController {
            return topViewController(base: presented)
        }
        return base
    }
    
    class func visibleViewController()-> UIViewController? {
        return appDelegate.window?.rootViewController?.topViewController
    }
}

extension UIViewController {
    fileprivate var topViewController: UIViewController {
        switch self {
        case is UINavigationController:
            return (self as! UINavigationController).visibleViewController?.topViewController ?? self
        case is UITabBarController:
            return (self as! UITabBarController).selectedViewController?.topViewController ?? self
        default:
            return presentedViewController?.topViewController ?? self
        }
    }
    
    func getSimpleClassName() -> String {
        let describing = String(describing: self)
        if let dotIndex = describing.index(of: "."), let commaIndex = describing.index(of: ":") {
            let afterDotIndex = describing.index(after: dotIndex)
            if(afterDotIndex < commaIndex) {
                return String(describing[afterDotIndex ..< commaIndex])
            }
        }
        return describing
    }
}



//MARK:-UNUserNotificationCenterDelegate
extension AppDelegate:UNUserNotificationCenterDelegate {
    @available(iOS 10.0, *)
    func userNotificationCenter(_ center: UNUserNotificationCenter,willPresent notification: UNNotification, withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Void) {
        completionHandler([.alert, .badge, .sound])
        _ = notification.request.content.userInfo
        var count = Defaults.shared.get(forKey: DefaultKey.notificationCount) as? Int ?? 0
        count = count + 1
        UIApplication.shared.applicationIconBadgeNumber = count
        Defaults.shared.set(value: count, forKey: DefaultKey.notificationCount)
        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "increaseNotificationCount"), object: nil, userInfo: [:])
    }
    
    @available(iOS 10.0, *)
    func userNotificationCenter(_ center: UNUserNotificationCenter,didReceive response: UNNotificationResponse,withCompletionHandler completionHandler: @escaping () -> Void) {
        let responseData = response.notification.request.content.userInfo
        if UserManager.isUserLoggedIn() {
            redirectUserToScreenOnNotificationClick(responseData: responseData)
            return
        }else{
            LoginViewController.isComingNotification = true
            LoginViewController.notificationData = responseData
            self.navigateToLogin()
        }
        
    }
}
