//
//  EnableNotification.swift
//  BaseProject
//
//  Created by TpSingh on 20/07/17.
//  Copyright © 2017 openkey. All rights reserved.
//

import UIKit
import UserNotificationsUI
import UserNotifications


class EnableNotification{
  
  func requstPermissionForNotification() {
    
    let notificationTypes: UIUserNotificationType = [UIUserNotificationType.alert, UIUserNotificationType.badge, UIUserNotificationType.sound]
    let pushNotificationSettings = UIUserNotificationSettings(types: notificationTypes, categories: nil)
    UIApplication.shared.registerUserNotificationSettings(pushNotificationSettings)
    UIApplication.shared.registerForRemoteNotifications()
    
  }
  
  func getNotificationSettings() {
    if #available(iOS 10.0, *) {
      UNUserNotificationCenter.current().getNotificationSettings { (settings) in
      DILog.print(items:"Notification settings: \(settings)")
        guard settings.authorizationStatus == .authorized else { return }
        UIApplication.shared.registerForRemoteNotifications()
      }
    } else {
        UIApplication.shared.registerUserNotificationSettings(UIUserNotificationSettings(types: [.badge, .sound, .alert], categories: nil))
        UIApplication.shared.registerForRemoteNotifications()
    
      // Fallback on earlier versions
    }
  }
  
  func registerForPushNotifications() {
    if #available(iOS 10.0, *) {
      UNUserNotificationCenter.current().requestAuthorization(options: [.alert, .sound, .badge]) {
        (granted, error) in
      DILog.print(items:"Permission granted: \(granted)")
        
        guard granted else { return }
        self.getNotificationSettings()
      }
    } else {
      // Fallback on earlier versions
      self.getNotificationSettings()
    }
  }
  
  
  
}
