//
//  LocationManager.swift
//  Omakase
//
//  Created by Harpreet on 03/10/17.
//  Copyright © 2017 openkey. All rights reserved.
//

import Foundation
import CoreLocation

class LocationManager: NSObject, CLLocationManagerDelegate {
    
    static let shared = LocationManager()
    
    let locationManager = CLLocationManager()
    
    /// This is the clouser that will return the current Address through google login in
    var success: (_ addrees: Address,_ timeStamp:Date) -> Void = { _ in }
    
    /// This clouser will return the error
    var failure: (_ error: DIError) -> () = { _ in }
    
   fileprivate static var statusLocation : CLAuthorizationStatus = CLAuthorizationStatus.authorizedWhenInUse
    
    var permissionStatus: Bool {
        if LocationManager.statusLocation == .denied || LocationManager.statusLocation == .notDetermined || LocationManager.statusLocation == .restricted {
            return false
        }
        return true
    }
    
    //MARK: - Permission Checks
    private var isEnabled: Bool {
        switch CLLocationManager.authorizationStatus() {
        case .authorizedAlways, .authorizedWhenInUse: return true
        default: return false
        }
        
    }
    private var notDetermined: Bool {
        switch CLLocationManager.authorizationStatus() {
        case .notDetermined: return true
        default: return false
        }
    }
    
    func start() -> Void {
        locationManager.delegate = self
        locationManager.desiredAccuracy = kCLLocationAccuracyBest
        if isEnabled {
            locationManager.startUpdatingLocation()
        } else if notDetermined {
            request()
        } else {
            failure(DIError.locationPermissionDenied())
        }
    }
    func request() -> Void {
        locationManager.requestWhenInUseAuthorization()
    }
    
    //MARK:Location Manager Delegate
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        if let currentLocation: CLLocation = manager.location {
            getAddressFromLocation(location: currentLocation,timeStamp:(locations.last?.timestamp)!)
        } else {
            LocationManager.statusLocation = .denied
            self.failure(DIError.locationPermissionDenied())
        }
        locationManager.stopUpdatingLocation()
        locationManager.delegate = nil
        
    }
    
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        failure(DIError.unKnowError())
    }
    
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        switch status {
        case .notDetermined:
            LocationManager.statusLocation = .notDetermined
            // If status has not yet been determied, ask for authorization
            request()
            break
        case .authorizedWhenInUse:
            // If authorized when in use
            LocationManager.statusLocation = .authorizedWhenInUse
            manager.desiredAccuracy = kCLLocationAccuracyBest
            manager.startUpdatingLocation()
            break
        case .authorizedAlways:
            // If always authorized
            
            LocationManager.statusLocation = .authorizedAlways
            manager.desiredAccuracy = kCLLocationAccuracyBest
            manager.startUpdatingLocation()
            break
        case .restricted:
            LocationManager.statusLocation = .restricted
            failure(DIError.locationPermissionDenied())
        // If restricted by e.g. parental controls. User can't enable Location Services
            break
        case .denied:
            LocationManager.statusLocation = .denied
            failure(DIError.locationPermissionDenied())
            // If user denied your app access to Location Services, but can grant access from Settings.app
            break
        }
    }
    
    private func getAddressFromLocation(location: CLLocation,timeStamp:Date) {
        CLGeocoder().reverseGeocodeLocation(location, completionHandler: { (placemarks, error) -> Void in
            if error != nil {
                self.success(Address(location: location), timeStamp)
                return
            }
            if let placemark = placemarks?.first {
                self.success(Address(placemark: placemark),timeStamp)
            } else {
                self.success(Address(location: location), timeStamp)
            }
        })
        
    }
    
}
