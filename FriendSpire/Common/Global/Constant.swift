//
//  Constant.swift
//  BaseProject
//
//  Created by TpSingh on 22/03/17.
//  Copyright © 2017 openkey. All rights reserved.
//

import UIKit

let iPHONE10 : CGFloat = 1792.0

let ACCEPTABLE_CHARACTERS = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz_ 1234567890åøæñßàáâãäåæçèéêëìíîïðñòóôõöøùúûüýþÿŊŋŌōŎŏŐőŒœŔŕŖŗŘřŚśŜŝŞşŠšŢţŤťŦŧŨũŪūŬŭŮůŰűŲųŴŵŶŷŸŹźŻżŽžſȀȁȂȃȄȅȆȇȈȉȊȋȌȍȎȏȐȑȒȓȔȕȖȗȘșȚț"

let appName = Bundle.main.object(forInfoDictionaryKey: "CFBundleName") as? String ?? ""
let appVersion =  (Bundle.main.infoDictionary?["CFBundleVersion"]  as? String ?? "1.0") + " V"
let GOOGLEAPIKEY = "AIzaSyCk2-1jxP9_LTdVPLji_kFLmISKQA_wUr0"

//let linkedInUrl = NSString(string:"https://api.linkedin.com/v1/people/~:(id,industry,firstName,gender,,lastName,email,headline,summary,publicProfileUrl,specialties,positions:(id,title,summary,start-date,end-date,is-current,company:(id,name,type,size,industry,ticker)),pictureUrls::(original),location:(name))?format=json")
//let MOST_POPULAR_MOVIES = "https://api.trakt.tv/movies/popular?extended=full,images"
//let CLIENT_ID = "ad005b8c117cdeee58a1bdb7089ea31386cd489b21e14b19818c91511f12a086"
//let SEARCH = "https://api.trakt.tv/search/movie?type=movie&query="
// Bundle.main.object(forInfoDictionaryKey: "CFBundleDisplayName") as! String

var RECORDS_PER_PAGE =  Constant.PageLimit.localApi  // used for pagination
let MILES_VALUE = 1609.344
let KM_VALUE =  1000.0
let RadiusLimit = 20000.0 //meters
let DistanceDefaultKM = "20000.0" //meters
let DistanceDefaultMILE = "32186.88" //meters
let grayBckgroundColor =  UIColor.init(red: 224/255, green: 231/255, blue: 242/255, alpha: 1)
let DAY5 = 5
let NEWYORK_LAT = 40.7128
let NEWYORK_LNG = 74.0060
let DiscardDecimal = 10.0
let Tutorail = "tutorial"
let notificationNetwork = "NetworkRefresh"

let ARR_TUTORIAL =  [[KeyTutorial.image: #imageLiteral(resourceName: "slide1"),KeyTutorial.title:AppMessages.TutorialMessage.one],
                     [KeyTutorial.image: #imageLiteral(resourceName: "slide2"),KeyTutorial.title:AppMessages.TutorialMessage.two],
                     [KeyTutorial.image: #imageLiteral(resourceName: "slide3"),KeyTutorial.title:AppMessages.TutorialMessage.three],
                     [KeyTutorial.image: #imageLiteral(resourceName: "slide4"),KeyTutorial.title:AppMessages.TutorialMessage.four],
                     [KeyTutorial.image: #imageLiteral(resourceName: "slide5"),KeyTutorial.title:AppMessages.TutorialMessage.five],
                     [KeyTutorial.image: #imageLiteral(resourceName: "slide6"),KeyTutorial.title:AppMessages.TutorialMessage.six],
                     [KeyTutorial.image: #imageLiteral(resourceName: "slide7"),KeyTutorial.title:AppMessages.TutorialMessage.seven],
                     [KeyTutorial.image: #imageLiteral(resourceName: "slide8"),KeyTutorial.title:AppMessages.TutorialMessage.eight],
                     [KeyTutorial.image: #imageLiteral(resourceName: "slide10"),KeyTutorial.title:AppMessages.TutorialMessage.nine],
                     [KeyTutorial.image: #imageLiteral(resourceName: "slide11"),KeyTutorial.title:AppMessages.TutorialMessage.ten]]


class Constant {
    
    
    struct PageLimit {
        static let  searchThirdParty = 10
        static let localApi = 15
    }
    struct Validations {
        static let  limitName = 21
        static let  limitEmail = 45
        static let  limitPass = 17
        static let  minPass = 8
        static let  limitOtp = 7
        static let  limitReferal = 9
    }
    
    struct Time {
        let now = { round(NSDate().timeIntervalSince1970)} // seconds
    }
    
    
    
    struct AppColor {
        static let navigationColor = UIColor.init(red: 107/255, green: 72/255, blue:157/255, alpha: 1.0)
        static let navigationBarTintColor = UIColor.white
        static let navigationColorTextColor = UIColor.white
        
    }
    
    struct ButtonName {
        static let hide:String = "HIDE".localized
        static let show:String = "SHOW".localized
    }
    
    enum FacebookPermissions:String {
        case email = "email"
        case publicProfile = "public_profile"
        case birthday = "user_birthday"
        case friends = "user_friends"
    }
    
    
    /**
     This enum represent the button tags given in story board
     Desc : User profile consist of categories tag use to differentiate
     */
    enum CategoriesAvailableEnum:Int {
        case all = 10
        case movie = 20
        case tv = 30
        case book = 40
        case restraunt = 50
        case bar = 60
    }
    
    /**
     This enum is as per backend to map accordingly at front end
     */
    enum CategoryTypeBackend : Int {
        case restraunt = 1
        case bars = 2
        case movie = 3
        case tv = 4
        case books = 5
    }
    
    struct Xib {
        static let reviewsTabelCellXib:String = "ReviewsTabelCellXib"
        static let bookFilterHeaderView:String = "BookFilterHeaderView"
        static let doubleSliderTableCell:String = "DoubleSliderTableCell"
        static let singleSliderTableCell:String = "SingleSliderTableCell"
        static let restaurantFilterHeader:String = "RestaurantFilterHeader"
        static let emptyTableCell:String = "EmptyTableCell"
        static let customTextFieldCell:String = "CustomTextFieldCell"
        
        
    }
    struct TableviewCellIdentifier {
        static let reviewsTabelCellXib:String = "ReviewsTabelCellXib"
        static let singleSliderTableCell:String = "SingleSliderTableCell"
        static let doubleSliderTableCell:String = "DoubleSliderTableCell"
        static let searchBarTableCell:String = "SearchBarTableCell"
        static let sliderTableCell:String = "SliderTableCell"
        static let movieTableCell:String = "MovieTableCell"
        static let emptyTableCell:String = "EmptyTableCell"
        static let changePasswrodButtonCell:String = "ChangePasswrodButtonCell"
        static let notificationTableViewCell:String = "NotificationTableViewCell"
        static let editProfileTableViewCell:String = "EditProfileTableViewCell"
        static let blockedUserTableCell:String = "BlockedUserTableCell"
        
        
        
    }
    struct RatingFromThirdParty {
        static let imdb:String = "IMDB"
        static let goodReads:String = "GoodReads"
        static let fourSquare:String = "FourSquare"
        
        
    }
    
    enum priceLevel : String {
        case one = "$"
        case two = "$$"
        case three = "$$$"
        case four = "$$$$"
        
    }
    
    enum pushNotificationType : String {
        case accept = "1"
        case reject = "2"
        case requestSent = "4"
        case commentOnPost = "6"
        case dailyReview = "7"
        case replyOnComment = "8"
        
    }
    
    
    
}
