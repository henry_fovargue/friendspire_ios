//
//  FacebookHandler.swift
//  FriendSpire
//
//  Created by abhishek on 24/07/18.
//  Copyright © 2018 openkey. All rights reserved.
//

import Foundation
import FBSDKCoreKit
import FBSDKLoginKit

enum FBPermissions: String {
    case email = "email"
    case publicProfile = "public_profile"
    case birthday = "user_birthday"
    case publishAction = "publish_actions"
    case aboutMe = "user_about_me"
    case location = "location"
    case friends = "user_friends"
}

class FacebookManager:DIBaseController {
    
    // MARK: - SingleTon
    static let shared = FacebookManager()
    
    // MARK: - Setup SDK
    //    func application(application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Void {
    //        FBSDKApplicationDelegate.sharedInstance().application(application, didFinishLaunchingWithOptions: launchOptions)
    //    }
    //
    //    func activate() -> Void {
    //        FBSDKAppEvents.activateApp()
    //    }
    
    func application(_ application: UIApplication, open url: URL, options: [UIApplicationOpenURLOptionsKey: Any]) -> Bool {
        if #available(iOS 9.0, *) {
            return FBSDKApplicationDelegate.sharedInstance().application(application, open: url, sourceApplication: options[UIApplicationOpenURLOptionsKey.sourceApplication] as! String, annotation: options[UIApplicationOpenURLOptionsKey.annotation])
        }
        return true
    }
    // MARK: - Login
    func login(_ permissions: [FBPermissions], success: @escaping (_ user: User) -> (), failure: @escaping (_ error: DIError) -> (), onController controller: UIViewController) -> Void {
        let loginManager: FBSDKLoginManager = FBSDKLoginManager()
        loginManager.loginBehavior = .native
        loginManager.logOut()
        
        let fbPermissions = permissions.map {
            $0.rawValue
        }
        
        loginManager.logIn(withReadPermissions: fbPermissions, from: controller) { (result, error) in
            if let error = error {
                failure(DIError(error: error))
                return
            }
            guard let fbResult = result else {
                failure(DIError.noResponse())
                return
            }
            self.showLoader()
            if fbResult.isCancelled {
                failure(DIError.isCanceled())
                return
            }
            if fbResult.grantedPermissions.contains(FBPermissions.email.rawValue) {
                self.userDetail(success: {
                    success($0)
                }, failure: {
                    failure($0)
                })
            } else {
                failure(DIError.emailRequestDenied())
            }
            
        }
    }
    
    
    func userDetail(success: @escaping (_ user: User) -> (), failure: @escaping (_ error: DIError) -> ()) {
        if (FBSDKAccessToken.current() != nil) {
            let graphRequest = FBSDKGraphRequest(graphPath: "me", parameters: ["fields": "picture.redirect(false),id, first_name, last_name, email, birthday, gender,about, location,friends"])
            let connection = FBSDKGraphRequestConnection()
            connection.add(graphRequest, completionHandler: { (connection, result, error) -> Void in
                guard let data = result as? Parameters else {
                    failure(DIError.invalidData())
                    return
                }
                DILog.print(items: result ?? "")
                let user = User()
                let decoder = JSONDecoder()
                do{
                    let jasonData = try JSONSerialization.data(withJSONObject: data, options: .prettyPrinted)
                    let modal = try decoder.decode(FBFriendModal.self, from: jasonData)
                    
                    user.id = modal.id ?? ""
                    user.email = data["email"] as? String ?? ""
                    user.snsType = 2
                    user.snsID =  modal.id ?? ""
                    user.firstName =  modal.firstName ?? ""
                    user.lastName =  modal.lastName ?? ""
                    if modal.id != nil {
                        user.profilePicUrl = "http://graph.facebook.com/\(modal.id ?? "")/picture?type=large"
                    }else{
                        user.profilePicUrl =   modal.picture?.data?.url ?? ""
                    }
                }catch {
                    DILog.print(items:"decoding error: \(error)")
                    failure(DIError.fbUserNotFound())
                    
                }
                
                success(user)
            })
            connection.start()
        }else {
            //            failure(DIError.fbUserNotFound())
        }
    }
    
    func getFriendList(sucess: @escaping (_ list : [FBFriendModal]) ->() , failure : @escaping (_ error: Error?)->()  ) {
        
        
        
        if (getToken() != nil) {
            let params = ["fields": "id, first_name, last_name, name, email, picture"]
            
            let graphRequest = FBSDKGraphRequest(graphPath: "/me/friends", parameters: params)
            let connection = FBSDKGraphRequestConnection()
            connection.add(graphRequest, completionHandler: { (connection, result, error) in
                if error == nil {
                    if let userData = result as? [String:Any] {
                        var userList = [FBFriendModal]()
                        let decoder = JSONDecoder()
                        do{
                            guard let arrFrnds =  userData["data"] as? NSArray else {return}
                            
                            if arrFrnds.count > 0 {
                                for obj in arrFrnds{
                                    let jasonData = try JSONSerialization.data(withJSONObject: obj, options: .prettyPrinted)
                                    let modal = try decoder.decode(FBFriendModal.self, from: jasonData)
                                    userList.append(modal)
                                }
                            }
                            
                        }catch {
                            DILog.print(items:"decoding error: \(error)")
                        }
                        
                        DILog.print(items: userData)
                        sucess(userList)
                    }
                } else {
                    DILog.print(items:"Error Getting Friends \(error.debugDescription)")
                    failure(error)
                }
                
            })
            
            connection.start()
        }
    }
    
    func getToken() -> FBSDKAccessToken?{
        return  FBSDKAccessToken.current()
    }
}



