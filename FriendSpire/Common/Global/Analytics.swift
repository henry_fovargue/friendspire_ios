//
//  Analytics.swift
//  FriendSpire
//
//  Created by shubam on 30/08/18.
//  Copyright © 2018 openkey. All rights reserved.
//

import Foundation
import UIKit
struct Analytics {
    static func trackEvent(withScreen screen: Screen, category: String, label: String, action: Actions, value: Int? = nil) {
        guard
            let tracker = GAI.sharedInstance().defaultTracker,
            let builder = GAIDictionaryBuilder.createEvent(withCategory: category, action: action.rawValue, label: label, value: NSNumber(integerLiteral: value ?? 0))
            else { return }
        
        tracker.set(kGAIScreenName, value: screen.rawValue)
        tracker.send(builder.build() as [NSObject : AnyObject])
    }
    
    static func trackPageView(withScreen screen: String) {
        guard
            let tracker = GAI.sharedInstance().defaultTracker,
            let builder = GAIDictionaryBuilder.createScreenView()
            else { return }
        
        tracker.set(kGAIScreenName, value: screen)
        tracker.send(builder.build() as [NSObject : AnyObject])
    }
    
    enum Actions: String {
        case search = "Search"
        case tap = "Tap"
        case toggle = "Toggle"
    }
    
    enum Screen: String {
        case exampleScreenName = "exampleScreenName"
        case setting = "SettingViewController"
    }
}
