//
//  DILoadingView.swift
//  BaseProject
//
//  Created by narinder on 28/02/17.
//  Copyright © 2017 openkey. All rights reserved.
//

import Foundation
import UIKit

@IBDesignable
class DILoadingView : UIView {
  
  override var layer: CAShapeLayer {
    get {
      return super.layer as! CAShapeLayer
    }
  }
  
  override class var layerClass: AnyClass {
    return CAShapeLayer.self
  }
  
  override func layoutSubviews() {
    super.layoutSubviews()
    layer.fillColor = nil
    layer.strokeColor = UIColor.black.cgColor
    layer.lineWidth = 10
    setPath()
  }
  
  override func didMoveToWindow() {
    animate()
  }
  
  private func setPath() {
   DILog.print(items: layer.lineWidth / 2)
    layer.path = UIBezierPath(ovalIn: bounds.insetBy(dx: layer.lineWidth/2, dy: layer.lineWidth/2)).cgPath
  }
  
  struct Pose {
    let secondsSincePriorPose: CFTimeInterval
    let start: CGFloat
    let length: CGFloat
    init(_ secondsSincePriorPose: CFTimeInterval, _ start: CGFloat, _ length: CGFloat) {
      self.secondsSincePriorPose = secondsSincePriorPose
      self.start = start
      self.length = length
    }
  }
  
  class var poses: [Pose] {
    get {
      return [
        Pose(0.0, 0.000, 0.7),
        Pose(0.6, 0.500, 0.5),
        Pose(0.6, 1.000, 0.3),
        Pose(0.6, 1.500, 0.1),
        Pose(0.2, 1.875, 0.1),
        Pose(0.2, 2.250, 0.3),
        Pose(0.2, 2.625, 0.5),
        Pose(0.2, 3.000, 0.7),
      ]
    }
  }
  
  func animate() {
    var time: CFTimeInterval = 0
    var times = [CFTimeInterval]()
    var start: CGFloat = 0
    var rotations = [CGFloat]()
    var strokeEnds = [CGFloat]()
    
    let poses = type(of: self).poses
    let totalSeconds = poses.reduce(0) { $0 + $1.secondsSincePriorPose }
    
    for pose in poses {
      time += pose.secondsSincePriorPose
      times.append(time / totalSeconds)
      start = pose.start
      rotations.append(start * 2 * CGFloat(Double.pi))
      strokeEnds.append(pose.length)
    }
    
    times.append(times.last!)
    rotations.append(rotations[0])
    strokeEnds.append(strokeEnds[0])
    
    animateKeyPath(keyPath: "strokeEnd", duration: totalSeconds, times: times, values: strokeEnds)
    animateKeyPath(keyPath: "transform.rotation", duration: totalSeconds, times: times, values: rotations)
    
    animateStrokeHueWithDuration(duration: totalSeconds * 5)
  }
  
  
  func animateKeyPath(keyPath: String, duration: CFTimeInterval, times: [CFTimeInterval], values: [CGFloat]) {
    let animation = CAKeyframeAnimation(keyPath: keyPath)
    animation.keyTimes = times as [NSNumber]?
    animation.values = values
    animation.calculationMode = kCAAnimationLinear
    animation.duration = duration
    animation.repeatCount = Float.infinity
    layer.add(animation, forKey: animation.keyPath)
  }
  
  
  func animateStrokeHueWithDuration(duration: CFTimeInterval) {
    let count = 36
    let animation = CAKeyframeAnimation(keyPath: "strokeColor")
    animation.keyTimes = (0 ... count).map { NSNumber(value: CFTimeInterval($0) / CFTimeInterval(count)) }
    animation.values = (0 ... count).map {
      UIColor(hue: CGFloat($0) / CGFloat(count), saturation: 1, brightness: 1, alpha: 1).cgColor
    }
    animation.duration = duration
    animation.calculationMode = kCAAnimationLinear
    animation.repeatCount = Float.infinity
    layer.add(animation, forKey: animation.keyPath)
  }
  
}



class DILoader: UIView {
  
  var diLoader:DILoadingView!
  var mainView:UIView!
  
  override init(frame: CGRect) {
    super.init(frame: frame)
    self.backgroundColor = UIColor(colorLiteralRed: 0, green: 0, blue: 0, alpha: 0.2)
    
   createMainView()
  }
  
  private func createMainView(){
    mainView = UIView(frame: CGRect(x: 0, y: 0, width: 120, height: 120))
    mainView.layer.cornerRadius = 10
    mainView.backgroundColor = UIColor.white
    
    self.addSubview(mainView)
    
    mainView.layer.shadowColor = UIColor(colorLiteralRed: 0, green: 0, blue: 0, alpha: 0.4).cgColor
    mainView.layer.shadowOpacity = 1
    mainView.layer.shadowOffset = CGSize.zero
    mainView.layer.shadowRadius = 10
    mainView.layer.shadowPath = UIBezierPath(rect: mainView.bounds).cgPath
    mainView.layer.shouldRasterize = true
    mainView.center = self.center
    
    showLoader(onView: mainView)
  }
  
  required init?(coder aDecoder: NSCoder) {
    fatalError("init(coder:) has not been implemented")
  }
  
  /*
   Show loader will add the custom loader view to current controller
   */
  func showLoader(onView view: UIView) {
    
    diLoader =  DILoadingView(frame: CGRect(x: 0, y: 0, width: 60, height: 60))
    let xpos = (view.frame.size.width/2.0) - (diLoader.frame.size.width/2.0)
    let ypos = (view.frame.size.height/2.0) - (diLoader.frame.size.width/2.0)
    diLoader.frame =  CGRect(x: xpos, y: ypos, width: 60, height: 60)
    view.addSubview(diLoader)
    diLoader.animate()
  }
  
}
