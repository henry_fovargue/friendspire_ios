//
//  UIView+Extension.swift
//  BaseProject
//
//  Created by Aj Mehra on 09/03/17.
//  Copyright © 2017 openkey. All rights reserved.
//

import Foundation
import UIKit

extension UIView {
    @IBInspectable
    var cornerRadius: CGFloat {
        get {
            return layer.cornerRadius
        }
        set {
            layer.cornerRadius = newValue
            layer.masksToBounds = newValue > 0
            
        }
    }
    
    @IBInspectable
    var borderWidth: CGFloat {
        get {
            return layer.borderWidth
        }
        set {
            layer.borderWidth = newValue
        }
    }
    
    @IBInspectable
    var borderColor: UIColor? {
        get {
            let color = UIColor.init(cgColor: layer.borderColor!)
            return color
        }
        set {
            layer.borderColor = newValue?.cgColor
        }
    }
    
    @IBInspectable
    var shadowRadius: CGFloat {
        get {
            return layer.shadowRadius
        }
        set {
            layer.shadowColor = UIColor.lightGray.cgColor
            layer.shadowOffset = CGSize(width: 0, height: 1)
            layer.shadowOpacity = 0.4
            layer.shadowRadius = shadowRadius
            if self.isKind(of: UITextField.self){
            self.layer.masksToBounds = false
            }
        }
    }
    @IBInspectable
    var shadowOpacity: CGFloat {
        get {
            return CGFloat(layer.shadowOpacity)
        }
        set {
                layer.shadowOpacity = Float(shadowOpacity)
        }
    }
    @IBInspectable
    var shadowOffsetY: CGFloat {
        get {
            return CGFloat(layer.shadowOffset.height)
        }
        set {
            layer.shadowOffset.height = shadowOffsetY
        }
    }
    
    static var identifier: String {
        return String(describing: self)
    }
    
    @IBInspectable
    var shadowColor: UIColor {
        get {
            return UIColor.gray
        }
        set {
           layer.shadowColor = shadowColor.cgColor
        }
    }
    
    
    
//    func addGradient(colors : [CGColor]){
//        
//        let gradient = CAGradientLayer()
//        gradient.colors = colors
//        gradient.frame = self.bounds
//        gradient.startPoint = CGPoint.zero
//        gradient.endPoint = CGPoint.init(x: 1.0, y: 1.0)
//        self.layer.insertSublayer(gradient, at: 0)
//    }
}

class GradientView: UIView {
    override open class var layerClass: AnyClass {
        return CAGradientLayer.classForCoder()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        let gradientLayer = layer as! CAGradientLayer
        gradientLayer.startPoint = CGPoint.init(x: 0.0, y: 1.0)
        gradientLayer.endPoint = CGPoint.init(x: 1.0, y: 1.0)
        gradientLayer.colors = [#colorLiteral(red: 0.9996038079, green: 0.2221312225, blue: 0.4596621394, alpha: 1).cgColor, #colorLiteral(red: 0.9994639754, green: 0.4576926827, blue: 0.3536314666, alpha: 1).cgColor]
    }
}
class GradientButton: UIButton {
    override open class var layerClass: AnyClass {
        return CAGradientLayer.classForCoder()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        let gradientLayer = layer as! CAGradientLayer
        gradientLayer.startPoint = CGPoint.init(x: 0.0, y: 1.0)
        gradientLayer.endPoint = CGPoint.init(x: 1.0, y: 1.0)
        gradientLayer.colors = [#colorLiteral(red: 0.9996038079, green: 0.2221312225, blue: 0.4596621394, alpha: 1).cgColor, #colorLiteral(red: 0.9994639754, green: 0.4576926827, blue: 0.3536314666, alpha: 1).cgColor]
    }
}
