//
//  File.swift
//  FriendSpire
//
//  Created by abhishek on 18/09/18.
//  Copyright © 2018 openkey. All rights reserved.
//

import Foundation

extension UICollectionView {
    
    func setEmptyMessage(_ message: String) {
        let messageLabel = UILabel(frame: CGRect(x: 0, y: 0, width: self.bounds.size.width, height: self.bounds.size.height))
        messageLabel.text = message
        messageLabel.textColor =  #colorLiteral(red: 0.7302808762, green: 0.7317310572, blue: 0.7744688392, alpha: 1)
        messageLabel.numberOfLines = 0;
        messageLabel.textAlignment = .center;
        messageLabel.font = UIFont.init(name: "SFUIText-Regular", size: 13.0)
        messageLabel.sizeToFit()
        
        self.backgroundView = messageLabel;
    }
    
    func restore() {
        self.backgroundView = nil
    }
}
