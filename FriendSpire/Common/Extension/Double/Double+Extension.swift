//
//  Double+Extension.swift
//  FriendSpire
//
//  Created by abhishek on 23/08/18.
//  Copyright © 2018 openkey. All rights reserved.
//

import Foundation

extension Double{
    
    //this will remove 0 after decimal if any
    var clean: String {
        return  self.truncatingRemainder(dividingBy: 1) == 0 ? String(format: "%.0f", self) : String(format: "%.1f", self)
    }
    
    func getDistanceString()->String{
        
        var strDistance = ""
        if  DistanceUnit.Miles.rawValue ==  User.sharedInstance.objAttributes?.distanceUnit.rawValue ?? User.sharedInstance.distanceUnit{
//            print("MilesConversion")
            let distance = ceil((self/MILES_VALUE)*10)/10
            if distance > DiscardDecimal{
                strDistance = String(format: "%.0f", distance) + " Miles"
            }else{
                strDistance = distance.clean + " Miles"
            }
        }else {
//            print("KMConversion")
            let distance = ceil((self/KM_VALUE)*10)/10
            if distance > DiscardDecimal{
                strDistance = String(format: "%.0f", distance) + " Kms"
            }else {
                strDistance = distance.clean + " Kms"
            }
        }
//        print("self value =-#$@#$#$ \(self)")
//        print("after conversion ======---- \(strDistance)")
        return strDistance
    }
}

