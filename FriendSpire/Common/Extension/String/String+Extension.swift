//
//  String+Extension.swift
//  BaseProject
//
//  Created by Aj Mehra on 09/03/17.
//  Copyright © 2017 Debut Infotech. All rights reserved.
//

import Foundation

extension String {
    /// This variable will holds the length of the string.
    var length: Int {
        return self.characters.count
    }
    
    /// This will check whether a string is empty or not by remove all spaces
    var isBlank: Bool {
        get {
            let trimmed = trimmingCharacters(in: CharacterSet.whitespaces)
            return trimmed.isEmpty
        }
    }
    /// This will check wheher a string is a valid email or not.
    var isValidEmail: Bool {
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,6}"
        let emailTest = NSPredicate(format: "SELF MATCHES %@", emailRegEx)
        return emailTest.evaluate(with: self)
        
    }
    
    func capitalizeFirst() -> String {
        let firstIndex = self.index(startIndex, offsetBy: 1)
        return self.substring(to: firstIndex).capitalized + self.substring(from: firstIndex).lowercased()
    }
    
    var htmlToAttributedString: NSMutableAttributedString? {
        
        let modifiedString = String(format:"<span style=\"font-family: '-apple-system', '\(UIFont.sfUiTextRegular)';color: '#4A4A4A' ; font-size: 13.0\">%@</span>", self)
        guard let data = modifiedString.data(using: .utf8) else { return NSMutableAttributedString() }
        do {
            let attributeString = try NSMutableAttributedString(data: data, options: [NSDocumentTypeDocumentAttribute: NSHTMLTextDocumentType, NSCharacterEncodingDocumentAttribute:String.Encoding.utf8.rawValue], documentAttributes: nil)
            let range = NSRange(location: 0, length: attributeString.length)
            attributeString.addAttributes([NSForegroundColorAttributeName : UIColor.lightGrayTextColor], range: range)
            return attributeString
        } catch {
            return NSMutableAttributedString()
        }
    }
    
    var isValidPassword: Bool {
        
        
        
        let PASS_REGEX =  "^(?=.{8,16}$)(?=(.*?[a-z].*?[A-Z])|(.*?[A-Z].*?[a-z])|(.*?[a-z].*?[0-9])|(.*?[0-9].*?[a-z])|(.*?[a-z].*?[!@#$%^&*()_+])|(.*?[!@#$%^&*()_+].*?[a-z])|(.*?[A-Z].*?[0-9])|(.*?[0-9].*?[A-Z])|(.*?[A-Z].*?[!@#$%^&*()_+])|(.*?[!@#$%^&*()_+].*?[A-Z])|(.*?[0-9].*?[!@#$%^&*()_+])|(.*?[!@#$%^&*()_+].*?[0-9].)).*$"
        let passwordTest = NSPredicate(format: "SELF MATCHES %@", PASS_REGEX)
        return passwordTest.evaluate(with: self)
        
        //        if self.characters.count > 7 && self.characters.count < 17{
        //            return true
        //        }
        //        return false
    }
    
    func isValidInRange(minLength min: Int = 7, maxLength max: Int)-> Bool {
        if self.characters.count > min && self.characters.count < max {
            return true
        }
        return false
    }
    
    func replace(_ string: String, replacement: String) -> String {
        return self.replacingOccurrences(of: string, with: replacement, options: NSString.CompareOptions.literal, range: nil)
    }
    
    var trim: String {
        return self.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines)
    }
    func condensed() -> String {
        let components = self.components(separatedBy: NSCharacterSet.whitespacesAndNewlines)
        return components.filter { !$0.isEmpty }.joined(separator: " ")
    }
    
    var trimmed: String {
        let components = self.components(separatedBy: NSCharacterSet.whitespacesAndNewlines)
        return components.filter { !$0.isEmpty }.joined(separator: " ")
    }
    
    var localized: String {
        return NSLocalizedString(self, tableName: nil, bundle: Bundle.main, value: "", comment: "")
        
    }
    
    func addDeleimeter() -> String {
        
        let components = self.components(separatedBy: NSCharacterSet.whitespaces)
        return components.filter { !$0.isEmpty }.joined(separator: "%20")
    }
    
    func allowSpecialChar() -> String {
        
        let components = self.components(separatedBy: NSCharacterSet.whitespaces)
        return components.filter { !$0.isEmpty }.joined(separator: "%20")
    }
    
    func removeZero() ->String{
        
//        guard let val =  Double(self) else {
            return self.replace("0.0", replacement: "-")
//        }
//        var str = val.clean
//        str = str.replace("0.0", replacement: "-")
//        if str.count == 1{
//            str = str.replace("0", replacement: "-")
//        }
//        return str
    }
    
    func inserting(separator: String, every n: Int) -> String {
        var result: String = ""
        let characters = Array(self.characters)
        stride(from: 0, to: characters.count, by: n).forEach {
            result += String(characters[$0..<min($0+n, characters.count)])
            if $0+n < characters.count {
                result += separator
            }
        }
        return result
    }
    
    func height(withConstrainedWidth width: CGFloat, font: UIFont) -> CGFloat {
        let constraintRect = CGSize(width: width, height: .greatestFiniteMagnitude)
        let boundingBox = self.boundingRect(with: constraintRect, options: .usesLineFragmentOrigin, attributes: [NSFontAttributeName: font], context: nil)
        
        return ceil(boundingBox.height)
    }
    
    func width(withConstrainedHeight height: CGFloat, font: UIFont) -> CGFloat {
        let constraintRect = CGSize(width: .greatestFiniteMagnitude, height: height)
        let boundingBox = self.boundingRect(with: constraintRect, options: .usesLineFragmentOrigin, attributes: [NSFontAttributeName: font], context: nil)
        
        return ceil(boundingBox.width)
    }
    
    
    
    //MARK:-ConvertDate From Default format to specific format.
    func convertDate(_ withFormat: DateFormat = .preDefined, toFormat: DateFormat = .display) -> String {
        let dateFormator = DIDateFormator.format(dateFormat: withFormat)
        guard let date = dateFormator.date(from: self) else {
            return "NA"
        }
        dateFormator.dateFormat = toFormat.format
        let dateToSend = dateFormator.string(from: date)
        return dateToSend == "" ? "NA" : dateToSend
    }
    
    func convertDateLocal(_ withFormat: DateFormat = .preDefined, toFormat: DateFormat = .display) -> String {
        let dateFormator = DIDateFormator.format(dateFormat: withFormat)
        guard let date = dateFormator.date(from: self) else {
            return "NA"
        }
//        dateFormator.dateFormat = toFormat.format
//        let dateToSend = dateFormator.string(from: date)
        return date.UTCToLocal(requiredFormat: toFormat)
    }
    
    func getDate(_ withFormat: DateFormat = .serverFormat) -> Date?{
        let dateFormator = DIDateFormator.format(dateFormat: withFormat)
        guard let date = dateFormator.date(from: self) else {
            return nil
        }
        return date
    }
    
    func onlyDigits(_ withFormat: DateFormat = .preDefined) -> String {
        let filtredUnicodeScalars = unicodeScalars.filter{CharacterSet.decimalDigits.contains($0)}
        return String(String.UnicodeScalarView(filtredUnicodeScalars))
    }
    
    
    //MARK:-Method for Data Parsing.
    var html2String : NSAttributedString? {
        return decodeString(encodedString: self)
    }
    
    func decodeString(encodedString:String) -> NSAttributedString?{
        guard let encodedData = encodedString.data(using: .utf8) else{
            return nil
        }
        do {
            let outputStr = try NSAttributedString(data: encodedData, options: [NSDocumentTypeDocumentAttribute: NSHTMLTextDocumentType, NSCharacterEncodingDocumentAttribute: String.Encoding.utf8.rawValue], documentAttributes: nil)
            return outputStr
        } catch ( _) {
            return nil
        }
    }
    
    
}

protocol OptionalString { }
extension String: OptionalString { }

extension Optional where Wrapped: OptionalString {
    var isBlankOption: Bool {
        guard var value = self as? String else { return true }
        value = value.trimmed
        return value.isEmpty
    }
    
    //    var isvalidEmail: Bool {
    //        guard let email = self as?String else { return false }
    //        return email.isValidEmail
    //    }
    
    
}


extension URL {
    func valueOf(_ queryParamaterName: String) -> String? {
        guard let url = URLComponents(string: self.absoluteString) else { return nil }
        return url.queryItems?.first(where: { $0.name == queryParamaterName })?.value
    }
}
