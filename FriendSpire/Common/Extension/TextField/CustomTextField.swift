//
//  CustomTextField.swift
//  BaseProject
//
//  Created by TpSingh on 11/04/17.
//  Copyright © 2017 openkey. All rights reserved.
//

import UIKit

@IBDesignable
class CustomTextField: UITextField {

  override func canPerformAction(_ action: Selector, withSender sender: Any?) -> Bool {
//  action ==  #selector(UIResponderStandardEditActions.paste(_:)) ||
    if action == #selector(UIResponderStandardEditActions.cut(_:)){
        return false
      }
      return super.canPerformAction(action, withSender: sender)
    }
    
    // Provides left padding for images
    override func leftViewRect(forBounds bounds: CGRect) -> CGRect {
        var textRect = super.leftViewRect(forBounds: bounds)
        textRect.origin.x += leftPadding
        return textRect
    }
    
    @IBInspectable var leftImage: UIImage? {
        didSet {
            updateView()
        }
    }
    
    @IBInspectable var leftPadding: CGFloat = 0
    
    @IBInspectable var color: UIColor = UIColor.lightGray {
        didSet {
            updateView()
        }
    }
    
    func updateView() {
        if let image = leftImage {
            leftViewMode = UITextFieldViewMode.always
           let view = UIView.init(frame: CGRect(x: 10, y: 0, width: 30, height: 20))
            let imageView = UIImageView(frame: CGRect(x: 8, y: 2, width: 15, height: 15))
            imageView.contentMode = .scaleAspectFit
            imageView.image = image
            // Note: In order for your image to use the tint color, you have to select the image in the Assets.xcassets and change the "Render As" property to "Template Image".
            imageView.tintColor = color
            view.addSubview(imageView)
            leftView = view
        } else {
            leftViewMode = UITextFieldViewMode.never
            leftView = nil
        }
        
    }
}


private var kAssociationKeyMaxLength: Int = 0
extension UITextField:UITextFieldDelegate{
    @IBInspectable var maxLength: Int {
        get {
            if let length = objc_getAssociatedObject(self, &kAssociationKeyMaxLength) as? Int {
                return length
            } else {
                return Int.max
            }
        }
        set {
            objc_setAssociatedObject(self, &kAssociationKeyMaxLength, newValue, .OBJC_ASSOCIATION_RETAIN)
            addTarget(self, action: #selector(checkMaxLength), for: .editingChanged)
        }
    }
    
    @objc func checkMaxLength(textField: UITextField) {
        guard let prospectiveText = self.text,
            prospectiveText.characters.count > maxLength
            else {
                return
        }
        
        let selection = selectedTextRange
        let maxCharIndex = prospectiveText.index(prospectiveText.startIndex, offsetBy: maxLength)
        text = prospectiveText.substring(to: maxCharIndex)
        selectedTextRange = selection
    }
    
}

extension UITextField{
    func bottomLine(){
        let border = CALayer()
        let width = CGFloat(2.0)
        border.borderColor = UIColor.darkGray.cgColor
        border.frame = CGRect(x: 0, y: self.frame.size.height - width, width: self.frame.size.width, height: self.frame.size.height)
        
        border.borderWidth = width
        self.layer.addSublayer(border)
        self.layer.masksToBounds = true
    }
}
