//
//  UITableView+Extension.swift
//  OneSchool
//
//  Created by Dhiraj on 06/06/18.
//  Copyright © 2018 DebutInfotech. All rights reserved.
//

import Foundation
import Foundation
import UIKit
extension UIFont {
    static let sfUiTextRegular = "sf-ui-text-regular-58646b56a688c"
    static let sfUiTextBold = "SFUIText-Bold"
    static let sfUiTextSemibold = "SFUIText-SemiBold"
}
extension UITableView {
    func pullToRefresh(_ vc: UIViewController, callBack: @escaping () -> Void) {
        let animator = CircleRefreshAnimator(frame: CGRect(x: 0, y: 0, width: 24, height: 24))//ArrowRefreshAnimator(frame: CGRect(x: 0, y: 0, width: 24, height: 24))
        addPullToRefresh(66.0, animator: animator) {
            callBack()
        }
    }
    
    func endRefresh() {
        endRefreshing()
    }
    func endInfiniteScroll() {
        endInfiniteScrolling()
    }
    func enableInfiniteScrolling(status: Bool) {
        enableInfiniteScroll = status
    }
    
    func endPull2RefreshAndInfiniteScrolling(count: Int) {
        
        if count > 0 && count % RECORDS_PER_PAGE == 0 {
            enableInfiniteScroll = true
        } else {
            enableInfiniteScroll = false
        }
        DispatchQueue.main.async {
        self.endRefreshing()
        self.endInfiniteScrolling()
        self.reloadData()
        }
    }
    
    
    func endReviewRefeshing(partion:Int,count:Int){
        if partion == 1 || (count % RECORDS_PER_PAGE == 0 && count > 0)  {
            enableInfiniteScroll = true
        } else {
            enableInfiniteScroll = false
        }
        DispatchQueue.main.async {

        self.endRefreshing()
        self.endInfiniteScrolling()
        self.reloadData()
        }
    }
    
    func infiniteScrolling(_ vc: UIViewController, callBack: @escaping () -> Void) {
        let animator = DefaultInfiniteAnimator(frame: CGRect(x: 0, y: 0, width: 30, height: 30))
        addInfiniteScroll(60, animator: animator) {
            callBack()
        }
    }
    
    func showEmptyScreen(_ message: String, errorIcon: UIImage = #imageLiteral(resourceName: "no-result") ,with color:UIColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)) {
        
        backgroundView = UIView(frame:CGRect(x: 0, y: 0, width: self.bounds.size.width, height: self.bounds.size.height))
        
        let emptyTextImg : UIImageView = UIImageView(frame: CGRect(x: self.bounds.size.width/2 - 45, y: self.bounds.size.height/2 - 120, width: 90, height: 50))
        emptyTextImg.contentMode = .scaleAspectFill
        emptyTextImg.image = errorIcon.withRenderingMode(.alwaysTemplate)
        emptyTextImg.tintColor = color
        self.backgroundView?.addSubview(emptyTextImg)
        
        let noDataLabel: UILabel = UILabel(frame: CGRect(x: 10, y: emptyTextImg.frame.maxY, width: self.bounds.size.width - 20, height: 100))
        noDataLabel.text = message
        noDataLabel.textColor = color
        noDataLabel.font = UIFont.init(name: "SFUIText-Regular", size: 17.0)
        noDataLabel.textAlignment = .center
        noDataLabel.numberOfLines = 0
        self.backgroundView?.addSubview(noDataLabel)
        
        self.separatorStyle = .none
    }
    
    func showEmptyScreen(withMessage message: String) {
        let rect = CGRect(x: 0, y: 0, width: self.bounds.size.width, height: self.bounds.size.height)
        let messageLabel = UILabel(frame: rect)
        messageLabel.text = message
        messageLabel.textColor = UIColor.ArrowLightGray
        messageLabel.numberOfLines = 0
        messageLabel.textAlignment = .center
        messageLabel.font = UIFont(name: UIFont.sfUiTextRegular, size: 13.0)
        messageLabel.sizeToFit()
        self.backgroundView = messageLabel
        self.separatorStyle = .none
    }
    
    func setEmptyMessage(_ message: String) {
        
        let messageLabel = UILabel(frame: CGRect(x: 0, y: 0, width: self.bounds.size.width, height: self.bounds.size.height))
        messageLabel.text = message
        messageLabel.textColor = #colorLiteral(red: 0.7302808762, green: 0.7317310572, blue: 0.7744688392, alpha: 1)
        messageLabel.numberOfLines = 0;
        messageLabel.textAlignment = .center;
        messageLabel.font = UIFont.init(name: "SFUIText-Regular", size: 13.0)
        messageLabel.sizeToFit()
        
        self.backgroundView = messageLabel;
        self.separatorStyle = .none;
    }
    
    func restore() {
        self.backgroundView = nil
        self.separatorStyle = .singleLine
    }
    
    
        
//        public func reloadData(_ completion: @escaping ()->()) {
//            UIView.animate(withDuration: 0, animations: {
//                self.reloadData()
//            }, completion:{ _ in
//                completion()
//            })
//        }
    
//        func scroll(to: scrollsTo, animated: Bool) {
//            DispatchQueue.main.asyncAfter(deadline: .now() + .milliseconds(300)) {
//                let numberOfSections = self.numberOfSections
//                let numberOfRows = self.numberOfRows(inSection: numberOfSections-1)
//                switch to{
//                case .top:
//                    if numberOfRows > 0 {
//                        let indexPath = IndexPath(row: 0, section: 0)
//                        self.scrollToRow(at: indexPath, at: .top, animated: animated)
//                    }
//                    break
//                case .bottom:
//                    if numberOfRows > 0 {
//                        let indexPath = IndexPath(row: numberOfRows-1, section: (numberOfSections-1))
//                        self.scrollToRow(at: indexPath, at: .bottom, animated: animated)
//                    }
//                    break
//                }
//            }
//        }
        
//        enum scrollsTo {
//            case top,bottom
//        }
    
}
