//
//  Date+Extension.swift
//  Omakase
//
//  Created by Harpreet on 03/10/17.
//  Copyright © 2017 openkey. All rights reserved.
//

import Foundation

enum DateFormat {
    case preDefined
    case year
    case serverFormat
    case editProfiledisplay
    case editProfileBackend
    case reviewsDate
    
  case display
    // "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'"
    
    var format: String {
        switch self {
        case .preDefined: return "yyyy-MM-dd'T'HH:mm:ss.000'Z'"
        case .serverFormat: return "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'"
        case .display: return "d MMMM yyyy"
        case .year: return "yyyy"
        case .editProfiledisplay: return "MMM dd yyyy"
        case .editProfileBackend: return "yyyy/MM/dd"
        case .reviewsDate : return "d MMM, yyyy"
        }
    }
}
extension Date {
    
    func interval(ofComponent comp: Calendar.Component, fromDate date: Date) -> Int {
        
        let currentCalendar = Calendar.current
        
        guard let start = currentCalendar.ordinality(of: comp, in: .era, for: date) else { return 0 }
        guard let end = currentCalendar.ordinality(of: comp, in: .era, for: self) else { return 0 }
        
        return end - start
    }
    
    func getFileNameWithDate() -> String {
        let formatter = DateFormatter()
        // initially set the format based on your datepicker date
        formatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        
        let myString = formatter.string(from: Date())
        // convert your string to date
        let yourDate = formatter.date(from: myString)
        //then again set the date format whhich type of output you need
        formatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        // again convert your date to string
        let myStringafd = formatter.string(from: yourDate!)
        return myStringafd
    }
    
    func string(requiredFormat: DateFormat = .preDefined) -> String {
        let dateFormator = DIDateFormator.format(dateFormat: requiredFormat)
        // dateFormator.doesRelativeDateFormatting = true
        return dateFormator.string(from: self)
    }
    func UTCToLocal(requiredFormat: DateFormat = .display) -> String {
        let dateFormatter = DIDateFormator.format(dateFormat: requiredFormat)
        dateFormatter.timeZone = NSTimeZone.local
        return dateFormatter.string(from: self)
    }
    
    func UTCToTestLocal(requiredFormat: DateFormat = .display) -> String {
        let dateFormatter = DIDateFormator.format(dateFormat: requiredFormat)
        dateFormatter.timeZone = TimeZone(abbreviation: "UTC")
        return dateFormatter.string(from: self)
    }
  
    
}



class DIDateFormator:NSObject {
    
    class func format(dateFormat:DateFormat = .preDefined) -> DateFormatter {
        let dateFormatter = DateFormatter()
        dateFormatter.timeZone = TimeZone(identifier: "UTC")
        dateFormatter.dateFormat = dateFormat.format
        dateFormatter.locale =  Locale(identifier: "en")
        return dateFormatter
    }
    
    class func langGenericFormat(dateFormat:DateFormat = .preDefined) -> DateFormatter {
        let dateFormatter = DateFormatter()
        dateFormatter.timeZone = TimeZone(identifier: "UTC")
        dateFormatter.dateFormat = dateFormat.format
        let lang =  UserDefaults.standard.string(forKey: "i18n_language") ?? "en"
        dateFormatter.locale =  Locale(identifier: lang)
        return dateFormatter
    }
    
}

