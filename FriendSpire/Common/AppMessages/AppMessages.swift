//
//  AppMessages.swift
//
//
//  Created by Aj Mehra on 14/10/16.
//  Copyright © 2016 Debut InfoTech. All rights reserved.
//

import Foundation
struct AppMessages {
    
    struct Unkown {
        static let title:String = "".localized
        static let message:String = "Sorry, something went wrong. We're working on getting this fixed as soon as we can.".localized
        static let camera:String = "Friendspire app is not authorized to use camera.".localized
        static let gallery : String = "Friendspire app is not authorized to use gallery images.".localized
    }
    
    
    
    struct AlertTitles {
        static let Ok:String = "OK".localized
        static let Cancel:String = "CANCEL".localized
        static let Yes:String = "Yes".localized
        static let Report:String = "report".localized
        static let No:String = "No".localized
        static let Alert:String = "Alert".localized
        static let logoutMessage:String = "logoutMessage".localized
        static let deleteAccount:String = "deleteAccountMeesage".localized
        static let pleaseWait:String = "pleaseWait".localized
        static let chooseGender:String = "chooseGender".localized
        static let genderMessage:String = "genderMessage".localized
        static let actionSheet:String = "actionSheet".localized
        static let unfollow:String = "unfollow".localized
        static let removeFlowers:String = "removeFlowers".localized
        static let removeBookMark:String = "removeBookmark".localized
        static let locationOff:String = "locationOff".localized
         static let error:String = "Error".localized
         static let setting:String = "Setting".localized
        static let delete:String = "delete".localized
       static let deleteReply:String = "deleteReply".localized
}
    
    struct UserName {
        static let empty:String = "emptyUsername".localized
        static let invalid:String = "validUsernameRange".localized
        static let emptyFirstname:String = "emptyFirstname".localized
        static let emptyLastname:String = "emptyLastname".localized
        static let maxLengthFirstname:String = "maxLengthFirstname".localized
        static let maxLengthLastname:String = "maxLengthLastname".localized
        static let emptyNameSignup :String = "emptyNameSignup".localized
        static let emptyLNameSignup :String = "emptyLastNameSignup".localized
        static let allowedChar :String = "specialCharSignup".localized
        static let allowedCharLastName :String = "specialLastNameCharSignup".localized
        
        
        
    }
    struct userAttibute{
        static let emptyOtp:String = "emptyOtp".localized
        static let emptyDOB:String = "emptyDOB".localized
        static let emptyGender:String = "emptyGender".localized
        static let emptyLocation:String = "emptyLocation".localized
        static let emailNotFound = "emailNotFound".localized
    }
    
    struct Location {
        static let title = "Warning"
        static let off = "off".localized
        static let empty:String =  "please enter your location"
        static let current:String = "currentLoc".localized
        static let custom:String = "customLoc".localized
        static let fetch:String = "fetch".localized
    }
    
    struct PhoneNumber {
        static let empty:String = "emptyPhoneNumber".localized
        static let invalid:String = "invalidPhoneNumber".localized
        static let newmpty:String = "emptyNewPhoneNumber".localized
        static let newinvalid:String = "invalidNewPhoneNumber".localized
        static let emptyMerchantMobileNo = "emptyMerchantPhoneNumber".localized
        static let invalidMerchantMobileNo = "invalidMerchantPhoneNumber".localized
    }
    
    struct Password {
        static let emptyPassword:String = "emptyPassword".localized
        static let invalidPassword:String = "invalidPassword".localized
        static let passwordNotMatch:String = "passwordNotMatch".localized
        static let newEmpty:String = "newEmptyPassword".localized
        static let currentEmpty:String = "emptyCurrentPassword".localized
        static let emptyConfirmPassword = "emptyConfirmPassword".localized
        static let invalidCurrentPassword:String = "validCurrentPasswordRange".localized
        static let invalidNewPassword:String = "validNewPasswordRange".localized
        static let confirmNew:String = "confirmNewPassword".localized
        static let newConfirmMismatch = "passwordNotMatchReset".localized
        static let emptyPasswordSignup:String = "emptyPasswordSignup".localized
        static let emptyPasswordReset:String = "emptyPasswordReset".localized
        static let emptyConfirmPasswordReset:String = "emptyConfirmPasswordReset".localized
        static let invalidReferal:String = "invalidReferal".localized

        
    }
    struct Email {
        static let emptyEmail:String = "emptyEmail".localized
        static let emptyEmailForgot:String = "emptyEmailForgot".localized
        static let emptyEmailSignup:String = "emptyEmailSignup".localized
        
        
        static let invalidEmail:String = "invalidEmail".localized
        static let invalidEmailForgot:String = "invalidEmailForgot".localized
        static let invalidEmailSignup:String = "invalidEmailSignup".localized
        
        
        
        static let emptyNewEmail:String = "emptyNewEmail".localized
        
        
    }
    
    struct EditProfile {
        static let gender:String = "Please select gender.".localized
        static let dob = "Please select date of birth".localized
        static let location = "Please select your location".localized
    }
    struct Image {
        static let invalid = "invalidImage".localized
    }
    struct Voucher {
        static let empty = "emptyVoucherCode".localized
        static let invalid = "invalidVoucherCode".localized
    }
    
    struct Pin {
        static let empty = "emptyPin".localized
        static let invalid = "invalidPin".localized
    }
    
    struct ContactUS {
        struct Title {
            static let empty = "emptyContactUSTitle".localized
        }
        struct Query {
            static let empty = "emptyContactUSQuery".localized
        }
    }
    struct QRCode {
        static let noSupport = "QRNotSupportted".localized
    }
    struct Item {
        static let descMissing = "emptyItemDesc".localized
    }
    struct Purchase {
        static let emptyTotalAmount = "emptyTotalPayableAmount".localized
        static let invalidVoucherAmount = "validTotalPayableAmountRange".localized
        static let emptyRefNo = "emptyTellerReferenceNumber".localized
    }
    struct VerficationPin {
        static let empty = "emptyOTP".localized
        static let invalid = "invalidOTP".localized
        static let expire = "OTPExpired".localized
    }
    struct ScreenTitles {
        static let login = "login".localized
        static let forgotPassword = "forgotPassword".localized
        static let userProfile = "userProfile".localized
        static let editProfile = "editProfile".localized
        static let privacy:String = "privacy".localized
        static let terms:String = "terms".localized
        static let about:String = "about".localized
        static let library:String = "library".localized

    }
    
    struct ButtonTitle {
        static let update = "UPDATE".localized
        static let logout = "LOGOUT".localized
    }
    
    struct ErrorAlerts {
        static let unknownError = "unknownError".localized
        static let invalidUrl = "invalidUrl".localized
        static let dataNotFound = "dataNotFound".localized
        static let invalidJson = "invalidJson".localized
        static let missingKeys = "missingKeys".localized
        static let invalidData = "invalidData".localized
        static let responseError = "responseError".localized
        static let dictNotFound = "dictNotFound".localized
        static let noInternet = "noInternet".localized
        static let noRecords = "noRecords".localized
 static let loading = "Loading..."
    }
    
    struct ListType {
        static let friends = "Friends".localized
        static let reviews = "Reviews".localized
        static let bookmarks = "Bookmarks".localized
        static let everyone = "Everyone".localized
    }
    
    struct Categories {
        
        static let all = "All".localized
        static let movie = "Movies".localized
        static let tv = "Television".localized
        static let books = "Books".localized
        static let bars = "Bars".localized
        static let restruant = "Restaurants".localized
        
        
    }
    
    struct RatingFor {
        static let movie = "movie".localized
        static let tv = "tv".localized
        static let books = "book".localized
        static let bars = "bar".localized
        static let restruant = "restraunt".localized
    }
    
    
    struct TableEmpty {
         static let follower = "tableFollower".localized
         static let following = "tableFollowing".localized
         static let fb = "tableFb".localized
        static let emptyfb = "tableFbEmpty".localized
        
    }
    
    struct RatingSelected {
        
        static let one = "one".localized
        static let two = "two".localized
        static let three = "three".localized
        static let four = "four".localized
        static let five = "five".localized
        
    }
   
    
    struct TutorialMessage {
        
        static let one = "Welcome to Friendspire \n  Let's take a quick tour of the app!"
        static let two = "Let's add some flair to your profile with a cool picture"
        static let three = "Now let's show you how to add a recommendation. First, we select the \n 'search' icon in the menu bar."
        static let four = "Then, we choose a category.\n Let's go with movies!"
        static let five = "Next, we search for the movie we \n are looking for. How about the \n animated classic 'Finding Nemo'? "
        static let six = "Finally,press the '+' to rate or press the 'Bookmark' to save for later. That was easy right?"
        static let seven = "Once you rate or bookmark something \n it goes to your library \n so you can always find it again"
        static let eight = "Now to the last stop:\n The 'Inspiration' tab. This is where \n you get inspired by your friends!"
        static let nine = "When choosing a category at the top \n we generate a list of content based on \n your friends reviews..."
        static let ten = "...or you can simply browse the \n 'Discover Daily' to find the latest stuff.\n Enjoy Friendspire!"
        
        static let InteractiveOne = "If you connect through Facebook we can help you find friends"
        static let InteractiveTwo = "let's add some friends."
        static let InteractiveThree = "Let's add some flair to your profile with a cool picture"

        
    }
}
