//
//  Titles.swift
//  BaseProject
//
//  Created by TpSingh on 04/04/17.
//  Copyright © 2017 openkey. All rights reserved.
//

import Foundation

struct Titles {
    struct Button {
        static let facebookLogin: String = "facebookLogin".localized
        static let googleLogin: String = "googleLogin".localized
        static let linkedInLogin: String = "linkedInLogin".localized
    }
    
    struct SettingScreenTitles{
        
        static let distance : String = "distanceMeasurements".localized
        
        struct GenralSettingTitles{
            
            static let distance : String = "distanceMeasurements".localized
            static let preferred : String = "preferredCategories".localized
            static let privateAccount : String = "privateAccount".localized
            static let locations : String = "locationsServices".localized
            static let push : String = "pushNotifications".localized
            static let bookmarks : String = "showBookmarks".localized
        }
        
        struct AccountSettingTitles{
            static let changePassword : String = "changePassword".localized
            static let deleteAccount : String = "deleteAccount".localized
            static let logout : String = "logout".localized
            
        }
    }
    
    
    
    struct NetworkScreenTitles{
        struct HeaderTitle {
            static let followerAnother : String = "followerAnother".localized
            static let follower : String = "follower".localized
            static let pendingRequest : String = "pendingRequest".localized
            static let following : String = "following".localized
            static let followingAnother : String = "followingAnother".localized
        }
    }
}


//All segue identifiers used in project are refer here
struct SegueIdentifiers {
    struct LoginScreen {
        static let segueOtpTime = "segueOtpTime"
        static let segueDashboard = "segue_Dashboard"
    }
    struct AddRecommendationScreen {
        static let segueSearchRecommendation = "segueSearchRecommendation"
    }
    
    struct RegistrationScreen {
        static let otp = "segue_OTP"
    }
    struct OtpScreen {
        static let signupCompleted = "segue_SignupCompleted"
        static let resetPassword = "segue_ResetPassword"
    }
    struct ForgotPasswordScreen {
        static let verification = "segue_Verification"
    }
    
    struct SettingScreen {
        static let genralSetting = "segue_GenralSetting"
        static let editProfile =  "segue_EditProfile"
        static let accountSetting =  "segue_AccountSetting"
        static let help =  "segue_Help"
        static let blockedUser =  "segue_BlockedUser"
        static let privacyPolicy =  "segue_PrivacyPolicy"
        static let aboutUs =  "segue_AboutUs"
        static let termsAndConditions =  "segue_TermsAndConditions"
    }
    
    struct NetworkScreen{
        static let otherUser = "segue_otherUser"
        static let searchDetails = "segueSearchDetails"
        struct SearchScreen {
            static let moveToUser = "segueMoveToUser"
        }
        
        struct UserProfile {
            static let showUserNetwork = "segueShowUserNetwork"
        }
        
    }
}


//All cell identifiers used are here
struct CellIdentifiers{
    
    struct NetworksScreen {
        static let headerView : String = "HeaderView"
        static let search : String = "cellSearch"
        struct SearchScreen {
            static let searchUser: String = "SearchUserCell"
        }
        struct UserProfile {
             static let latestRecomendationCell: String = "LatestRecomendationCell"
        }
    }
    
    struct SearchRecomendationScreen {
        static let search : String = "cellSearchResult"
        static let category : String = "categoryName"
        static let categoryIcon : String = "CategoryIcon"
        static let recomendationList = "addRecomendationCell"
        static let searchMoreCell = "SearchMoreCell"
        
    }
    
    struct SettingScreen {
        
        static let settingsCell : String = "SettingsCell"
        struct GenralSetting {
            static let milesCell : String = "MilesCell"
            static let privateCell : String = "PrivateCell"
        }
        
        struct AccountSetting {
            static let accountSettingCell : String = "AccountSettingCell"
        }
    }
    
    struct InpirationScreen {
        struct  searchScreen {
            static let searchCell = ""
        }
        
        struct InpirationFeed {
            static let footer = "footer"
        }
    }
    
    struct  libraryScreen {
        struct CategoryList {
            static let movieCell = "movieCell"
            static let movieNib = "RecomendationCell"
            static let restrauntCell = "restrauntCell"
            static let restrauntNib = "RestrauntCell"
            static let restrauntMapCell = "restrauntMapCell"
            static let bookmarkCell = "bookmarkCell"
            
        }
    }
    
    
}





