//
//  Utility.swift
//  BaseProject
//
//  Created by MAC on 02/03/17.
//  Copyright © 2017 openkey. All rights reserved.
//

import UIKit
import Alamofire
import  CoreLocation
class Utility: NSObject {
    
    class var isConnectedToInternet:Bool {
        return NetworkReachabilityManager()!.isReachable
    }
    
    class func enableNetworkLog(value:Bool = true){
        Defaults.shared.set(value: value, forKey: .enableLog)
    }
    
    
    class func cancelPreviousRequest(){
        Alamofire.SessionManager.default.session.getAllTasks { (tasks) in
            tasks.last?.cancel()
        }
    }
    
    
    func runCode(in timeInterval:TimeInterval, _ code:@escaping ()->(Void))
    {
        DispatchQueue.main.asyncAfter(
            deadline: .now() + timeInterval,
            execute: code)
    }
    
    
    class func getModifiedJoinString(using arr : [String]?)->String{
        var strGenre = ""
        if arr == nil || arr?.count == 0{
            return strGenre
        }
        if arr?.count ?? 0 > 1 {
            let upto = arr?.count ?? 0
            strGenre =  arr?[0..<upto].joined(separator: ", ") ?? ""
        }else {
            strGenre = arr?.joined(separator: ", ") ?? ""
        }
        return strGenre
    }
    
    class func getEmojiAccordindToRating(rating:Float) -> UIImage {
        if rating <= 0.0 {
            return UIImage()
        }else if rating <= 1.49 {
            return #imageLiteral(resourceName: "smile1")
        }else if rating <= 2.49{
            return #imageLiteral(resourceName: "smile2")
        }else if rating <= 3.49{
            return #imageLiteral(resourceName: "smile3")
        }else if rating <= 4.49{
            return #imageLiteral(resourceName: "smile4")
        }else{
            return #imageLiteral(resourceName: "smile5")
        }
    }
    
    class func userCurrentLocationString() -> String?{
        if User.sharedInstance.isLocationEnabled ?? true == true && User.sharedInstance.userCurrentLocation?.latitude != nil && User.sharedInstance.userCurrentLocation?.longitude != nil{
            
            return "[\(User.sharedInstance.userCurrentLocation?.latitude ?? 0.0),\(User.sharedInstance.userCurrentLocation?.longitude ?? 0.0)]"
            
        }
        return nil
    }
    
    class  func getIcon(using category : String) -> UIImage{
        var image = #imageLiteral(resourceName: "movie")
        switch category {
        case AppMessages.Categories.movie:
            image = #imageLiteral(resourceName: "movie")
            break
        case AppMessages.Categories.tv:
            image = #imageLiteral(resourceName: "television")
            break
        case AppMessages.Categories.books:
            image = #imageLiteral(resourceName: "book")
            break
        case AppMessages.Categories.restruant:
            image = #imageLiteral(resourceName: "Restraunt")
            break
        case AppMessages.Categories.bars:
            image = #imageLiteral(resourceName: "bar")
            break
            
        default:
            DILog.print(items: "NA")
            break
            
        }
        
        return image
    }
    
    class  func getColor(using category : String) -> UIColor{
        
        var color = #colorLiteral(red: 0.5461447239, green: 0.3740770221, blue: 0.7494846582, alpha: 1)
        switch category {
        case AppMessages.Categories.movie:
            color = #colorLiteral(red: 0.5461447239, green: 0.3740770221, blue: 0.7494846582, alpha: 1)
            break
        case AppMessages.Categories.tv:
            color = #colorLiteral(red: 0.2260053158, green: 0.6834762096, blue: 0.9318693876, alpha: 1)
            break
        case AppMessages.Categories.books:
            color = #colorLiteral(red: 0.1082813069, green: 0.59888798, blue: 0.5456139445, alpha: 1)
            break
        case AppMessages.Categories.restruant:
            color = #colorLiteral(red: 0.9959798455, green: 0.6115841269, blue: 0.3324229121, alpha: 1)
            break
        case AppMessages.Categories.bars:
            color = #colorLiteral(red: 0.9861864448, green: 0.3759915829, blue: 0.6583402753, alpha: 1)
            break
            
        default:
            DILog.print(items: "NA")
            break
            
        }
        
        return color
    }
    
    class func getBackendType(using category : String) -> Int{
        
        var type = 0
        switch category {
        case AppMessages.Categories.movie:
            type = Constant.CategoryTypeBackend.movie.rawValue
            break
        case AppMessages.Categories.tv:
            type = Constant.CategoryTypeBackend.tv.rawValue
            break
        case AppMessages.Categories.books:
            type = Constant.CategoryTypeBackend.books.rawValue
            break
        case AppMessages.Categories.restruant:
            type = Constant.CategoryTypeBackend.restraunt.rawValue
            break
        case AppMessages.Categories.bars:
            type = Constant.CategoryTypeBackend.bars.rawValue
            break
            
        default:
            DILog.print(items: "NA")
            break
            
        }
        
        return type
    }
    
    class func getBackendTypeString(using category : Int) -> String{
        
        var type =  AppMessages.Categories.movie
        switch category {
            
        case Constant.CategoryTypeBackend.movie.rawValue:
            type = AppMessages.Categories.movie
            break
        case Constant.CategoryTypeBackend.tv.rawValue:
            type = AppMessages.Categories.tv
            break
        case Constant.CategoryTypeBackend.books.rawValue :
            type = AppMessages.Categories.books
            break
        case Constant.CategoryTypeBackend.restraunt.rawValue:
            type = AppMessages.Categories.restruant
            break
        case Constant.CategoryTypeBackend.bars.rawValue:
            type = AppMessages.Categories.bars
            break
            
        default:
            DILog.print(items: "NA")
            break
            
        }
        
        return type
    }
    
    
    class func rateForString(using category : Int) -> String{
        
        var type =  "Rate"
        switch category {
            
        case Constant.CategoryTypeBackend.movie.rawValue:
            type = AppMessages.RatingFor.movie
            break
        case Constant.CategoryTypeBackend.tv.rawValue:
            type = AppMessages.RatingFor.tv
            break
        case Constant.CategoryTypeBackend.books.rawValue :
            type = AppMessages.RatingFor.books
            break
        case Constant.CategoryTypeBackend.restraunt.rawValue:
            type = AppMessages.RatingFor.restruant
            break
        case Constant.CategoryTypeBackend.bars.rawValue:
            type = AppMessages.RatingFor.bars
            break
            
        default:
            DILog.print(items: "NA")
            break
        }
        
        return type.capitalizeFirst()
    }
    
    class func getPriceLevel(value:Int) -> String {
        switch value {
        case 1:
            return Constant.priceLevel.one.rawValue
        case 2:
            return Constant.priceLevel.two.rawValue
        case 3:
            return Constant.priceLevel.three.rawValue
        case 4:
            return Constant.priceLevel.four.rawValue
            
        default:
            return "NA"
        }
    }
    
    class func getRawOnly(_ str : String) -> Int {
        switch str {
        case "Male":
            return 1
        case "Female":
            return  2
        default:
            return  3
        }
    }
    
    class func priceLevelInt(value:Constant.priceLevel.RawValue) -> Int {
        switch value {
        case Constant.priceLevel.one.rawValue:
            return 1
        case Constant.priceLevel.two.rawValue:
            return 2
        case Constant.priceLevel.three.rawValue:
            return 3
        case Constant.priceLevel.four.rawValue:
            return 4
        default:
            return 0
        }
    }
    
    class func resetFilter() {
        RestuarantFilterModel.objFeedListRequest = FeedListRequest()
        RestuarantFilterModel.bookSharedInstance = RestuarantFilterModel()
        RestuarantFilterModel.sharedInstance = RestuarantFilterModel()
        RestuarantFilterModel.filterCount = 0
        RestuarantFilterModel.filterCountKey = 0
    }
    
    class func minutesToHoursMinutes (minutes : Int) -> String {
        return "\(minutes / 60)h \(minutes % 60)m"
    }
    
    
    class  func openHours(value:String)->String{
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "HHmm"
        let date = dateFormatter.date(from: value)
        let localFormatter = DateFormatter()
        //        localFormatter.timeZone = NSTimeZone.init(abbreviation: "Europe/Berlin")
        localFormatter.dateFormat = "h:mm a"
        let stringDate = localFormatter.string(from: date ?? Date())
        return stringDate
    }
    
    //MARK:- DateFormatters
    class func localDateFormatter(dateFormat:String)->DateFormatter{
        let formatter = DateFormatter()
        formatter.dateFormat = dateFormat
        formatter.timeZone = NSTimeZone.local
        return formatter
    }
    
    class func utcDateFormatter(dateFormat:String)->DateFormatter{
        let formatter = DateFormatter()
        formatter.dateFormat = dateFormat
        formatter.timeZone = NSTimeZone.init(abbreviation: "UTC")! as TimeZone
        return formatter
    }
    
    class  func getOffSet()->String{
        let seconds = TimeZone.current.secondsFromGMT()
        let hours = seconds/3600
        let minutes = abs(seconds/60) % 60
        let tz = String(format: "%+.2d:%.2d", hours, minutes)
        return tz
    }
    
    class func getTimeZone() -> String{
        return TimeZone.current.identifier
    }
    
    /**
     this will return the offset as per calcultaion of page size and array count
     */
    class func getCurrentOffset(count:Int ) -> Int{
        //check if offset is less than 0 return 1
        return count/RECORDS_PER_PAGE > 0 ? count/RECORDS_PER_PAGE : 1
    }
    
    class  func resizeImage(image: UIImage, newWidth: CGFloat) -> UIImage {
        
        let scale = newWidth / image.size.width
        let newHeight = image.size.height * scale
        UIGraphicsBeginImageContext(CGSize.init(width: newWidth, height: newHeight))
        image.draw(in: CGRect.init(x: 0.0, y: 0.0, width: newWidth, height: newHeight))
        let newImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        
        return newImage ?? image
    }
    
    
    class func getPagingSpinner() -> UIActivityIndicatorView {
        let pagingSpinner = UIActivityIndicatorView(activityIndicatorStyle: .gray)
        pagingSpinner.startAnimating()
        pagingSpinner.color = .darkGray
        pagingSpinner.hidesWhenStopped = true
        return pagingSpinner
    }
    
    class func compareBooksFilterObject(obj1:RestuarantFilterModel,obj2:RestuarantFilterModel) -> Bool {
        if obj1.pages != obj2.pages {
            return false
        }else if obj1.genre != obj2.genre {
            return false
        }else if obj1.fourSquareRating != obj2.fourSquareRating {
            return false
        }else if obj1.showMyRated != obj2.showMyRated {
            return false
        }else if obj1.rating != obj2.rating {
            return false
        }else if obj1.publishYear != obj2.publishYear {
            return false
        }
        return true
    }
    
    class func compareRestaurantFilterObject(obj1:RestuarantFilterModel,obj2:RestuarantFilterModel) -> Bool {
        if obj1.location != obj2.location {
            return false
        }else if obj1.distance != obj2.distance {
            return false
        }else if obj1.priceLevel != obj2.priceLevel {
            return false
        }else if obj1.cuisines != obj2.cuisines {
            return false
        }else if obj1.openNow != obj2.openNow {
            return false
        }else if obj1.showMyRated != obj2.showMyRated {
            return false
        }else if obj1.rating != obj2.rating {
            return false
        }else if obj1.fourSquareRating != obj2.fourSquareRating {
            return false
        }
        return true
    }
    
    
    class func fetchUserLocation(completion : @escaping ((Bool)->())){
        //MARK:-Get UserCurrentLocation.
        LocationManager.shared.start()
        LocationManager.shared.success = { address,timestamp in
            User.sharedInstance.userCurrentLocation = CLLocationCoordinate2D.init(latitude: address.lat ?? 0.0, longitude: address.long ?? 0.0)
            completion(true)
        }
        LocationManager.shared.failure = {
            if $0.code == .locationPermissionDenied {
            } else {
            }
            completion(false)
        }
        
    }
}



@IBDesignable
class PlaceHolderTextView: UITextView {
    
    @IBInspectable var placeholder: String = "" {
        didSet{
            updatePlaceHolder()
        }
    }
    
    private var originalText: String = ""
    
    private func updatePlaceHolder() {
        
        if self.text == "" || self.text == placeholder  {
            
            self.text = placeholder
            self.textColor = UIColor.lightGray
            self.originalText = ""
        } else {
            self.textColor =  #colorLiteral(red: 0.2392156863, green: 0.2509803922, blue: 0.3254901961, alpha: 1)
            self.originalText = self.text
        }
        
    }
    
    override func becomeFirstResponder() -> Bool {
        let result = super.becomeFirstResponder()
        self.text = self.originalText
        self.textColor = #colorLiteral(red: 0.2392156863, green: 0.2509803922, blue: 0.3254901961, alpha: 1)
        return result
    }
    override func resignFirstResponder() -> Bool {
        let result = super.resignFirstResponder()
        updatePlaceHolder()
        
        return result
    }
    
    
}
extension PlaceHolderTextView:UITextViewDelegate {
    
    
    
    
}

