//
//  APIManager.swift
//  BaseProject
//
//  Created by narinder on 01/03/17.
//  Copyright © 2017 openkey. All rights reserved.
//

import Foundation


struct Otp {
    
    var otpCode : String = ""
    var email : String = ""
    
    func getDictonary() -> Parameters {
        var dict:[String:Any] = ["otp_code":otpCode]
        if email != "" {
            dict["email"] = email
        }
        
        return dict
    }
}

struct ResendOtp {
    
    var type : Int = 1
    var email : String = ""
    
    func getDictonary() -> Parameters {
        var dict:[String:Any] = ["type":type]
        if email != "" {
            dict["email"] = email
        }
        
        return dict
    }
}

struct ResetPassword {
    
    var password : String = ""
    var email : String = ""
    var resetToken : String = ""
    
    
    func getDictonary() -> Parameters {
        var dict = [String: Any]()
        
        if resetToken != "" {
            dict["reset_token"] = resetToken
        }
        if email != "" {
            dict["email"] = email
        }
        if password != "" {
            dict["password"] = password
        }
        return dict
    }
}


struct FollowRequest {
    var friendId : String = ""
    var userName : String = ""
    func getDictonary() -> Parameters {
        let dict:[String:Any] = ["friend_id":friendId]
        return dict
    }
}

struct PushUpdate {
    var push : Int = 1
    
    func getDictonary() -> Parameters {
        let dict:[String:Any] = ["push_notification":push]
        return dict
    }
}

struct DistanceUpdate {
    var distanceUnit : DistanceUnit = DistanceUnit.Miles
    
    func getDictonary() -> Parameters {
        let dict:[String:Any] = ["distance_unit":distanceUnit.rawValue]
        return dict
    }
}

struct PrivateAccountUpdate {
    var isPrivate : Int = 0
    
    func getDictonary() -> Parameters {
        let dict:[String:Any] = ["is_private":isPrivate]
        return dict
    }
}


struct SearchRecomendationRequest{
    
    var title : String = ""
    var type : Int = 1
    var page : Int = 1
    var userID : String = ""
    var latLng : String = ""
    
    func getDictonary() -> Parameters {
        var dict : [String: Any] = ["type":type,"page":page]
        if title != "" {
            dict["title"] = title
        }
        if userID != "" {
            dict["userid"] = userID
        }
        if latLng != "" {
            dict["lat_lng"] = latLng
        }
        return dict
    }
}

struct RatingRequest{
    
    var rating : Int = 0
    var review : String = ""
    var referenceid : String = ""
    var type : Int = 1
    
    
    func getDictonary() -> Parameters {
        var dict : [String: Any] = ["type":type]
        if referenceid != "" {
            dict["referenceid"] = referenceid
        }
        if review != "" {
            dict["review"] = review
        }
        if rating != 0 {
            dict["rating"] = rating
        }
        
        return dict
    }
}

struct ReplyRequest{
    
    var recommendation_id : String = ""
    var comment_id : String = ""
    var comment : String = ""
    
    
    func getDictonary() -> Parameters {
        var dict = [String: Any]()
        if recommendation_id != "" {
            dict["recommendation_id"] = recommendation_id
        }
        if comment_id != "" {
            dict["comment_id"] = comment_id
        }
        if comment != "" {
            dict["comment"] = comment
        }
        
        return dict
    }
}

struct FeedListRequest{
    var openNow : String = ""
    var friends : Int = 0
    var page : Int = 1
    var latLong : String = ""
    var category : Int = 1
    var searchText : String = ""
    var cuisine : String = ""
    var rating : String = ""
    var friendspireRating : String = ""
    var distance : String = ""
    var price : String = ""
    var removeRated : String = ""
    var pages : String = ""
    var duration : String = ""
    var publishYear : String = ""
    var genre : String = ""
    var releaseYear : String = ""
    var userID : String = ""
    var filterLatLong : Int = 0
    var fromMap : Int = 0
    
    func getDictonary() -> Parameters {
        
        var dict : [String: Any] = ["category":category,"friends":friends,"page":page]
        if userID != ""{
            dict["userid"] = userID
        }
        
        if latLong != "" && latLong != "[]" {
            if RestuarantFilterModel.sharedInstance.location != "Current Location" {
                RestuarantFilterModel.filterCountKey += 1
            }
            dict["lat_long"] = latLong
        }
        if filterLatLong != 0{
            dict["filterLatLong"] = filterLatLong
        }
        if fromMap != 0{
            dict["map"] = fromMap
        }
        if searchText != "" {
            dict["search_text"] = searchText
        }
        if cuisine != "" && cuisine != "[]" {
            dict["cuisine"] = cuisine
            //            RestuarantFilterModel.filterCountKey += 1
        }
        if rating != "" && rating != "[]" {
            dict["rating"] = rating
            RestuarantFilterModel.filterCountKey += 1
        }
        if friendspireRating != "" && friendspireRating != "[]" {
            dict["friendspire_rating"] = friendspireRating
            RestuarantFilterModel.filterCountKey += 1
        }
//        print(distance)
        if distance != "" && distance != "[]"{
            // check for 20 miles in listing and in map
            if User.sharedInstance.distanceUnit == DistanceUnit.Miles.rawValue{
                
                if (distance != DistanceDefaultMILE){
                    //                if (distance != "38624.256" && fromMap == 0){
                    dict["distance"] = distance
                    RestuarantFilterModel.filterCountKey += 1
                }
            }else{
                // check for 20 km in listing and in map
                if (distance != DistanceDefaultKM){
                    dict["distance"] = distance
                    RestuarantFilterModel.filterCountKey += 1
                }
            }
        }
        if price != "" && price != "[]"{
            dict["price"] = price
            RestuarantFilterModel.filterCountKey += 1
        }
        if removeRated != "" {
            dict["remove_rated"] = removeRated
            RestuarantFilterModel.filterCountKey += 1
        }
        if openNow != "" {
            dict["open_now"] = openNow
            RestuarantFilterModel.filterCountKey += 1
        }
        if pages != "" && pages != "[]" {
            dict["pages"] = pages
            RestuarantFilterModel.filterCountKey += 1
        }
        if duration != "" && duration != "[]" {
            dict["duration"] = duration
            RestuarantFilterModel.filterCountKey += 1
        }
        if genre != "" && genre != "[]" {
            dict["genre"] = genre
            //            RestuarantFilterModel.filterCountKey += 1
        }
        if publishYear != "" && publishYear != "[]" {
            dict["publish_year"] = publishYear
            dict["release_year"] = publishYear
            RestuarantFilterModel.filterCountKey += 1
        }
        if releaseYear != "" {
            dict["release_year"] = releaseYear
        }
        
        return dict
    }
}

struct InpirationListRequest{
    var page : Int = 1
    var lat : String = ""
    var lng : String = ""
    var currentCity : String = ""
    var homeCity : String = ""
    
    func getDictonary() -> Parameters {
        
        var dict : [String: Any] = ["page":page]
        
        
        if lat != "" && lat != "[]" {
            dict["lat"] = lat
        }
        if lng != "" && lng != "[]" {
            dict["lng"] = lng
        }
        
        return dict
    }
}



//{
//    "category": "1",
//    "friends": "0",
//    "page": "1",
//    "lat_long": "[28.5048,77.0970]",
//    "search_text": "i ",
//    "cuisine": "['Café']",
//    "rating": "[1, 10]",
//    "friendspire_rating": "[0, 0]",
//    "distance": "0",
//    "price": "2",
//    "remove_rated": "1",
//    "pages": "[100, 200]",
//    "publish_year": "[0, 3000]",
//    "genre": "['Drama']",
//    "release_year": "[0, 2001]"
//}
/*
 
 struct UpdateProfile {
 
 var firstName: String = ""
 var lastName : String = ""
 var email: String = ""
 var password: String = ""
 var profilePicUrl: String = ""
 var firebaseProfileImageName: String = ""
 var dateOfBirth: String = ""
 var gender = "FEMALE"
 
 func getDictionary() -> [String: Any]? {
 
 
 var dict = [String: Any]()
 if firstName != "" {
 dict["first_name"] = firstName
 }
 if lastName != "" {
 dict["last_name"] = lastName
 }
 if email != "" {
 dict["email"] = email
 }
 if password != "" {
 dict["password"] = password
 }
 if profilePicUrl != "" {
 dict["profile_pic"] = profilePicUrl
 }
 if firebaseProfileImageName != "" {
 dict["firebaseProfileImageName"] = firebaseProfileImageName
 }
 if dateOfBirth != ""{
 dict["dob"] = dateOfBirth
 }
 if gender != ""{
 dict["gender"] = gender
 }
 
 DILog.print(items: dict)
 
 //let jsonData = try? JSONSerialization.data(withJSONObject: dict, options: .prettyPrinted)
 return dict//jsonData
 }
 }
 
 
 
 
 struct ForgotPasswordKey {
 var email: String = ""
 func getDictionary() -> [String: Any]? {
 let dict:[String: Any] = ["email": email]
 
 //let jsonData = try? JSONSerialization.data(withJSONObject: dict, options: .prettyPrinted)
 return dict//jsonData
 
 }
 
 }
 */



