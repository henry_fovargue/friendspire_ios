//
//  DIWebLayerCategoriesDetailApi.swift
//  FriendSpire
//
//  Created by Harpreet_kaur on 07/08/18.
//  Copyright © 2018 openkey. All rights reserved.
//

import Foundation

class DIWebLayerCategoriesDetailApi: DIWebLayer {
    
    func getCategoryDeatils(category:Int = 0 , postID:String = "", success: @escaping (_ data: MovieModelData) -> (), failure: @escaping (_ error: DIError) -> ()) {
        
        self.webService( httpMethod: .get, parameters: nil, functionName: "posts/detail?postId=\(postID)&category=\(category)", success: { response in
            DILog.print(items: response)
            let decoder = JSONDecoder()
            if let responseDict = response["data"] as? [String:Any] {
                var dict = responseDict
                if category == 5 {
                    dict["country"] = [String]()
                    dict["genre"] = [String]()
                }
                do{
                    let jasonData = try JSONSerialization.data(withJSONObject: dict, options: .prettyPrinted)
                    let modal = try decoder.decode(MovieModelData.self, from: jasonData)
                    success(modal )
                }
                catch{
                    DILog.print(items:error)
                }
            }
            
        }) {
            failure($0)
            
        }
        
    }
    
    func getRestaurantCategoryDeatils(category:Int = 0 , postID:String = "5b6efd351e990d8f465ef6c4" , latLong:[String] = [] , success: @escaping (_ data: RestaurantModelData,_ latLong:[NSNumber]) -> (), failure: @escaping (_ error: DIError) -> ()) {
        
        self.webService( httpMethod: .get, parameters: nil, functionName: "posts/detail?postId=\(postID)&category=\(category)&lat_long=[\(latLong.joined(separator: ","))]", success: { response in
            DILog.print(items: response)
            let decoder = JSONDecoder()
            
            if let responseDict = response["data"]  as? NSDictionary{
                do{
                    let jasonData = try JSONSerialization.data(withJSONObject: responseDict, options: .prettyPrinted)
                    let modal = try decoder.decode(RestaurantModelData.self, from: jasonData)
                    let latLong = responseDict["location"] as? [NSNumber] ?? [NSNumber]()
                    success(modal,latLong)
                }
                catch{
                    DILog.print(items:error)
                }
            }
            
        }) {
            failure($0)
            
        }
        
    }
    
    func getReviewsBasedOnCategory (postID:String = "5b6efd351e990d8f465ef6c4",page:Int = 0,success: @escaping (_ response:Response) -> (), failure: @escaping (_ error: DIError) -> ()) {
        self.webService(httpMethod: .get, parameters: nil, functionName: "posts/reviews?post_id=\(postID)&page_no=\(page)", success: { response in
            success(response)
            return
        }) {
            failure($0) }
    }
    
    func getReviewAndComment (recommendationID:String = "5b6efd351e990d8f465ef6c4",success: @escaping (_ response:Response) -> (), failure: @escaping (_ error: DIError) -> ()) {
        self.webService(httpMethod: .get, parameters: nil, functionName: "comment?recommendation_id=\(recommendationID)", success: { response in
            success(response)
            return
        }) {
            failure($0) }
    }
    
    
    func getCuisines (type:Int,success: @escaping (_ data: [String]) -> (), failure: @escaping (_ error: DIError) -> ()) {
        //"posts/cuisine?type=\(type)"  //Old url for cuisines
        
        self.webService(httpMethod: .get, parameters: nil, functionName: "posts/new_cuisine?type=\(type)", success: { response in
            if let data = response["data"] as? [String]{
                success(data)
                return
            }
        }) {
            failure($0) }
    }
    func getGenre (type:Int,success: @escaping (_ data: [String]) -> (), failure: @escaping (_ error: DIError) -> ()) {
        self.webService(httpMethod: .get, parameters: nil, functionName: "posts/genre?type=\(type)", success: { response in
            if let data = response["data"] as? [String]{
                success(data)
                return
            }
        }) {
            failure($0) }
    }
    
    func reportReview (parameter:Parameters?,success: @escaping (_ data: Response) -> (), failure: @escaping (_ error: DIError) -> ()) {
        self.webService(httpMethod: .post, parameters: parameter, functionName: "report", success: { response in
            success(response)
            return
        }) {
            failure($0) }
    }
    
    //MARK:-Address Update.
    func updateUserAddress (parameter:Parameters?,success: @escaping (_ message: String) -> (), failure: @escaping (_ error: DIError) -> ()) {
        self.webService(httpMethod: .put, parameters: parameter, functionName: "users/addressUpdate", success: { response in
            success(response["message"] as? String ?? "")
            return
        }) {
            failure($0) }
    }
    
    //MARK:-Address Update.
    func updateDeviceToken (parameter:Parameters,success: @escaping (_ message: String) -> (), failure: @escaping (_ error: DIError) -> ()) {
        self.webService(httpMethod: .put, parameters: parameter, functionName: "update_device", success: { response in
            success(response["message"] as? String ?? "")
            return
        }) {
            failure($0) }
    }
    
    func getContactCategory (success: @escaping (_ data: NSArray) -> (), failure: @escaping (_ error: DIError) -> ()) {
        self.webService(httpMethod: .get, parameters: nil, functionName: "get_contact_category", success: { response in
            if let data = response["data"] as? NSArray{
                success(data)
                return
            }
        }) {
            failure($0) }
    }
    
    //MARK:-Contact Administartor.
    func contactAdministrator (parameter:Parameters,success: @escaping (_ message: String) -> (), failure: @escaping (_ error: DIError) -> ()) {
        self.webService(httpMethod: .post, parameters: parameter, functionName: "contact_request", success: { response in
            success(response["message"] as? String ?? "")
            return
        }) {
            failure($0) }
    }
    //MARK:-Update Profile
    func updateProfile (parameter:Parameters,success: @escaping (_ message: String) -> (), failure: @escaping (_ error: DIError) -> ()) {
        self.webService(httpMethod: .put, parameters: parameter, functionName: "profile", success: { response in
            if let data = response["status"] as? Int, data == 1{
                success(response["message"] as? String ?? "")
                return
            }
        }) {
            failure($0) }
    }
    //MARK:-Get User Data.
    func getUserData (success: @escaping (_ message: String) -> (), failure: @escaping (_ error: DIError) -> ()) {
        self.webService(httpMethod: .get, parameters: nil, functionName: "profile/\(User.sharedInstance.id ?? "")", success: { response in
            if let data = response["data"] as? Parameters{
                let user = User(data)
                user.oauthToken = User.sharedInstance.oauthToken
                user.isLocationEnabled  = User.sharedInstance.isLocationEnabled
                UserManager.saveCurrentUser(user: user)
                success(response["message"] as? String ?? "")
                return
            }
        }) {
            failure($0) }
    }
    
    //MARK:-Get Change Email OTPCode
    func getOtpToChangeEmail (parameter:Parameters,success: @escaping (_ message: String) -> (), failure: @escaping (_ error: DIError) -> ()) {
        self.webService(httpMethod: .put, parameters: parameter, functionName: "profile/email_update", success: { response in
            success(response["message"] as? String ?? "")
            return
        }) {
            failure($0) }
    }
    //MARK:-Get Change Email OTPCode
    func verifyEmail (parameter:Parameters,success: @escaping (_ message: String) -> (), failure: @escaping (_ error: DIError) -> ()) {
        self.webService(httpMethod: .post, parameters: parameter, functionName: "profile/otpVerify", success: { response in
            success(response["message"] as? String ?? "")
            return
        }) {
            failure($0) }
    }
    
    
    //MARK:-Get Link To Share Post.
    func getLinkToSharePost (id:String,type:Int,success: @escaping (_ message: String) -> (), failure: @escaping (_ error: DIError) -> ()) {
        self.webService(httpMethod: .get, parameters: nil, functionName: "share/\(id)/\(type)", success: { response in
            if let data = response["link"] as? String {
                success(data)
                return
            }
        }) {
            failure($0) }
    }
    //MARK:-Get Link To Share UserProfile.
    func getLinkToShareUserProfile (id:String,success: @escaping (_ message: String) -> (), failure: @escaping (_ error: DIError) -> ()) {
        self.webService(httpMethod: .get, parameters: nil, functionName: "share/profile/\(id)", success: { response in
            if let data = response["link"] as? String {
                success(data)
                return
            }
        }) {
            failure($0) }
    }
    //MARK:-Get Link To Share App
    func getLinkToShareApp(success: @escaping (_ message: String) -> (), failure: @escaping (_ error: DIError) -> ()) {
        self.webService(httpMethod: .get, parameters: nil, functionName: "invite", success: { response in
            if let data = response["link"] as? String {
                success(data)
                return
            }
        }) {
            failure($0) }
    }
    
    //MARK:-Get List Of Notifications.
    func getNotifications (page:Int,success: @escaping (_ data: [Notifications]) -> (), failure: @escaping (_ error: DIError) -> ()) {
        self.webService(httpMethod: .get, parameters: nil, functionName: "notification/\(page)", success: { response in
            if let status = response["status"] as? Int, status == 1 {
                let notificationsDataArray = NotificationArray(response as Parameters)
                NotificationArray.sharedInstance = notificationsDataArray
                success(NotificationArray.sharedInstance.data)
                return
            }
        }) {
            failure($0) }
    }
    
    //MARK:-Update Notifications.
    func updateNotifications(parameters: Parameters?,success: @escaping (_ message: String) -> (), failure: @escaping (_ error: DIError) -> ()) {
        self.webService(httpMethod: .put ,parameters: parameters, functionName: "notification", success: { dataResponse in
            if let status = dataResponse["status"] as? Int , status == 1, let message = dataResponse["message"] as? String  {
                success(message)
                return
            }
        }) {
            failure($0)
        }
    }
    
    //MARK:-Clear Notifications Count.
    func clearNotificationsCount(success: @escaping (_ message: String) -> (), failure: @escaping (_ error: DIError) -> ()) {
        self.webService(httpMethod: .put ,parameters: nil, functionName: "settings/update_batch_count", success: { dataResponse in
            if let status = dataResponse["status"] as? Int , status == 1, let message = dataResponse["message"] as? String  {
                success(message)
                return
            }
        }) {
            failure($0)
        }
    }
    
    //MARK:-Update Notifications.
    func logout(success: @escaping (_ message: String) -> (), failure: @escaping (_ error: DIError) -> ()) {
        self.webService(httpMethod: .put ,parameters: nil, functionName: "logout", success: { dataResponse in
            if let status = dataResponse["status"] as? Int , status == 1, let message = dataResponse["message"] as? String  {
                success(message)
                return
            }
        }) {
            failure($0)
        }
    }
    
}
