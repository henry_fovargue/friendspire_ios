//
//  DIWebLayerUserAPI.swift
//  BaseProject
//
//  Created by TpSingh on 05/04/17.
//  Copyright © 2017 openkey. All rights reserved.
//

import UIKit

enum StatusInResponse : Int{
    case proceed = 1
    case popUp
}

class DIWebLayerUserAPI: DIWebLayer {
    
    
    //MARK:-  LOGIN PROCESS
    
    func login (parameters: Parameters?, success: @escaping (_ response: (Dictionary<String, Any>)) -> (), failure: @escaping (_ error: DIError) -> ()) {
        
        
        self.webService(parameters: parameters, functionName: "login", success: { dataResponse in
            
            DILog.print(items: dataResponse)
            success(dataResponse)
            //            failure(DIError.unKnowError())
            
        }) {
            failure($0) }
    }
    
    func socialLogin(parameters: Parameters?,success: @escaping (_ user: User?, [String:Any]?) -> (), failure: @escaping (_ error: DIError) -> ()) {
        
        self.webService(parameters: parameters, functionName: "is_social_media_exists", success: { response in
            DILog.print(items: response)
            if let responseDict = response["data"] as? NSDictionary {
                let user = User(responseDict as! Parameters)
                user.oauthToken = responseDict["token"] as? String ?? ""
                success(user,nil)
                return
            }
            success(nil,response)
            
        }) {
            failure($0) }
    }
    
    
    
    
    //MARK:-  FORGOT PASSWORD
    func forgotPassword(parameters: Parameters?,success: @escaping (_ message: String) -> (), failure: @escaping (_ error: DIError) -> ()) {
        
        self.webService(parameters: parameters, functionName: "forgot_password", success: { response in
            DILog.print(items: response)
            
            if let message = response["message"] as? String {
                success(message)
            }
            failure(DIError.unKnowError())
            
        }) {
            failure($0) }
        
    }
    
    func forgotOtp(parameters: Parameters?,success: @escaping (_ message: [String:Any]) -> (), failure: @escaping (_ error: DIError) -> ()) {
        
        self.webService(parameters: parameters, functionName: "forgot_password/otp_verify", success: { response in
            DILog.print(items: response)
            
            success(response)
            
        }) {
            failure($0) }
        
    }
    
    func resendOtp(parameters: Parameters?,success: @escaping (_ message: String) -> (), failure: @escaping (_ error: DIError) -> ()) {
        
        self.webService(parameters: parameters, functionName: "resend_otp", success: { response in
            DILog.print(items: response)
            
            if let message = response["message"] as? String {
                success(message)
            }
            
        }) {
            failure($0) }
    }
    
    func resetPassword(parameters: Parameters?,success: @escaping (_ message: String) -> (), failure: @escaping (_ error: DIError) -> ()) {
        
        self.webService( httpMethod: .put, parameters: parameters, functionName: "reset_Password", success: { response in
            DILog.print(items: response)
            
            if let message = response["message"] as? String {
                success(message)
            }
            failure(DIError.unKnowError())
            
            
        }) {
            failure($0) }
        
    }
    
    
    
    //MARK:-  REGISTERATION PROCESS
    
    func registation (parameters: Parameters?, success: @escaping (_ response: (Dictionary<String, Any>)) -> (), failure: @escaping (_ error: DIError) -> ()) {
        
        self.webService(parameters: parameters, functionName: "signup", success: { dataResponse in
            
            success(dataResponse)
            
        }) {
            failure($0)
        }
    }
    
    func applyReferal(parameters: Parameters?, success: @escaping (_ response: (Dictionary<String, Any>)) -> (), failure: @escaping (_ error: DIError) -> ()) {
        
        self.webService(parameters: parameters, functionName: "validatecode", success: { dataResponse in
            
            success(dataResponse)
            
        }) {
            failure($0)
        }
    }
    
    func linkReferralToProfile(parameters: Parameters?, success: @escaping (_ response: (Dictionary<String, Any>)) -> (), failure: @escaping (_ error: DIError) -> ()) {
        
        self.webService(parameters: parameters, functionName: "invitecode", success: { dataResponse in
            
            success(dataResponse)
            
        }) {
            failure($0)
        }
    }

    func regiseterOtp(parameters: Parameters?,success: @escaping (_ user: User) -> (), failure: @escaping (_ error: DIError) -> ()) {
        
        self.webService(parameters: parameters, functionName: "signup/otp_verify", success: { response in
            DILog.print(items: response)
            
            if let responseDict = response["data"] as? NSDictionary {
                let user = User(responseDict as! Parameters)
                user.oauthToken = responseDict["token"] as? String ?? ""

                success(user)
                return
            }
            failure(DIError.unKnowError())
            
        }) {
            failure($0) }
        
    }
    
    //    func resendOtp(parameters: Parameters?,success: @escaping (_ message: String) -> (), failure: @escaping (_ error: DIError) -> ()) {
    //
    //        self.webService(parameters: parameters, functionName: "otp_verification", success: { response in
    //            DILog.print(items: response)
    //
    //            if let message = response["message"] as? String {
    //                success(message)
    //            }
    //            failure(DIError.unKnowError())
    //
    //
    //        }) {
    //            failure($0) }
    //
    //    }
    
    //Change Password
    func changePassword(parameters: Parameters?,success: @escaping (_ response: Response) -> (), failure: @escaping (_ error: DIError) -> ()) {
        
        self.webService( httpMethod: .put, parameters: parameters, functionName: "change_password", success: { response in
            DILog.print(items: response)
            success(response)
            return
            
        }) {
            failure($0) }
    }
    
    //Delete Account
    func deleteAccount(success: @escaping (_ response: Response) -> (), failure: @escaping (_ error: DIError) -> ()) {
        
        self.webService( httpMethod: .put, parameters: nil, functionName: "profile/deleteAccount", success: { response in
            success(response)
            return
        }) {
            failure($0)
        }
    }
    //Notifications
    func getNotificationList(parameters: Parameters?,pageNo:Int,success: @escaping (_ response: Response) -> (), failure: @escaping (_ error: DIError) -> ()) {
        self.webService( httpMethod: .get, parameters: parameters, functionName: "notification/\(pageNo)", success: { response in
            
            DILog.print(items: response)
            //            let decoder = JSONDecoder()
            /*if let data = response["data"]{
             do{
             let jasonData = try JSONSerialization.data(withJSONObject: data, options: .prettyPrinted)
             let modal = try decoder.decode(NetworkModal.self, from: jasonData)
             
             success(modal)
             return
             }catch {
             print("decoding error: \(error)")
             }
             }*/
            success(response)
            return
        }) {
            failure($0) }
        
    }
    
    
    //Notifications
    func faqsList(parameters: Parameters?,success: @escaping (_ faqs: [FAQ]) -> (), failure: @escaping (_ error: DIError) -> ()) {
        self.webService( httpMethod: .get, parameters: parameters, functionName: "faqs", success: { response in
            if let data = response["data"] as? [Parameters] {
                do {
                    //get data from object
                    let jsonData = try JSONSerialization.data(withJSONObject: data, options: .prettyPrinted)
                    let faqs = try JSONDecoder().decode([FAQ].self, from: jsonData)
                    success(faqs)
                    return
                } catch (let error) {
                   DILog.print(items:"error in decoding \(error)")
                }
            }
            
        }) {
            failure($0)
        }
    }
    
    
    
    //MARK:-searchdata?title=baa&type=3
    //MARK:- Add Recomendation Screen
    func getSearchResultRecomendation(for modal: SearchRecomendationRequest,success: @escaping (_ list: [SearchRecomendationModal]) -> (), failure: @escaping (_ error: DIError) -> ()) {
        
        var str = ""
        if  modal.type == 1 || modal.type == 2{  // for restraunt and bar
            str =   "searchdata?title=\(modal.title)&type=\(modal.type)&lat_long=\(modal.latLng)"
        }else {
            str =  "searchdata?title=\(modal.title)&type=\(modal.type)"
        }
        
        self.webService( httpMethod: .get, parameters: nil, functionName: str, success: { response in
            DILog.print(items: response)
            var searchList = [SearchRecomendationModal]()
            
            let decoder = JSONDecoder()
            do{
                guard let arrResults =  response["data"] as? NSArray else {return}
                
                if arrResults.count > 0 {
                    for obj in arrResults{
                        let jasonData = try JSONSerialization.data(withJSONObject: obj, options: .prettyPrinted)
                        let modal = try decoder.decode(SearchRecomendationModal.self, from: jasonData)
                        searchList.append(modal)
                    }
                }
                
            }catch {
//                print("decoding error: \(error)")
                failure(DIError.unKnowError())
            }
            
            success(searchList)
            return
            
        }) {
            failure($0)
        }
    }
    
    func searchRecomendationResultThirdParty(for modal: SearchRecomendationRequest,success: @escaping (_ list: ([SearchRecomendationModal],Int)) -> (), failure: @escaping (_ error: DIError) -> ()) {
        
        var str = ""
        if  modal.type == 1 || modal.type == 2{  // for restraunt and bar
            str =   "posts?search_text=\(modal.title)&category=\(modal.type)&lat_long=\(modal.latLng)&page=\(modal.page)"
        }else {
            str =  "posts?search_text=\(modal.title)&category=\(modal.type)&page=\(modal.page)"
        }
        
        self.webService( httpMethod: .get, parameters: nil, functionName:str , success: { response in
            DILog.print(items: response)
            
            let decoder = JSONDecoder()
            do{
                guard let result =  response["data"] as? [String:Any] else {
                    failure(DIError.invalidData())
                    return
                }
                
                guard let arrResult = result["result"] as? NSArray else{
                    failure(DIError.invalidData())
                    return
                }
                
                guard let count = result["max_pages"] as? Int else{
                    failure(DIError.invalidData())
                    return
                }
                var arr = [SearchRecomendationModal]()
                for obj in arrResult{
                    let jasonData = try JSONSerialization.data(withJSONObject: obj, options: .prettyPrinted)
                    let modal = try decoder.decode(SearchRecomendationModal.self, from: jasonData)
                    arr.append(modal)
                }
                
                success((arr,count))
            }catch {
                print("decoding error: \(error)")
                failure(DIError.unKnowError())
            }
            return
            
        }) {
            failure($0)
        }
    }
    
    //    func getRecomendationListing(for modal: SearchRecomendationRequest,success: @escaping (_ list: [RecomendationModal]) -> (), failure: @escaping (_ error: DIError) -> ()) {
    //
    //        self.webService( httpMethod: .get, parameters: nil, functionName: "searchdata?title=\(modal.title)&type=\(modal.type)", success: { response in
    //            DILog.print(items: response)
    //            var searchList = [RecomendationModal]()
    //
    //            let decoder = JSONDecoder()
    //            do{
    //                guard let arrResults =  response["data"] as? NSArray else {return}
    //
    //                if arrResults.count > 0 {
    //                    for obj in arrResults{
    //                        let jasonData = try JSONSerialization.data(withJSONObject: obj, options: .prettyPrinted)
    //                        let modal = try decoder.decode(RecomendationModal.self, from: jasonData)
    //                        searchList.append(modal)
    //                    }
    //                }
    //
    //            }catch {
    //                print("decoding error: \(error)")
    //                failure(DIError.unKnowError())
    //            }
    //
    //            success(searchList)
    //            return
    //
    //        }) {
    //            failure($0)
    //        }
    //    }
    
    //    func getReviewsListing(for modal: SearchRecomendationRequest,success: @escaping (_ list: [RecomendationModal]) -> (), failure: @escaping (_ error: DIError) -> ()) {
    //        self.webService( httpMethod: .get, parameters: nil, functionName: "recommendation/reviewlist?type=\(modal.type)", success: { response in
    //            DILog.print(items: response)
    //            var searchList = [RecomendationModal]()
    //
    //            let decoder = JSONDecoder()
    //            do{
    //                guard let arrResults =  response["data"] as? NSArray else {return}
    //
    //                if arrResults.count > 0 {
    //                    for obj in arrResults{
    //                        let jasonData = try JSONSerialization.data(withJSONObject: obj, options: .prettyPrinted)
    //                        let modal = try decoder.decode(RecomendationModal.self, from: jasonData)
    //                        searchList.append(modal)
    //                    }
    //                }
    //
    //            }catch {
    //                print("decoding error: \(error)")
    //                failure(DIError.unKnowError())
    //            }
    //
    //            success(searchList)
    //            return
    //
    //        }) {
    //            failure($0)
    //        }
    //    }
    
    //Bookmarks screen
    func getBookmarkListing(for parameters: Parameters?,success: @escaping (_ list: ([FeedModal],SupportiveAttribute)) -> (), failure: @escaping (_ error: DIError) -> ()) {
        //        var str = ""
        //       if  modal.userID != ""{
        //         str =   "bookmark/\(modal.page)?type=\(modal.type)&userid=\(modal.userID)"
        //       }else {
        //        str =  "bookmark/\(modal.page)?type=\(modal.type)"
        //        }
        self.webService( httpMethod: .post, parameters:parameters, functionName:"bookmark/list" , success: { response in
            DILog.print(items: response)
            var searchList = [FeedModal]()
            let objAttributes = SupportiveAttribute()
            
            
            let decoder = JSONDecoder()
            do{
                if let distanceUnit = response["distance_unit"] as? Int{
                    objAttributes.distanceUnit = distanceUnit == 1 ? DistanceUnit.Miles : DistanceUnit.Kms
                    User.sharedInstance.objAttributes = objAttributes  // save in locally managed user as well
                }
                if let count = response["count"] as? Int{
                    objAttributes.count = count
                }
                
                guard let arrResults =  response["data"] as? NSArray else {return}
                
                if arrResults.count > 0 {
                    for obj in arrResults{
                        let jasonData = try JSONSerialization.data(withJSONObject: obj, options: .prettyPrinted)
                        let modal = try decoder.decode(FeedModal.self, from: jasonData)
                        searchList.append(modal)
                    }
                }
                
            }catch {
//                print("decoding error: \(error)")
                failure(DIError.unKnowError())
            }
            
            success((searchList,objAttributes))
            return
            
        }) {
            failure($0)
        }
    }
    
    func addBookmark(parameters: Parameters?,success: @escaping (_ status: Int) -> (), failure: @escaping (_ error: DIError) -> ()) {
        
        self.webService( httpMethod: .post, parameters: parameters, functionName: "bookmark", success: { response in
            DILog.print(items: response)
            
            if let status = response["status"] as? Int {
                success(status)
                return
            }
            failure(DIError.unKnowError())
            
        }) {
            failure($0) }
    }
    
    func removeBookmark(parameters: Parameters?,success: @escaping (_ status: Int) -> (), failure: @escaping (_ error: DIError) -> ()) {
        
        self.webService( httpMethod: .delete, parameters: parameters, functionName: "bookmark", success: { response in
            DILog.print(items: response)
            
            if let status = response["status"] as? Int {
                success(status)
                return
            }
            failure(DIError.unKnowError())
            
        }) {
            failure($0) }
    }
    
    //MARK- Add Rating Screen
    
    func getListOfFeeds(parameters: Parameters?,success: @escaping (_ list: ([FeedModal],SupportiveAttribute)) -> (), failure: @escaping (_ error: DIError) -> ()) {
        
        self.webService( httpMethod: .post, parameters: parameters,  functionName: "posts", success: { response in
//            DILog.print(items: response)
            var feedList = [FeedModal]()
            let objAttributes = SupportiveAttribute()
            
            
            let decoder = JSONDecoder()
            do{
                if let distanceUnit = response["distance_unit"] as? Int{
                    objAttributes.distanceUnit = distanceUnit == 1 ? DistanceUnit.Miles : DistanceUnit.Kms
                    User.sharedInstance.objAttributes = objAttributes  // save in locally managed user as well
                }
                if let count = response["count"] as? Int{
                    objAttributes.count = count
                }
                
                guard let arrResults =  response["data"] as? NSArray else {return}
                
                if arrResults.count > 0 {
                    for obj in arrResults{
                        let jasonData = try JSONSerialization.data(withJSONObject: obj, options: .prettyPrinted)
                        let modal = try decoder.decode(FeedModal.self, from: jasonData)
                        feedList.append(modal)
                    }
                }
                
            }catch {
//                print("decoding error: \(error)")
                failure(DIError.unKnowError())
            }
            
            success((feedList,objAttributes))
            return
            
        }) {
            failure($0)
        }
    }
    
    func getRatingDetail(for refernceId: String,success: @escaping (_ list: RatingModal) -> (), failure: @escaping (_ error: DIError) -> ()) {
        self.webService( httpMethod: .get, parameters: nil, functionName: "recommendation?referenceid=\(refernceId)", success: { response in
            DILog.print(items: response)
            let decoder = JSONDecoder()
            do{
                guard let result =  response["data"] else {
                    failure(DIError.invalidData())
                    return
                }
                let jasonData = try JSONSerialization.data(withJSONObject: result, options: .prettyPrinted)
                let modal = try decoder.decode(RatingModal.self, from: jasonData)
                success(modal)
                
            }catch {
//                print("decoding error: \(error)")
                failure(DIError.unKnowError())
            }
            return
            
        }) {
            failure($0)
        }
    }
    
    
    func addRating(parameters: Parameters?,success: @escaping (_ status: Int,_ rating : Double?, _ count : Int64? ) -> (), failure: @escaping (_ error: DIError) -> ()) {
        
        self.webService( httpMethod: .post, parameters: parameters, functionName: "recommendation", success: { response in
            DILog.print(items: response)
            
            
            if let status = response["status"] as? Int {
                guard let data = response["data"] as? [String : Any] else {
                    return  success(status,0.0,0)
                }
                let everyOneRating = data["new_friendspire_rating"] as? Double ?? 0.0
                let everyOneCount = data["user_count"] as? Int64 ?? 0
                
                success(status,everyOneRating,everyOneCount)
                return
            }
            failure(DIError.unKnowError())
            
        }) {
            failure($0) }
    }
    
    func updateRating(parameters: Parameters?,success: @escaping (_ status: Int,_ rating : Double?, _ count : Int64? ) -> (), failure: @escaping (_ error: DIError) -> ()) {
        
        self.webService( httpMethod: .put, parameters: parameters, functionName: "recommendation", success: { response in
            DILog.print(items: response)
            
            if let status = response["status"] as? Int {
                guard let data = response["data"] as? [String : Any] else {
                    return  success(status,0.0,0)
                }
                let everyOneRating = data["new_friendspire_rating"] as? Double ?? 0.0
                let everyOneCount = data["user_count"] as? Int64 ?? 0
                
                success(status,everyOneRating,everyOneCount)
                return
            }
            failure(DIError.unKnowError())
            
        }) {
            failure($0) }
    }
    
    func removeRating(parameters: Parameters?,success: @escaping (_ status: Int,_ rating : Double?, _ count : Int64? ) -> (), failure: @escaping (_ error: DIError) -> ()) {
        
        self.webService( httpMethod: .delete, parameters: parameters, functionName: "recommendation", success: { response in
            DILog.print(items: response)
            
            if let status = response["status"] as? Int {
                guard let data = response["data"] as? [String : Any] else {
                    return  success(status,0.0,0)
                }
                let everyOneRating = data["new_friendspire_rating"] as? Double ?? 0.0
                let everyOneCount = data["user_count"] as? Int64 ?? 0
                
                success(status,everyOneRating,everyOneCount)
                return
            }
            failure(DIError.unKnowError())
            
        }) {
            failure($0) }
    }

    func addComment(parameters: Parameters?,success: @escaping (_ status: Comments) -> (), failure: @escaping (_ error: DIError) -> ()) {
        
        self.webService( httpMethod: .post, parameters: parameters, functionName: "comment", success: { response in
            DILog.print(items: response)

            let decoder = JSONDecoder()
            do{
                guard let result =  response["data"] else {
                    failure(DIError.invalidData())
                    return
                }
                let jasonData = try JSONSerialization.data(withJSONObject: result, options: .prettyPrinted)
                let modal = try decoder.decode(Comments.self, from: jasonData)
                success(modal)
                
            }catch {
//                print("decoding error: \(error)")
                failure(DIError.unKnowError())
            }
            return
            
        }) {
            failure($0) }
    }
    
    func updateComment(parameters: Parameters?,success: @escaping (_ status: Int) -> (), failure: @escaping (_ error: DIError) -> ()) {
        
        self.webService( httpMethod: .put, parameters: parameters, functionName: "comment", success: { response in
            DILog.print(items: response)
            if let status = response["status"] as? Int {
                return  success(status)
            }
            failure(DIError.unKnowError())
            
        }) {
            failure($0) }
    }
    
    func deleteComment(parameters: Parameters?,success: @escaping (_ status: Int) -> (), failure: @escaping (_ error: DIError) -> ()) {
        
        self.webService( httpMethod: .delete, parameters: parameters, functionName: "comment", success: { response in
            DILog.print(items: response)
            if let status = response["status"] as? Int {
                return  success(status)
            }
            failure(DIError.unKnowError())
            
        }) {
            failure($0) }
    }
    
    
    //MARK:-
    //MARK:- NetWorks Screen
    
    func getNetworkDetail(parameters: Parameters?,success: @escaping (_ modal: NetworkModal) -> (), failure: @escaping (_ error: DIError) -> ()) {
        
        self.webService( httpMethod: .get, parameters: parameters, functionName: "network", success: { response in
            DILog.print(items: response)
            let decoder = JSONDecoder()
            if let data = response["data"]{
                do{
                    let jasonData = try JSONSerialization.data(withJSONObject: data, options: .prettyPrinted)
                    let modal = try decoder.decode(NetworkModal.self, from: jasonData)
                    
                    success(modal)
                    return
                }catch {
                    DILog.print(items:"decoding error: \(error)")
                }
            }
            
            failure(DIError.unKnowError())
            
        }) {
            failure($0) }
        
    }
    
    func syncFB(parameters: Parameters?,success: @escaping (_ message: String) -> (), failure: @escaping (_ error: DIError) -> ()) {
        
        self.webService( httpMethod: .put, parameters: parameters, functionName: "sync_friends", success: { response in
            DILog.print(items: response)
            
            if let message = response["message"] as? String {
                success(message)
                return
            }
            failure(DIError.unKnowError())
            
        }) {
            failure($0) }
        
    }
    
    
    func getFriends(parameters: Parameters?,success: @escaping (_ list: [FbModal]) -> (), failure: @escaping (_ error: DIError) -> ()) {
        
        self.webService( httpMethod: .get, parameters: parameters, functionName: "get_friends", success: { response in
            DILog.print(items: response)
            
            var userList = [FbModal]()
            let decoder = JSONDecoder()
            do{
                guard let arrFrnds =  response["friends"] as? NSArray else {return}
                
                if arrFrnds.count > 0 {
                    for obj in arrFrnds{
                        let jasonData = try JSONSerialization.data(withJSONObject: obj, options: .prettyPrinted)
                        let modal = try decoder.decode(FbModal.self, from: jasonData)
                        userList.append(modal)
                    }
                }
                
            }catch {
                DILog.print(items:"decoding error: \(error)")
            }
            
            success(userList)
            return
                
                failure(DIError.unKnowError())
            
        }) {
            failure($0) }
        
    }
    
    func getFollowing(upto: Int? = 1,success: @escaping (_ list: [FollowerModal]) -> (), failure: @escaping (_ error: DIError) -> ()) {
//        print("get followings api call")
        self.webService( httpMethod: .get, parameters: nil, functionName: "following/\(upto!)", success: { response in
            DILog.print(items: response)
            
            var userList = [FollowerModal]()
            let decoder = JSONDecoder()
            do{
                guard let arrFrnds =  response["data"] as? NSArray else {return}
                
                if arrFrnds.count > 0 {
                    for obj in arrFrnds{
                        let jasonData = try JSONSerialization.data(withJSONObject: obj, options: .prettyPrinted)
                        let modal = try decoder.decode(FollowerModal.self, from: jasonData)
                        userList.append(modal)
                    }
                }
                
            }catch {
                DILog.print(items:"decoding error: \(error)")
            }
            
            success(userList)
            return
            
            //                failure(DIError.unKnowError())
            
        }) {
            failure($0) }
    }
    
    func getPendingRequest(upto: Int? = 1,success: @escaping (_ list: [FollowerModal]) -> (), failure: @escaping (_ error: DIError) -> ()) {
        
        self.webService( httpMethod: .get, parameters: nil, functionName: "network/pending/\(upto!)", success: { response in
            DILog.print(items: response)
            
            var userList = [FollowerModal]()
            let decoder = JSONDecoder()
            do{
                guard let arrFrnds =  response["data"] as? NSArray else {return}
                
                if arrFrnds.count > 0 {
                    for obj in arrFrnds{
                        let jasonData = try JSONSerialization.data(withJSONObject: obj, options: .prettyPrinted)
                        let modal = try decoder.decode(FollowerModal.self, from: jasonData)
                        userList.append(modal)
                    }
                }
                
            }catch {
                DILog.print(items:"decoding error: \(error)")
            }
            
            success(userList)
            return
            
            //                failure(DIError.unKnowError())
            
        }) {
            failure($0) }
    }
    
    func getFollowers(upto: Int? = 1,success: @escaping (_ list: [FollowerModal]) -> (), failure: @escaping (_ error: DIError) -> ()) {
//        print("get folloers api call")
        self.webService( httpMethod: .get, parameters: nil, functionName: "follow/\(upto!)", success: { response in
            DILog.print(items: response)
            var userList = [FollowerModal]()
            
            let decoder = JSONDecoder()
            do{
                guard let arrFrnds =  response["data"] as? NSArray else {return}
                
                if arrFrnds.count > 0 {
                    for obj in arrFrnds{
                        let jasonData = try JSONSerialization.data(withJSONObject: obj, options: .prettyPrinted)
                        let modal = try decoder.decode(FollowerModal.self, from: jasonData)
                        userList.append(modal)
                    }
                }
                
            }catch {
                DILog.print(items:"decoding error: \(error)")
            }
            
            success(userList)
            return
            
            //                failure(DIError.unKnowError())
            
        }) {
            failure($0) }
        
    }
    
    func getOtherUserFollower(for user : String ,upto: Int? = 1,success: @escaping (_ list: [FollowerModal]) -> (), failure: @escaping (_ error: DIError) -> ()) {
        
        self.webService( httpMethod: .get, parameters: nil, functionName: "network/follow_list/\(user)/\(upto!)", success: { response in
            DILog.print(items: response)
            var userList = [FollowerModal]()
            
            let decoder = JSONDecoder()
            do{
                guard let arrFrnds =  response["data"] as? NSArray else {return}
                
                if arrFrnds.count > 0 {
                    for obj in arrFrnds{
                        let jasonData = try JSONSerialization.data(withJSONObject: obj, options: .prettyPrinted)
                        let modal = try decoder.decode(FollowerModal.self, from: jasonData)
                        userList.append(modal)
                    }
                }
                
            }catch {
//                print("decoding error: \(error)")
                failure(DIError.unKnowError())
            }
            
            success(userList)
            return
        }) {
            failure($0) }
        
    }
    
    
    
    func searchOtherUserFollower(for user : String ,upto: Int? = 1,searchingFor:String,success: @escaping (_ list: [SearchFriendModal]) -> (), failure: @escaping (_ error: DIError) -> ()) {
        
        self.webService( httpMethod: .get, parameters: nil, functionName: "network/follow_list/\(user)/\(upto!)?search_text=\(searchingFor)", success: { response in
            DILog.print(items: response)
            var userList = [SearchFriendModal]()
            
            let decoder = JSONDecoder()
            do{
                guard let arrFrnds =  response["data"] as? NSArray else {return}
                
                if arrFrnds.count > 0 {
                    for obj in arrFrnds{
                        let jasonData = try JSONSerialization.data(withJSONObject: obj, options: .prettyPrinted)
                        let modal = try decoder.decode(SearchFriendModal.self, from: jasonData)
                        userList.append(modal)
                    }
                }
                
            }catch {
//                print("decoding error: \(error)")
                failure(DIError.unKnowError())
            }
            
            success(userList)
            return
        }) {
            failure($0) }
        
    }
    
    
    func getOtherUserFollowing(for user : String ,upto: Int? = 1,success: @escaping (_ list: [FollowerModal]) -> (), failure: @escaping (_ error: DIError) -> ()) {
        
        self.webService( httpMethod: .get, parameters: nil, functionName: "network/following_list/\(user)/\(upto!)", success: { response in
            DILog.print(items: response)
            var userList = [FollowerModal]()
            
            let decoder = JSONDecoder()
            do{
                guard let arrFrnds =  response["data"] as? NSArray else {return}
                
                if arrFrnds.count > 0 {
                    for obj in arrFrnds{
                        let jasonData = try JSONSerialization.data(withJSONObject: obj, options: .prettyPrinted)
                        let modal = try decoder.decode(FollowerModal.self, from: jasonData)
                        userList.append(modal)
                    }
                }
                
            }catch {
//                print("decoding error: \(error)")
                failure(DIError.unKnowError())
            }
            
            success(userList)
            return
        }) {
            failure($0) }
        
    }
    
    func searchOtherUserFollowing(for user : String ,upto: Int? = 1,searchingFor:String,success: @escaping (_ list: [SearchFriendModal]) -> (), failure: @escaping (_ error: DIError) -> ()) {
        
        self.webService( httpMethod: .get, parameters: nil, functionName: "network/following_list/\(user)/\(upto!)?search_text=\(searchingFor)", success: { response in
            DILog.print(items: response)
            var userList = [SearchFriendModal]()
            
            let decoder = JSONDecoder()
            do{
                guard let arrFrnds =  response["data"] as? NSArray else {return}
                
                if arrFrnds.count > 0 {
                    for obj in arrFrnds{
                        let jasonData = try JSONSerialization.data(withJSONObject: obj, options: .prettyPrinted)
                        let modal = try decoder.decode(SearchFriendModal.self, from: jasonData)
                        userList.append(modal)
                    }
                }
                
            }catch {
//                print("decoding error: \(error)")
                failure(DIError.unKnowError())
            }
            
            success(userList)
            return
            
            
            
        }) {
            failure($0) }
        
    }
    
    
    func getSearchResult(for str: String,success: @escaping (_ list: [SearchFriendModal]) -> (), failure: @escaping (_ error: DIError) -> ()) {
        
        self.webService( httpMethod: .get, parameters: nil, functionName: "network/search/\(str)", success: { response in
            DILog.print(items: response)
            var searchList = [SearchFriendModal]()
            
            let decoder = JSONDecoder()
            do{
                guard let arrResults =  response["data"] as? NSArray else {return}
                
                if arrResults.count > 0 {
                    for obj in arrResults{
                        let jasonData = try JSONSerialization.data(withJSONObject: obj, options: .prettyPrinted)
                        let modal = try decoder.decode(SearchFriendModal.self, from: jasonData)
                        searchList.append(modal)
                    }
                }
                
            }catch {
//                print("decoding error: \(error)")
                failure(DIError.unKnowError())
            }
            
            success(searchList)
            return
            
        }) {
            failure($0)
        }
    }
    
    func getSearchDetailResult(for str: String,success: @escaping (_ list: [SearchFriendModal]) -> (), failure: @escaping (_ error: DIError) -> ()) {
        
        self.webService( httpMethod: .post, parameters: nil, functionName: "network/search/\(str)", success: { response in
            DILog.print(items: response)
            var searchList = [SearchFriendModal]()
            
            let decoder = JSONDecoder()
            do{
                guard let arrResults =  response["data"] as? NSArray else {return}
                
                if arrResults.count > 0 {
                    for obj in arrResults{
                        let jasonData = try JSONSerialization.data(withJSONObject: obj, options: .prettyPrinted)
                        let modal = try decoder.decode(SearchFriendModal.self, from: jasonData)
                        searchList.append(modal)
                    }
                }
                
            }catch {
//                print("decoding error: \(error)")
                failure(DIError.unKnowError())
            }
            
            success(searchList)
            return
            
        }) {
            failure($0)
        }
    }
    
    
    func followUser(parameters: Parameters?,success: @escaping (_ status: Int,_ user: FollowerModal?) -> (), failure: @escaping (_ error: DIError) -> ()) {
        
        self.webService( httpMethod: .put, parameters: parameters, functionName: "follow", success: { response in
            DILog.print(items: response)
            
            if let status = response["status"] as? Int {
                
                let decoder = JSONDecoder()
                do{
                    guard let data =  response["data"] as? [String:Any] else {
                        success(status,nil)
                        return
                    }
                    
                            let jasonData = try JSONSerialization.data(withJSONObject: data, options: .prettyPrinted)
                            let modal = try decoder.decode(FollowerModal.self, from: jasonData)
                     success(status,modal)
                    return
                }catch {
//                    print("decoding error: \(error)")
                    failure(DIError.unKnowError())
                    return
                }
                
//                success(status,nil)
//                return
            }
            
            failure(DIError.unKnowError())
            
        }) {
            failure($0) }
    }
    
    func unfollowUser(parameters: Parameters?,success: @escaping (_ status: Int) -> (), failure: @escaping (_ error: DIError) -> ()) {
        
        self.webService( httpMethod: .put, parameters: parameters, functionName: "network/unfollow", success: { response in
            DILog.print(items: response)
            
            if let status = response["status"] as? Int {
                success(status)
                return
            }
            failure(DIError.unKnowError())
            
        }) {
            failure($0) }
    }
    
    
    func acceptRequest(parameters: Parameters?,success: @escaping (_ status: Int) -> (), failure: @escaping (_ error: DIError) -> ()) {
        
        self.webService( httpMethod: .put, parameters: parameters, functionName: "network/accept", success: { response in
            DILog.print(items: response)
            
            if let status = response["status"] as? Int {
                success(status)
                return
            }
            failure(DIError.unKnowError())
            
        }) {
            failure($0) }
    }
    
    func rejectRequest(parameters: Parameters?,success: @escaping (_ status: Int) -> (), failure: @escaping (_ error: DIError) -> ()) {
        
        self.webService( httpMethod: .put, parameters: parameters, functionName: "network/reject", success: { response in
            DILog.print(items: response)
            
            if let status = response["status"] as? Int {
                success(status)
                return
            }
            failure(DIError.unKnowError())
            
        }) {
            failure($0) }
    }
    
    
    
    
    //MARK:- UserProfile Screen
    //Category wise library screen
    func getReviewListing(for parameters: Parameters?,success: @escaping (_ list: ([FeedModal],SupportiveAttribute)) -> (), failure: @escaping (_ error: DIError) -> ()) {
        //        var str = ""
        //        if  modal.userID != ""{
        //            str =   "recommendation/reviewlist/\(modal.page)?type=\(modal.type)&userid=\(modal.userID)"
        //        }else {
        //            str =  "recommendation/reviewlist/\(modal.page)?type=\(modal.type)"
        //        }
        self.webService( httpMethod: .post, parameters: parameters, functionName: "recommendation/reviewlist/", success: { response in
            DILog.print(items: response)
            var searchList = [FeedModal]()
            let objAttributes = SupportiveAttribute()
            
            let decoder = JSONDecoder()
            do{
                if let distanceUnit = response["distance_unit"] as? Int{
                    objAttributes.distanceUnit = distanceUnit == 1 ? DistanceUnit.Miles : DistanceUnit.Kms
                    User.sharedInstance.objAttributes = objAttributes  // save in locally managed user as well
                }
                if let count = response["count"] as? Int{
                    objAttributes.count = count
                }
                
                guard let arrResults =  response["data"] as? NSArray else {return}
                
                if arrResults.count > 0 {
                    for obj in arrResults{
                        let jasonData = try JSONSerialization.data(withJSONObject: obj, options: .prettyPrinted)
                        let modal = try decoder.decode(FeedModal.self, from: jasonData)
                        searchList.append(modal)
                    }
                }
                
            }catch {
//                print("decoding error: \(error)")
                failure(DIError.unKnowError())
            }
            
            success((searchList,objAttributes))
            return
            
        }) {
            failure($0)
        }
    }
    
    func getReviewListingForProfile(for modal: SearchRecomendationRequest,success: @escaping (_ list: [ProfileRecomendation]) -> (), failure: @escaping (_ error: DIError) -> ()) {
        
        var str = ""
        if  modal.latLng != ""{
            str =   "recommendation/profilereview?profile_id=\(modal.userID)&page=\(modal.page)&lat_long=\(modal.latLng)"
        }else {
            str =  "recommendation/profilereview?profile_id=\(modal.userID)&page=\(modal.page)"
        }
        
        self.webService( httpMethod: .get, parameters: nil, functionName: str, success: { response in
            DILog.print(items: response)
            var searchList = [ProfileRecomendation]()
            
            let decoder = JSONDecoder()
            do{
                
                let objAttributes = SupportiveAttribute()
                
                if let distanceUnit = response["distance_unit"] as? Int{
                    objAttributes.distanceUnit = distanceUnit == 1 ? DistanceUnit.Miles : DistanceUnit.Kms
                    User.sharedInstance.objAttributes = objAttributes  // save in locally managed user as well
                }
                
                guard let arrResults =  response["data"] as? NSArray else {return}
                
                if arrResults.count > 0 {
                    for obj in arrResults{
                        let jasonData = try JSONSerialization.data(withJSONObject: obj, options: .prettyPrinted)
                        let modal = try decoder.decode(ProfileRecomendation.self, from: jasonData)
                        searchList.append(modal)
                    }
                }
                
            }catch {
//                print("decoding error: \(error)")
                failure(DIError.unKnowError())
            }
            
            success(searchList)
            return
            
        }) {
            failure($0)
        }
    }
    
    func getOtherUserProfile(for user : String ,success: @escaping (_ user: UserModal) -> (), failure: @escaping (_ error: DIError) -> ()) {
        
        self.webService( httpMethod: .get, parameters: nil, functionName: "network/user_profile/\(user)", success: { response in
            DILog.print(items: response)
            
            let decoder = JSONDecoder()
            do{
                guard let userLoc =  response["data"] as? NSDictionary else {return}
                
                
                let jasonData = try JSONSerialization.data(withJSONObject: userLoc, options: .prettyPrinted)
                let modal = try decoder.decode(UserModal.self, from: jasonData)
                
                
                if user == UserManager.getCurrentUser()?.id{
                    //MARK:-Save to UserDefault.
                    let encoder = JSONEncoder()
                    if let encoded = try? encoder.encode(modal) {
                        let defaults = UserDefaults.standard
                        defaults.set(encoded, forKey: "SavedPerson")
                    }
                }
                success(modal)
                return
                
            }catch {
                DILog.print(items:"decoding error: \(error)")
            }
            
            failure(DIError.unKnowError())
            
        }) {
            failure($0) }
        
    }
    
//    func getOtherUserProfileCount(for user : String ,success: @escaping (_ user: UserModal) -> (), failure: @escaping (_ error: DIError) -> ()) {
//        
//        self.webService( httpMethod: .get, parameters: nil, functionName: "network/user_profile/\(user)", success: { response in
//            DILog.print(items: response)
//            
//            let decoder = JSONDecoder()
//            do{
//                guard let userLoc =  response["data"] as? NSDictionary else {return}
//                
//                
//                let jasonData = try JSONSerialization.data(withJSONObject: userLoc, options: .prettyPrinted)
//                let modal = try decoder.decode(UserModal.self, from: jasonData)
//                
//                
//                if user == UserManager.getCurrentUser()?.id{
//                    //MARK:-Save to UserDefault.
//                    let encoder = JSONEncoder()
//                    if let encoded = try? encoder.encode(modal) {
//                        let defaults = UserDefaults.standard
//                        defaults.set(encoded, forKey: "SavedPerson")
//                    }
//                }
//                success(modal)
//                return
//                
//            }catch {
//                print("decoding error: \(error)")
//            }
//            
//            failure(DIError.unKnowError())
//            
//        }) {
//            failure($0) }
//        
//    }
    
    //MARK:- Settings
    
    func getSettings(success: @escaping (_ status: SettingsModal) -> (), failure: @escaping (_ error: DIError) -> ()) {
        
        self.webService( httpMethod: .get, parameters: nil, functionName: "settings", success: { response in
            DILog.print(items: response)
            
            
            let decoder = JSONDecoder()
            do{
                guard let result =  response["data"] else {return}
                
                let jasonData = try JSONSerialization.data(withJSONObject: result, options: .prettyPrinted)
                let modal = try decoder.decode(SettingsModal.self, from: jasonData)
                success(modal)
                
                return
            }catch {
//                print("decoding error: \(error)")
                failure(DIError.unKnowError())
            }
            return
            
        }) {
            failure($0)
        }
    }
    
    func updatePushStatus(parameters: Parameters?,success: @escaping (_ status: Bool) -> (), failure: @escaping (_ error: DIError) -> ()) {
        
        self.webService( httpMethod: .put, parameters: parameters, functionName: "settings/update_push_status", success: { response in
            DILog.print(items: response)
            
            if let status = response["status"] as? Int {
                success(status == 0 ? false : true)
                return
            }
            failure(DIError.unKnowError())
            
        }) {
            failure($0) }
    }
    
    func updatePrivateStatus(parameters: Parameters?,success: @escaping (_ status: Bool) -> (), failure: @escaping (_ error: DIError) -> ()) {
        
        self.webService( httpMethod: .put, parameters: parameters, functionName: "settings/private_account", success: { response in
            DILog.print(items: response)
            
            if let status = response["status"] as? Int {
                success(status == 0 ? false : true)
                return
            }
            failure(DIError.unKnowError())
            
        }) {
            failure($0) }
    }
    
    func updateDistanceStatus(parameters: Parameters?,success: @escaping (_ status: Bool) -> (), failure: @escaping (_ error: DIError) -> ()) {
        
        self.webService( httpMethod: .put, parameters: parameters, functionName: "settings/update_distance_unit", success: { response in
            DILog.print(items: response)
            
            if let status = response["status"] as? Int {
                success(status == 0 ? false : true)
                return
            }
            failure(DIError.unKnowError())
            
        }) {
            failure($0) }
    }
    //MARK:-  Inpiration Dashboard screen
    
    
    func getListOfInpiration(modal: InpirationListRequest?,success: @escaping (_ list: [InspirationFeedModal]) -> (), failure: @escaping (_ error: DIError) -> ()) {
        //        &current_city=\(modal?.currentCity ?? "")&home_city=\(modal?.homeCity ?? "")
        let str = "discover/\(modal?.page ?? 1)?lat=\(modal?.lat ?? "0.0")&long=\(modal?.lng ?? "0.0")&current_city=\(modal?.currentCity ?? "")&home_city=\(modal?.homeCity ?? "")".addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed) ?? ""
        self.webService( httpMethod: .get, parameters: nil,  functionName: str, success: { response in
            //            DILog.print(items: response)
            var feedList = [InspirationFeedModal]()
            let objAttributes = SupportiveAttribute()
            
            if let count = response["notification_count"] as? Int {
                Defaults.shared.set(value: count, forKey: DefaultKey.notificationCount)
            }
            
            let decoder = JSONDecoder()
            do{
                if let distanceUnit = response["distance_unit"] as? Int{
                    objAttributes.distanceUnit = distanceUnit == 1 ? DistanceUnit.Miles : DistanceUnit.Kms
                    User.sharedInstance.objAttributes = objAttributes  // save in locally managed user as well
                }
                guard let arrResults =  response["data"] as? NSArray else {return}
                
                if arrResults.count > 0 {
                    for obj in arrResults{
                        let jasonData = try JSONSerialization.data(withJSONObject: obj, options: .prettyPrinted)
                        let modal = try decoder.decode(InspirationFeedModal.self, from: jasonData)
                        feedList.append(modal)
                    }
                }
                
            }catch {
//                print("decoding error: \(error)")
                failure(DIError.unKnowError())
            }
            
            success(feedList)
            return
            
        }) {
            failure($0)
        }
    }
    
    
    ///discover/1?lat=30.7145&long=76.7149
    
    
    
    //MARK:-  EDIT PROFILE
    func uploadProfilePic(image: UIImage, parameters: Parameters?, success: @escaping (_ response: Response) -> (), failure: @escaping (_ error: DIError) -> ()) -> Void {
        
        self.mutipart(parameters: parameters, image: image, key: "image", fileName: "image.jpeg", functionName: "profile/updateProfileImage", success: { (response) in
            success(response)
        }) {
            failure($0)
        }
    }
    
    
    func updateUserProfile (parameters: Parameters?, success: @escaping (_ message: String) -> (), failure: @escaping (_ error: DIError) -> ()) {
        
        self.webService(httpMethod:.put,  parameters: parameters, functionName: "profile", success: { dataResponse in
            if let message = dataResponse["message"] as? String {
                
                success(message)
                
            }
            failure(DIError.unKnowError())
            
        }) {
            failure($0) }
    }
    
    func updateTutorialStatus (parameters: Parameters?, success: @escaping (_ message: String) -> (), failure: @escaping (_ error: DIError) -> ()) {
        
        self.webService(httpMethod:.put,  parameters: parameters, functionName: "profile/tutorialRead", success: { dataResponse in
            if let message = dataResponse["message"] as? String {
                success(message)
            }
            
            failure(DIError.unKnowError())
            
        }) {
            failure($0) }
    }
    
    
    //MARK:-Get Blocked User
    func blockUser(parameters: Parameters?, success: @escaping (_ message: String) -> (), failure: @escaping (_ error: DIError) -> ()) {
        self.webService(httpMethod : .post ,parameters: parameters , functionName: "block", success: { dataResponse in
            if let status = dataResponse["status"] as? Int , status == 1 {
                success(dataResponse["message"] as? String ?? "")
                return
            }
        }) {
            
            failure($0)
        }
    }
    //MARK:-Get Blocked User
    func unBlockUser(parameters: Parameters?, success: @escaping (_ message: String) -> (), failure: @escaping (_ error: DIError) -> ()) {
        self.webService(httpMethod : .put ,parameters: parameters , functionName: "block", success: { dataResponse in
            if let status = dataResponse["status"] as? Int , status == 1 {
                success(dataResponse["message"] as? String ?? "")
                return
            }
        }) {
            failure($0)
        }
    }
    
    //MARK:-Get Blocked User
    func getBlockedUser(page:Int, success: @escaping (_ data: [BlockedUser]) -> (), failure: @escaping (_ error: DIError) -> ()) {
        self.webService(httpMethod : .get ,parameters: nil, functionName: "block?page=\(page)", success: { dataResponse in
            if let status = dataResponse["status"] as? Int , status == 1 {
                let blockedUsersDataArray = BlockedUserArray(dataResponse as Parameters)
                success(blockedUsersDataArray.data)
                return
            }
        }) {
            failure($0)
            
        }
    }
    
}
