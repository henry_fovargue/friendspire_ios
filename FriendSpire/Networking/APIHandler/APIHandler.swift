//
//  DiSessionManger.swift
//  BaseProject
//
//  Created by Aj Mehra on 08/03/17.
//  Copyright © 2017 Debut Infotech. All rights reserved.
//

import UIKit
import Foundation
import Alamofire


enum HttpContentType: String {
    case urlecncoded = "application/x-www-form-urlencoded"
    case json = "application/json"
    case multipart = "multipart/form-data; boundary="
}

typealias Parameters = [String: Any]
typealias Response = [String: Any]

/// This Enum is used to assign the base Url for different stagging of the application like whether application is under development or QA or production
// dev :- Development Environment
// qa :- For QA
// live :- Live URL for App Store
// beta :- Client Testing
enum BaseUrl: String {
    case dev
    case qa
    case beta
    case live
    case local
    
    var version: String {
        switch self {
        case .dev: return ""
        case .qa: return ""
        case .beta: return ""
        case .live: return ""
        case .local: return ""
        }
    }
    var url: String {
        switch self {
        case .dev: return "http://js-server.debutinfotech.com:3003/" //dev
        case .qa: return "http://js-server.debutinfotech.com:3007/" //qa
//        case .dev: return "http://192.168.0.190:3003/" //dev temp
//        case .qa: return "http://192.168.0.190:3007/" //qa temp
//        case .beta: return "http://friendspiredev.debutinfotech.com:3005/"  //client
        case .beta: return "http://friendspireapi.debutinfotech.com" //beta testing for live instance
        case .live: return "http://api.friendspire.com/"
        case .local: return "http://192.168.0.139:3003"
        }
    }
}

enum APICallBacks {
    case success, failure
}

struct DIError {
    var title: String?
    var message: String?
    var code: DIErrorCode?
    var icon: UIImage?
    
    init(error: Error) {
        self.message = error.localizedDescription
        //        self.code = DIErrorCode(rawValue: error._code)
    }
    init(title: String?, message: String, code: DIErrorCode, icon: UIImage?) {
        self.message = message
        self.title = title
        self.code = code
        self.icon = icon
    }
    
    static func locationPermissionDenied() -> DIError {
        return DIError(title: "Warning", message: "Location is required.Go to settings & enable location", code: .locationPermissionDenied, icon: nil)
    }
    
    static func unKnowError() -> DIError {
        return DIError(title: "OOPS", message: "Sorry, something went wrong. We're working on getting this fixed as soon as we can.", code: .unknown, icon: nil)
    }
    static func noNetwork() -> DIError {
        return DIError(title: "OOPS", message: "Sorry, please connect to wi-fi or a cellular network to get online again.", code: .unknown, icon: nil)
    }
    static func fbUserNotFound() -> DIError {
        return DIError(title: "OOPS", message: "Sorry, something went wrong. We're working on getting this fixed as soon as we can.", code: .unknown, icon: nil)
    }
    static func emailRequestDenied() -> DIError {
        return DIError(title: "Permission Denied", message: "User has not provided access for email address", code: .unknown, icon: nil)
    }
    static func isCanceled() -> DIError {
        return DIError(title: "Cancelled", message: "User has cancel the facebook login request", code: .unknown, icon: nil)
    }
    static func noResponse () -> DIError {
        return DIError(title: "OOPS", message: "Sorry, something went wrong. We're working on getting this fixed as soon as we can.", code: .unknown, icon: nil)
    }
    static func invalidUrl () -> DIError {
        return DIError(title: "Invalid url", message: "Url is invalid.", code: .invalidUrl, icon: nil)
    }
    static func nilData () -> DIError {
        return DIError(title: "", message: "Data not found.", code: .nilData, icon: nil)
    }
    static func invalidJSON () -> DIError {
        return DIError(title: "", message: "Invalid JSON response.", code: .invalidJSON, icon: nil)
    }
    static func missingKey () -> DIError {
        return DIError(title: "", message: "Missing key for dictionary or array.", code: .missingKey, icon: nil)
    }
    static func invalidData () -> DIError {
        return DIError(title: "", message: "Invalid data.", code: .invalidData, icon: nil)
    }
    static func serverResponse (message: String) -> DIError {
        return DIError(title: "", message: message, code: .invalidData, icon: nil)
    }
    static func serverResponseError (error: Error) -> DIError {//hdfgytfggh   sss@ddd.drf
        return DIError(title: "Response Error", message: error.localizedDescription, code: .invalidData, icon: nil)
    }
    static func invalidAppInfoDictionary () -> DIError {
        return DIError(title: "No Info Dictionary", message: "Unable to find info dictionary for application.", code: .invalidAppInfoDict, icon: nil)
    }
    static func newSocialAccount() -> DIError {
        return DIError(title: "No Email Found", message: "There is no email address is assoicated with this account, Please resgister or login with other option", code: .newSocialAccount, icon: nil)
    }
    static func emailVerified (message: String) -> DIError {
        return DIError(title: "Oops", message: message, code: .emailVerified, icon: nil)
    }
}

enum DIErrorCode {
    case unknown, invalidUrl, nilData, invalidJSON, missingKey, invalidData, invalidAppInfoDict, locationPermissionDenied,newSocialAccount,emailVerified    
}

class APIHandler {
    
    //Envirnoment Change
    var baseURL: BaseUrl {
        return .qa
    }
    
    func defaultHeader() -> [String: String] {
        var header = [String: String]()
         header["token"] = ""
        if UserManager.isUserLoggedIn() {
            let token = User.sharedInstance.oauthToken
            header["token"] = token
        }
        header["timezone"] = Utility.getTimeZone()
        header["offset"] = Utility.getOffSet()
        return header
    }
    
    
    func post( apiMethod: Alamofire.HTTPMethod = .post, parameters: Parameters?, functionName: String, success: @escaping (_ responseValue: Data) -> (), failure: @escaping (_ error: DIError) -> ()) {
       
//        print(apiMethod,parameters,functionName,self.defaultHeader())
        
        Alamofire.request(absolutePath(forApi: functionName), method: apiMethod, parameters: parameters, encoding: JSONEncoding.default, headers: defaultHeader())
            .responseJSON { response in
               
                
                switch(response.result) {
                case .success(let JSON):
                    
                    if let httpResponse = response.response {
                        if httpResponse.statusCode == 200 {
//                            DILog.print(items: JSON)
                            if let data = response.data {
                                success(data)
                                return
                            }
                        } else if httpResponse.statusCode == 401{
                            
                            guard let responseData = response.data else {
                                failure(DIError.invalidData())
                                return
                            }
                            do {
                                let json = try JSONSerialization.jsonObject(with: responseData, options: .allowFragments)
                                guard let responseDict = json as? [String: AnyObject] else {
                                    failure(DIError.invalidData())
                                    return
                                }
                                let alert = UIAlertController(title: "Alert!", message: responseDict["message"] as? String ?? "", preferredStyle: .alert)
                                alert.addAction(UIAlertAction(title: "ok".localized, style: .default, handler: { (action) in
                                    for (key, _) in UserDefaults.standard.dictionaryRepresentation() {
                                        UserDefaults.standard.removeObject(forKey: key)
                                    }
                                    appDelegate.navigateToLogin()
                                }))
                                let controller = UIApplication.topViewController()
                                if controller?.presentedViewController == nil {
                                    controller?.present(alert, animated: true, completion: nil)
                                }else{
                                    return
                                }
                                
                            } catch {
                                failure(DIError.invalidJSON())
                                return
                            }
                            
                        }
                        else {
                            DILog.print(items: JSON)
                            if let data = response.data {
//                                DILog.print(items: data)
                                success(data)
                                return
                            }
                        }
                    }
                    failure(DIError.unKnowError())
                    
                case .failure(let error):
                    DILog.print(items: error.localizedDescription)
                    var message:DIError!
                    if let httpStatusCode = response.response?.statusCode {
                        switch(httpStatusCode) {
                        case 400:
                            message =  DIError.invalidJSON()//"Username or password not provided."
                        case 401:
                            message =  DIError.invalidData()//"Username or password not provided."
                        default :
                            message =  DIError.invalidData()
                        }
                    } else {
                        message = DIError.serverResponseError(error: error)
                    }
                    
                    failure(message)
                      return
                }
                
                //
                //
                //        if let errorResponse = result.error {
                //
                //         DILog.print(items: errorResponse.localizedDescription)
                //          failure(self.parseError(error: errorResponse))
                //
                //        } else if let httpResponse = result.response {
                //          if httpResponse.statusCode == 200 {
                //            if let data = result.data {
                //              success(data)
                //              return
                //            }
                //          }
                //        }
                //        failure(DIError.nilData())
        }
    }
    
    
    func multipart(parameters: Parameters?, image: UIImage, key: String, fileName: String, functionName: String, success: @escaping (_ responseData: Data?) -> (), failure: @escaping (_ error: DIError) -> ()) {
        
        Alamofire.upload(multipartFormData: { (multiPart) in
            
            multiPart.append(UIImageJPEGRepresentation(image, 0.5)!, withName: key, fileName: fileName, mimeType: "image/jpeg")
            
            if parameters != nil {
                for (key, value) in parameters! {
                    multiPart.append((value as AnyObject).data(using: String.Encoding.utf8.rawValue)!, withName: key)
                }
            }
        }, usingThreshold: 10000, to: absolutePath(forApi: functionName), method: .post, headers: defaultHeader()) { (encodingResult) in
            switch encodingResult {
            case .success(let upload, _, _):
                
                upload.responseJSON { response in
                    
                    guard response.result.isSuccess else {
                        failure(DIError.nilData())
                        return
                    }
                    
                    guard let responseJSON = response.result.value as? [String: AnyObject] else {
                        DILog.print(items: "Invalid information received from service")
                        failure(DIError.nilData())
                        return
                    }
                    DILog.print(items: responseJSON)
                    
                    if let data = response.data {
                        success(data)
                        return
                    }
                }
            case .failure(let error):
                failure(self.parseError(error: error))
                break
            }
        }
    }
    
    
    /// This method will create the complete path for the api request with the help of base url, version and domain Name
    ///
    /// - Parameter apiName: Name of api domin like login
    /// - Returns: complete Url String
    func absolutePath(forApi apiName: String) -> String {
        var path = baseURL.url
        let version = baseURL.version
        if !path.hasSuffix("/") {
            path += "/"
        }
        if version.isEmpty {
            return path + apiName
        }
        if !version.hasSuffix("/") {
            path += version + "/"
        } else {
            path += version
        }
        return path + apiName
    }
    
    /// This method will parse the error catch through the Api session manager and return an object of DIError with details about the error for debugging purpose
    ///
    /// - Parameter error: error catch by session manager
    /// - Returns: Convert Object to display error to user
    func parseError(error: Error) -> DIError {
        return DIError(title: "", message: "Something Went Wrong.", code: .invalidUrl, icon: nil)
    }
}

extension Data {
    mutating func append(_ string: String) {
        if let data = string.data(using: .utf8) {
            append(data)
        }
    }
}
