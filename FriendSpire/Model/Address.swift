//
//  Address.swift
//  Omakase
//
//  Created by Harpreet on 03/10/17.
//  Copyright © 2017 openkey. All rights reserved.
//

import Foundation
import Foundation
import CoreLocation
import GooglePlaces
import GoogleMaps

class Address: NSObject {
    var city: String?
    var state: String?
    var country: String?
    var pinCode: String?
    var lat: Double?
    var long: Double?
    var formatted: String?
    
    init(placemark: CLPlacemark) {
        city = placemark.locality
        state = placemark.administrativeArea
        country = placemark.country
        pinCode = placemark.addressDictionary?["ZIP"] as? String
        lat = placemark.location?.coordinate.latitude
        long = placemark.location?.coordinate.longitude
        if let addrList = placemark.addressDictionary?["FormattedAddressLines"] as? [String] {
            formatted = addrList.joined(separator: ", ")
        }
        
    }
    
    init(gmsPlace: GMSPlace) {
        lat = gmsPlace.coordinate.latitude
        long = gmsPlace.coordinate.longitude
        city = gmsPlace.name
        formatted = gmsPlace.formattedAddress ?? "Current Location"
    }
    
    init(location:CLLocation) {
        lat = location.coordinate.latitude
        long = location.coordinate.longitude
        formatted = "Current Location"
    }
    convenience init(lat:CLLocationDegrees,long:CLLocationDegrees,sucess: @escaping (_ status: Bool) -> ()){
        self.init()
        self.lat = lat
        self.long = long
        getAddress(lat: lat, long: long) { (true) in
            sucess(true)
            return
        }
    }
    
    func formatAddress() -> String? {
        if let city = self.city, let country = self.country {
            return city + ", " + country
        }else if let city = self.city {
            if let country = self.country {
                return city + ", " + country
            }else {
                return city
            }
        }else if let country = self.country {
            return country
        }
        return nil
    }
    
    override init() {
        
    }
    
    
    func getAddress(lat:CLLocationDegrees,long:CLLocationDegrees,sucess: @escaping (_ status: Bool) -> ()) {
        let currentLocation = CLLocation(latitude: lat, longitude: long)
        CLGeocoder().reverseGeocodeLocation(currentLocation, completionHandler: {(placemarks, error) -> Void in
            if error != nil {
                return
            }
            if (placemarks?.count)! > 0 {
                let pm = placemarks?[0]
                if let locality = pm?.locality, let country = pm?.country {
                    self.city = locality
                    self.country = country
                    if let addrList = pm?.addressDictionary?["FormattedAddressLines"] as? [String] {
                        self.formatted = addrList.joined(separator: ", ")
                    }
                    sucess(true)
                    return
                }
            }
            else {
            }
        })
    }
}
