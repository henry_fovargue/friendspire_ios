

import Foundation

struct InspirationFeedModal : Codable {
    
    var id : String?
    var type : Int?
    var country : [String]?
    var genre : [String]?
    var image : [String]?
    var title : String?
    var recomendationCount : Int64?
    var updated_at : String?
    var rated_at : String?
    var friendspire_rating : Double?
    var algorithm : Int?
    var friends : [FriendsInspiration]?
    var city : String?
    var reason : String?
    var cuisine : [String]?
    var author : String?

    enum CodingKeys: String, CodingKey {
        
        case id = "_id"
        case type = "type"
        case country = "country"
        case genre = "genre"
        case image = "image"
        case title = "title"
        case updated_at = "updated_at"
        case rated_at = "rated_at"
        case friendspire_rating = "friendspire_rating"
        case algorithm = "algorithm"
        case friends = "friends"
        case reason = "reason"
        case city = "city"
        case author = "author"
        case cuisine = "cuisine"
        case recomendationCount = "recomendation_count"
    }
}

struct FriendsInspiration : Codable {
    let user_detail : UserDetailInspiration?
    let rating : Int?
    let review : String?
    
    enum CodingKeys: String, CodingKey {
        
        case user_detail = "user_detail"
        case rating = "rating"
        case review = "review"
    }
}

struct UserDetailInspiration : Codable {
    let _id : String?
    let profile_pic : String?
    let lastName : String?
    let firstName : String?
    
    enum CodingKeys: String, CodingKey {
        
        case _id = "_id"
        case profile_pic = "profile_pic"
        case lastName = "lastName"
        case firstName = "firstName"
    }
}

struct LocationInspiration : Codable {
    let coordinates : [Double]?
    let _id : String?
    
    enum CodingKeys: String, CodingKey {
        
        case coordinates = "coordinates"
        case _id = "_id"
    }
}

struct Timings : Codable {
    
    let end : String?
    let start : String?
    
    enum CodingKeys: String, CodingKey {
        case end = "end"
        case start = "start"
    }
}
