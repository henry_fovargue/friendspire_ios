//
//  BlockedUserModel.swift
//  FriendSpire
//
//  Created by Harpreet_kaur on 25/10/18.
//  Copyright © 2018 openkey. All rights reserved.
//

import Foundation
class BlockedUserArray: NSObject{
    static var sharedInstance = NotificationArray()
    
    var status:Int?
    var message:String?
    var data:[BlockedUser] = []
    
    //MARK:- Default Initializer
    override init () {
    }
    
    convenience init(_ dictionary: Parameters) {
        self.init()
        status = dictionary["status"] as? Int
        message = dictionary["message"] as? String ?? ""
        if (dictionary["data"] != nil) {
            data = BlockedUser.modelsFromDictionaryArray(array: dictionary["data"] as! NSArray)
        }
    }
}

class BlockedUser: NSObject {
    
    var id:String?
    var firstName:String?
    var lastName:String?
    var profilePic:String?
    
    //MARK:- Default Initializer
    override init () {
    }
    
    public class func modelsFromDictionaryArray(array:NSArray) -> [BlockedUser]
    {
        var blockedUsersArray:[BlockedUser] = []
        for item in array
        {
            blockedUsersArray.append(BlockedUser(dictionary: item as! NSDictionary)!)
        }
        return blockedUsersArray
    }
    
    required public init?(dictionary: NSDictionary) {
        id = dictionary["_id"] as? String
        firstName = dictionary["firstName"] as? String ?? ""
        lastName = dictionary["lastName"] as? String ?? ""
        profilePic = dictionary["profile_pic"] as? String ?? ""
    }
}
