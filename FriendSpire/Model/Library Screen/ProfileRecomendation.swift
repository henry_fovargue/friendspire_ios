////
////  ProfileRecomendation.swift
////  FriendSpire
////
////  Created by abhishek on 18/08/18.
////  Copyright © 2018 openkey. All rights reserved.
////
//
import Foundation

struct ProfileRecomendation: Codable {
    
    
    let id : String?
    let type : Int?
    let user_id : String?
    let reference_id : String?
    let review : String?
    let rating : Int?
    let post : Post?
    var is_bookmarked : Bool?
    
    enum CodingKeys: String, CodingKey {
        
        case id = "_id"
        case type = "type"
        case user_id = "user_id"
        case reference_id = "reference_id"
        case review = "review"
        case rating = "rating"
        case post = "post"
        case is_bookmarked = "is_bookmarked"
    }
    
    
}

struct Post : Codable {
    
    let id : String?
    let reference_id : String?
    let type : Int?
    let v : Int?
    let actors : String?
    let author : String?
    let awards : String?
    let country : [String]?
    let created_at : String?
    let director : String?
    let genre : [String]?
    let image : [String]?
    let language : [String]?
    let pages_duration : Int?
    let rated : String?
    let rating : Float?
    let release_date : String?
    let title : String?
    let updated_at : String?
    let year : Int?
    let friendspire_rating : Float?
    let rated_at : String?
    let city :String?
    let reference :String?
    let cuisine : [String]?
    let description :String?
    let location : [Double]?
    let priceTier :Int?
    let distance :Double?
    let closedAt :String?
    let isOpen :Int?
    let isBookmarked :Bool?
    let endYear : Int?
    let startYear : Int?
    let friendsRating : Float?
    let userRating : Float?
    
    enum CodingKeys: String, CodingKey {
        
        case id = "_id"
        case reference_id = "reference_id"
        case type = "type"
        case v = "__v"
        case actors = "actors"
        case author = "author"
        case awards = "awards"
        case country = "country"
        case created_at = "created_at"
        case director = "director"
        case genre = "genre"
        case image = "image"
        case language = "language"
        case pages_duration = "pages_duration"
        case rated = "rated"
        case rating = "rating"
        case release_date = "release_date"
        case title = "title"
        case updated_at = "updated_at"
        case year = "year"
        case friendspire_rating = "friendspire_rating"
        case rated_at = "rated_at"
        case city
        case reference
        case cuisine
        case description
        case location
        case priceTier = "price_tier"
        case distance
        case closedAt = "closed_at"
        case isOpen = "is_open"
        case isBookmarked = "is_bookmarked"
        case friendsRating = "friends_rating"
        case startYear = "start_year"
        case endYear = "end_year"
        case userRating = "user_rating"
    }
}

