//
//  Notification.swift
//  Noah
//
//  Created by Harpreet_kaur on 21/03/18.
//  Copyright © 2018 Harpreet. All rights reserved.
//


import Foundation
import UIKit
class NotificationArray: NSObject{
    static var sharedInstance = NotificationArray()
    
    var status:Int?
    var message:String?
    var data:[Notifications] = []
    
    //MARK:- Default Initializer
    override init () {
    }
    
    convenience init(_ dictionary: Parameters) {
        self.init()
        status = dictionary["status"] as? Int
        message = dictionary["message"] as? String ?? ""
        if (dictionary["data"] != nil) {
            data = Notifications.modelsFromDictionaryArray(array: dictionary["data"] as! NSArray)
        }
    }
}

class Notifications: NSObject {
    static var sharedInstance = Notifications()
    
    var id:String?
    var message:String?
    var userDetail:UserInfo?
    var is_read:Int?
    var type:Int?
    var postInfo:PostInfo?
    var commentId:String?
    
    //MARK:- Default Initializer
    override init () {
    }
    
    public class func modelsFromDictionaryArray(array:NSArray) -> [Notifications]
    {
        var NotificationsArray:[Notifications] = []
        for item in array
        {
            NotificationsArray.append(Notifications(dictionary: item as! NSDictionary)!)
        }
        return NotificationsArray
    }
    
    required public init?(dictionary: NSDictionary) {
        id = dictionary["_id"] as? String
        if let data = dictionary["from"] as? [String:Any] {
            userDetail = UserInfo(dictionary: data)
        }
        if let postData = dictionary["reference_id"] as? [String:Any] {
            postInfo = PostInfo(dictionary: postData)
        }else if let postData = dictionary["recommendation_id"] as? [String:Any] {
            postInfo = PostInfo(dictionary: postData)
        }
        if let id = dictionary["comment_id"] as? [String:Any]{
          commentId =  id["_id"] as? String ?? ""
        }
        
        message = dictionary["message"] as? String ?? ""
        is_read = dictionary["is_read"] as? Int
        type = dictionary["type"] as? Int
    }
}

class UserInfo: NSObject {
    
    var id:String?
    var profilePic:String?
    var firstName:String?
    var lastName:String?
    
    //MARK:- Default Initializer
    override init () {
    }
    required public init?(dictionary: [String:Any] ) {
        id = dictionary["_id"] as? String
        profilePic = dictionary["profile_pic"] as? String
        lastName = dictionary["lastName"] as? String
        firstName = dictionary["firstName"] as? String
    }
}


class PostInfo: NSObject {
    
    var id:String?
    var type:Int?
    var recomendationId:String?

    //MARK:- Default Initializer
    override init () {
    }
    
    required public init?(dictionary: [String:Any] ) {
        id = dictionary["_id"] as? String
        type = dictionary["type"] as? Int
        recomendationId = dictionary["reference_id"] as? String
    }
}
