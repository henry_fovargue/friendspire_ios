//
//  RestaurantModel.swift
//  FriendSpire
//
//  Created by Harpreet_kaur on 07/08/18.
//  Copyright © 2018 openkey. All rights reserved.
//

import Foundation


class RestaurantModel : Codable {
    
    var data : RestaurantModelData?
    var message : String?
    var status : Int?
    
}

class RestaurantModelData : Codable {
    var id:String!
    var address : [String] = [String]()
    var city : String?
    var country : [String]?
    var cuisine : [String] = [String]()
    var descriptionField : String?
    var distance : Double?
    var image : [String] = [String]()
    var isOpen : Int?
    var phone : String?
    var priceTier : Int?
    var rating : Float?
    var referenceId : String?
    var timings : [[[String:String]]] = [[[String:String]]]()
    var title : String?
    var type : Int?
    var url : String?
    var reviews : [ReviewsModel] = [ReviewsModel]()
    var isBookMarked : Bool?
    var userReview : String?
    var userRating : Float?
    var friendsRating : Float?
    var profilePic : String?
    var friendspireRating : Float?
    var location : [Double]?
    var refrenceUrl : String?
    var countFriends : Int64?
    var everyoneCount : Int64?
    var userUpdatedAt : String?
    var userRecommendationId:String?
    var userCommentCount : Int?

    enum CodingKeys: String, CodingKey {
        
        case id = "_id"
        case address
        case city
        case country
        case cuisine
        case descriptionField = "description"
        case distance
        case image
        case isOpen = "is_open"
        case location
        case phone
        case priceTier = "price_tier"
        case rating
        case referenceId = "reference_id"
        case timings
        case title
        case type
        case url
        case reviews
        case isBookMarked = "is_bookmarked"
        case userReview = "user_review"
        case userRating = "user_rating"
        case friendsRating = "friends_rating"
        case profilePic = "profile_pic"
        case friendspireRating = "friendspire_rating"
        case refrenceUrl = "reference_url"
        case countFriends = "friends_count"
        case everyoneCount = "everyone_count"
        case userUpdatedAt = "user_updated_at"
        case userRecommendationId = "user_recommendation_id"
        case userCommentCount = "comment_count_of_user_review"
    }
    
}

class Location : Codable {
    
    var coordinates : [Float]?
    
}
//
//class Timimg : Codable {
//
//  //  struct CustomAttributesData:Codable {
//        var end:String
//        var start:String
//
//        enum CodingKeys: String, CodingKey {
//
//            case end
//            case start
//        }
//  //  }
//
//}


