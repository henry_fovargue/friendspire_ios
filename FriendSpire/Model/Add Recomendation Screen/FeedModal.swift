//
//  FeedModal.swift
//  FriendSpire
//
//  Created by abhishek on 13/08/18.
//  Copyright © 2018 openkey. All rights reserved.
//

enum OpenStatus :Int {
    case NA = 0
    case open = 1
    case closed = 2
}

import Foundation

class FeedModal: Codable {
    
    var   id : String?
    var   type :Int?
    var   referenceId :String?
    var   city :String?
    var   reference :String?
    var   country : [String]?
    var   cuisine : [String]?
    var   description :String?
    var   image :[String]?
    var   location : [Double]?
    var   priceTier :Int?
    var   rating :Double?
    var   title :String?
    var   distance :Double?
    var   closedAt :String?
    var   isOpen :Int?
    var   isBookmarked :Bool?
    var   genre :[String]?
    var   pagesDuration : Int?
    var   year : Int?
    var   friendspireRating : Double?
    var   endYear : Int?
    var   startYear : Int?
    var   author : String?
    var   friendsRating : Float?
    var   userRating : Int?
    var   userReview : String?
    var   countFriends : Int64?
    var   everyoneCount : Int64?
    var   myBookmarkStatus: Int?
    var   myRating:Int?
    
    enum CodingKeys: String, CodingKey {
        
        case    id = "_id"
        case    type
        case    referenceId = "reference_id"
        case    city
        case    reference
        case    country
        case    cuisine
        case    description
        case    image
        case    location
        case    priceTier = "price_tier"
        case    rating
        case    title
        case    distance
        case    closedAt = "closed_at"
        case    isOpen = "is_open"
        case    isBookmarked = "is_bookmarked"
        case    genre
        case    pagesDuration = "pages_duration"
        case    year = "year"
        case    friendspireRating = "friendspire_rating"
        case    friendsRating = "friends_rating"
        case    startYear = "start_year"
        case    endYear = "end_year"
        case    author = "author"
        case    userRating = "user_rating"
        case   userReview = "user_review"
        case   countFriends = "count_friends"
        case   everyoneCount = "everyone_count"
        case   myBookmarkStatus = "my_bookmark_status"
        case   myRating = "my_rating"
        
    }
}















