//
//  SearchRecomendationModal.swift
//  FriendSpire
//
//  Created by abhishek on 09/08/18.
//  Copyright © 2018 openkey. All rights reserved.
//

import Foundation

class SearchRecomendationModal: Codable {
    
    var id : String?
    var title :String?
    var address : [String]?
    var city : String?
    var country : [String]?
    var image : [String]?
    var rating : Float?
    var author : String?
    var year : Int?
    var end_year : Int?
    var start_year : Int?
    var type : Int?
    
    enum CodingKeys: String, CodingKey {
        case title =  "title"
        case id =  "_id"
        case address = "address"
        case city = "city"
        case country = "country"
        case image = "image"
        case rating = "rating"
        case author = "author"
        case year = "year"
        case end_year = "end_year"
        case start_year = "start_year"
        case type = "type"
    }
}
