//
//  RecomendationModal.swift
//  FriendSpire
//
//  Created by abhishek on 10/08/18.
//  Copyright © 2018 openkey. All rights reserved.
//

import Foundation

class RecomendationModal: Codable {
    
    var id : String?
    var userId :String?
    var type :Int?
    var status :Int?
    var reference :ReferenceModal?
    
    //Used in Reviews Library Only
    var review :String?
    var rating :Double?
    
    enum CodingKeys: String, CodingKey {
        case userId =  "user_id"
        case id =  "_id"
        case type =  "type"
        case status =  "status"
        case reference =  "reference_id"
        case review =  "review"
        case rating =  "rating"
    }
}

class ReferenceModal: Codable {
    
    var   id : String?
    var   image :[String]?
    var   rating :Double?
    var   friendspireRating :Double?
    var   title :String?
    var   type :Int?
    var   genre :[String]?
    var   year : Int?
    var   referenceId :String?
    var   city :String?
    var   reference :String?
    var   country : [String]?
    var   cuisine : [String]?
    var   description :String?
    var   location : [Double]?
    var   priceTier :Int?
    var   distance :Double?
    var   closedAt :String?
    var   isOpen :Int?
    var   isBookmarked :Bool?
    var   pagesDuration : Int?
    var   endYear : Int?
    var   startYear : Int?
    var   author : String?
    var   friendsRating : Float?
    var   userRating : Float?
    
    enum CodingKeys: String, CodingKey {
        
        case    image =  "image"
        case    id =  "_id"
        case    rating =  "rating"
        case    friendspireRating =  "friendspire_rating"
        case    title =  "title"
        case    type =  "type"
        case    referenceId = "reference_id"
        case    city
        case    reference
        case    country
        case    cuisine
        case    description
        case    location
        case    priceTier = "price_tier"
        case    distance
        case    closedAt = "closed_at"
        case    isOpen = "is_open"
        case    isBookmarked = "is_bookmarked"
        case    genre
        case    pagesDuration = "pages_duration"
        case    year = "year"
        case    friendsRating = "friends_rating"
        case    startYear = "start_year"
        case    endYear = "end_year"
        case    author = "author"
        case    userRating = "user_rating"
    }
}

class SupportiveAttribute {
    // variable from outer of array
    var distanceUnit :DistanceUnit = DistanceUnit.Miles
    var count : Int = 0
}





///RAting modal


class RatingModal: Codable {
    
    var id : String?
    var userId :String?
    var type :Int?
    var status :Int?
    var referenceId :String?
    
    //Used in Reviews Library Only
    var review :String?
    var rating :Double?
    
    enum CodingKeys: String, CodingKey {
        case userId =  "user_id"
        case id =  "_id"
        case type =  "type"
        case status =  "status"
        case referenceId =  "reference_id"
        case review =  "review"
        case rating =  "rating"
    }
}
