//
//  SearchFriendModal.swift
//  FriendSpire
//
//  Created by abhishek on 26/07/18.
//  Copyright © 2018 openkey. All rights reserved.
//

import Foundation

class SearchFriendModal: Codable {
    
    var firstName : String?
    var lastName : String?
    var id : String?
    var picture : String?
    var followers : Int?
    
    enum CodingKeys: String, CodingKey {
        case firstName =  "firstName"
        case lastName =  "lastName"
        case id =  "_id"
        case picture =  "profile_pic"
        case followers =  "followers"
    }
}
