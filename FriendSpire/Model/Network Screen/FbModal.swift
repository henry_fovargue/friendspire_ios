//
//  FbModal.swift
//  FriendSpire
//
//  Created by abhishek on 24/07/18.
//  Copyright © 2018 openkey. All rights reserved.
//

//Note *  This class modal used for repsponse come from Friendspire server

import UIKit

enum userStatus : Int{
    case NotFollowed = 0
    case Followed
    case Requested
}

class FbModal: Codable {
    
    var userId : UserId?
    var status : Int?
    
    enum CodingKeys: String, CodingKey {
        case userId =  "user_id"
        case status =  "status"
    }
}

class UserId: Codable {
    
    
    var firstName : String?
    var id : String?
    var lastName : String?
    var picture : String?
    
    enum CodingKeys: String, CodingKey {
        case firstName
        case id =  "_id"
        case lastName
        case picture =  "profile_pic"
    }
}
