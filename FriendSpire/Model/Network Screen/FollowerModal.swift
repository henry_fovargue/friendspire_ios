//
//  FollowerModal.swift
//  FriendSpire
//
//  Created by abhishek on 25/07/18.
//  Copyright © 2018 openkey. All rights reserved.
//

import Foundation

class FollowerModal: Codable {
    
    var firstName : String?
    var id : String?
    var status : Int?
    var picture : String?
    var lastName : String?
    
    enum CodingKeys: String, CodingKey {
        case firstName =  "firstName"
        case lastName =  "lastName"
        case id =  "_id"
        case status =  "status"
        case picture =  "profile_pic"
    }
}


