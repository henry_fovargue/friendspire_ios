//
//  NetworkModal.swift
//  FriendSpire
//
//  Created by abhishek on 25/07/18.
//  Copyright © 2018 openkey. All rights reserved.
//

import Foundation

class NetworkModal: Codable {
    
    var pending : Int?
    var followers : Int?
    var following : Int?
   
    
    enum CodingKeys: String, CodingKey {
        case pending =  "pending"
        case followers =  "followers"
        case following =  "following"
    }
}
