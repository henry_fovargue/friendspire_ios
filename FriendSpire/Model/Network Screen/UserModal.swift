//
//  UserModal.swift
//  FriendSpire
//
//  Created by abhishek on 27/07/18.
//  Copyright © 2018 openkey. All rights reserved.

import Foundation

class UserModal: Codable {
    static var shared = UserModal()
    var id : String?
    var profilePic : String?
    var isPrivate : Int?
    var lastName : String?
    var firstName : String?
    var followings : Int?
    var followers : Int?
    var followStatus : Int?
    var profileData : ProfileData?
    var pushNotification : Int?
    var isBlocked : Int?

    
    enum CodingKeys: String, CodingKey {
        
        case id =  "_id"
        case profilePic =  "profile_pic"
        case isPrivate =  "is_private"
        case lastName =  "lastName"
        case firstName =  "firstName"
        case followings =  "followings"
        case profileData =  "profile_data"
        case followers =  "followers"
        case followStatus =  "follow_status"
        case isBlocked =  "is_blocked"

        
    }
}

class ProfileData: Codable {
    
    var all : [Int]?
    var movie : [Int]?
    var tv : [Int]?
    var restaurant : [Int]?
    var bar : [Int]?
    var books : [Int]?
    
    var pushNotification : Int?
    
    enum CodingKeys: String, CodingKey {
        
        case all =  "all"
        case movie =  "movie"
        case tv =  "tv"
        case restaurant =  "restaurant"
        case bar =  "bar"
        case books =  "books"
        
    }
}
