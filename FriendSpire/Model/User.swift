//
//	User.swift
//

import Foundation
import CoreLocation

class User: NSObject, NSCoding {
    
    static var sharedInstance = User()
    
    //MARK:- Properties
    
    var id: String?
    var aboutMe: String?
    var deviceToken: String?
    var oauthToken: String?
    var email: String?
    var firstName: String?
    var lastName: String?
    var profilePicUrl: String?
    var dateOfBirth: String?
    var gender: Int?
    var firebaseProfileImageName: String?
    var snsType: Int?
    var snsID: String?
    var password: String?
    var confirmPassword: String?
    var location: String?
    var isEmailVerified : Int?
    var latLong:[Double]?
    var fbToken : String?
    var connectedTofb: Int?
    var userCurrentLocation : CLLocationCoordinate2D?
    var isLocationEnabled : Bool?
    var objAttributes : SupportiveAttribute?
    var distanceUnit : Int?
    var homeCity : String?
    var referralCode : String?
    var tutorialRead: Int?
    
    // MARK: Declaration for string constants to be used to decode and also serialize.
    private struct SerializationKeys {
        static let email = "email"
        static let id = "id"
        static let lastName = "lastName"
        static let oauthToken = "oauthToken"
        static let firstName = "firstName"
        static let aboutMe = "aboutMe"
        static let deviceToken = "deviceToken"
        static let profilePicUrl = "profilePic_url"
        static let firebaseProfileImageName = "firebaseProfileImageName"
        static let location = "Location"
        static let distanceUnit = "distance_unit"
        static let referralCode = "reference_code"
        static let snsType = "snsType"
        static let snsID = "snsID"
        static let isEmailVerified = "isEmailVerified"
        static let fbToken = "fbToken"
        static let connectedTofb = "connectedTofb"
        static let isLocationEnabled = "isLocationEnabled"
        static let latLong = "longlat"
        static let homeCity = "homeCity"
        static let tutorialRead = "tutorial_read"
    }
    
    func resetUserInstance() {
        self.email = nil
        self.id = nil
        self.lastName = nil
        self.firstName = nil
        self.deviceToken = nil
        self.aboutMe = nil
        self.oauthToken = nil
        self.profilePicUrl = nil
        self.firebaseProfileImageName = nil
        self.location = nil
        self.userCurrentLocation = nil
        self.objAttributes = nil
        self.distanceUnit = nil
        self.homeCity = nil
        self.referralCode = nil
        self.tutorialRead = nil
    }
    
    
    //MARK:- Default Initializer
    override init () {
        // uncomment this line if your class has been inherited from any other class
        //super.init()
    }
    
    /**
     * Instantiate the instance using the passed dictionary values to set the properties values
     */
    
    convenience init(_ dictionary: Parameters) {
        self.init()
        id = dictionary["_id"] as? String ?? ""
        aboutMe = dictionary["aboutMe"] as? String ?? ""
        deviceToken = dictionary["deviceToken"] as? String ?? ""
        dateOfBirth = dictionary["dob"] as? String ?? ""
        gender = dictionary["gender"] as? Int
        email = dictionary["email"] as? String ?? ""
        firstName = dictionary["firstName"] as? String ?? ""
        lastName = dictionary["lastName"] as? String ?? ""
        profilePicUrl = dictionary["profile_pic"] as? String ?? ""
        firebaseProfileImageName = dictionary["firebaseProfileImageName"] as? String ?? ""
        location = dictionary["address"] as? String ?? ""
        distanceUnit = dictionary["distance_unit"] as? Int ?? 1
        latLong = dictionary["longlat"] as? [Double] ?? [Double]()
        homeCity = dictionary["homeCity"] as? String ?? ""
        snsType = dictionary["sns_type"] as? Int
        snsID = dictionary["sns_id"] as? String
        referralCode = dictionary["reference_code"] as? String
        tutorialRead = dictionary["tutorial_read"] as? Int
    }
    
    
    // MARK: NSCoding Protocol
    required public init(coder aDecoder: NSCoder) {
        
        self.email = aDecoder.decodeObject(forKey: SerializationKeys.email) as? String
        self.id = aDecoder.decodeObject(forKey: SerializationKeys.id) as? String
        self.lastName = aDecoder.decodeObject(forKey: SerializationKeys.lastName) as? String
        self.firstName = aDecoder.decodeObject(forKey: SerializationKeys.firstName) as? String
        self.deviceToken = aDecoder.decodeObject(forKey: SerializationKeys.deviceToken) as? String
        self.aboutMe = aDecoder.decodeObject(forKey: SerializationKeys.aboutMe) as? String
        self.oauthToken = aDecoder.decodeObject(forKey: SerializationKeys.oauthToken) as? String
        self.profilePicUrl = aDecoder.decodeObject(forKey: SerializationKeys.profilePicUrl) as? String
        self.firebaseProfileImageName = aDecoder.decodeObject(forKey: SerializationKeys.firebaseProfileImageName) as? String
        self.location = aDecoder.decodeObject(forKey: SerializationKeys.location) as? String
        self.snsType = aDecoder.decodeObject(forKey: SerializationKeys.snsType) as? Int
        self.snsID = aDecoder.decodeObject(forKey: SerializationKeys.snsID) as? String
        self.fbToken = aDecoder.decodeObject(forKey: SerializationKeys.fbToken) as? String
        self.connectedTofb = aDecoder.decodeObject(forKey: SerializationKeys.connectedTofb) as? Int
        self.latLong = aDecoder.decodeObject(forKey: SerializationKeys.latLong) as? [Double]
        self.isLocationEnabled = aDecoder.decodeObject(forKey: SerializationKeys.isLocationEnabled) as? Bool
        self.distanceUnit = aDecoder.decodeObject(forKey: SerializationKeys.distanceUnit) as? Int
        self.homeCity = aDecoder.decodeObject(forKey: SerializationKeys.homeCity) as? String
        self.referralCode = aDecoder.decodeObject(forKey: SerializationKeys.referralCode) as? String
        self.tutorialRead = aDecoder.decodeObject(forKey: SerializationKeys.tutorialRead) as? Int

    }
    
    public func encode(with aCoder: NSCoder) {
        aCoder.encode(email, forKey: SerializationKeys.email)
        aCoder.encode(id, forKey: SerializationKeys.id)
        aCoder.encode(lastName, forKey: SerializationKeys.lastName)
        aCoder.encode(firstName, forKey: SerializationKeys.firstName)
        aCoder.encode(deviceToken, forKey: SerializationKeys.deviceToken)
        aCoder.encode(oauthToken, forKey: SerializationKeys.oauthToken)
        aCoder.encode(aboutMe, forKey: SerializationKeys.aboutMe)
        aCoder.encode(profilePicUrl, forKey: SerializationKeys.profilePicUrl)
        aCoder.encode(firebaseProfileImageName, forKey: SerializationKeys.firebaseProfileImageName)
        aCoder.encode(isLocationEnabled, forKey: SerializationKeys.isLocationEnabled)
        aCoder.encode(snsType, forKey: SerializationKeys.snsType)
        aCoder.encode(snsID, forKey: SerializationKeys.snsID)
        aCoder.encode(fbToken, forKey: SerializationKeys.fbToken)
        aCoder.encode(location, forKey: SerializationKeys.location)
        aCoder.encode(latLong, forKey: SerializationKeys.latLong)
        aCoder.encode(connectedTofb, forKey: SerializationKeys.connectedTofb)
        aCoder.encode(distanceUnit, forKey: SerializationKeys.distanceUnit)
        aCoder.encode(homeCity, forKey: SerializationKeys.homeCity)
        aCoder.encode(referralCode, forKey: SerializationKeys.referralCode)
        aCoder.encode(tutorialRead, forKey: SerializationKeys.tutorialRead)

        
    }
    
    
    func getDictionary() -> [String: Any] {
        
        var dict = [String: Any]()
        
        if email != nil {
            dict["email"] = email
        }
        if firstName != nil {
            dict["firstName"] =  firstName
        }
        if lastName != nil {
            dict["lastName"] =  lastName
        }
        if password != nil {
            dict["password"] =  password
        }
        if confirmPassword != nil {
            dict["confirmPassword"] =  confirmPassword
        }
        if dateOfBirth != nil {
            dict["dob"] =  dateOfBirth
        }
        if snsType != nil {
            dict["sns_type"] =  snsType
        }
        if snsID != nil {
            dict["sns_id"] =  snsID
        }
        if profilePicUrl != nil {
            dict["profile_pic"] =  profilePicUrl
        }
        if aboutMe != nil {
            dict["aboutMe"] =  aboutMe
        }
        if gender != nil {
            dict["gender"] = gender
        }
        if isEmailVerified != nil {
            dict["is_email_verified"] =  isEmailVerified
        }
        if location != nil {
            dict["location"] =  location
        }
        if homeCity != nil {
            dict["homeCity"] =  homeCity
        }
        if referralCode != nil {
            dict["code"] =  referralCode
        }
        if tutorialRead != nil {
            dict["tutorial_read"] =  tutorialRead
        }
        return dict
        
    }
    
    func setHomeCity(){
        
        guard let locaion =  UserManager.getCurrentUser()?.latLong else{ return }
        if locaion.count > 1{
            let currentLocation = CLLocation(latitude: locaion[1] , longitude: locaion[0] )
            CLGeocoder().reverseGeocodeLocation(currentLocation, completionHandler: {(placemarks, error) -> Void in
                if error != nil {
                    return
                }
                if (placemarks?.count)! > 0 {
                    let pm = placemarks?[0]
                    if let locality = pm?.locality{
                        User.sharedInstance.homeCity = locality
                    }
                }
                else {
                }
            })
        }
    }
}



class TutorialStatus: NSObject, NSCoding {
    
    static var sharedInstance = TutorialStatus()
    
    
    var inpirationScreen : Bool?
    var detailScreen : Bool?
    var libraryScreen : Bool?
    var addRecommendationScreen : Bool?
    var networkScreen : Bool?
    var editprofileScreen : Bool?
    var categorySelectionScreen : Bool?
    var friendSuggestion : Bool?
    var friendSuggestionBegin : Bool?
    
    
    override init() {
        
    }
    
    class func getCurrentStatus()-> TutorialStatus?{
        guard let userData = Defaults.shared.get(forKey: .tutorial) as? Data, let status =
            NSKeyedUnarchiver.unarchiveObject(with: userData) as? TutorialStatus  else {
                return nil
        }
        return status
    }
    
    class func saveCurrentStatus(status:TutorialStatus){
        
        let encodedData: Data = NSKeyedArchiver.archivedData(withRootObject: status)
        Defaults.shared.set(value: encodedData, forKey: .tutorial)
    }
    class func resetTutorial(){
        User.sharedInstance.tutorialRead = 0
        Defaults.shared.remove(.tutorial)
    }
    
    // MARK: Declaration for string constants to be used to decode and also serialize.
    private struct SerializationKeys {
        
        static let inpirationScreen = "inpirationScreen"
        static let detailScreen = "detailScreen"
        static let libraryScreen = "libraryScreen"
        static let addRecommendationScreen = "addRecommendationScreen"
        static let networkScreen = "networkScreen"
        static let editprofileScreen = "editprofileScreen"
        static let categorySelectionScreen = "categorySelectionScreen"
        static let friendSuggestion = "friendSuggestion"
        static let friendSuggestionBegin = "friendSuggestionBegin"
        
        
    }
    
    // MARK: NSCoding Protocol
    required public init(coder aDecoder: NSCoder) {
        
        self.inpirationScreen = aDecoder.decodeObject(forKey: SerializationKeys.inpirationScreen) as? Bool
        self.detailScreen = aDecoder.decodeObject(forKey: SerializationKeys.detailScreen) as? Bool
        self.libraryScreen = aDecoder.decodeObject(forKey: SerializationKeys.libraryScreen) as? Bool
        self.addRecommendationScreen = aDecoder.decodeObject(forKey: SerializationKeys.addRecommendationScreen) as? Bool
        self.networkScreen = aDecoder.decodeObject(forKey: SerializationKeys.networkScreen) as? Bool
        self.editprofileScreen = aDecoder.decodeObject(forKey: SerializationKeys.editprofileScreen) as? Bool
        self.categorySelectionScreen = aDecoder.decodeObject(forKey: SerializationKeys.categorySelectionScreen) as? Bool
        self.friendSuggestion = aDecoder.decodeObject(forKey: SerializationKeys.friendSuggestion) as? Bool
        self.friendSuggestionBegin = aDecoder.decodeObject(forKey: SerializationKeys.friendSuggestionBegin) as? Bool
        
    }
    
    public func encode(with aCoder: NSCoder) {
        
        aCoder.encode(inpirationScreen, forKey: SerializationKeys.inpirationScreen)
        aCoder.encode(detailScreen, forKey: SerializationKeys.detailScreen)
        aCoder.encode(libraryScreen, forKey: SerializationKeys.libraryScreen)
        aCoder.encode(addRecommendationScreen, forKey: SerializationKeys.addRecommendationScreen)
        aCoder.encode(networkScreen, forKey: SerializationKeys.networkScreen)
        aCoder.encode(editprofileScreen, forKey: SerializationKeys.editprofileScreen)
        aCoder.encode(categorySelectionScreen, forKey: SerializationKeys.categorySelectionScreen)
        aCoder.encode(friendSuggestion, forKey: SerializationKeys.friendSuggestion)
        aCoder.encode(friendSuggestionBegin, forKey: SerializationKeys.friendSuggestionBegin)
        
    }
    
}




