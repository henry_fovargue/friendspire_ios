//
//  RestuarantFilterModel.swift
//  FriendSpire
//
//  Created by Harpreet_kaur on 07/08/18.
//  Copyright © 2018 openkey. All rights reserved.
//

import Foundation
struct RestuarantFilterModel {
    
    //MARK:- SahredInstance.
    static var objFeedListRequest = FeedListRequest()
    static var sharedInstance = RestuarantFilterModel()
    static var bookSharedInstance = RestuarantFilterModel()
    static var filterCountKey:Int = 0
    static var filterCount:Int = 0
    var filterApplied:Bool?
    var category : Int?
    var friends : Int?
    var page : Int?
    var address : Address = Address()
    var searchText : String?
    var location : String = "Current Location"
    var distance : Float =  20.0
//    {
//        if DistanceUnit.Miles.rawValue == User.sharedInstance.distanceUnit{
//          return 24.0
//
//        }else {
//         return 40.0
////            distance =  distance * KM_VALUE
//        }
//    }()
    
//
    var priceLevel : [String] = [String]()
    var selectedPages: [Int] = [Int]()
    var selectedGenre: [Int] = [Int]()
    var cuisines : [String] = [String]()
    var rating : [String] = ["1.0","5.0"]
    var fourSquareRating : [String] = ["1.0","10.0"]
    var removeRated : Int?
    var pages : [String] = [String]()
    var publishYear : [String] = ["1950", "\(Int(Date().string().convertDate(.preDefined, toFormat: .year)) ?? 2000)"]
    // var releaseYear : [Int] = [Int]()
    var genre : [String] = [String]()
    var openNow : Bool?
    var showMyRated : Bool?
   // for miles to km track
    var distanceUnit : Int?
    
    
    func getDictionarForRestaurant() -> FeedListRequest {
       return RestuarantFilterModel.objFeedListRequest
    }
    
    func getDictionarForBooks() -> FeedListRequest {
     return RestuarantFilterModel.objFeedListRequest
    }
    
}


class FilterRatingModel:NSObject {
    
    var friendspireMin : Float?
    var friendspireMax : Float?
    var foursquareMin : Float?
    var foursquareMax : Float?
    
    
}
