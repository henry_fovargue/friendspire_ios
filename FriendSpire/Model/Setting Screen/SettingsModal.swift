//
//  SettingsModal.swift
//  FriendSpire
//
//  Created by abhishek on 27/07/18.
//  Copyright © 2018 openkey. All rights reserved.
//

import Foundation

enum DistanceUnit:Int{
    case Miles = 1
    case Kms
}
class SettingsModal: Codable {
    
    var isLocation : Bool?
    var isCategories : Bool?
    var isPrivate : Int?
    var distanceUnit : Int?
    var id : String?
    var pushNotification : Int?
    
    enum CodingKeys: String, CodingKey {
        case pushNotification =  "push_notification"
        case distanceUnit =  "distance_unit"
        case id =  "_id"
        case isPrivate =  "is_private"
    }
}
