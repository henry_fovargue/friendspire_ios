//
//  MoviesModel.swift
//  FriendSpire
//
//  Created by Harpreet_kaur on 07/08/18.
//  Copyright © 2018 openkey. All rights reserved.
//

import Foundation


class MovieModel : Codable {
    
    var data : MovieModelData?
    var message : String?
    var status : Int?
    
    
    enum CodingKeys: String, CodingKey {
        case data =  "data"
        case message =  "message"
        case status =  "status"
    }
    
}

class MovieModelData : Codable {
    
    var id:String?
    var actors : String?
    var author : String?
    var awards : String?
    var endYear : Int?
    var startYear : Int?
    var country : [String] = [String]()
    var descriptionField : String?
    var director : String?
    var genre : [String] = [String]()
    var image : [String] = [String]()
    var language : [String] = [String]()
    var pagesDuration : Int?
    var rated : String?
    var rating : Float?
    var referenceId : String?
    var releaseDate : String?
    var title : String?
    var type : Int?
    var year : Int?
    var reviews : [ReviewsModel] = [ReviewsModel]()
    var isBookMarked : Bool?
    var userReview : String?
    var userRating : Float?
    var friendsRating : Float?
    var profilePic : String?
    var friendspireRating : Float?
    var publisher : String?
    var isbn : String?
    var countFriends : Int64?
    var everyoneCount : Int64?
    var userUpdatedAt : String?
    var userRecommendationId:String?
    var userCommentCount : Int?
    
    enum CodingKeys: String, CodingKey {
        case id = "_id"
        case actors
        case author
        case awards
        case country
        case descriptionField = "description"
        case director
        case genre
        case image
        case language
        case pagesDuration = "pages_duration"
        case rated
        case rating
        case referenceId = "reference_id"
        case releaseDate = "release_date"
        case title
        case type
        case publisher
        case isbn
        case year
        case startYear = "start_year"
        case endYear = "end_year"
        case reviews
        case isBookMarked = "is_bookmarked"
        case userReview = "user_review"
        case userRating = "user_rating"
        case friendsRating = "friends_rating"
        case profilePic = "profile_pic"
        case friendspireRating = "friendspire_rating"
        case countFriends = "friends_count"
        case everyoneCount = "everyone_count"
        case userUpdatedAt = "user_updated_at"
        case userRecommendationId = "user_recommendation_id"
        case userCommentCount = "comment_count_of_user_review"
    }
}






