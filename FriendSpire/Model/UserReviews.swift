//
//  UserReviews.swift
//  FriendSpire
//
//  Created by Harpreet_kaur on 02/08/18.
//  Copyright © 2018 openkey. All rights reserved.
//

import Foundation


class ReviewsModel: Codable {
    
    var id : String?
    var rating : Int?
    var review : String?
    var userId : UserId?
    var userImage : String?
    var friends : Float?
    var updatedAt: String?
    var comments : [Comments]?
    var commentCount : Int?
    
    enum CodingKeys: String, CodingKey {
        case id = "_id"
        case comments = "comment_data"
        case rating
        case review
        case userId = "user_id"
        case userImage =  "profile_pic"
        case friends
        case updatedAt = "updated_at"
        case commentCount = "comment_count"

    }
}



class Comments : Codable {
    
    var id : String?
    var updatedAt : String?
    var recommendationId : String?
    var userId : String?
    var comment : String?
    var commentedUserData : UserId?
    
    enum CodingKeys: String, CodingKey {
        case id = "_id"
        case updatedAt = "updated_at"
        case recommendationId = "recommendation_id"
        case userId = "user_id"
        case comment = "comment"
        case commentedUserData = "commented_user_data"
    }
}


//MARK:-UserSelf Reviews.
class UserReviewsModel : Codable {
    
    
    //MARK:-Variables.
    var id : String!
    var createdAt : String!
    var isDeleted : Int!
    var isPrivate : Int!
    var rating : Int!
    var referenceId : String!
    var review : String!
    var type : Int!
    var updatedAt : String!
    var userId : String!
    
    enum CodingKeys: String, CodingKey {
        case id =  "_id"
        case createdAt =  "created_at"
        case isDeleted =  "is_deleted"
        case isPrivate =  "is_private"
        case rating
        case referenceId =  "reference_id"
        case review
        case type
        case updatedAt =  "updated_at"
        case userId =  "user_id"
    }
    
}
